<html>
<head>
<title>GPS Customer Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.pack.js"></script>

<script type="text/javascript" src="js/jquery-easing-compatibility.1.2.pack.js"></script>
<script type="text/javascript" src="js/coda-slider.1.1.1.pack.js"></script>
<link href="styles.css" rel="stylesheet"/>
	<script type="text/javascript">
	
		var theInt = null;
		var $crosslink, $navthumb;
		var curclicked = 0;
		
		theInterval = function(cur){
			clearInterval(theInt);
			
			if( typeof cur != 'undefined' )
				curclicked = cur;
			
			$crosslink.removeClass("active-thumb");
			$navthumb.eq(curclicked).parent().addClass("active-thumb");
				$(".stripNav ul li a").eq(curclicked).trigger('click');
			
			theInt = setInterval(function(){
				$crosslink.removeClass("active-thumb");
				$navthumb.eq(curclicked).parent().addClass("active-thumb");
				$(".stripNav ul li a").eq(curclicked).trigger('click');
				curclicked++;
				if( 5 == curclicked )
					curclicked = 0;
				
			}, 3000);
		};
		
		$(function(){
			
			$("#main-photo-slider").codaSlider();
			
			$navthumb = $(".nav-thumb");
			$crosslink = $(".cross-link");
			
			$navthumb
			.click(function() {
				var $this = $(this);
				theInterval($this.parent().attr('href').slice(1) - 1);
				return false;
			});
			
			theInterval();
		});
		
		
		
		function stopShow(){
                         window.clearInterval(theInt);
		}

		function startShow(){
                         theInterval();
		}
		
		function toggleShow(domObj){
		 var arg = domObj.innerHTML.toLowerCase();
                         if(arg == "stop"){
                               stopShow();
                               domObj.innerHTML = "Start";
                         }
                         else {
                              startShow();
                              domObj.innerHTML = "Stop";
                         }
		}

		function checkBrowser(){
		if(navigator.appName.indexOf("Internet Explorer") >= 0){
		window.location.replace("index-ie.php");
		}
		}
		

	</script>
<script>
function closeLoader(){
	document.getElementById("loader").style.display = "none";
}

</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="closeLoader(); checkBrowser();">
<div id="loader">
Loading ... Please wait
</div>
<!-- ImageReady Slices (new_cust_page.psd) -->
<table id="Table_01" width="1200" height="901" border="0" cellpadding="0" cellspacing="0" align="center"> 
	<tr>
		<td colspan="6" height="26" align="right" id="topmenu">
			<a href="http://monagis.com" title="Mona GeoInformatics Institute (http://monagis.com)">Home</a>
		</td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="images/new_cust_page_04.gif" width="1" height="874" alt=""></td>
		<td colspan="5">
			<img src="images/new_cust_page_05.gif" width="1199" height="180" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" id="td6" align="right">
		<br><br><br><br>
		<a href="#1" class="cross-link active-thumb">What's New?</a>
		<a href="#2" class="cross-link active-thumb">Latest Bugs Fixed</a>
		<a href="#3" class="cross-link">Garmin Product Line</a>
		<a href="#4" class="cross-link">GPS Faq</a>
		<a href="#5" class="cross-link">Report An Error</a>

		</td>
		<td id="td7" height="694" width="694" align="center" valign="top">
			<div id="tv">
			<div id="stopbutton" onClick="toggleShow(this);">Stop</div>
<div class="slider-wrap">
		<div id="main-photo-slider" class="csw">
			<div class="panelContainer">
			
				<div class="panel" title="Panel 1">
					<div class="wrapper">
					<table width="100%" cellspacing="0" cellpadding="4" height="480">
                                               <tr>
                                                   <td valign="top" bgcolor="black" width="215px">
                                                       <br/>
                                                       <img src="images/jamnav18.png"/>
                                                   </td>
                                                   <td bgcolor="black" fgcolor="white"><br>
                                                       <h1 style="color:#d0e073; font-size:50px; font-family:Gill Sans MT Condensed,Calibri,Candara,Verdana;">JAMNAV 1.8 is out!</h1>
                                                        <span style="color:white">Synopsis of features available.</span>
                                                        </br></br>
    <ul style="margin:5px; color:white;">
    <li>New road classes with speed limits and other additional attributes.
    <li>New road symbology displayed to users.
    <li>Improved route calculation.
    <li>Faster (manual and automatic) recalculation time
    <li>Additional location information in the order road name town parish displayed to users as part of their POI search.
    <li>POI symbology and POI name displayed to users while navigating.
    <li>Sangster International Airport runway displayed to users
    <li>Caribbean Sea displayed to users.
    </ul>

    <br>


                                                   </td>
                                               </tr>
					</table>
					</div>
				</div>




				<div class="panel" title="Panel 2">
					<div class="wrapper">
						<img src="images/bug.png" alt="temp" />
						<div class="photo-meta-data">
							<b>Latest Bugs Fixed</b><br />
							<span>We are committed to improving the JAMNAV dataset.</span>
						</div>
						<br><br>
						<span style="margin:5px; color:white">Keep checking here to see information about errors we have fixed. </span>
					</div>
				</div>
				<div class="panel" title="Panel 3">
					<div class="wrapper" id="garmin_prod_info">
						<img  src="images/nuvi.png" alt="temp" />
						<div class="photo-meta-data">
							<b>Garmin Product Line</b><br />
							<span>See the links below to quickly access Garmin's newest products</span>
						</div>
						<table width="90%" cellpadding="4">
						       <tr>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=30986" target="_blank">
                                                                  <img width="100px" src="images/n1200.png" border="0">
                                                                </a>
                                                           </td>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=30986" class="heading1" target="_blank">Nuvi 1200</a>
                                                           <br>
                                                               Ultra-thin, affordable, feature-rich navigation
preloaded street maps for lower 48 states, Hawaii and Puerto Rico, speaks street names, optional MSNÃÂ® Direct, optional FM lifetime traffic, Where am I?, photo navigation, ecoRouteÃÂ
                                                           </td>
						       </tr>

                				       <tr>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=14926" target="_blank">
                                                                  <img width="100px" src="images/n755t.png" border="0">
                                                                </a>
                                                           </td>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=14926" class="heading1" target="_blank">Nuvi 765T</a>
                                                           <br>
                                                             Advanced navigation with lane assist
BluetoothÃÂ® wireless, FM lifetime traffic, optional MSNÃÂ® Direct, lane assist, 3-D building view, preloaded street maps for North America, photo navigation, speaks street names, widescreen, FM transmitter, MP3 player, route planning, Where am I?, Garmin LocateÃÂ
                                                           </td>
						       </tr>


						       <tr>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=10573" target="_blank">
                                                                  <img width="100px" src="images/gvn.png" border="0">
                                                                </a>
                                                           </td>
                                                           <td><a href="https://buy.garmin.com/shop/shop.do?cID=134&pID=10573" class="heading1" target="_blank">Nuvi 765T</a>
                                                           <br>
This GPS sensor turns your vehicle's audio/video system into a powerful street navigator.
                                                           </td>
						       </tr>



                                                       <tr>
                                                       <td></td>
                                                       <td><a href="https://buy.garmin.com/shop/shop.do?cID=134" target="_blank">View More Products</a></td>
                                                       </tr>
                				</table>
					</div>
				</div>
				
				<div class="panel" title="Panel 4">
					<div class="wrapper">
						<img src="images/faq.png" alt="temp" />
						<div class="photo-meta-data">
							<b>GPS Faq</b><br />
							<span>Click <a href="faq_and_disclaimer.doc" target="_blank">here</a> to download the GPS Faq.</span>
						</div>
						<br><br>
					</div>
				</div> <!-- end panel -->


				<div class="panel" title="Panel 5">
					<div class="wrapper">
						<img src="images/reporterror.png" alt="temp" />
						<div class="photo-meta-data">
							<b>Report An Error</b><br />
							<span>Click <a href="../">here</a> to report an error you experienced while using your navigation unit.</span>
						</div>
						<br><br>
					</div>
				</div> <!-- end panel -->

			</div>
		</div>
</div>

			</div>
		</td>
		<td>
			<img src="images/new_cust_page_08.gif" width="93" height="694" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/spacer.gif" width="1" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="37" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="16" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="359" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="694" height="1" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="93" height="1" alt=""></td>
	</tr>
</table>

<br><br>




</body>
</html>
