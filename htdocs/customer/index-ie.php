<?php


?>
<html>
	<head>
		<title>MGI GPS Customer Only Page</title>
		<style type="text/css">
		.icon {
			float:left;
			cursor:pointer;
		}
		
		#infoPanel {
			width:500px;
			height:185px;
			font-size:12px;	
			font-family:Calibri,Verdana;
			overflow:auto;
			background-color:rgb(230,230,250);
			padding:10px;
			border-style:solid;
			border-width:1px;
		}
		</style>
		<script>
		var messages = {};
		var newStr = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"4\" >";
		newStr += "<tr><td valign=\"top\" bgcolor=\"black\" width=\"125px\"><br/><img src=\"images/jamnav18.png\" width='120px'/>";
		newStr += "</td><td bgcolor=\"black\" fgcolor=\"white\"><br><h1 style=\"color:#d0e073; font-size:20px; font-family:Gill Sans MT Condensed,Calibri,Candara,Verdana;\">JAMNAV 1.8 is out!</h1>";
		newStr += "<span style=\"color:white\">Synopsis of features available.</span></br></br><ul style=\"margin:5px; color:white;\">";
		newStr += "<li>New road classes with speed limits and other additional attributes.</li><li>New road symbology displayed to users.</li>";
		newStr += "<li>Improved route calculation.</li><li>Faster (manual and automatic) recalculation time</li>";
		newStr += "<li>Additional location information in the order road name town parish displayed to users as part of their POI search.</li>";
    	newStr += "<li>POI symbology and POI name displayed to users while navigating.</li>";
    	newStr += "<li>Sangster International Airport runway displayed to users</li>";
    	newStr += "<li>Caribbean Sea displayed to users.</li></ul><br></td></tr></table>";

//		var newStr = "<p style='text:align:left';><b>JAMNAV v1.6 is out</b><br>The following represents the new features made available as part of JAMNAV v 1.6:";
//		newStr += "<br><ul>";
//		newStr += "<li>New road classes with speed limits and other additional attributes.</li>";   
//		newStr += "<li>New road symbology displayed to users.</li>";   
//		newStr += "<li>Improved route calculation.</li>";   
//		newStr += "<li>Faster (manual and automatic) recalculation time</li>";   
//		newStr += "<li>Additional location information in the order road name – town – parish displayed to users as part of their POI search.</li>";   
//		newStr += "<li>POI symbology and POI name displayed to users while navigating. </li>";   
//		newStr += "<li>Sangster International Airport runway displayed to users </li>";   
//		newStr += "<li>Caribbean Sea displayed to users.</li>";   
//		newStr += "</ul>";
		
		newStr += "</p><br>";
		messages["bugfixes"] = newStr;
		messages["faq"] = "Click <a href='faq_and_disclaimer.doc'>here</a> to download the GPS customer FAQ.";
		messages["reporterror"] = "Click <a href='../'>here</a> if you need to report an error that you experienced while using your navigation unit.";
		messages["news"] = "Keep checking back periodically, as we will have news and updates about our system. Thank you for your business!";
		messages["garmin"] = "Click <a target='_blank' href='https://buy.garmin.com/shop/shop.do?cID=134'>here</a> to view Garmin\'s catalogue of products.";
		function changeView(cat){
			showView(messages[cat]);
		}
		
		function showView(msg){
			if(msg){
				document.getElementById("infoPanel").innerHTML = msg;
			}
		}
		
		function checkBrowser(){
			if(navigator.appName.indexOf("Internet Explorer") >= 0){
				window.location.replace("index-ie.php");
			}
		}
		
		</script>
	</head>
<body>



<table align="center">  
<tr>
<td align="center">
<center><img src="../jamnav-trimmed.gif" align="center"/></center>
</td>
</tr>
<tr>
<td align="center">
<div class="icon" id="bugfixes" onmouseover="changeView(this.id);">
	<img src="images/bugfixes.png" title="See important information about bug fixes in JAMNAV"/>
</div>

<div class="icon" id="faq" onmouseover="changeView(this.id);">
	<img src="images/faq.png" title="Download the FAQ"/>
</div>
<div class="icon" id="garmin" onmouseover="changeView(this.id);">
	<a target='_blank' href='https://buy.garmin.com/shop/shop.do?cID=134' style="cursor:pointer;"><img border="0" src="images/garmin.png" title="See information about new Garmin products"/></a>
</div>

<div class="icon" id="reporterror" onmouseover="changeView(this.id);">
	<a href='../'><img border="0" src="images/reporterror.png" title="Report an error you have experienced."/></a>
</div>
</td>
</tr>
<tr>
<td align="center">
<div id="infoPanel">
Information panel... Hover over an icon above to see information displayed here. 
</div>
</td>
</tr>
</table>

</body>
</html>
