<?php
require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");


$pg = new PermGuru ( "gpsform" );

$kia = new Kia ( );

if(!$pg->isLoggedIn()){
	if($_GET["action"] == "lg"){
		$usr = $_POST["usr"];
		$psw = $_POST["psw"];
		
		if(!empty($usr) && !empty($psw)){
			if($pg->login($usr,$psw,false)){
				header("Location: garrep.php");
			} else {
				echo "<span style='color:red'>Could not log you in!!!!</span><br>";
			}
			
		}
	}
	else if($_GET["action"] == "lo"){
		$pg->logout();
		header("Location: garrep.php");
	}
}

if(!$pg->isLoggedIn()) {
	echo "<html><head><title>Garmin Server Log</title><body>No unauthorized access allowed to this page. Please login!<br>";
	?>
	<form method="POST" action="garrep.php?action=lg">
		Username:&nbsp;<input name="usr">&nbsp;
		Password:&nbsp;<input type="password" name="psw"/>&nbsp;
		<input type="submit" value="Login"/>
		
	</form>
	<?
	echo "</body></html>";
	die;
}

$sql = "select count(lock_code) as num_locks, lock_timestamp::DATE as ts, product_type from gps_lock natural join gps_product group by lock_timestamp::DATE, product_type order by ts";

$result = $kia->runSQL($sql);

$dcMonthArray = array();

$unitMonthArray = array();


if($result){
	while($row = $kia->loopResult($result)){
		$timeObj = strtotime($row["ts"]);
		$monthStr = date("Y-m",$timeObj);
		$num_locks = $row["num_locks"];
		$prod_type = $row["product_type"];
		
		if(!$dcMonthArray[$monthStr]) $dcMonthArray[$monthStr] = 0;
		if(!$unitMonthArray[$monthStr]) $unitMonthArray[$monthStr] = 0;
		
		
		if($prod_type == "Card")
		$dcMonthArray[$monthStr] += $num_locks;
		else if($prod_type == "Internal Memory")
		$unitMonthArray[$monthStr] += $num_locks;
	}
}


$start_year = date("Y");

$start_month = 1;
// Determine what half of the year we are in and generate a report based on that half
if(date("m") > 6){
	$start_month = 7;
}

if (isset($_GET["start_month"])) $start_month = $_GET["start_month"];

?>
<html>
	<head>
	<style>
	BODY {
		background-color:rgb(200,200,210);
		font-size:11px;
		}	
	
	h1 {
		font-family:Verdana,Arial,Calibri;	
	}
	
	
	.header TD {
		color:white;
		background-color:rgb(40,40,80);
		font-weight:bold;
		padding:10px;
	}
	
	
	TD {
		border-style:solid;
		border-width:1px;
		font-size:11px;
		padding:5px;
		
	}
	
	#sig TD {
		border:none;
	}
	
	#sig .box {
		border-style:solid;
		border-width:1px;
		width:250px;
		height:40px;
		background-color:white;
	}
	</style>
		<title>Garmin Report</title>
<link href="cal.css" type="text/css" rel="stylesheet"/>
<script src="jquery-1.2.6.min.js"></script>
<!--<script src="jquery-ui-personalized-1.5.2.min.js"></script>-->
<script src="jquery-ui-personalized-1.5.3.min.js"></script>
<script src="jquery.blockUI.js"></script>
<script src="jquery.highlightFade.js"></script>
<script src="crudmachine-client.js"></script>
<script src="SpryAssets/xpath.js" type="text/javascript">
        </script>
<script src="SpryAssets/SpryData.js" type="text/javascript">
        </script>
<script src="datasets.js"></script>
<script src="main.js"></script>
<script src="calendar.js"></script>
	</head>
	
<body>
<h1>Garmin MPC Royalty Report</h1>

<table>
	<tr><td style="background-color:black; padding:8px; color:white;"><b>Licensee:</b></td><td style="border:none;">Mona Informatix Ltd.</td></tr>
	<tr><td  style="background-color:black;  padding:8px; color:white;"><b>Reporting Period:</b></td>
	<td  style="border:none;">
	<?
	 echo date("F Y",mktime(0,0,0,$start_month,1,$start_year)) . " through " . date("F Y",mktime(0,0,0,$start_month+5,1,$start_year));	
	 ?>
	 </td></tr>
</table>
<br><br>
<img style='border-style:solid; border-width:1px;' src='jamnav_logo.jpg' height='70px'><br><br>
<table cellpadding="5" cellspacing="0" border="0">	
<?


echo "<tr class='header'><td>Derivative Product:</td><td>Distribution Type:</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	echo "<td>" . date("M-Y",mktime(0,0,0,$i,1,$start_year)) . "</td>";
}

echo "<td>Total</td></tr>";

echo "<tr><td colspan='6' style='border:none;'><b>JAMNAV by MonaGIS License #1436</b></td></tr>";

echo "<tr><td></td></tr>"; // blank row

// Handle the data card rows
echo "<tr><td>Locked Routable</td><td>GPS Unit</td>";
$total = 0;
for($i = $start_month; $i < $start_month+6; $i++){
	$amount = ($unitMonthArray[date("Y-m",mktime(0,0,0,$i,1,$start_year))]);
	if(!$amount) $amount = 0;
	$total += $amount;
	echo "<td>" . $amount . "</td>";
}
echo "<td>$total</td></tr>";

$total = 0;
$amount = 0;

echo "<tr><td>Non-Locked Routable</td><td>GPS Unit</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	if(!$amount) $amount = 0;
	echo "<td>" . $amount . "</td>";
	$total+=$amount;
}

echo "<td>$total</td></tr>";

echo "<tr><td></td></tr>"; // blank row
$total = 0;




// Handle the internal memory card row
echo "<tr><td>Locked Routable</td><td>Data Card</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	$amount = ($dcMonthArray[date("Y-m",mktime(0,0,0,$i,1,$start_year))]);
	if(!$amount) $amount = 0;
	$total += $amount;
	echo "<td>" . $amount . "</td>";
}

echo "<td>$total</td></tr>"; $total = 0;

echo "<tr><td>Non-Locked Routable</td><td>Data Card</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	if(!$amount) $amount = 0;
	$total += $amount;
	echo "<td>" . $amount . "</td>";
}

echo "<td>$total</td></tr>"; $total = 0;

 
echo "<tr><td></td></tr>"; // blank row


$total = 0;
// Handle the internal memory card row
echo "<tr><td>Locked Routable</td><td>CD-Rom</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	$amount = 0;
	$total += $amount;
	echo "<td>" . $amount . "</td>";
}
echo "<td>$total</td></tr>"; $total = 0;


echo "<tr><td>Non-Locked Routable</td><td>CD-Rom</td>";
for($i = $start_month; $i < $start_month+6; $i++){
	if(!$amount) $amount = 0;
	$total += $amount;
	echo "<td>" . $amount . "</td>";
}

echo "<td>$total</td></tr>"; $total = 0;

echo "<tr><td></td></tr>";

?>
</table>
<br><br><br>
<b>This report is certified to be correct by:</b><br>
<table id="sig">
	<tr><td>Signature:</td><td class="box">&nbsp;</td></tr>
	<tr><td>Name:</td><td class="box">&nbsp;</td></tr>
	<tr><td>Title:</td><td class="box">&nbsp;</td></tr>
</table>

<br><br><br>

<input type="button" value="Print" onclick="print()"/>
&nbsp;

</body>
</html>
