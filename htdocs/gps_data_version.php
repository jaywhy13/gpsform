<?php
include_once ("../include/dbal.php");
$dbal = new DbAbstractionLayer ( );
header("Content-type: text/xml");


$versions = array();
if($dbal->connect()){
	$sql = "select * from gps_data_version";
//	echo "$sql";

    $sql .= " ORDER BY version_release_date DESC";

	$result = $dbal->queryDb($sql);
	if($result){
		while($row = $dbal->loopResult($result)){
			$version_no =  $row["version_no"];
            $version_name = $row["version_name"];
            $version_release = $row["version_release_date"];

			$params = array();
            $params [] = "<version>";
            $params [] = "<number>$version_no</number>";
            $params [] = "<name>$version_name</name>";
            $params [] = "<release_date>$version_release</release_date>";
			$params [] = "</version>";
			$versions [] = join("",$params);
		}
	}

}

echo "<versions> " . join(",",$versions) . "</versions>";

?>

