/**
* @author JDub
* @fileoverview This file allows you to a purely javascript-css calendar to your pages. Use it by doing cal = new Cal (parentId,month,year).
* Then you can use the nextMonth and previousMonth functions to advance the calendar, all instance methods are updated in the GUI. That's it !!!
*
*/
var dayNames				=	["Sun","Mon","Tues","Wed","Thurs","Fri","Sat"];
var monthNames				=	["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"];
var previousMonthIcon		=	null;
var nextMonthIcon			=	null;
var daysInWeek				=	7;


CAL_DAY_SELECTED = "daySelected";

/**
* Calender object constructor
* @constructor
* @param {String} objId id of the parent the calendar needs to be added
* @param {int} month index of the month
* @param {int} index of the year
*/
function Cal (objId,month,year) {
	if(!document.getElementById (objId))
	{
		alert ("Dom obj not found");
		return;
	}

	var domObj = document.getElementById (objId);
	domObj.className = "calendar";
	this.month = 0;
	this.year = 0;
	var tmpDate = new Date();
	this.parentId = objId;
	if(month){
		this.month = month;
	} else {
		this.month = tmpDate.getMonth();
	}

	if(year)
	{
		this.year = year;
	} else {
		this.year = tmpDate.getFullYear();
	}

	// Create events
	this.events = new Object();
	this.events[CAL_DAY_SELECTED] = new Array();
	this.draw (); // draw the calendar

}

/**
* Clear calendar.. called before a month is redrawn
*
*/
Cal.prototype.clear = function (){
	var obj = document.getElementById (this.parentId);
	while (obj.hasChildNodes ())
	{
		obj.removeChild (obj.lastChild);
	}
};


/**
* Advances the calendar by one month
*/
Cal.prototype.nextMonth = function ()
{
	this.moveByMonth (1);
};

/**
* Goes back one month
*
*/
Cal.prototype.previousMonth = function (){
	this.moveByMonth (-1);
};


Cal.prototype.moveByMonth = function (increment){
	this.month = this.month + increment;
	if(this.month > 11)
	{
		this.year = this.year + 1;
		this.month = 0;
	}
	if(this.month < 0)
	{
		this.year = this.year - 1;
		this.month = 11;
	}
	this.redraw ();
};


Cal.prototype.moveByYear  = function (increment){
	this.year = this.year + increment;
	this.redraw ();
};


/**
* Redraws the calendar... useless if month and year are not changed
*
*/
Cal.prototype.redraw = function (){
	this.clear ();
	var parent = document.getElementById (this.parentId);
	parent.appendChild (getCalenderHeader(this));
	parent.appendChild (getCalendar (this.month,this.year,this));
	parent.appendChild( getCalendarFooter(this));

};

function getCalenderHeader(cal){
	if(!cal) retrun;
	var div = document.createElement("div");
	var spanLastMonth = document.createElement("span");
	var spanNextMonth = document.createElement("span");
	var spanThisMonth = document.createElement("span");

	spanLastMonth.cal = cal;
	spanNextMonth.cal = cal;
	spanThisMonth.appendChild(document.createTextNode (monthNames[cal.month] + ", " + cal.year) );
	spanThisMonth.className = "monthName";
	var img;
	if(previousMonthIcon){
		img = new Image();
		img.src = previousMonthIcon;
		img.title = "Previous Month";
		img.width = 24;
		img.height = 24;
		spanLastMonth.appendChild(img);
	} else {
		spanLastMonth.appendChild(document.createTextNode ("Last Month") );
	}

	if(nextMonthIcon){
		img = new Image();
		img.src = nextMonthIcon;
		img.width = 24;
		img.height = 24;
		spanNextMonth.appendChild(img);
		img.title = "Next Month";
	} else {
		spanNextMonth.appendChild(document.createTextNode ("Next Month") );
	}

	spanNextMonth.onclick = function(){
		this.cal.nextMonth();
	};
	spanLastMonth.onclick = function(){
		this.cal.previousMonth();
	};

	div.className =  "calendarHeader";
	div.appendChild(document.createElement("br"));
	div.appendChild(spanLastMonth);
	div.appendChild(spanThisMonth);
	div.appendChild(spanNextMonth);


	return div;

}


function getCalendarFooter(cal){
	var closeCal = document.createElement("a");
	closeCal.href = "javascript:void(0)";
	closeCal.appendChild(document.createTextNode("Close Calendar"));
	closeCal.cal = cal;
	closeCal.onclick = function(){
		this.cal.hide();
	};
	return closeCal;
}


/**
* Draws the calendar
*
*/
Cal.prototype.draw = function (){
	var parent = document.getElementById (this.parentId);
	parent.appendChild (getCalenderHeader(this));
	parent.appendChild (getCalendar (this.month,this.year,this));
	parent.appendChild( getCalendarFooter(this));
};

Cal.prototype.registerForEvent = function(eventType,f){
	if(this.events[eventType])
	{
		this.events[eventType].push (f);
	}
};

Cal.prototype.hide = function() {
	if(document.getElementById( this.parentId ) ) {
		document.getElementById(this.parentId).style.display = "none";
	}
};

Cal.prototype.show = function(){
	if(document.getElementById( this.parentId ) ) {
		document.getElementById(this.parentId).style.display = "";
	}
}

Cal.prototype.notifyListeners = function(eventType,arg){
	if(this.events[eventType]) {
		for(var i = 0 ; i < this.events[eventType].length; i++){
			var f = this.events[eventType][i];
			f(eventType,arg);
		}
	}
}


/**
* Returns the number of days in a month
* @param month the index of the month
* @param year the index of the month
*/
function daysInMonth (month,year)
{
	return 32 - new Date (year,month,32).getDate ();
}

/**
* Returns the number of days in last month
* @param month the index of the month
* @param year the index of the month
*/
function daysInLastMonth (month,year)
{
	if(month == 0)
	{
		return daysInMonth (11,year-1);
	}
	else {
		return daysInMonth (month-1,year);
	}
}

function oneDayPassed (index){
	return index < 6 ? (index + 1 ) : 0;
}

/**
* Returns a UI calendar component
* @param {int} month the index of the month
* @param {int} year the index of the month
* @param {Cal} [optional] calendar instance. Needed if you want event handling though
*/
function getCalendar (month,year,cal)
{

	var days = daysInMonth (month,year);

	var daysLastMonth = daysInLastMonth (month,year);

	var firstDay = getFirstDay (month,year);

	var lastDay = getLastDay (month,year);
	var currentWeekTr = null;

	var dayIndex = 0;

	var ol, li, calendarTbl;

	calendarTbl = document.createElement ("table");
	calendarTbody = document.createElement("tbody");
	processObj(calendarTbl);

	calendarTbl.className = "calendar";
	calendarTbl.appendChild(calendarTbody);


	// Days of the week
	var dowTr = document.createElement ("tr");
	dowTr.className = "daysOfTheWeek";
	processObj(dowTr);
	calendarTbody.appendChild (dowTr);

	for(var i = 0; i < dayNames.length; i++)
	{
		var dowTd = document.createElement ("td");
		processObj(dowTd);
		dowTd.appendChild (document.createTextNode (dayNames[i]));
		dowTr.appendChild (dowTd);
	}


	// Add the days for last month if firstDay != 0
	if(firstDay != 0)
	{

		var startAt = daysLastMonth - (firstDay - 1);

		var lastMonthTr = document.createElement ("tr");
		calendarTbody.appendChild (lastMonthTr);
		lastMonthTr.setAttribute ("startAt",startAt);
		processObj(lastMonthTr);

		currentWeekTr = lastMonthTr;
		currentWeekTr.className = "week";

		for(var i = 0; i < firstDay; i++)
		{
			var dayTd = document.createElement ("td");
			dayTd.className = "lastMonthDay";
			processObj(dayTd);
			currentWeekTr.appendChild (dayTd);
			dayTd.appendChild (document.createTextNode (startAt+i));
			dayTd.month = month - 1;
			dayTd.year = year;
			dayTd.day = (startAt+i);
			if(month - 1 < 0){
				dayTd.month = 11;
				dayTd.year = year - 1;
			}
			underLineToday(dayTd,startAt+i,month-1,year);
			if(cal){
				dayTd.cal = cal;
				dayTd.onclick = function()
				{
					this.cal.notifyListeners(CAL_DAY_SELECTED,getValue(this));
				};
			}
			dayIndex = oneDayPassed (dayIndex);
		}
	}



	var thisMonthTr = document.createElement ("tr");
	processObj(thisMonthTr);
	calendarTbody.appendChild (thisMonthTr);

	if(currentWeekTr == null){ // meaning the 1st day of this month started on sunday...
		currentWeekTr = thisMonthTr;
	}

	for(i = 0; i < days; i++)
	{
		if(dayIndex == 0){
			currentWeekTr = document.createElement("tr");
			currentWeekTr.className = "week";
			calendarTbody.appendChild(currentWeekTr);
		}

		var dayTd = document.createElement ("td");
		processObj(dayTd);
		dayTd.className = "thisMonthDay";
		currentWeekTr.appendChild (dayTd);
		dayTd.appendChild(document.createTextNode (i+1));
		dayTd.month = month;
		dayTd.year = year;
		dayTd.day = i + 1;
		underLineToday(dayTd,i+1,month,year);
		if(cal){
			dayTd.cal = cal;
			dayTd.onclick = function()
			{
				this.cal.notifyListeners(CAL_DAY_SELECTED,getValue(this));
			};
		}

		dayIndex =  oneDayPassed(dayIndex);
	}


	if(lastDay < 6){

		for(i = lastDay; i < 6; i++)
		{
			if(dayIndex == 0){
				currentWeekTr = document.createElement("tr");
				currentWeekTr.className = "week";
				calendarTbody.appendChild(currentWeekTr);
			}

			var dayTd = document.createElement ("td");
			dayTd.className = "nextMonthDay";

			processObj(dayTd);
			dayTd.appendChild (document.createTextNode ((i+1)-lastDay));
			dayTd.month = month + 1;
			dayTd.year = year;
			if(month + 1 > 11) {
				dayTd.year = year + 1;
				dayTd.month = 0;
			}
			dayTd.day = (i+1)-lastDay;
			underLineToday(dayTd,((i+1)-lastDay),month+1,year);
			if(cal){
				dayTd.cal = cal;
				dayTd.onclick = function()
				{
					this.cal.notifyListeners(CAL_DAY_SELECTED,getValue(this));
				};
			}
			currentWeekTr.appendChild(dayTd);
			dayIndex =  oneDayPassed(dayIndex);

		}
	}

	return calendarTbl;

}


function getValue(obj) {
	//	alert("Now parsing: " + obj.day + "-" + obj.month + "-" + obj.year);
	return Date.parse(monthNames[obj.month] + " " + obj.day + ", " + obj.year);
}


/**
* Tells what day (index) a month started on
* @param month the index of the month
* @param year the index of the month
*/
function getFirstDay (month,year){
	return new Date (year,month,1).getDay ();
}

/**
* Tells the last day (index) that a month ended on
* @param month the index of the month
* @param year the index of the month
*/
function getLastDay (month,year)
{
	var numDays = daysInMonth (month,year);
	return new Date (year,month,numDays).getDay ();
}


function makeSelectable (domObj)
{

}

function processObj(obj){
	return;
	if(!obj.style.margin){
		obj.style.margin = 0;
	}

	if(!obj.style.padding){
		obj.style.padding = 0;
	}

	if(!obj.style.display){
		if(obj.nodeName.toLowerCase() == "td"){
			obj.style.display = "inline";
			obj.style.margin = "0.2em";
			obj.style.padding = "0.2em";
		}

	}
}


function underLineToday(obj,date,month,year){
	var d = new Date();
	if(date == d.getDate() && month == d.getMonth() && year == d.getFullYear()){
		obj.style.textDecoration = "underline";
		obj.style.fontWeight = "bold";
		obj.style.backgroundColor = "rgb(200,230,5)";
	}
}


