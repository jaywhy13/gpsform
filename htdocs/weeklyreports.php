<?php
require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");
require_once("../include/blabber.php");

$pg = new PermGuru("gpsform");
$kia = new Kia();

$minYearWeek = "3000";
$maxYearWeek = "1000";

function yearWeekCompareTo($first, $second) {
    $firstPieces = explode(".", $first);
    $secondPieces = explode(".", $second);

    $firstYear = $firstPieces[0];
    $firstWeek = $firstPieces[1];
    $secondYear = $secondPieces[0];
    $secondWeek = $secondPieces[1];

    if ($firstYear > $secondYear) {
        return 1;
    } else if ($secondYear > $firstYear) {
        return -1;
    } else if ($firstYear == $secondYear) {
        if ($firstWeek > $secondWeek) {
            return 1;
        } else if ($secondWeek > $firstWeek) {
            return -1;
        } else {
            return 0;
        }
    }
}

function countTotalsByWeeks($tableName, $dateColumn, $whereCond = "", $countParam = "COUNT(*)", $groupByAdditions = array(), $countColumnName = "count") {
    $kia = $GLOBALS["kia"];
    $sql = "SELECT $countParam, date_part('week',$dateColumn) AS week, date_part('year',$dateColumn) AS year FROM $tableName ";
    if ($whereCond != "") {
        $sql .= " WHERE $whereCond ";
    }

    $sql .= "GROUP BY date_part('week',$dateColumn), date_part('year',$dateColumn)";
    if (sizeof($groupByAdditions) > 0) {
        $sql .= ", " . implode(", ", $groupByAdditions);
    }

    $sql .= " ORDER BY date_part('year',$dateColumn), date_part('week',$dateColumn)";

    $res = $kia->runSQL($sql);
    $arr = pg_fetch_all($res);

    $resArr = array();
    foreach ($arr as $row) {
        $week = $row["week"];
        $year = $row["year"];
        $count = $row[$countColumnName];
        $yearWeek = "$year.$week";
        if (yearWeekCompareTo($yearWeek, $GLOBALS["minYearWeek"]) === -1) {
            $GLOBALS["minYearWeek"] = $yearWeek;
        }
        if (yearWeekCompareTo($yearWeek, $GLOBALS["maxYearWeek"]) === 1) {
            $GLOBALS["maxYearWeek"] = $yearWeek;
        }
        $resArr[$yearWeek] = $count;
    }

//    echo $sql. "<br>";
//    echo "<pre>";
//    print_r($resArr);
//    echo "</pre>";

    return $resArr;
}

function getWeekStr($year, $weekNumber) {
    if ($weekNumber < 10) {
        $weekNumber = "0" . $weekNumber;
    }
    return date(
                    "Y-m-d", strtotime($year . "W" . $weekNumber));
}

function printTable($tableData) {
    $minYearWeekPieces = explode(".", $GLOBALS["minYearWeek"]);
    $maxYearWeekPieces = explode(".", $GLOBALS["maxYearWeek"]);

    $minYear = $minYearWeekPieces[0];
    $minWeek = $minYearWeekPieces[1];

    $maxYear = $maxYearWeekPieces[0];
    $maxWeek = $maxYearWeekPieces[1];
    $totals = array();
    echo "<table id='report' cellpadding='5' cellspacing='0' border='0'><thead><tr class='header'><th>Week No.</th><th>Week Starting</th>";
    foreach ($tableData as $heading => $data) {
        echo "<th>$heading</th>";
        $totals[$heading] = 0;
    }
    echo "</thead>";


    $year = $minYear;
    $week = $minWeek;
    
    $year = 2008;
    echo "<tbody>";
    $i = 0;
    while ($year <= $maxYear) {
        if($week == 0) {
            $week++;
            continue;
        }
        $i++;
        $yearWeek = "$year.$week";
        $weekStr = getWeekStr($year, $week);
        $class = ($i % 2 == 1) ? "odd" : "even";
        echo "<tr class='$class'>";

        echo "<td>Week $week</td>";
        echo "<td>$weekStr</td>";
        foreach ($tableData as $heading => $data) {
            $amount = 0;
            if ($data["$yearWeek"]) {
                $amount = $data["$yearWeek"];
            }
            echo "<td align='center'>$amount</td>";
            $totals[$heading]+= $amount;
        }
        echo "</tr>";
        $week++;

        if ($year == $maxYear && $week >= $maxWeek + 1) {
            break;
        }

        if ($week >= 53) {
            $year++;
            $week = $week % 53; // just in case its a leap year
        }
    }
    echo "</tbody>";
    echo "<tfoot>";
    echo "<tr><td colspan='2'>Totals</td>";
    foreach ($totals as $heading => $total) {
        echo "<td align='center'><b>$total</b></td>";
    }
    echo "</tr>";
    echo "</tfoot>";
    echo "</table>";
}

$orderArr = countTotalsByWeeks("gps_order", "order_date");
$packagesSoldArr = countTotalsByWeeks("gps_order_item NATURAL JOIN gps_order", "order_date", "", "SUM(order_item_quantity)", array(), "sum");
$normalUpdatesArr = countTotalsByWeeks("order_expense", "created_at", "","SUM(oe_quantity) AS count");
$specialUpdatesArr = countTotalsByWeeks("gps_order_item NATURAL JOIN gps_order", "order_date", "package_id = 'gps_1307461442007fis29810is'", "SUM(order_item_quantity)", array(), "sum");

$tableData = array(
    "Order Count" => $orderArr,
    "Packages Sold" => $packagesSoldArr,
    "Normal Updates" => $normalUpdatesArr,
    "Special Updates" => $specialUpdatesArr
);




//echo "Min week is: $minYearWeek<br>";
//echo "Max week is: $maxYearWeek<br>";
?>
<html>
    <head>
        <script src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
        <script src="http://tablesorter.com/jquery.tablesorter.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#report").tablesorter();
            });

            $("th").hover(function(){
                this.style.backgroundColor = "rgb(230,230,255)";
            },null);




        </script>
        <title>Weekly Reports</title>
        <style type="text/css">
            BODY,TR,TD {
                font-size:11px;	
                font-family:Verdana;
            }
            TD {
                border-style:solid;
                border-width:1px;
                border-top:none;
                border-left:none;
                border-right:none;
            }

            TH {
                cursor:pointer;
            }

            .odd TD {
                background-color:rgb(240,240,255);
            }

            .header TH {
                font-weight:bold;
            }

        </style>
    </head>
    <body>
        <img src="images/chart_bar.png"/>&nbsp;&nbsp;<a href="summaries.php">Charts</a>
        <br/>
        <img src="images/1333502015_20.png">&nbsp;&nbsp;<a href="updatesummary.php">Update Details</a>
        <br/>
<? printTable($tableData); ?>
        <script src="js/jquery-1.5.1.min.js"></script>
    </body>
</html>