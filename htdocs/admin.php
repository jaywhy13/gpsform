<html>
    <head>
        <title>JIS - JamNav Internal System</title>
        <link href="cal.css" type="text/css" rel="stylesheet"/>
        <link href="styles.css" type="text/css" rel="stylesheet"/>
        <script src="jquery-1.2.6.min.js"></script>
        <!--<script src="jquery-ui-personalized-1.5.2.min.js"></script>-->
        <script src="jquery-ui-personalized-1.5.3.min.js"></script>
        <script src="jquery.blockUI.js"></script>
        <script src="jquery.highlightFade.js"></script>
        <script src="crudmachine-client.js"></script>
        <script src="SpryAssets/xpath.js" type="text/javascript">
        </script>
        <script src="SpryAssets/SpryData.js" type="text/javascript">
        </script>
        <script src="datasets.js"></script>
        <script src="main.js"></script>
        <script>
            function checkBrowser(){
                var browserName = navigator.appName;
            }

        </script>
        <style type="text/css">
            BODY,TABLE,TD,SELECT,INPUT,TEXTAREA {
                font-size: 10px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .inputForm {
                border-style: solid;
                border-width: 1px;
                padding: 10px;
                /*width: 300px;*/
                background-color: rgb(240, 240, 252);
                position: absolute;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .inputForm INPUT,SELECT,TEXTAREA {
                width: 180px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
                border-style:solid;
                border-width:1px;
                border-color:rgb(150,150,250);
                background-color:#bcbcef;
                color:black;
                font-weight:bold;
                padding:3px;
            }

            .inputForm INPUT {
                background-image:url("inputbg.png");
                background-repeat:repeat-x;
            }


            .inputForm .caption {
                width:95%;
                background-color:444466;
                padding:10px;
                font-size:13px;
                color:white;
                font-weight:bold;
                cursor:pointer;
            }

            #customer_list {
                right: 0px;
                top: 0px;
                position: absolute;
                width: 750px;
                padding: 5px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            #package_list {
                left: 30px;
                position:absolute;
                bottom:50px;
                height:300px;
                overflow:auto;
                width:400px;
                border-left-style:solid;
                border-left-width:3px;
                border-left-color:#96324B;
                display:none;

            }

            #inventory_list {
                left: 460px;
                position:absolute;
                bottom:50px;
                height:300px;
                overflow:auto;
                width:400px;
                border-left-style:solid;
                border-left-width:3px;
                border-left-color:#96324B;
                /*display:none;*/

            }

            #customer_list TABLE TD {
                background-color: white;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .customer {
                padding: 5px;
                background-color: rgb(230, 230, 249);
                font-size: 10px;
                border-style: solid;
                border-width: 1px;
                margin-bottom: 2px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .customer_name {
                cursor: pointer;
            }

            .customer SPAN {
                font-size: 11px;
                font-weight: bold;
                padding: 5px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .customer_operation {
                display: none;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .customer_operation TD {
                padding: 10px;
                background-color: none;
                color: rgb(50, 40, 240);
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            TABLE TD {
                font-size: 10px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .paymentDialog {
                width: 500px;
                position: absolute;
                top: 40px;
                border-style: solid;
                border-width: 1px;
                background-color: white;
                padding: 5px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
            }

            .orderInfoDialog {
                background-color: white;
            }




            .invoiceTbl {
                font-size: 10px;
                font-family: Calibri, Century Gothic, Verdana, Bitstream Vera Sans;
                width: 560px;
            }

            .invoiceTbl TD {
                border-style: solid;
                border-width: 1px;
            }

            .invoiceTblHeader TD {
                background-color: black;
                color: white;
                font-weight: bold;
                border: none;
            }

            .orderDetail {
                width:98%;
            }


            .orderDetailTd {
                border-bottom-style:solid;
                margin-bottom:3px;
            }

            .orderDetailDialog {
                border-style: solid;
                border-width: 1px;
                width: 400px;
                padding: 5px;
                background-color:white;

            }

            .orderDetailDialogTblContainer {
                background-color:rgb(200,200,250);
                padding:5px;
            }

            .orderDetail TD {
                background: none !important;
            }

            .orderDetailHeader TD {
                color: white;
                background-color: black !important;
                font-weight: bold;
            }

            .orderDetailTblHeader {
                color: white;
                background-color: black;
                font-weight: bold;
            }

            #package_list {


            }

            #package_list .header {
                font-size:13px;
                font-weight:bold;
            }

            .gps_package_tbl {

            }

            .gps_package TD {
                background-color:rgb(150,50,75);
                padding:5px;
                color:white;
                font-weight:bold;
                border-top-style:solid;
            }

            .gps_package TD A {
                color:white;
            }

            .headerRow {
                font-size:11px;
                font-weight:bold;
                background-color:rgb(240,220,235);
            }

            .headerRow TD {
                padding:5px;
            }


            #cal_div {
                border-style:solid;
                border-width:1px;
                padding:5px;
                right:0px;
                bottom:0px;
                width:270px;
                background-color:white;
            }

            A IMG {
                padding-right:5px;
            }


            .paymentListing {
                background-color:rgb(240,255,220);
                padding:10px;
            }

            .paymentTable TD {
            }

            .paymentTableHeader TD {
                background-color:black;
                color:white;
                font-weight:bold;
                padding-top:3px;
                padding-bottom:2px;
                padding-right:4px;
            }

            #notification {
                position:absolute;
                width:250px;
                background-color:black;
                top:10px;
                right:10px;
                z-index:50;
                -moz-opacity:0.8;
                filter:alpha(opacity:80);
                display:none;
                font-weight:bold;
                color:white;
                padding:5px;
            }

            #notification .notificationMessage {
                width:90%;
                border-style:solid;
                padding:5px;
                border-style:solid;
                border-width:1px;
                borer-color:white;
                margin:5px;
                font-size:10px;
                font-weight:normal;
                text-align:justify;
            }

            #notification A {
                color:red;
                margin:3px;
            }

            .notificationMessage .title {
                display:block;
                font-weight:bold;
                font-decoration:underline;
                background-color:black;
            }

            A IMG {
                /*margin:2px;*/
            }


            .orderNo {
                color:black !important;
                font-weight:bold;
                font-size:13px;
                text-align:left;
            }


            .orderSummaryHeader SPAN {
                font-size:12px;
                width:250px;
                padding:5px;
                margin:15px;
                margin-left:2px;
                margin-bottom:10px;

                color:white;
                background-color:rgb(50,50,120);
                cursor:pointer;
                border-style:solid;
                border-width:1px;
                font-decoration:underline;
            }

            .orderInfo {
                display:none;
            }

            #fixed_footer {
                width:100%;
                left:0px;
                position:fixed;
                bottom:0px;
                background-color:black;
                color:white;
                font-size:12px;
                font-weight:bold;
                height:35px;
                border-style:solid;
                border-width:2px;
                border-color:red;
                border-right:none;
                border-left:none;
                border-bottom:none;
                -moz-opacity:0.9;
                padding:5px;

            }

            .menu_item {
                padding:3px;
                padding-right:10px;
                padding-left:10px;
                margin-right:10px;
                display:inline-block;
                background-color:rgb(80,80,80);
                font-size:10px;
                height:20px;
            }


            .menu_item SPAN, .menu_item A {
                font-size:10px;
            }

            #menu_tbl TD {
                padding-right:20px;
            }
        </style>
    </head>

    <body onload="doStartupStuff(); checkBrowser(); scheduleNotificationChecker();">
        <div id="notification">

        </div>

        <img src="jis.gif" width="300px" />
        <div id="loginInfo"></div>


        <div id="customer_list"><img style="margin: -10px; margin-top: 2px;"
                                     src="images/User-64x64.png" />&nbsp;<b style='font-size: 13px;'>Customers</b>&nbsp;&nbsp;
            <a href="javascript:void(0)" onClick="createForm('gps_customer','Create Customer');"><img src="images/Button-Add-16x16.png" border="0">&nbsp;Add New</a>
            &nbsp; &nbsp;<a href="javascript:void(0)" onCLick="searchBySerial();">Search By Serial</a>
            &nbsp;&nbsp;<input type="text" onkeyup="filterCustomers(this.value)" id="filterCust">
            <div id="spry_customer_list" spry:region="dsCustomer dsOrder">
                <div spry:repeat="dsCustomer" class="customer"
                     id="cust_{dsCustomer::cust_id}"><span class="customer_name"
                                                      onclick="toggleCustomerOperationPanel('{dsCustomer::cust_id}')"><img src="images/user-24x24.png">&nbsp;
                        <span spry:if="'{dsCustomer::cust_fname}' == ''">
                            {dsCustomer::cust_lname}
                        </span>
                        <span spry:if="'{dsCustomer::cust_fname}' != ''">
                            {dsCustomer::cust_lname}, {dsCustomer::cust_fname}
                        </span></span>
                    &nbsp;&nbsp;&nbsp;<span id="delinquency_{dsCustomer::cust_id}"></span>
                    <div id="cust_operate_{dsCustomer::cust_id}" class="customer_operation"
                         style="display: none;">
                        <div id="cust_detail_{dsCustomer::cust_id}"><img
                                src='images/phone-16x16.png' />&nbsp;Contact:
                            {dsCustomer::cust_contact}&nbsp; &nbsp;<span spry:if="'{dsCustomer::cust_email}' != ''"><img src="images/email.png"/>&nbsp;{dsCustomer::cust_email}</span></div>
                        <a href="javascript:void(0)"
                           onclick="editForm('gps_customer','Edit Customer',dsCustomer.getData()[{ds_RowNumber}]);">Edit
                            Customer</a> &nbsp;&nbsp;<a href="javascript:void(0)"
                                                    onClick="deleteAction(dsCustomer.getData()[{ds_RowNumber}],'gps_customer',function(){dsCustomer.loadData();});">Delete
                            Customer</a> &nbsp;&nbsp;<a href="javascript:void(0)" onClick="manageCustomerUnits(dsCustomer.getData()[{ds_RowNumber}]);">Manage Customer Units</a>

                        <br>
                        <br>
                        <div id="order_info_{dsCustomer::cust_id}"></div>

                        <div id="product_info_{dsCustomer::cust_id}"></div>
                    </div>
                </div>
                <br>
            </div>
            <br><br><br>
        </div>
        <br><br>


        <div class="span-17 column last" style="display:none;">

            <h1>Web Content Delivery Demonstration</h1>	    				    		    

            <div>
                <h4>Step 1: Validate Garmin Communicator Plugin and API Unlock Status</h4>
                <ul class="demoList">    		        
                    <li><div id="pluginStatus">Garmin Communicator plugin version 4.0.4.0 found!</div></li>
                    <li><div id="apiUnlockStatus">The API was not unlocked successfully.</div></li>
                </ul>    		    	    
            </div>    


            <div>
                <h4>Step 2: Search for Devices</h4>
                <ul class="demoList">    		           		       
                    <li><div id="deviceStatus"></div></li>
                    <li><select name="deviceSelect" id="deviceSelect" disabled="true"></select></li>
                    <li><input type="button" value="Find Devices" id="findDevices" disabled="disabled"></li>
                </ul>    		         		    
            </div>

            <div>
                <h4>Step 3: Select File for Download</h4>
                <ul class="demoList">
                    <li><select name="fileSelect" id="fileSelect" disabled="true"></select></li>
                </ul>    		    
            </div>

            <div>
                <h4>Step 4: Obtain Unlock Code and its GMA signature for File</h4>
                <ul class="demoList">
                    <li><div id="unlockCode"></div></li>
                    <li><input type="button" id="obtainUnlock" value="Obtain Unlock Code" onclick="getUnlockCode()" disabled="disabled"></li>
                </ul>
            </div>

            <div>
                <h4>Step 5: Download Selected File and Unlock File to GPS Unit</h4>
                <ul class="demoList">                        
                    <li><div id="downloadStatus"></div></li>
                    <li><div id="status"></div></li>
                    <li><div id="progressBar" style="display: none;" align="left">            
                            <div id="progressWrapper" style="border: solid 1px black; width: 400px;"><div id="progressBarDisplay" style="background-color: #3F4C6A;">&nbsp;</div></div>            
                        </div></li>
                    <li><input type="button" id="downloadFile" value="Download Files" disabled="disabled"></li>
                </ul>
            </div>
        </div>

        <div id="cal_div">
        </div>

        <div id="new_inv_div"></div>

        <div id="fixed_footer">

            <div class="menu">
                <table id="menu_tbl"><tr>
                        <td onclick="jQuery('#package_list').show('slow');" style="cursor:pointer; color:white; font-weight:bold;"><img src="images/Word-files-48x48.png" width="16" height="16"/>&nbsp;Package Catalogue</td>
                        <td onclick="jQuery('#inventory_list').show('slow');" style="cursor:pointer; color:white; font-weight:bold;"><img src="images/Word-files-48x48.png" width="16" height="16"/>&nbsp;MGI Inventory</td>
                        <td><img src="images/Green-Dollar-16x16.png" border="0">&nbsp;<a href="javascript:void(0)" onClick="createForm('rate_of_exchange','Add New Rate');">Add New Rate</a></td>
                        <td><img src="images/Green-Dollar-16x16.png" border="0">&nbsp;<a href="javascript:void(0)" onClick="createForm('order_expense_type','Add New Expense Type');">Add New Expense</a></td>
                        <td><img src="images/edit-icon.png" border="0">&nbsp;<a href="javascript:void(0)" onClick="createForm('gps_data_version','Add New Version');">Add New Version</a></td>
                        <td><img src="images/GPSDevice-Map-icon.png" border="0">&nbsp;<a href="javascript:void(0)" onClick="createForm('garmin_unit','Create New Garmin Unit');">Add New Unit</a></td>
                        <td><img src="images/page-edit-icon.png" border="0">&nbsp;<a href="report.php" style="font-weight:bold; color:yellow;">Generate Reports</a></td>
                        <td><img src="images/delete-user-icon.png" border="0" width="24" height="24">&nbsp;<a href="javascript:void(0)" onclick="logout();" id="usr_a_greeting">Logout</a></td>
                    </tr></table>
            </div>

            <!-- inventory listing -->
            <div id="inventory_list">
                <div style="width:100%; background-color:black; color:#96324B; font-weight:bold; padding:10px;">MGI Inventory
                    <img onclick="jQuery('#inventory_list').hide('slow');" src="images/delete-icon.png" style="float:right; margin-right:15px; cursor:pointer;" title="Minimize Dialog">
                    <img onClick="addInventoryItemUI();" src="images/add-icon.png" style="float:right; margin-right:15px; cursor:pointer;" title="Add Inventory Item">
                </div>

                <div id="status_div" style="width:100%; color:black; background-color:rgb(230,230,230); font-weight:bold; padding:10px;">
                    Status:&nbsp;
                    <input type="checkbox"/>Sold&nbsp;
                    <input type="checkbox"/>Unassigned&nbsp;
                </div>

                <div id="inventory_list_listing" style="width:100%; height:100%; background-color:white;">
                    &nbsp;
                </div>
            </div>

            <!-- package listing -->
            <div id="package_list">

                <div id="package_list_rgn" spry:region="dsPackage" style="width:100%;">
                    <div style="width:100%; background-color:black; color:#96324B; font-weight:bold; padding:10px;">Package Catalogue
                        <img onclick="jQuery('#package_list').hide('slow');" src="images/delete-icon.png" style="float:right; margin-right:15px; cursor:pointer;" title="Minimize Dialog">
                        <img onClick="createForm('gps_package','Create GPS Package');" src="images/add-icon.png" style="float:right; margin-right:15px; cursor:pointer;" title="Add New Package">
                        <img onClick="sellPackage();" src="images/cash-register-icon.png" style="float:right; margin-right:15px; cursor:pointer;" title="Sell Package">
                    </div>
                    <table class="package_tbl" cellpadding="2" cellspacing="0" width="100%">
                        <tbody>
                            <tr class="headerRow"><td></td><td>Package Name</td><td>Type</td><td>Cost</td></tr>
                            <tr class="gps_package" spry:repeat="dsPackage" class="package">
                                <td><a href="javascript:void(0)" onClick="deleteAction(dsPackage.getData()[{ds_RowNumber}],'gps_package',function(){dsPackage.loadData();});"><img src="images/Button-Delete-16x16.png" border="0"></a></td>
                                <td><a href="javascript:void(0)" onClick="editForm('gps_package','Edit Package',dsPackage.getData()[{ds_RowNumber}]);">{dsPackage::package_name}</td><td>({dsPackage::package_type})</td>
                                <td>US ${dsPackage::package_cost}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="calendar.js"></script>
        <script type="text/javascript" src="scripts/prototype/prototype.js"></script>
        <script type="text/javascript" src="scripts/util/Util-Broadcaster.js"></script>
        <script type="text/javascript" src="scripts/util/Util-BrowserDetect.js"></script>
        <script type="text/javascript" src="scripts/util/Util-DateTimeFormat.js"></script>
        <script type="text/javascript" src="scripts/util/Util-PluginDetect.js"></script>
        <script type="text/javascript" src="scripts/util/Util-XmlConverter.js"></script>
        <script type="text/javascript" src="scripts/device/GarminObjectGenerator.js"></script>
        <script type="text/javascript" src="scripts/device/GarminPluginUtils.js"></script>
        <script type="text/javascript" src="scripts/device/GarminDevice.js"></script>
        <script type="text/javascript" src="scripts/device/GarminDevicePlugin.js"></script>
        <script type="text/javascript" src="scripts/device/GarminDeviceControl.js"></script>
        <script type="text/javascript" src="scripts/ContentDelivery.js"></script>
        <script src="scripts/garmin-gma.js"></script>
        <script>
            jQuery(document).ready(function(){
//               gmaLoad(); // call gma load! 
            });
            
        </script>
    </body>
</html>
