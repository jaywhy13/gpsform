<?php
include_once ("../include/dbal.php");
$dbal = new DbAbstractionLayer ( );
header("Content-type: text/xml");

$filter_status = "unfixed,fixed,repeat";

if(!empty($_GET['filter_err_type'])) $filter_err_type = $_GET["filter_err_type"];
if(!empty($_GET['filter_status'])) $filter_status = $_GET["filter_status"];



$gps_errors = array();
if($dbal->connect()){
	$sql = "select ge.email_addr,ge.err_id as id, ge.fid, ge.description as error_description, et.name as err_type_name, gp.name as parish, ga.name as area, status, repeat_err_id , ge.err_type as err_type_id ";
	$sql .= "from gps_err ge, gps_err_type et, gps_parish gp, gps_area ga " .
	" where ge.err_type = et.id AND ge.parish_id = gp.parish_id AND ge.area_id = ga.area_id ";
	if(isset($filter_err_type)){
		
		$filter_err_type_pieces = explode(",",$filter_err_type);
		
		for($i = 0; $i < count($filter_err_type_pieces); $i++){
			$filter_err_type_pieces[$i] = "'" . $filter_err_type_pieces[$i] . "'";
		}

        $filter_status_arr = explode(",",$filter_status);
        for($i = 0; $i < sizeof($filter_status_arr); $i++){
            $filter_status_arr[$i] = "'" . $filter_status_arr[$i] . "'";
        }
        $filter_status = join(",",$filter_status_arr);


		$sql .= " AND err_type IN ("  . join(",",$filter_err_type_pieces) . ") AND status IN  ( $filter_status )";
		
	}
	
//	echo "$sql";

    $sql .= " ORDER BY status DESC, err_id DESC";
	
	$result = $dbal->queryDb($sql);
	if($result){
		while($row = $dbal->loopResult($result)){
			$id =  $row["id"];
			$message = htmlentities($row["error_description"]);
			$err_type_name = $row["err_type_name"];
			$parish = $row["parish"];
			$area = htmlentities($row["area"]);
			$status = $row["status"];
			$repeat = $row["repeat_err_id"];
			$err_type_id = $row["err_type_id"];
			$email_addr = $row["email_addr"];
			
			$params = array();
			$params [] =  "<gps_err>";
			$params [] = "<id>$id</id>";
			$params [] = "<message>$message</message>";
			$params [] = "<err_type>$err_type_name</err_type>";
			$params [] = "<parish>$parish</parish>";
			$params [] = "<area>$area</area>";	
			$params [] = "<status>$status</status>";
			$params [] = "<repeat_err_id>$repeat</repeat_err_id>";
			$params [] = "<err_type_id>$err_type_id</err_type_id>";
			$params [] = "<email_addr>$email_addr</email_addr>";
			$params [] = "</gps_err>";
			$gps_errors [] = join("",$params);
		}
	}
	
}

echo "<gps_err_list> " . join(",",$gps_errors) . "</gps_err_list>";

?>

