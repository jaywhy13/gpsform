//var dsArea = new Spry.Data.XMLDataSet("scripts/area.php?read", "areas/area");
var custUrl = "crudmachine.php?action=read&rel=gps_customer&p=x";
var dsCustomer = new Spry.Data.XMLDataSet(
custUrl,
"gps_customers/gps_customer", {
	useCache :false,
	sortOnLoad : [ "cust_lname", "cust_fname" ]
});
var dsOrder = new Spry.Data.XMLDataSet(
"crudmachine.php?action=read&rel=gps_order&p=x",
"gps_orders/gps_order", {
	useCache :false,
	sortOnLoad :"order_id"
});
var dsPackage = new Spry.Data.XMLDataSet(
"crudmachine.php?action=read&rel=gps_package&p=x",
"gps_packages/gps_package", {
	useCache :false,
	sortOnLoad : [ "package_type", "package_name" ]
});


var dsCustObs = new Object();
dsCustObs.onPostLoad = function(){
	var data = dsCustomer.getData();
	if(!data) return;
	for(var i = 0; i < data.length; i++){
		checkDelinquency(dsCustomer.getData()[i]["cust_id"]);
		//		window.setTimeout("checkDelinquency(dsCustomer.getData()[" + i + "][\"cust_id\"])",200);
	}

	var filterNode = jQuery("#filterCust").get()[0];
	if(filterNode.disabled)  filterNode.disabled = false;
};

dsCustomer.addObserver(dsCustObs);
