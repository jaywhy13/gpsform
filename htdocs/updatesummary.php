<?php
require_once ("../include/kiaapi.php");
include_once("../include/permguru.php");


$kia = new Kia();
$pg = new PermGuru("gpsform");

function get_ordinal($num){
	if($num == 0){
		return "Initial";
	} else {

		return ordinalSuffix($num);
	}
}

function ordinalSuffix($number, $ss=0)
{

	/*** check for 11, 12, 13 ***/
	if ($number % 100 > 10 && $number %100 < 14)
	{
		$os = 'th';
	}
	/*** check if number is zero ***/
	elseif($number == 0)
	{
		$os = '';
	}
	else
	{
		/*** get the last digit ***/
		$last = substr($number, -1, 1);

		switch($last)
		{
			case "1":
				$os = 'st';
				break;

			case "2":
				$os = 'nd';
				break;

			case "3":
				$os = 'rd';
				break;

			default:
				$os = 'th';
		}
	}

	/*** add super script ***/
	$os = $ss==0 ? $os : '<sup>'.$os.'</sup>';

	/*** return ***/
	return $number.$os;
}

if(!$pg->isLoggedIn() && 1==2){
	?>
	<html>
		<head><title>Unauthorized</title></head>
		<body>
		No unauthorized access is allowed on this page. Please click <a href="admin.php">here</a> to login.
		</body>
	</html>
	<?
	die;
}
?>
<html>
<head>
	<title>Customer Update Summary</title>
	<style type="text/css">
	.table,tr,td {
		font-family:Verdana;
		font-size:10px;
	}
	
	.odd td, .even td {
		border-style:solid;
		border-width:1px;
		border-bottom-style:none;
		border-left-style:none;
		border-right-width:1px;
		
	}
	
	.header td {
		font-weight:bold;
		background-color:rgb(40,40,70);
		color:white;
	}
	.odd td {
		background-color:rgb(220,220,230);
	}
		
	.even td {
		background-color:white;
	}
	
	.totals td {
		text-align:center;
	}
	</style>
</head>
<body>
<table border="0" cellpadding="5" cellspacing="0">
<?
// Get the maximum number of updates sent out
$sql = "select max(total) from (select product_serial,count(product_serial) as total from gps_customer_update group by product_serial) as subq";
$res = $kia->runSQL($sql);
while($row = $kia->loopResult($res)){
	$total = $row["max"];
}

$columnTotals = array();
$ultimate_total = 0;

echo "<tr class='header'><td colspan='2'>Customer</td><td>Unit</td><td>Serial</td>";
for($i = 0; $i < $total; $i++){
	echo "<td>" . get_ordinal($i) . " Load</td>";
	$columnTotals[$i] = 0;
}

echo "<td><b>Total</b></td>";

echo "</tr>";

$sql = "SELECT * FROM gps_product, gps_customer, gps_order where gps_order.order_id = gps_product.order_id and gps_customer.cust_id = gps_order.cust_id ORDER BY cust_lname, cust_fname, product_serial";
$res = $kia->runSQL($sql);
$row_count = 1;
while($row = $kia->loopResult($res)) {
	$cust_lname = $row["cust_lname"];
	$cust_fname = $row["cust_fname"];
	if(!$cust_fname) $cust_fname = "&nbsp;";
	$serial = $row["product_serial"];
	$prod_name = "";

	$sql = "SELECT * FROM gps_product, garmin_unit where garmin_unit.unit_id = gps_product.unit_id and product_serial = '$serial'";
	$garminRes = $kia->runSQL($sql);
	while($garminRow = $kia->loopResult($garminRes)){
		$prod_name = $garminRow["unit_name"];
	}

	$class_name = $row_count % 2 == 0 ? "even" : "odd";
	$align_type = "left";

	if(trim($prod_name) == "") {
		$align_type = "center";
		$prod_name = " - ";
	}

	echo "<tr class='$class_name'><td>$cust_lname</td><td>$cust_fname</td><td align='$align_type'>$prod_name</td><td>$serial</td>";

	$sql = "SELECT * FROM gps_customer_update WHERE product_serial = '$serial' order by dispatch_date";
	$verRes = $kia->runSQL($sql);
	$versions = 0;
	$p = 0;
	while($verRow = $kia->loopResult($verRes)){
		$version_no = $verRow["version_no"];
		$dispatch_date = $verRow["dispatch_date"];
		echo "<td>$version_no ($dispatch_date)</td>";
		$versions++;
		$columnTotals[$p]++;
		$p++;

	}

	if($versions < $total){
		for($j = 0; $j < $total - $versions; $j++){
			echo "<td align='center'>-</td>";
		}
	}

	echo "<td>$versions</td>";

	echo "</tr>";
	$row_count++;
}


$class_name = $row_count % 2 == 0 ? "even" : "odd";

echo "<tr class='$class_name totals'><td colspan='2'></td><td></td><td></td>";

for($i = 0; $i < $total; $i++){
	echo "<td>" . $columnTotals[$i] . "</td>";
	$ultimate_total += $columnTotals[$i];
}

echo "<td style='border-style:solid; border-width:2px;'>$ultimate_total</td></tr>";

?>
</table>
<?php
echo "<br>$row_count rows";
?>
 <script src="js/jquery-1.5.1.min.js"></script>
</body>
</html>
