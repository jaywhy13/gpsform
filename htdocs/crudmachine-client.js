var formIncrement = 0;
var cm_alias = {};
var keyCache = {};
var allowScribbling = false;


String.prototype.reverse = function() {
    splitext = this.split("");
    revertext = splitext.reverse();
    reversed = revertext.join("");
    return reversed;
};

/**
* Adds an alias for a database column. Alias can be later retireved with
* getAlias.
*
* @param {String}
*            key the key
*
* @param {String}
*            value the value
*
*/
function addAlias(key, value) {
    cm_alias[key] = value;
}

/**
* Retrieves the alias for a column name
*
* @param {String}
*            key the key
*
* @return {String} value the value
*/
function getAlias(key) {
    if (cm_alias[key]) {
        return cm_alias[key];
    } else {
        return key;
    }
}

// function cm_callback(action,relation,message){ } // Callback signature

/**
* Builds an edit form dialog for user to edit data for a database table
*
* @param {String}
*            tbl name of the database table
*
* @param {String}
*            caption title to be used to caption the form
*
* @param {Object}
*            row associative array supplied to the function. This will be used
*            as the local data and placed in the form.
*
* @param {bool}
*            readOnlyKeys shows the keys for the table as disabled objects
*
* @param {bool}
*            onlyForeignKeys decides whether only foreign keys should be
*            retrieved. This is useful for when editing a form that has foreign
*            key constraints but has a serial for the primary key. Perfect
*            example would be the gps_order_payment table. Foreign keys are:
*            order id, customer id and package id, but the payment id is a
*            serial. It needs not be sent up to the server as the db will
*            generate an id.
*
*/
function editForm(tbl, caption, row, readOnlyKeys, onlyForeignKeys) {
    if (!readOnlyKeys)
        var readOnlyKeys = false;
    if (!onlyForeignKeys)
        var onlyForeignKeys = false;

    createForm(tbl, caption, true, row, readOnlyKeys, onlyForeignKeys);
}

/**
* Builds a create form for a table and captions it with caption
*
* @param {String}
*            tbl name of the database table
*
* @param {String}
*            caption title to be used to caption the form
*
* @param {Object}
*            row associative array supplied to the function. This will be used
*            as the local data and placed in the form.
*
* @param {bool}
*            readOnlyKeys shows the keys for the table as disabled objects
*
* @param {bool}
*            onlyForeignKeys decides whether only foreign keys should be
*            retrieved. This is useful for when editing a form that has foreign
*            key constraints but has a serial for the primary key. Perfect
*            example would be the gps_order_payment table. Foreign keys are:
*            order id, customer id and package id, but the payment id is a
*            serial. It needs not be sent up to the server as the db will
*            generate an id.
*
*/
function createForm(tbl, caption, editMode, row, readOnlyKeys, onlyForeignKeys,usr_callback) {
    if (!readOnlyKeys)
        var readOnlyKeys = false;
    if (!onlyForeignKeys)
        var onlyForeignKeys = false;

    var msg;
    if (editMode) {
        msg = "Edit Form: ";
    } else {
        msg = "Create Form: ";
    }

    msg += "[ Table: " + tbl + ", Caption: " + caption + ", Data: "
    + describe(row, true) + " ]";

    scribble(msg);

    var listUrl = "crudmachine.php?action=list&rel=" + tbl;
    // Construct url to retrieve list of columns for relation: tbl ...
    jQuery
    .get(listUrl, null, ( function(data) {
        return function(data) {
            // Analyze data received... list of columns
            // e.g.
            // ({"message":{"type":"notification","content":"order_id,cust_id,package_id,order_item_quantity,order_item_total"}})
            var obj = eval(data);
            var componentList = new Array();
            if (obj.message) {
                var message = obj.message.content;
                var columnList = message.split(",");
                // list of the ids to be used for the inputs

                // Start constructing the table
                var tblId = "tbl_" + tbl + "_" + formIncrement;
                var tblStr = "<table id='" + tblId + "'><tbody id='tbody_" + tbl + "_"
                + formIncrement + "'>";

                // Add rows for each column
                for ( var i = 0; i < columnList.length; i++) {
                    var column = columnList[i];
                    var inpId = column + "_" + formIncrement;
                    componentList.push(inpId);
                    tblStr += "<tr><td valign='top' id='" + column + "_caption_"
                    + formIncrement + "' valign='top'>" + getAlias(column)
                    + "</td><td id='" + column + "_container_"
                    + formIncrement
                    + "'><input type='text' id='" + inpId
                    + "' onFocus='styleFocus(this);' onBlur='styleBlur(this);'/></td></tr>";
                }

                // Store the ids for the cancel and create/save button
                var cancelButtonId = "jis_cancel_" + formIncrement;
                var createButtonId = "jis_create_" + formIncrement;
                var divId = "jis_frm_" + formIncrement;

                var buttonCaption = "Create";
                if (editMode)
                    buttonCaption = "Save";

                // Close the table
                tblStr += "<tr><td><input type='button' id='"
                + createButtonId
                + "' value='"
                + buttonCaption
                + "'></td><td><input type='button' value='Close' id='"
                + cancelButtonId + "'/></td></tr>";
                tblStr += "</tbody></table>";

                var handleId = "jis_handle_" + formIncrement;
                // Make a div for it... and add the table in it
                jQuery(
                    "<div id='" + divId + "' class='inputForm'><a name='" + divId + "'></a><div class='caption' id='" + handleId + "'>"
                    + caption + "</div><img src='delete.png' style='position:absolute; right:2px; top:2px; cursor:pointer;' onClick=killForm('" + divId + "'); title='Close form'/><br>" + tblStr + "</div>")
                .appendTo("body");

                // Save some properties on the button
                var btn = jQuery(("#" + createButtonId)).get()[0];
                if (editMode) { // add update functionality for save
                    hookEditAction(btn, tbl, divId);
                } else { // add create functionality for create
                    hookCreateAction(btn, tbl, divId);
                }

                // Setup the cancel button
                btn = jQuery("#" + cancelButtonId).get()[0];
                btn.container = divId;
                btn.onclick = function() { // add functionality to
                    // close
                    // the form
                    killForm(this.container);
                };

                // Setup the draggable part of things
                makeDraggable(divId,handleId);

                // By now... the form is blank with all the fields that
                // are
                // REQUIRED... all non-needed keys are not placed on the
                // form

                if (editMode) { // do special stuff for edit mode
                    if (row) {
                        var args = getInputAsArgs(divId);

                        var fi = formIncrement;
                        getKeysFromRow(
                            tbl,
                            row,
                            ( function() {
                                return function(args) {
                                    for ( var item in args) {
                                        var val = args[item];
                                        var caption = getAlias(item);
                                        var hiddenCompId = item
                                        + "_" + fi;
                                        // alert(hiddenCompId + "
                                        // with
                                        // value: " + val);
                                        if (typeof val == 'undefined') {
                                            // This means that the
                                            // local info
                                            // (row)
                                            // did not have all the
                                            // keys
                                            /*
										* alert("Could not find
										* a value for: " +
										* caption + ". Saving
										* data from this form
										* may corrupt your
										* data!");
										*/
                                            continue;
                                        }

                                        if (readOnlyKeys) {
                                            var tbodyId = "tbody_"
                                            + tbl + "_"
                                            + fi;

                                            discardComponentRowInForm(hiddenCompId); // remove
                                            // the
                                            // row
                                            // from
                                            // the
                                            // row

                                            jQuery(
                                                "<tr><td>"
                                                + caption
                                                + "</td><td><input type='text' disabled='true' id='"
                                                + hiddenCompId
                                                + "' value='"
                                                + val
                                                + "'></td></tr>")
                                            .prependTo(
                                                "#"
                                                + tbodyId);
                                        } else {

                                            jQuery(
                                                "<input type='hidden' id='"
                                                + hiddenCompId
                                                + "' value='"
                                                + val
                                                + "'>")
                                            .appendTo(
                                                "#"
                                                + divId);

                                        }

                                    }

                                    // Loop through the rows and set
                                    // back the darta
                                    for ( var item in row) {
                                        var inpId = item + "_" + fi;
                                        var inp = jQuery(
                                            "#" + inpId).get()[0];
                                        if (inp) {
                                            inp.value = row[item];
                                        }
                                    }
                                    cm_callback( {
                                        "action" :"editform",
                                        "relation" :tbl,
                                        "components" :componentList,
                                        "formIncrement" :fi,
                                        "createButtonId" :createButtonId,
                                        "cancelButtonId" :cancelButtonId,
                                        "formId" :divId
                                    });
								
                                    window.location.hash = "#" + divId; // focus the window

                                };
                            })(tbl, row, divId, fi, readOnlyKeys),
                            onlyForeignKeys, componentList,
                            createButtonId, cancelButtonId);
                    }
                } else {
                    // Normal create mode .. NOT edit
                    var func_data = {
                        "action" :"createform",
                        "relation" :tbl,
                        "components" :componentList,
                        "formIncrement" :formIncrement,
                        "createButtonId" :createButtonId,
                        "cancelButtonId" :cancelButtonId,
                        "formId" :divId
                    };

                    
                    
                    if(usr_callback){
                        usr_callback(func_data);
                    } else {
                        cm_callback( func_data );
                    }
                }
            } // end if(obj.message)
            formIncrement++;
        };
    })(caption, tbl, editMode, row, readOnlyKeys,usr_callback));
}

/**
* It takes all the INPUT and SELECT fields and returns their data in an assoc
* array. It also checks to see if the fields are valid. Returns null if there are errors
*
* @param {String}
*            containerId id of the component holding all the input and select
*            fields
*
* @return Array
*/
function getInputAsArgs(containerId,silent) {
    var args = {};
    if(!silent) var silent = false;

    var errorsExperienced = false;

    var checkComponent = function(nodeId) {
        var messages = new Array();
        var node = document.getElementById(nodeId);
        if (node) {
            if (node.cc_valid)
                return node.cc_valid(node,silent);
            else
                return true;
        } else {
            return true;
        }
    };

    jQuery("#" + containerId + " INPUT").each( function() {
        var id = this.id;
        if (id.match(/_[0-9]+$/)) {
            id = id.replace(/_[0-9]+$/, '');
        }

        if (checkComponent(this.id))
            args[id] = process(id, this.value);
        else
            errorsExperienced = true;
    });

    jQuery("#" + containerId + " SELECT").each( function() {
        var id = this.id;
        if (id.match(/_[0-9]+$/)) {
            id = id.replace(/_[0-9]+$/, '');
        }

        if (checkComponent(this.id))
            args[id] = process(id, this.options[this.selectedIndex].value);
        else
            errorsExperienced = true;

    });
	
    jQuery("#" + containerId + " TEXTAREA").each( function() {
        var id = this.id;
        if (id.match(/_[0-9]+$/)) {
            id = id.replace(/_[0-9]+$/, '');
        }

        if (checkComponent(this.id))
            args[id] = process(id, this.value);
        else
            errorsExperienced = true;
    });
	

    if (errorsExperienced)
        return null;

    return args;
}

/**
* Performs a creation action, based on input in container containerId. It grabs
* all the input from input and select boxes within component "containerId" and
* constructs data as input to table "tbl". PS: Callback is deprecated. This
* function uses cm_callback for notifications.
*
* @param {String}
*            containerId id of the container
*
* @param {String}
*            tbl name of the database relation
*
*
*/
function createAction(containerId, tbl, callback) {
    var args = getInputAsArgs(containerId);
    if (!args)
        return;

    jQuery.get("crudmachine.php?action=create&rel=" + tbl, args, ( function(
        data) {
        return function(data) {
            var obj = eval(data);
            if (obj.message.content)
                alert(obj.message.content);
            if (obj.message.type == "notification") {
                resetForm(containerId);
                killForm(containerId);
                var obj2 = {};
                obj2["action"] = "create";
                obj2["relation"] = tbl;
                obj2["message"] = obj.message;
                obj2["params"] = args;
                obj2["data"] = data;
                if (cm_callback)
                    cm_callback(obj2);
                if (callback){
                    callback(obj2);
                }
            }

        };
    })(containerId, tbl, callback, args));
}

/**
* Performs an update action based on data in container "containerId".
*
* @param {String}
*            containerId the id of the container
*
* @param {String}
*            tbl the name of the database table
*/
function updateAction(containerId, tbl) {
    var args = getInputAsArgs(containerId);
    var msg = "Update Action: [ Table: " + tbl + ", Container: " + containerId
    + ", Data: " + describe(args, true) + " ] ";
    scribble(msg);
    if (!args)
        return;
	
    var url = "crudmachine.php?action=update&rel=" + tbl;
    scribble("Update Action URL: " + url + describe(args,true));
    jQuery.get(url, args, ( function() {
        return function(data) {
            var obj = eval(data);
            if (obj.message) {
                alert(obj.message.content);
                cm_callback( {
                    "action" :"update",
                    "relation" :tbl,
                    "message" :obj.message,
                    "params" :args,
                    "data" :data
                });
            }

            if (obj.message.type == "notification")
                killForm(containerId);
        };
    })(containerId, tbl, args));
}

/**
* Performs a delete action based on data in container containerId
*
* @param {String}
*            containerId the id of the container
*
* @param {String}
*            tbl the name of the database table
*/
function deleteAction(row, tbl, callback) {
    if (!confirm("Are you sure you want to delete this record?"))
        return;

    var msg = "Delete Action: [ Table: " + tbl + ", Data: "
    + describe(row, true) + " ] ";

    scribble(msg);

    getKeysFromRow(tbl, row, ( function() {
        return function(args) {
            var msg = "Delete Args: [ Data: "
            + describe(args, true) + " ] ";

            scribble(msg);
            var url = "crudmachine.php?action=delete&rel=" + tbl;
            jQuery.get(url, args, ( function() {
                return function(data, status) {
                    var obj;

                    try {
                        obj = eval(data);
                    } catch (e) {
                        alert("Fatal error parsing data recevied from server. "
                            + data);
                        return;
                    }

                    if (obj.message.content) {
                        if (obj.message.type == "notification") {
                            alert(obj.message.content);
                            if (cm_callback)
                                cm_callback( {
                                    "action" :"delete",
                                    "relation" :tbl,
                                    "params" : args
                                });
                            if (callback)
                                callback();
                        } else {
                            alert(obj.message.content);
                        }
                    }
                // obj = null;
                // callback = null;
                };
            })(callback,args));

        };
    })(tbl, callback));
}

function readAction(row, tbl, callback, options) {
    if (!options)
        var options = {};
    var url = "crudmachine.php?action=read&rel=" + tbl;
    if (options["viewname"])
        url += "&vn=" + options["viewname"];
    jQuery.get(url, row, ( function() {
        return function(data) {
            var obj = eval(data);

            if (callback)
                callback( {
                    "dataobj" :obj,
                    "relation" :tbl,
                    "action" :"read"
                });
            else {
                cm_callback( {
                    "dataobj" :obj,
                    "relation" :tbl,
                    "action" :"read"
                });

            }

        };
    })(row, tbl, callback));
}

function produceTr(row, useStrings,widths) {
    if (!useStrings)
        var useStrings = false;
    if (useStrings) {
        var str = "<tr>";
        for ( var item in row) {
            var widthStr = "";
            if(widths && widths[item]) {
                widthStr = widths[item];
            }
            str += "<td " + widthStr + ">" + row[item] + "</td>";
        }
        str += "</tr>";
        return str;
    } else {
        var tr = document.createElement("tr");
        var td;
        for ( var item in row) {
            td = document.createElement("td");
            if(widths){
                if(widths[item]) td.setAttribute("width",widths[item]);
            }
            td.innerHTML = row[item];
            tr.appendChild(td);
        }

        return tr;
    }
}

/**
* Takes an associative array (row of data), hits the server and calls the
* callback with only the keys
*
* @param {String}
*            tbl name of the relation
*
* @param {Object}
*            userrow associative array
*
* @param {function}
*            callback callback function that is called with the keys-only
*            associative array
*
* @param {boolean}
*            onlyForeignKeys specifies that only foreign keys should be
*            retrieved
*/
function getKeysFromRow(tbl, userrow, callback, onlyForeignKeys) {
    if (!onlyForeignKeys)
        var onlyForeignKeys = false;

    if (!callback)
        return;

    // Check the key cache
    /*
	if (keyCache[tbl]) {
	var cachedObj = keyCache[tbl];
	var msg = "Cached  Object: [ Data: " + describe(cachedObj,true) + " ]";
	scribble(msg);
	msg = 	  "UserRow Object: [ Data: " + describe(userrow,true) + " ]";
	scribble(msg);


	for ( var item in userrow) {
	if (cachedObj[item]) {
	cachedObj[item] = userrow[item];
	}
	keyCache[tbl] = cachedObj;
	}

	var msg = "Updated Cached Object: [ Data: " + describe(cachedObj,true) + " ]";
	scribble(msg);

	callback(keyCache[tbl]);
	return;
	}*/

    var action = "listkeys";
    if (onlyForeignKeys)
        action = "listkeys";

    jQuery.get("crudmachine.php?action=" + action + "&rel=" + tbl, null,
        ( function() {
            return function(data) {

                var obj = eval(data);
                if (obj.message.content) {
                    var cols = obj.message.content.split(",");
                    var keys = {};
                    for ( var i = 0; i < cols.length; i++) {
                        keys[cols[i]] = userrow[cols[i]];
                    }
                    keyCache[tbl] = keys;
                    callback(keys);
                    return;
                }
                callback(null);
            };
        })(tbl, userrow, callback));
}

/**
* Given the name of a relation, it retireves the list of keys from the server
* and calls the callback.
*
* @param {String}
*            tbl the name of the relation in the database
*
* @param {function}
*            callback the callback function. It should have signature function (
*            assoc_array )
*/
function getKeysFromServer(tbl, callback) {
    if (!callback)
        return;

    /*
	if (keyCache[tbl]) {
	callback(keyCache[tbl]);
	return;
	}*/


    jQuery.get("crudmachine.php?action=listkey&rel=tbl", null, ( function() {
        var obj = eval(data);
        if (obj.message.content) {
            var cols = obj.message.content.split(",");
            keyCache[tbl] = cols;
            callback(cols);
            return;
        }
        callback(null);
    })(tbl, callback));
}

/**
* Tells whether a component id holds data for a column name "field"
*
* @param {String}
*            id the id of the component
*
* @param {String}
*            field the name of the column
*/
function isComponentForColumn(id, field) {
    return id.match("^" + field + "_[0-9]+");
}

/**
* Grabs the form increment from the id
*
* @param {String}
*            id the id of the component
* @return {int} the form increment
*/
function grabFormIncrement(id) {
    return id.reverse().substring(0, id.reverse().indexOf("_"));
}

/**
* Gets the column name from the id ... essentially strips the "_[0-9]+" part of
* the id
*
* @param {String}
*            id the id of the component
*/
function grabColumnName(id) {
    if (id.match(/_[0-9]+$/))
        return id.replace(/_[0-9]+/, '');
    return null;
}

var spryDsData = {};
var spryDsCount = 0;
var useSpryCaching = false;

/**
* This function aims to replace ids with an alias column when the alias is in a
* different table. <br>
* So, for example if a form shows "package_id" for the gps_order table, but you
* want to see the package name. This function may be used to achieve this. The
* best place to utilize this function is in the callback. I.e. after the data
* is loaded, then we change out the key fields to what we want thme to be.
* Essentially, input fields are replaced with select fields. <br>
* <br>
* What it does is that it creates a spry dataset and sets the url, it creates a
* spry region with the select then it calls load data. If there is a callback
* function is places that callback function in an update region observer.
*
*
* @param {String}
*            componentId id of the component
*
* @param {String}
*            tbl name of the relation in the database
*
* @param {String}
*            expression this expression is passed directly to spry so it can be
*            parsed.
*
* @param {String}
*            idCol id of the column in the table that should be used as the
*            value field for the select component
*
* @param {String}
*            groupBy name of the column for the results to be grouped by. They
*            will be grouped into optgroups.
*
*/
function doSpryAliasing(componentId, tbl, expression, idCol, groupBy, callback) {
    var node = jQuery("#" + componentId).get()[0];
    if (node) {
        var dsNameWithoutIndex = "ds_" + tbl;
        var dsName = dsNameWithoutIndex + "_" + spryDsCount;
        var dsUrl = "crudmachine.php?p=x&action=read&rel=" + tbl;
        try {
            eval(dsName);
        } catch (m) {
            if (spryDsData[dsNameWithoutIndex]) {
                alert("Loading from local data");
                var arr = spryDsData[dsNameWithoutIndex];

                eval(dsName + " = new Spry.Data.DataSet(arr);");
            // eval(dsName + ".loadData();");
            } else {
                eval(dsName + " = new Spry.Data.XMLDataSet('" + dsUrl + "','"
                    + tbl + "s/" + tbl + "',{useCache:false});");
            }

        }

        var parent = node.parentNode;
        parent.removeChild(node);
        var regionId = "rgn_" + componentId;
        var strHTML = "<div id='" + regionId + "' spry:region='" + dsName
        + "'>";
        strHTML += "<select id='" + componentId + "'><option spry:repeat='"
        + dsName + "' value='{" + idCol + "}'>" + expression
        + "</option></select>";
        strHTML += "</div>";
        parent.innerHTML = strHTML;

        // If there is a callback ... add an observer
        if (callback) {
            var obs = {};
            obs.onPostUpdate = ( function() {
                return function(notifier, data) {
                    var obj = {};
                    obj["regionId"] = regionId;
                    obj["observer"] = obs;
                    obj["dsName"] = dsName;
                    obj["componentId"] = componentId;
                    if (!spryDsData[dsNameWithoutIndex] && useSpryCaching) {
                        var rowData = eval(dsName + ".getData()");
                        spryDsData[dsNameWithoutIndex] = rowData;
                    }

                    callback(obj);
                    Spry.Data.Region.removeObserver(this);
                // removeSpryStuff(document.getElementById(regionId));
                // alert("on post update called " + regionId);
                };
            })
            (callback, regionId, obs, dsName, dsNameWithoutIndex,
                componentId);
        }
        // alert("Calling udpate for region: " + regionId);
        Spry.Data.Region.addObserver(regionId, obs);
        // alert("obs added to " + regionId);
        // Spry.Data.initRegions(jQuery("#" + regionId).get()[0]);
        var rgn = document.getElementById(regionId);
        rgn.setAttribute("spry:region", dsName);
        rgn["spry:region"] = dsName;
        var tmpStr = "";

        Spry.Data.initRegions(document.getElementById(regionId));

    // Spry.Data.updateRegion(regionId);
    // alert("init regions called for " + regionId);
    }

    spryDsCount++;
}

function doSpryDsAliasing(componentId, tbl, expression, idCol, groupBy,
    callback, formatf, orderBy) {
    var node = jQuery("#" + componentId).get()[0];
    var disableComp = node.disabled;
    var previousValue = null;
    if (node.value != "" || node.value != null) {
        previousValue = node.value;
    }

    if (node) {
        var dsNameWithoutIndex = "ds_" + tbl;
        var dsName = dsNameWithoutIndex + "_" + spryDsCount;
        var dsUrl = "crudmachine.php?p=x&action=read&rel=" + tbl;
        if(orderBy){
            dsUrl += "&ob=" + orderBy.join(",");
        //			alert(dsUrl);
        }
        try {
            eval(dsName);
        } catch (m) {
            eval(dsName + " = new Spry.Data.XMLDataSet('" + dsUrl + "','" + tbl
                + "s/" + tbl + "',{useCache:false});");
        }

        // Get rid of the old node if no formatf function was supplied
        if (!formatf) {
            var parent = node.parentNode;
            parent.removeChild(node);

            var regionId = "rgn_" + componentId;
            var strHTML = "<div id='" + regionId + "'>";
            strHTML += "<select id='" + componentId + "'></select>";
            strHTML += "</div>";
            parent.innerHTML = strHTML;
        }

        // Add an observer to the dataset we just created or just made a link to
        var obs = {};
        obs.onPostLoad = ( function() {
            return function(notifier, data) {
                var obj = {};
                obj["regionId"] = regionId;
                obj["observer"] = obs;
                obj["dsName"] = dsName;
                obj["componentId"] = componentId;

                var ds = eval(dsName);
                if (groupBy)
                    ds.sort(groupBy);
                var arr = ds.getData();

                if (formatf) {
                    formatf(obj, arr); // send object with the region id and
                    // other data and the array from the
                    // dataset
                    return;
                }

                var selectNode = jQuery("#" + componentId).get()[0];
                selectNode.disabled = disableComp;

                // Sort the dataset if there's a groupby

                var optgroups = {};
                for ( var i = 0; i < arr.length; i++) {
                    var row = arr[i];
                    var id = row[idCol];
                    var expression_template = expression;
                    var value = replaceSpryVars(row, expression);

                    var option = document.createElement("option");
                    option.setAttribute("value", id);
                    var txt = document.createTextNode(value);
                    option.appendChild(txt);

                    if (groupBy) {
                        var group = row[groupBy[0]];
                        var optgroup = null;
                        if (!optgroups[group]) {
                            optgroup = document.createElement("optgroup");
                            optgroup.setAttribute("label", group);
                            optgroups[group] = optgroup;
                            selectNode.appendChild(optgroup);
                        }

                        optgroup = optgroups[group];

                        optgroup.appendChild(option);
                    } else {
                        selectNode.appendChild(option);
                    }

                    if (previousValue == id) {
                        selectNode.selectedIndex = i;
                    }

                }

                // Call the call back
                if (callback)
                    callback(obj);
                Spry.Data.Region.removeObserver(this);
            };
        })
        (callback, regionId, obs, dsName, dsNameWithoutIndex,
            componentId, groupBy, expression, idCol, disableComp,
            previousValue, formatf);

        var ds = eval(dsName);
        ds.addObserver(obs);
        ds.loadData();
    }
    spryDsCount++;
}

/**
* Take a spry expression and an associative array and it will remove the {vars}
* and return the actual values
*
* @param row
*            associate array with all the values
* @param expression
*            the spry expression
* @return
*/
function replaceSpryVars(row, expression) {
    var tmp_expression = expression;
    var regexp = new RegExp(/\{[A-Za-z0-9_]+\}/g);

    while (matches = regexp.exec(tmp_expression)) {
        var match = matches[0];
        var keyword = match.substring(1, match.length - 1);
        if (row[keyword]) {
            tmp_expression = tmp_expression.replace(match, row[keyword]);
        } else {
            tmp_expression = tmp_expression.replace(match, "");
        }
        regexp = new RegExp(/\{[A-Za-z0-9_]+\}/g);
    }

    return tmp_expression;
}

function removeSpryStuff(node) {
    if (node) {
        for ( var attrib in node.attributes)
            alert(attrib);

        node.removeAttribute("spry:region");
    }
}

/**
* Disposes of a form
*
* @param {String}
*            containerId the id of the container to be disposed
*/
function killForm(containerId) {
    jQuery("#" + containerId).fadeOut("fast");
}

/**
* Resets all data in the form
*
* @param {String}
*            containerId the id of the container to be disposed
*/
function resetForm(containerId) {

    jQuery("#" + containerId + " INPUT").each( function() {
        if (this.type != "button")
            this.value = "";
    });
}

/**
* Makes any container HTML object editable ...
*
* @param node
* @param defaultVal
* @param eventf
*            object array with event functions.... onblur
* @return
*/
function makeEditableOnClick(node, defaultVal, eventf) {
    node.val = "";

    node.mode = "display";
    if (eventf["onblur"]) {
        node.eonblur = eventf["onblur"];
    }

    node.title = "Click to change";

    node.setValue = function(val) {

        if (!val)
            return;
        this.innerHTML = val;
        this.val = val;

        if (this.eonblur) {
            this.eonblur();
        }
    };

    node.getValue = function() {
        return this.val;
    };

    node.onclick = function() {
        var newVal = prompt("Enter the new value", this.val);
        if (newVal)
            this.setValue(newVal);
    };

    node.val = defaultVal;
    node.innerHTML = defaultVal;
}

/**
* Changes the display property to "none" for the row that the component is in.
* Also changes the type of input to be hidden
*
* @param componentId
*            id of the component whose row is to be hidden
* @return
*/
function hideRowInForm(componentId) {
    if (document.getElementById(componentId)) {
        var component = document.getElementById(componentId);
        // component.type = "hidden";
        var node = component;
        while (node.nodeName.toLowerCase() != "tr") {
            node = node.parentNode;
        }
        node.style.display = "none";
    }
}

function unhideRowInForm(componentId) {
    if (document.getElementById(componentId)) {
        var component = document.getElementById(componentId);
        var node = component;
        while (node.nodeName.toLowerCase() != "tr") {
            node = node.parentNode;
        }
        node.style.display = "";
    }
}

/**
* Discards the row that contains the component... removes it from the DOM. The
* function finds the parent tr node then removes it from the tbody
*
* @param componentId
*            the id of the component to be removed
* @return
*/
function discardComponentRowInForm(componentId) {
    if (document.getElementById(componentId)) {
        var component = document.getElementById(componentId);
        var node = component;
        while (node.nodeName.toLowerCase() != "tr") {
            node = node.parentNode;
        }
        node.parentNode.removeChild(node);
    }
}


function addComponentRowInForm(relation,fi,component,addFirst) {
    var tbodyId = "tbody_" + relation + "_" + fi;
    if(document.getElementById(tbodyId)){
        var tbody = document.getElementById(tbodyId);
        var tr = document.createElement("tr");

        var captionTd = document.createElement("td");
        captionTd.id = component + "_caption_" + fi;
        captionTd.innerHTML = getAlias(component);

        var componentTd = document.createElement("td");
        componentTd.id = component + "_container_" + fi;

        var componentNode = document.createElement("input");
        componentNode.id = component + "_" + fi;

        // connect every one now
        tr.appendChild(captionTd);
        tr.appendChild(componentTd);
        componentTd.appendChild(componentNode);

        if(addFirst){
            tbody.insertBefore(tr,tbody.firstChild);
        } else {
            tbody.appendChild(tr);
        }


    }
}

function describe(obj, returnStr) {
    if (!obj) {
        if (returnStr)
            return "null obj";

        alert("Cannot describe a null object");
        return;
    }
    var str = "";
    for ( var prop in obj) {
        str += prop + " = " + obj[prop] + ",\n";
    }

    if (returnStr)
        return str;
    alert(str);
}

function scribble(msg) {
    if(!allowScribbling) return;
    var cc_scribbler_div;
    if (!document.getElementById("cc_scribbler")) {
        cc_scribbler_div = document.createElement("div");
        cc_scribbler_div.id = "cc_scribbler";
        var init = "<input type='button' onClick=jQuery('#cc_scribbler').hide('slow'); value='Close'/>&nbsp;&nbsp;<input value='Clear' type='button' onClick='this.parentNode.innerHTML = this.parentNode.init;'/><br>";
        cc_scribbler_div.innerHTML = init;
        cc_scribbler_div.init = init;
        document.body.appendChild(cc_scribbler_div);
        cc_scribbler_div.style.width = 600;
        cc_scribbler_div.style.height = 250;
        cc_scribbler_div.style.overflow = "auto";
        cc_scribbler_div.style.borderStyle = "solid";
        cc_scribbler_div.style.borderWidth = 1;
        cc_scribbler_div.style.position = "absolute";
        cc_scribbler_div.style.backgroundColor = "white";
        cc_scribbler_div.style.padding = 10;
        cc_scribbler_div.style.left = 50;
        cc_scribbler_div.style.top = 50;
        makeDraggable("cc_scribbler");
    }

    cc_scribbler_div = document.getElementById("cc_scribbler");
    jQuery("#cc_scribbler").show("slow");

    var node = document.createTextNode(msg);
    var space = document.createElement("br");

    cc_scribbler_div.appendChild(node);
    cc_scribbler_div.appendChild(space);

// window.setTimeout("jQuery('#cc_scribbler').hide('slow');", 5500);
}

function hookCreateAction(node, tbl, containerId) {
    node.container = containerId;
    node.tbl = tbl;
    node.onclick = function() {
        // this.disabled = true;
        // this.value = "Please wait ...";
        createAction(this.container, this.tbl);
    };
}

function hookEditAction(node, tbl, containerId) {

    node.container = containerId;
    node.tbl = tbl;
    node.onclick = function() {
        // this.disabled = true;
        // this.value = "Please wait ...";
        updateAction(this.container, this.tbl);
    };
}

function chainFunction(node, event, f) {
    if (event == "onclick") {
        var orig = node.onclick;
        if (!orig)
            node.onclick = f;
        else {
            node.onclick = ( function() {
                return function() {
                    if (f())
                        orig();
                };
            })(orig, f);
        }
    }
}

function getTrailingNumber(id) {
    return id.reverse().substring(0, id.reverse().indexOf("_")).reverse();
}

/**
* Changes the caption for a component in a form
*
* @param containerId
*            the id of the component whose caption needs changing
* @param caption
*            the new html string to be used as the caption
*/
function changeComponentCaption(containerId, caption) {
    var fi = getTrailingNumber(containerId);
    var columnName = grabColumnName(containerId);
    var captionId = columnName + "_caption_" + fi;
    if (document.getElementById(captionId)) {
        document.getElementById(captionId).innerHTML = caption;
    } else {
        alert("cannot find " + captionId + ", passed " + containerId);
    }
}

function isArray(obj) {
    if (obj.constructor.toString().indexOf("Array") == -1)
        return false;
    else
        return true;
}

function changeNodeToSelect(nodeId, options, delim) {
    if (!delim)
        var delim = ":";
    var node = jQuery("#" + nodeId).get()[0];
    if (!node)
        return;

    var selectNode = document.createElement("select");
    var parent = node.parentNode;

    var previousValue = null;
    if (node.value != "" && node.value != null)
        previousValue = node.value;

    var index = -1;

    if (isArray(options)) {
        for ( var i = 0; i < options.length; i++) {
            var optionStr = options[i];
            var val = optionStr.split(delim)[0];
            var caption = optionStr.split(delim)[1];
            var option = document.createElement("option");
            option.value = val;
            option.appendChild(document.createTextNode(caption));
            selectNode.appendChild(option);
            if (previousValue != null && previousValue == val)
                index = i;
        }
    } else { // treat it like an object
        for ( var item in options) {
            var val = item;
            var caption = options[item];
            var option = document.createElement("option");
            option.value = val;
            option.appendChild(document.createTextNode(caption));
            selectNode.appendChild(option);
            if (previousValue != null && previousValue == val)
                index = i;
        }
    }

    if (index > 0)
        selectNode.selectedIndex = index;

    parent.removeChild(node);
    selectNode.id = nodeId;
    parent.appendChild(selectNode);
}

/**
* Makes a component draggabe
*
* @param containerId
*            id of the container
*/
function makeDraggable(containerId,handleId) {
    if(!handleId){
        jQuery("#" + containerId).draggable( {
            "zIndex" :1000,
            "opacity" :0.9
        });
    }
    else {
        jQuery("#" + containerId).draggable( {
            "zIndex" :1000,
            "opacity" :0.9,
            "handle":"#" + handleId
        });

    }

}

/**
* Adds functionality to the form to set fields as required
*
* @param componentList
*            array listing the ids of the components
* @param requiredList
*            json object with values being strings either
*            ("notnull","isnumber") or function
*/
function stipulateRequirements(componentList, requiredList) {

    for ( var i = 0; i < componentList.length; i++) {
        var component = componentList[i];
        if (requiredList[grabColumnName(component)]) {
            var rule = requiredList[grabColumnName(component)];
            if (typeof rule == "string") {
                if (document.getElementById(component)) {
                    var node = document.getElementById(component);
                    if (rule == "notnull") {
                        node.cc_valid = cc_notnull;
                    } else if (rule == "isnumber") {
                        node.cc_valid = cc_isnumber;
                    } else if (rule == "isnumbercheckzero") {
                        node.cc_valid = cc_isnumbercheckzero;
                    } else if (rule == "isnumbergtzero"){
                        node.cc_valid = cc_numbergtzero;
                    }

                }
            } else if (typeof rule == "function") {
                if (document.getElementById(component)) {
                    var node = document.getElementById(component);
                    node.cc_valid = rule;
                }
            }
        }
    }
}

function cc_notnull(node, silent) {
    if (!silent)
        var silent = false;
    var val = getComponentValue(node, true);
    if (val == "" || val == null) {
        if (!silent)
            alert(getAlias(grabColumnName(node.id)) + " cannot be blank!");
        return false;
    }
    return true;
}

function cc_isnumber(node, silent) {
    if (!silent)
        var silent = false;

    if (!cc_notnull(node, false)) {
        return false;
    }

    var val = getComponentValue(node, true);
    if (!isNaN(val)) {
        return true;
    }

    alert(getAlias(grabColumnName(node.id)) + " must be numeric");
    return false;
}


function cc_isnumbercheckzero(node, silent) {
    if (!silent)
        var silent = false;

    if (!cc_notnull(node, false)) {
        return false;
    }

    var val = getComponentValue(node, true);
    if (!isNaN(val)) {
        return true;
    }

    alert(getAlias(grabColumnName(node.id)) + " must be numeric");
    return false;
}

function cc_numbergtzero(node,silent){
    if(!silent) var silent = false;

    if(!cc_notnull(node,false)){
        return false;
    }

    var val = getComponentValue(node, true);
    if(!isNaN(val)){
        return val > 0;
    }

    alert(grabAlias(grabColumnName(node.id)) + " must be greater than zero");
    return false;
    
}



function getComponentValue(obj, proc) {
    var val = obj.type == "select" ? obj.options[obj.selectedIndex].value
    : obj.value;

    if (proc) {
        var item = grabColumnName(obj.id);
        val = process(item, val);
    }

    return val;
}

var globalProcessors = {  };

function addGlobalProcessor(item, f) {
    if (typeof f == "function") {
        globalProcessors[item] = f;
    } else {
        alert("Processor rejected! " + f);
    }
}

function process(item, value) {
    if (globalProcessors[item]) {
        var f = globalProcessors[item];
        return f(value);
    } else {
        return value;
    }
}

function styleFocus(inp){
	
}

function styleBlur(inp){
	
}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

/** 
 * Creates an object that encapsulates the data from crudmachine php script. It has the following methods:<br>
 * isNotification() - true if the message is a notification<br>
 * isError() - true if the message is an error<br>
 * getObject() - returns the actual object (evaled)<br>
 * hasMessage() - returns true if the obj has a "message" property
 * getMessage() - returns the content of the message if it has a message property. Null other wise
 *
 */
function makeCMCO(data){
    if(data){
        var obj = eval(data);
        var mo = {};
        mo.obj  = obj;
        mo.isError = function(){
            return this.obj.message.type == "error";
        };
		
        mo.isNotification = function(){
            return this.obj.message.type == "notification";
        };
		
        mo.getObject = function(){
            return this.obj;
        }
		
        mo.hasMessage = function(){
            if(this.obj.message) return true;
            return false;
        };
		
        mo.getMessage = function(){
            if(this.hasMessage()) return this.obj.message.content;
            return null;
        }
					
        return mo;
		
    }
    return null;
}


function disableComponent(id){
    jQuery("#" + id).get()[0].disabled = true;
    jQuery("#" + id).css("-moz-opacity",0.6);
}

function enableComponent(id){
    jQuery("#" + id).get()[0].disabled = false;
    jQuery("#" + id).css("-moz-opacity",1.0);
}

