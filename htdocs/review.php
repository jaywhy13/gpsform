<html>
<head>
    <title>Error Review Console</title>
    <!-- <script src="jquery-1.2.6.min.js"></script> -->
    <script src="jquery-latest.js"></script>
    <script src="jquery-ui-latest.js"></script>
    <script src="spry/xpath.js" language="javascript"></script>
    <script src="spry/SpryAutoSuggest.js" language="javascript"></script>
    <script src="spry/SpryData.js" language="javascript"></script>
    <link href="spry/SpryAutoSuggest.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        BODY {
            font-size:10px;
            font-family:Century Gothic;
        }

        TD {
            font-size:10px;
        }

        #trHeaderRow {
            background-color:black;
            font-weight:bold;
            font-size:10px;
            color:rgb(230,240,255);
        }

        #tblErrList {
            margin-right:10px;
            padding:10px;
        }

        #scribblePane	 {
            position:absolute;
            right:0px;
            background-color:white;
            width:200px;
            height:500px;
        }


        .trOddRow TD {
            background-color:rgb(230,240,255);

        }

        .trHover TD {
            background-color:rgb(230,255,220);
            border-top-style:solid;
            border-bottom-style:solid;
            border-top-width:1px;
            border-bottom-width:1px;
        }


        #divControlPanel {
            background-color:white;
            border-color:black;
            border-style:solid;
            border-width:1px;
            padding:5px;
            width:190px;
            height:260px;
            overflow:auto;
        }

        #inpQuery {
            background-image:url('icons/search3-small.png');
            padding:5px;
            background-position:right center;
            background-repeat:no-repeat;
            background-color:#eeeeff;
            font-weight:bold;
        }


        #errStatusDialog {
            position:fixed;
            width:500px;
            top:100px;
            left:150px;
            background-color:rgb(240,240,240);
            border-style:solid;
            border-width:1px;
            border-color:#eeeeff;
            display:none;
            padding-left:10px;
            padding-right:10px;
        }

        errStatusDialog .error_listing {
            overflow:auto;
        }

        .error {
            padding:5px;
            background-color:white;
            font-size:11px;
        }

        .error_tr td {
            background-color:white;
            margin-bottom:5px;
        }

        .error_tr td.changed {
            background-color:rgb(245,255,245);
        }

        .error_tr td.working,  .error_tr td.working *, td.working {
            background-color:#eeeeee;
            color:#bbbbbb;
        }


        td.working IMG {
            -moz-opacity:0.5;
        }





        #errStatusDialog .dialogTitle {
            font-size:12px;
            font-weight:bold;
            padding:10px;
            background-color:black;
            color:white;
            margin-top:10px;
            margin-bottom:10px;
            cursor:pointer;
        }

        #errStatusDialog .error_listing {
            height:280px;
            overflow:auto;
        }


        .error_action {
            display:none;
        }

        .error_action img {
            margin-right:2px;
        }

        .changed .error_action {
            font-size:10px;
            font-weight:bold;
            display:inline-block;
            padding:5px;
            margin:2px;
            background-color:rgb(255,255,230);
            border-style:solid;
            border-width:1px;
            border-color:white;
        }

        .error_version {
            display:inline-block;
        }



        .err_img {
            width:48px;
            padding:5px;
            cursor:pointer;
            border-style:solid;
            border-width:1px;
            border-color:gray;
            margin:5px;
            float:right;
        }

        .alert_message, .alert_error {
            position:absolute;
            right:20px;
            display:none;
            background-color:black;
            color:rgb(102,255,51);
            font-weight:bold;
            font-size:10px;
            width:150px;
            height:30px;
            padding:5px;
            border-style:solid;
            border-width:1px;
            border-color:#eeeeee;
        }

        .alert_error {
            background-color:rgb(153,0,0);
            color:white;
        }

        .controls {
            padding-top:5px;
            padding-bottom:5px;
        }

        /**
        * === repeat div ===
        */
        .repeat {

        }

        .repeat input {
            width:99%;
            border-style:solid;
            border-width:1px;
            border-color:black;
            padding:5px;
        }

        .repeat .results {
            height:200px;
            border-style:solid;
            border-width:1px;
            border-color:rgb(230,230,240);
            margin-top:5px;
            margin-bottom:5px;
            display:none;
            background-color:white;
            overflow:auto;
        }

        .repeat .results TD {
            font-size:11px;
            font-family:Verdana,Arial,Tahoma;
        }

        .result_tr td {
            cursor:pointer;
            border-style:solid;
            border-width:1px;
            border-color:#eeeeee;
            border-left:none;
            border-right:none;
        }

        tr.hover td {
            background-color:rgb(230,240,230);
        }


        span.repeat_note {
            font-family:Verdana,Arial;
            font-size:10px;
            font-weight:bold;
            display:inline-block;
            padding:3px;
            color:rgb(200,200,250);
        }


        .disabled {
            -moz-opacity:0.3;
            cursor:pointer;
        }


    </style>
    <script src="jquery-1.2.6.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var urlErrListBase = "gps_err.php?";
        var dsErrList = new Spry.Data.XMLDataSet(urlErrListBase,"gps_err_list/gps_err",{useCache:false});
        dsErrList.setColumnType("err_id", "number");

        var dsErrTypeList = new Spry.Data.XMLDataSet("gps_err_type.php","gps_err_type_list/gps_err_type");

        var statusSelected = new Array();


        function refreshErrorListing(){
            statusSelected = new Array();
            jQuery(".err_type_checkbox:checked").each(function(){
                var status = this.id.substring(5,this.id.length);
                statusSelected.push(status);
            });

            var selectedTypes = new Array();
            $(".error_filter").each(function(){
                var name = this.name.substring(7,this.name.length);
                if(this.checked) selectedTypes.push(name);
            });


            dsErrList.url = (constructUrl(urlErrListBase,{"filter_err_type":(selectedTypes.join(",")),"filter_status":statusSelected.join(",")}));
            dsErrList.loadData();
        }


        /**
         * Clears the errors selected
         */
        function clearErrorSelections(){
            $(".cb_err_selector").each(function(){
                this.checked = false;
            });

            errorSelectionChanged(null);
        }


        /**
         * Constructs a url based on a base url and params
         */
        function constructUrl(baseUrl,params) {
            var url = "";

            var paramArr = new Array();
            if(baseUrl) url = baseUrl;

            if(url.charAt(url.length-1) == "?") prefix = ""; else prefix = "&";

            for(var param in params){
                paramArr.push(param + "=" + params[param]);
            }
            return url + prefix + paramArr.join("&");
        }


        function scribble(msg){
            document.getElementById("scribblePanel").appendChild(document.createTextNode(msg));
            document.getElementById("scribblePanel").appendChild(document.createElement("br"));
        }


        // List of the errors selected
        var cbIdsSelected = {};

        /**
         * If passed null, updates the list of selections. If passed a valid checkbox obj it checks to see if
         * it is checked. If yes, it adds it to the list of checked items. If not, removes it from the listing
         */
        function errorSelectionChanged(cbObj){
            if(!cbObj){
                for(var cbId in cbIdsSelected){
                    var cbDomObj = document.getElementById(cbId);
                    if(!cbDomObj){
                        delete cbIdsSelected[cbId];
                    } else {
                        if(!cbDomObj.checked){
                            delete cbIdsSelected[cbId];
                        }
                    }
                }

            } else if(cbObj.checked){
                cbIdsSelected[cbObj.id] = "checked";
            } else {
                if(cbIdsSelected[cbObj.id])
                {
                    delete cbIdsSelected[cbObj.id];
                }
            }

            updateErrorSelectionList();
            updateErrorOperationList();
        }

        /**
         * Returns the err id given the checkbox id
         */
        function cbIdToErrId(cbId){
            var tmp = cbId.split("_");
            return tmp[tmp.length-1];
        }

        /**
         * Returns an array of selected checkbox ids
         */
        function getErrorsSelected(){
            var errArr = new Array();
            for(var cbId in cbIdsSelected){
                if(cbIdsSelected[cbId] == "checked"){
                    errArr.push(cbId);
                } else {
                    delete cbIdsSelected[cbId];
                }
            }
            return errArr;
        }

        /*
         * Returns the number of errors selected
         */
        function getNumErrorsSelected(){
            return getErrorsSelected().length;
        }

        /**
         * Changes the selection of an error
         */
        function changeErrorSelection(errId,selected){
            var cbId = "cb_" + errId;
            var cbDomObj = document.getElementById(cbId);
            cbDomObj.checked = false;
            errorSelectionChanged(cbDomObj);
        }


        function deselectError(errId){
            changeErrorSelection(errId,false);
        }

        function selectError(errId){
            changeErrorSelection(errId,true);
        }


        /**
         * Updates the list of errors shown with the minus sign to remove the error from the lsiting
         */
        function updateErrorSelectionList(){
            var spanDomObj = document.getElementById("spanErrorsSelected");
            spanDomObj.innerHTML = "";
            var i = 0;
            for(var cbId in cbIdsSelected){
                i++;
                var errId = cbId.substring(3,cbId.length);
                var aTag = document.createElement("a");
                aTag.appendChild(document.createTextNode("Error # " + errId));
                aTag.href = "review.php#err_" + errId;
                try {
                    aTag.title = document.getElementById("tdErrMsg_" + errId).innerHTML  == "" ? "" : document.getElementById("tdErrMsg_" + errId).innerHTML;
                } catch (e) { }
                aTag.errId = errId;

                var img = document.createElement("img");
                img.src = "icons/remove.png";
                img.width = 16;
                img.height = 16;
                img.style.cursor = "pointer";
                img.errId = errId;
                img.title = "Click to remove the selection of Error # " + errId;
                img.onclick = function(){
                    changeErrorSelection(this.errId,false);
                };
                spanDomObj.appendChild(img);

                spanDomObj.appendChild(document.createTextNode(" "));

                spanDomObj.appendChild(aTag);
                spanDomObj.appendChild(document.createElement("br"));
            }

            if(i == 0){
                spanDomObj.innerHTML = "None";
            }
        }

        /**
         * Updates the list of operations that may be performed on the selected errors
         */
        function updateErrorOperationList(){
            var spanDomObj = document.getElementById("spanErrorOperations");
            spanDomObj.innerHTML = "";

            if(getNumErrorsSelected() > 0){
                var tbl = document.createElement("table");
                var tbody = document.createElement("tbody");
                tbl.appendChild(tbody);


                // Delete Errors
                var tr = document.createElement("tr");
                tbody.appendChild(tr);

                var td = document.createElement("td");
                tr.appendChild(td);
                var img = document.createElement("img");
                img.src = "icons/cancel.png";
                img.width = 24;
                td.appendChild(img);

                td = document.createElement("td");
                td.innerHTML = "Delete";
                td.onclick = function(){
                    deleteSelectedErrors();
                };
                td.style.cursor = "pointer";
                tr.appendChild(td);


                // Change error statuts
                tr = document.createElement("tr");
                tbody.appendChild(tr);

                td = document.createElement("td");
                tr.appendChild(td);
                img = document.createElement("img");
                img.src = "icons/refresh.png";
                img.width = 24;
                td.appendChild(img);


                td = document.createElement("td");
                td.onclick = function(){
                    launchErrorStatusDialog(); // launches the error status dialog
                };
                tr.appendChild(td);
                td.style.cursor = "pointer";
                td.innerHTML = "Change Status";



                spanDomObj.appendChild(tbl);
            }
        }

        /**
         * Performs a selection of a row
         */
        function selectCheckboxForRow(trId){
            var errId = trId.substring(9,trId.length);
            var cbId = "cb_" + errId;
            var cbDomObj = document.getElementById(cbId);
            if(cbDomObj.checked){
                cbDomObj.checked = false;
            } else {
                cbDomObj.checked = true;
            }
            errorSelectionChanged(cbDomObj);
        }

        function highlight(errId){

        }

        function unhighlight(errId){

        }

        /**
         * Deletes the selected errors
         */
        function deleteSelectedErrors(){
            if(getNumErrorsSelected() == 0) return;
            var doDelete = confirm("Are you sure you want to delete " + getNumErrorsSelected() + " errors?");
            if(doDelete){
                var url = "alter_error.php?action=delete&err_id=" + getErrorsSelected().join(",").replace(/cb_/g,"");
                //		alert(url);
                jQuery.get(url,function(data){
                    if(data.indexOf("success") > -1){
                        clearErrorSelections();
                        reloadDataSets();
                        alert(data.split(":")[1]);
                    } else if(data.indexOf("error:") > -1){
                        alert(data.split(":")[1]);
                    }
                });
            }
        }

        var divErrStatusDialog = null;
        var divErrStatusDialogId = "errStatusDialog";
        var idConsideredForRepeat = -1;


        /**
         * Initializes the error status dialog
         */
        function initErrorStatusDialog(){
            if(divErrStatusDialog == null){
                divErrStatusDialog = document.createElement("div");
                divErrStatusDialog.id = divErrStatusDialogId;
                document.body.appendChild(divErrStatusDialog);
                jQuery("#" + divErrStatusDialogId).show("slow");

                var divDialogTitle = document.createElement("div");
                divDialogTitle.className = "dialogTitle";
                divDialogTitle.innerHTML = "Error Status";

                divErrStatusDialog.appendChild(divDialogTitle);

                var errorList = document.createElement("div");
                errorList.className = "error_listing";
                divErrStatusDialog.appendChild(errorList);


                // Add the repeat search div
                var divRepeat = document.createElement("div");
                divRepeat.className = "repeat";

                var spanCaption = document.createElement("span");
                spanCaption.className = "title";

                var inpFilter = document.createElement("input");
                inpFilter.className = "filter";
                inpFilter.id = "filter";
                inpFilter.onkeyup = scheduleRepeatSearch;
                inpFilter.onkeydown = cancelRepeatSearch;

                var divResults = document.createElement("div");
                divResults.className = "results";

                divRepeat.appendChild(spanCaption);
                divRepeat.appendChild(inpFilter);
                divRepeat.appendChild(divResults);
                divErrStatusDialog.appendChild(divRepeat);


                // Add the controls bar
                var controls = document.createElement("div");
                controls.className = "controls";

                var inp = document.createElement("input");
                inp.setAttribute("type","button");
                inp.value = "Refresh";
                inp.onclick = launchErrorStatusDialog;
                controls.appendChild(inp);

                inp = document.createElement("input");
                inp.setAttribute("type","button");
                inp.value = "Close";
                inp.onclick = function(){
                    jQuery("#" + divErrStatusDialogId).hide("fast");
                };
                controls.appendChild(inp);

                divErrStatusDialog.appendChild(controls);

                //                jQuery("#" + divErrStatusDialogId).draggable();

            }


        }




        /**
         * Assoc array used by the launchErrorStatusDialog function to store the status of the different errors
         */
        var errIdStatus = {};

        /**
         * Creates status object for storage in the errIdStatus
         */
        function createErrIdStatusObj(errId){
            return {
                "id":errId,
                "status":"unfixed",
                "syncState":"stale",
                "changed":false

            };
        }

        function getErrorImage(errId){
            var img = new Image();
            img.src =  "";
            var imgData = new Array("unfixed:icons/error.png","fixed:icons/accept.png","repeat:icons/repeat.png");
            img.currentIndex = 0;
            img.originalIndex = 0;
            img.src = "icons/error.png";
            img.errStatus = "unfixed";
            img.imgData = imgData;
            img.setData = function(index,skipIdSet){
                if(!skipIdSet){
                    var skipIdSet = false;
                }
                var dataStr = this.imgData[index];
                var imgSrc = dataStr.split(":")[1];
                var errStatus = dataStr.split(":")[0];
                this.src = imgSrc;
                this.errStatus = errStatus;
                this.currentIndex = index;

                if(this.errIdSet && !skipIdSet){
                    if(this.currentIndex != this.originalIndex || this.originalIndex == 2)
                        $("#esd_description_" + this.errId).addClass("changed");
                    else
                        $("#esd_description_" + this.errId).removeClass("changed");

                    // Update the error status object
                    var errStatusObj = errIdStatus[errId];
                    errStatusObj.status = this.errStatus;
                    errStatusObj.changed = $("#esd_description_" + this.errId).hasClass("changed");
                    errIdStatus[errId] = errStatusObj;
                    errorStatusToggled(errId);

                }
            };

            img.errIdSet = false;
            if(errId){
                img.errId = errId;
                img.errIdSet = true;
            }
            img.toggleImage = function(){
                var index = this.currentIndex;
                index++;

                if(index == this.imgData.length) index = 0;
                this.currentIndex = index;
                this.setData(index);

            };

            img.onclick = function(){
                this.toggleImage();
            };

            img.id = "err_img_" + errId;
            img.className = "err_img";

            var imgObj = $("#err_row_img_" + errId).get()[0];
            if(imgObj){
                var imgSrc = imgObj.src;
                img.src = imgSrc;
                for(var i = 0; i < imgData.length; i++){
                    var imgDataSrc = imgData[i].split(":")[1];
                    if(imgSrc.indexOf(imgDataSrc) > -1){
                        img.originalIndex = i;
                        img.setData(i,true);
                        break;
                    }
                }
            }
            return img;
        }


        /**
         * Launches the error status dialog allowing the user to change the status of errors
         */
        function launchErrorStatusDialog(){
            initErrorStatusDialog();

            jQuery("#" + divErrStatusDialogId).show("slow");


            var divErrorListing = jQuery("#" + divErrStatusDialogId + " .error_listing").get()[0];
            divErrorListing.innerHTML = "";

            var tbl, tbody;

            tbl = document.createElement("table");
            tbody = document.createElement("tbody");
            tbody.id = "error_tbody";

            divErrorListing.appendChild(tbl);
            tbl.appendChild(tbody);
            tbl.setAttribute("cellspacing",0);
            tbl.setAttribute("width","98%");

            // Clear out the errIdStatus obj
            errIdStatus = {};

            for(var cbId in cbIdsSelected){
                var errId = cbIdToErrId(cbId);
                addErrorRow(tbody,errId); // add an error row to this
                errIdStatus[errId] = createErrIdStatusObj(errId);
            }



        }

        function removeErrorRow(errId){
            var tbody = jQuery("#error_tbody").get()[0];
            if(!tbody) return;
            var tr = jQuery("#error_tr_" + errId).get()[0];
            if(tr){
                tbody.removeChild(tr);
            }

            var count = jQuery("#error_tbody .error_tr").get().length;
            if(count == 0){
                jQuery("#" + divErrStatusDialogId).hide("fast");
            }
        }


        function addErrorRow(tbody,errId){
            var divErrorListing = jQuery("#" + divErrStatusDialogId + " .error_listing").get()[0];

            var description = jQuery("#tdErrMsg_" + errId).get()[0].innerHTML;

            var tr = document.createElement("tr");
            tr.className = "error_tr";
            tr.id = "error_tr_" + errId;
            var td = document.createElement("td");
            td.setAttribute("valign","top");

            td.appendChild(getErrorImage(errId));
            tr.appendChild(td);

            td = document.createElement("td");
            td.id = "esd_description_" + errId;
            td.errId = errId;
            td.status = "unfixed";
            td.className = "error";
            //            td.appendChild(document.createTextNode(description));
            td.innerHTML = "<b>Error #" + errId + ":</b><br>" + description;
            td.setAttribute("valign","top");
            tr.appendChild(td);

            var divActions = document.createElement("div");
            divActions.className = "error_actions";
            var updateSpan = addErrorAction("icons/save-flat.png","Update",function(){
                updateError(errId);
            });
            divActions.appendChild(updateSpan); // add save icon
            td.appendChild(divActions);


            tbody.appendChild(tr);

        }

        var errId = 450000;

        function addErrorAction(imgUrl,caption,f){
            var span = document.createElement("span");
            span.className = "error_action";
            var img = document.createElement("img");
            img.src = imgUrl;
            img.setAttribute("height",16);
            span.appendChild(img);
            span.appendChild(document.createTextNode(caption));
            span.style.cursor = "pointer";
            if(f){
                span.onclick = f;
                span.onclickf = f;
            }

            span.disable = function(){
                jQuery(this).addClass("disabled");
                this.onclick = null;
            };


            span.enable = function(){
                this.onclick = this.onclickf;
                jQuery(this).removeClass("disabled");
            };

            return span;
        }


        function reloadDataSets(){
            window.setTimeout("dsErrList.loadData()",500);
        }

        var f = "boo";


        function toggleComponentEnabled(jQueryStr,enabled){
            var nodes = $(jQueryStr).get();
            for(var i in nodes){
                var node = nodes[i];
                if(enabled){
                    if(node.enable){
                        node.enable();
                        node.disabled = false;
                    }
                } else {
                    if(node.disable){
                        node.disable();
                        node.disabled = true;
                    }
                }
            }
        }


        /**
         * Enables all the error actions
         */
        function enableErrorActions(errId){
            toggleComponentEnabled("#esd_description_" + errId + " .error_actions SPAN",true);
            toggleComponentEnabled("#esd_description_" + errId + " .error_actions SELECT",true);
        }

        function disableErrorActions(errId){
            toggleComponentEnabled("#esd_description_" + errId + " .error_actions SPAN",false);
            toggleComponentEnabled("#esd_description_" + errId + " .error_actions SELECT",false);

        }


        function updateError(errId){
            disableErrorActions(errId);
            if(!errId) return;
            var errStatusObj = errIdStatus[errId];
            if(!errStatusObj){
                doError("Could not find information on error # " + errId);
                return;
            }

            var status = errStatusObj.status;
            var repeat = errStatusObj.repeat;
            var version = errStatusObj.version;


            // Add all the extra parameters
            var extraParams = "";
            if(status == "repeat"){ // check if it is a repeat node ...
                if(!repeat){
                    alert("Please select which error #" + errId + " repeats");
                    enableErrorActions(errId);
                    return;
                } else {
                    extraParams = "&repeat_id=" + repeat;
                }
            }


            // Check for version ... exit if no version found
            if(status == "repeat" || status == "fixed"){
                if(version == null || version == ""){
                    alert("Please indicate the version number within which the error was fixed.");
                    enableErrorActions(errId);
                    return;
                }

                extraParams += "&version=" + version;
            }

            $("#esd_description_" + errId).addClass("working");
            $($("#err_img_" + errId).get()[0].parentNode).addClass("working");
            $.get("alter_error.php?action=update&status=" + status + "&err_id=" + errId + extraParams,(function(){
                return function(data){
                    var obj = parseServerResponse(data);
                    doAlert(obj.message);
                    if(!obj.isError()){
                        deselectError(errId);
                        reloadDataSets();
                        window.setTimeout("removeErrorRow(" + errId + ");",1050);
                    }
                };
            })(errId));

        }

        /**
         * The server mostly responds now in a delimited fashion. This function splits the response and gives an object that has a status, message  property and an isError function.
         *
         */
        function parseServerResponse(data){
            var parts = data.split(":");
            return {
                "isError":function(){ return parts[0] == "error"},
                "status":parts[0],
                "message":parts[1]
            };
        }


        /**
         * Messaging Functionality
         *
         */

        var messageCount = 0;
        var messageIndex = 0;
        var messageSlots = new Array();
        for(i = 0; i < 100; i++){
            messageSlots[i] = "slot" + i;
        }


        function getSlot(){
            messageSlots.sort().reverse();
            var slotStr = messageSlots.pop();
            return slotStr.substring(4,slotStr.length);
        }

        function addSlot(i){
            messageSlots.push("slot" + i);
        }

        function doAlert(message){
            var div = document.createElement("div");
            var id = "message" + messageIndex;
            div.id = id;
            div.innerHTML = message;
            div.className = "alert_message";
            document.body.appendChild(div);
            var slotNumber = getSlot();
            div.style.top = slotNumber * 50 + 50;
            messageCount++;
            messageIndex++;
            showMessage(id)
        }

        function doError(message){
            var div = document.createElement("div");
            var id = "message" + messageIndex;
            div.id = id;
            div.innerHTML = message;
            div.className = "alert_error";
            document.body.appendChild(div);
            var slotNumber = getSlot();
            div.style.top = slotNumber * 50 + 50;
            messageCount++;
            messageIndex++;
            showMessage(id);
        }

        function hideMessage(id){
            jQuery("#" + id).hide("slow",(function(){
                return function(){
                    var slotNumber = id.substring(7,id.length);
                    addSlot(slotNumber);
                    document.body.removeChild(document.getElementById(id));
                };
            })(id));
        }

        function showMessage(id){
            jQuery("#" + id).show("slow",(function(){
                return function(){
                    window.setTimeout("hideMessage('" + id + "')",3000);
                };
            })(id))
        }


        /**
         * Searching for repeat errors
         *
         */
        function focusRepeatSearchArea(){
            $("#filter").get()[0].focus();
            $("#filter").get()[0].select();

        }


        function showRepeatSearchArea(){
            $(".repeat .results").show("slow");
        }

        function hideRepeatSearchArea(){
            $(".repeat .results").hide("slow");
        }

        function activateSearchArea(){
            showRepeatSearchArea();
            focusRepeatSearchArea();
        }

        function deactivateSearchArea(){
            hideRepeatSearchArea();
        }


        var repeatSearchTimer = null;
        var repeatSearchQuery = "";

        function scheduleRepeatSearch(){
            repeatSearchQuery = getSearchQuery();
            repeatSearchTimer = window.setTimeout("performRepeatSearch()",500);
        }

        function getSearchQuery(){
            return $("#filter").get()[0].value;
        }

        function queryValid(){
            return repeatSearchQuery != null && repeatSearchQuery != "" && (repeatSearchQuery.length > 3 || !isNaN(repeatSearchQuery));
        }

        function cancelRepeatSearch(){
            clearTimeout(repeatSearchTimer);
        }

        function performRepeatSearch(){
            if(queryValid()){
                $("#filter").attr("disabled",true);
                $("#filter").css("background-color","#eeeeee");
                var url = "search_error.php?q=" + repeatSearchQuery + "&err_id=" + idConsideredForRepeat;
                $.get(url,function(data){
                    try {
                        var obj = eval(data);
                        if(!obj.message) return;
                        var message = obj.message.content;
                        printRepeatResults(message);
                        $("#filter").attr("disabled",false);
                        $("#filter").css("background-color","#ffffff");

                    } catch (e){
                        alert("Fatal error fetching data!\n" + e.lineNumber + ": " + e.message + "\n" + data);
                        $("#filter").attr("disabled",false);
                        $("#filter").css("background-color","#ffffff");
                    }
                });
            }
        }

        function printRepeatResults(delimitedStr,delim){
            if(!delim) var delim = "|";

            var divResults = jQuery(".results").get()[0];
            divResults.innerHTML = "";

            if(delimitedStr == "" || delimitedStr == null){
                divResults.innerHTML = "No results";
                return;
            }

            var parts = delimitedStr.split(delim);

            var tbl = document.createElement("table");
            tbl.className = "results_tbl";
            tbl.setAttribute("width","100%");
            tbl.setAttribute("cellpadding",5);
            tbl.setAttribute("cellspacing",0);
            var tbody = document.createElement("tbody");


            tbl.appendChild(tbody);
            divResults.appendChild(tbl);
            tbody.appendChild(getRepeatResultTh());

            for(var i = 0; i < parts.length; i++){
                var part = parts[i];
                var str_parts = part.split("::");
                var id = str_parts[0];
                var status = str_parts[1];
                var description = str_parts[2];
                var extras = str_parts[3];
                if(extras != "" && extras != null){
                    description += "<br><b><u>Location:</u> </b>" + extras;
                }

                tbody.appendChild(getRepeatResultTr(id,status,description));
            }
        }

        function getRepeatResultTr(id,status,description){
            var tr = document.createElement("tr");
            tr.className = "result_tr";

            tr.errId = id;
            tr.onmouseover = function(){
                $(this).addClass("hover");
            };
            tr.onmouseout = function(){
                $(this).removeClass("hover");
            };

            tr.onclick = function(){
                assignRepeatId(this.errId);
            };

            var td = document.createElement("td");
            td.innerHTML = id;
            td.className = "rep_err_id";
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = description;
            td.className = "rep_err_description";
            tr.appendChild(td);


            td = document.createElement("td");
            td.innerHTML = status;
            td.className = "rep_err_status";
            tr.appendChild(td);

            return tr;
        }

        function getRepeatResultTh(){
            var tr = document.createElement("tr");
            tr.className = "header";

            var td = document.createElement("td");
            td.innerHTML = "Id";
            td.className = "rep_err_id";
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = "Description";
            td.className = "rep_err_description";
            tr.appendChild(td);


            td = document.createElement("td");
            td.innerHTML = "Status";
            td.className = "rep_err_status";
            tr.appendChild(td);

            return tr;

        }




        function errorStatusToggled(errId){
            var errStatusObj = errIdStatus[errId];
            var status = errStatusObj.status;

            // Do special changes for repeat errors
            if(status == "repeat"){
                activateSearchArea();
                idConsideredForRepeat = errId;
                $(".repeat .title").get()[0].innerHTML = "Find repeat error for # " + errId;
            } else {
                if(errId == idConsideredForRepeat){
                    deactivateSearchArea();
                }

                // Removes ALL repeat nodes
                var nodes = $("#esd_description_" + errId + " .repeat_note").get();
                for(var i in nodes){
                    var node = nodes[i];
                    node.parentNode.removeChild(node);
                }
                delete errIdStatus[errId].repeat;
            }

            // Take care of version number selection
            if(status == "repeat" || status == "fixed"){
                var span = createErrorVersionSpan(errId);
                var divErrorActions = $("#esd_description_" + errId + " .error_actions").get()[0];
                if(divErrorActions){
                    divErrorActions.appendChild(span);
                    span.update();
                }
            } else {
                removeNodesByFilter("#esd_description_" + errId + " .error_actions SELECT");
            }

        }


        /*
         *  Assigns error id "errId" to idConsideredForRepeat. Creates a span and adds it to the error actions div
         */
        function assignRepeatId(errId){
            // idConsideredForRepeat
            errStatusObj = errIdStatus[idConsideredForRepeat];
            if(!errStatusObj) return;
            errStatusObj.repeat = errId;
            var span = document.createElement("span");
            span.className = "repeat_note";
            span.innerHTML = "Repeats # " + errId;

            // remove all repeat notes
            var nodes = $("#esd_description_" + idConsideredForRepeat + " .repeat_note").get();
            for(var i in nodes){
                var node = nodes[i];
                node.parentNode.removeChild(node);
            }

            // insert the span before error_actions
            var divErrorActions = $("#esd_description_" + idConsideredForRepeat + " .error_actions").get()[0];
            if(divErrorActions){
                divErrorActions.parentNode.insertBefore(span,divErrorActions);
            } else { // just insert if no update actions exist
                $("#esd_description_" + idConsideredForRepeat).get()[0].appendChild(span);
            }
        }

        var errorVersionCount = 0;

        // DEAL WITH THE ERROR VERSIONS CORRECTION INFORMATION
        function createErrorVersionSpan(errId){
            var span = document.createElement("span");
            span.className = "error_version";
            var selId = "region_" + errorVersionCount;
            errorVersionCount++;
            span.id = selId;
            span.update = function(){
                $(this).empty(); // remove ALL components
                var sel = document.createElement("select");

                var option = document.createElement("option");
                option.value = null;
                option.text = "Select version";
                sel.appendChild(option);

                this.appendChild(sel)
                jQuery.ajax({
                    "type":"GET",
                    "url":"gps_data_version.php",
                    "dataType":"xml",
                    "success":(function(id,sel,errId){
                        return function(data){
                            var doc = data;
                            $(doc).find("version").each(function(){
                                var number = $(this).find("number").text();
                                var name = $(this).find("name").text();
                                var releaseDate = $(this).find("release_date").text();

                                var option = document.createElement("option");
                                option.value = number;
                                option.appendChild(document.createTextNode(name + " (" + releaseDate + " )"));
                                sel.onchange = function(){
                                    var val = this.options[this.selectedIndex].value;
                                    assignVersionForError(errId,val);
                                };
                                sel.appendChild(option);

                            });
                        };
                    })(this.id,sel,errId)
                });
            };

            // remove all nodes like this one
            removeNodesByFilter("#esd_description_" + errId + " .error_version");
            return span;
        }

        function assignVersionForError(errId,version){

            if(errId) var errStatusObj = errIdStatus[errId];
            else return;


            if(version){
                if(errStatusObj){
                    errStatusObj["version"] = version;
                    errIdStatus[errId] = errStatusObj;
                }
            } else {
                delete errStatusObj.version;
            }
        }

        function removeNodesByFilter(str){
            var nodes = $(str).get();
            for(var i in nodes){
                var node = nodes[i];
                node.parentNode.removeChild(node);
            }
        }

        function describe(obj, returnStr) {
            if (!obj) {
                if (returnStr)
                    return "null obj";

                alert("Cannot describe a null object");
                return;
            }
            var str = "";
            for ( var prop in obj) {
                str += prop + " = " + obj[prop] + ",\n";
            }

            if (returnStr)
                return str;
            alert(str);
        }




    </script>
</head>
<body>
<a name="top"></a>
<div id="scribblePanel">
</div>
<div>


<div style="position:fixed; top:350px;" id="divControlPanel">
    <b>Control Panel</b><br>
    <a href="review.php#top">To Top</a><br/>
    <a href="review.php#base">To Base</a><br/>
    <span id="spanErrorSummary">

    </span>
    <br>
    <br>
    <b>Errors Selected:</b></br>
    <span id="spanErrorsSelected">None</span>
    <br/>
    <br/>
    <b>Error Operations:</b>
    <br/>
    <span id="spanErrorOperations">

    </span>

</div>


<table width="100px" style="float:left;" cellpadding="5" cellspacing="0">
    <tr>
        <td>
            <b>Search:</b><br/><input type="text" name="query" style="" id="inpQuery">
            <br/>
            <a href="javascript:void(0)" onclick="document.getElementById('inpQuery').value='';">Clear</a>
        </td>
    </tr>

    <tr>
        <td spry:region="dsErrTypeList"><b>Error Types</b><br>
            <form id="frmFilter">
                <span>
                    <table width="98%">
                        <tr spry:repeat="dsErrTypeList">
                            <td>
                                <img width="32px" height="32px" spry:if="'{id}'=='road'" src="icons/road.png" title="Road Error"/>
                                <img width="24px" height="24px" spry:if="'{id}' == 'poi'" src="icons/blueglobe.png" title="Point of Interest Error"/>
                                <img width="24px" height="24px" spry:if="'{id}' == 'routing'" src="icons/compass.png" title="Routing Error"/>
                            </td>
                            <td>
                                <input type="checkbox" name="filter_{id}" class="error_filter" checked="checked" onclick="refreshErrorListing();">
                            </td>
                            <td>
                                {name}
                            </td>
                        </tr>
                    </table>
                </span>
            </form>
        </td>
    </tr>

    <tr>
        <td>
        <b>Error Types</b></td>
    </tr>
    <tr>
        <td>
            <table width="100%">
                <tr><td><img src="icons/error.png" width="24" height="24"/></td><td><input id="cbox_unfixed" onchange="refreshErrorListing();" class="err_type_checkbox" type="checkbox" name="unfixed" checked="checked"/>&nbsp;Unfixed Error</td></tr>
                <tr><td><img src="icons/repeat.png" width="24" height="24"/></td><td><input id="cbox_repeat" onchange="refreshErrorListing();" class="err_type_checkbox" type="checkbox" name="repeated" checked="checked"/>&nbsp;Repeated Error</td></tr>
                <tr><td><img src="icons/accept.png" width="24" height="24"/></td><td><input id="cbox_fixed" onchange="refreshErrorListing();" class="err_type_checkbox" type="checkbox" name="fixed" checked="checked"/>&nbsp;Fixed Error</td></tr>
            </table>
        </td>
    </tr>

</table>


<div spry:region="dsErrList">
<table cellpadding="5" cellspacing="0" id="tblErrList">
<tr id="trHeaderRow"> 
    <th scope="col" align="center"></td>
    <th scope="col" align="center" spry:sort="id">Error #</td>
    <th scope="col" width="45px" align="center">Type</td>
    <th scope="col" spry:sort="parish">Parish</td>
    <th scope="col">Area</td>
    <th scope="col" width="50%">Message</td>
    <th scope="col">Status</td>
</tr>
<tr spry:repeat="dsErrList" spry:odd="trOddRow" spry:hover="trHover" id="trErrRow_{id}" onmouseover="highlight('{id}');" onmouseout="unhighlight('{id}');" onclick="selectCheckboxForRow(this.id);">
    <td>
        <input type="checkbox" id="cb_{id}" class="cb_err_selector" onclick="errorSelectionChanged(this)"/>
        <a name="err_{id}"></a>
    </td>
    <td align="right">{id}</td>
    <td id="trErrRow_{id}" align="center">
        <img width="32px" height="32px" spry:if="'{err_type_id}'=='road'" src="icons/road.png" title="Road Error"/>
        <img width="24px" height="24px" spry:if="'{err_type_id}' == 'poi'" src="icons/blueglobe.png" title="Point of Interest Error"/>
        <img width="24px" height="24px" spry:if="'{err_type_id}' == 'routing'" src="icons/compass.png" title="Routing Error"/>
    </td>

    <td id="tdErrParish_{id}" class="err_parish">{parish}</td>
    <td id="tdErrArea_{id}" classs="err_area">{area}</td>
    <td id="tdErrMsg_{id}" class="err_message">{message}<span spry:if="'{message}'==''">&nbsp;</span> <span spry:if="'{email_addr}'" style="font-weight:bold; color:green;"><br>Submitted by: {email_addr}</span></td>
    <td id="tdErrStatus_{id}">
        <span class="err_status" style="display:none">{status}</span>
        <span spry:if="'{status}'=='unfixed'"><img width="24px" src="icons/error.png" id="err_row_img_{id}" title="Unfixed Error"/> &nbsp;This error has not been fixed.</span>
        <span spry:if="'{status}' == 'repeat'"><img width="30px" src="icons/repeat.png" id="err_row_img_{id}"  title="Repeated Error"/>&nbsp;This error is a repeat of an error that has been fixed (# {repeat_err_id})</span>
        <span spry:if="'{status}' == 'fixed'"><img width="30px" src="icons/accept.png" id="err_row_img_{id}" title="Fixed Error"/>&nbsp;This error has been fixed</span>
    </td>
</tr>
</table>
</div>
</div>
<a name="base"></a>
</body>
</html>