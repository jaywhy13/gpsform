<?php

$q = strtolower($_GET["q"]);
$id = $_GET["err_id"];

require_once ("../include/kiaapi.php");

$kia = new Kia ( );
$kia->setPrinterType ( KIA_JSON );

$result_arr = array();

$ids_found = array();

if(is_numeric($id) && !empty($id)){
    $ids_found [] = $id;
}

function perform_search($sql){
    $result_arr = $GLOBALS['result_arr'];
    $ids_found = $GLOBALS['ids_found'];

    if(sizeof($ids_found) > 0){
        $sql = $sql .=  "AND err_id NOT IN (" . join(",",$ids_found) . ")";
    }

    $kia = $GLOBALS['kia'];
    $result = $kia->runSQL($sql);

    while($row = $kia->loopResult($result)){
        $id = $row["err_id"];

        $ids_found [] = $id;
        $status = $row["status"];
        $description = $row["description"];

        $parish_id = $row["parish_id"];
        $area_id = $row["area_id"];

        $extras = "";
        // See if we can get some extra information
        if($parish_id >= 0){
            $sql = "SELECT * FROM gps_parish WHERE parish_id = $parish_id";
            if($kia->resultCount($sql) == 1){
                $parish_result = $kia->runSQL($sql);
                while($parish_row = $kia->loopResult($parish_result)){
                    $extras = $parish_row["name"];
                }
            }
        }

        if($area_id >= 0){
            $sql = "SELECT * FROM gps_area WHERE area_id = $area_id";
            if($kia->resultCount($sql) == 1){
                $area_result = $kia->runSQL($sql);
                while($area_row = $kia->loopResult($area_result)){
                    if($extras == "") $extras = $area_row["name"];
                    else $extras = $area_row["name"] . ", " . $extras;
                }
            }
        }

        $description = htmlentities($description);
        $addition = "$id::$status::$description::$extras";
        $result_arr[] = $addition;
        
    }

    $GLOBALS['result_arr'] = $result_arr;
    $GLOBALS['ids_found'] = $ids_found;
    
}


if(is_numeric($q)){
    $sql = "SELECT * FROM gps_err WHERE err_id = $q";
} else {
    $sql = "SELECT * FROM gps_err WHERE lower(description) LIKE '%$q%'";
    perform_search($sql);
}

// Search areas
$sql = "SELECT * FROM gps_err, gps_area WHERE gps_err.area_id = gps_area.area_id AND lower(gps_area.name) LIKE '%$q%'";
perform_search($sql);

// Search parishes
$sql = "SELECT * FROM gps_err, gps_parish WHERE gps_err.parish_id = gps_parish.parish_id AND lower(gps_parish.name) LIKE '%$q%'";
perform_search($sql);


$kia->getPrinter()->kiamsg(join("|",$result_arr));

?>