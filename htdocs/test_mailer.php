<?php


function notify_customer($to,$err_no){
	if(empty($to) || !isset($to)) return;
	
	if(empty($err_no) || !isset($err_no)) return;

	$date = date("F j, Y, g:i a");

	$message = '

<div style="font-family: verdana,helvetica,sans-serif; font-size: 10px;">

To our valued client, <br>

Your JAMNAV&copy; error report logged on ' . $date . ' has been received. The report has been logged as error # ' . $err_no . '. We will endeavor to correct same as soon as possible.<br/><br/>

You may use the error # provided to monitor the status of your report by visiting <a href="http://projects.monagis.com/jamnaverrors/track_error.php?id=' . $err_no . '">this link</a>.<br/><br/>

We encourage the continued use of our online error reporting facility as we provide you with the highest level customer service experience. <br><br>

Regards. <br/>

<div>
	<font size="2" style="background-color: rgb(255, 255, 255); font-family: verdana; font-style: italic;">
		<span style="font-weight: bold;">Paul A.M. Greene</span>
	</font>
	<font size="2" style="font-family: verdana; font-style: italic;">
		<br/>GPS Navigation Systems Specialist<br/><br/>
	</font>
	<font size="2" style="font-family: verdana; font-style: italic;">
		<span style="font-weight: bold;">Mona GeoInformatics Institute (MGI)</span><br/>
		The University of the West Indies, Mona, Kingston 7.<br/>
		Office: (876) 977-3160-2 / UWI Ext: 2653 / Fax:
			(876) 977-3164<br/>Website:
		<a target="_blank" href="http://www.monagis.com">http://www.monagis.com</a><br/><br style="color: rgb(0, 0, 0);"/>
		<span style="color: rgb(0, 0, 0); font-weight: bold;">Providers of high-end GIS, GPS and remotely sensed services</span>.<br/>
		Creators of <span style="font-size: small;"><span style="color: rgb(255, 204, 0);"><strong>JAM</strong></span>
		<span style="color: rgb(51, 153, 102);"><strong>NAV</strong></span>
		<strong><sup>©</sup></strong>
		</span> - GPS navigation map data for Jamaica.<br/><span style="font-size: small;">
		<span style="color: rgb(255, 204, 0);"><strong>JAM</strong></span><span style="color: rgb(51, 153, 102);"><strong>NAV</strong></span><strong><sup>©</sup></strong></span> website: <a target="_blank" href="http://projects.monagis.com/jamnav">http://projects.monagis.com/<wbr/>jamnav</a></font><span style="font-style: italic;">"</span><br style="font-family: verdana;"/><br style="font-family: verdana;"/><span style="font-family: verdana;"> </span><br/>
		</font></div><br/></div>
	</div>
';

	$subject = "Error # $err_no logged [" . $date . "]";

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	$headers .= 'From: Paul Greene <pgreene@monainformatixltd.com>' . "\r\n";
	return mail($to,$subject,$message,$headers);

}

$email = $_GET["email"];

if(empty($email) || !isset($email)){
	die("No email given");
}

if(notify_customer($email,150) == TRUE){
	echo "Mail sent to $email";
} else {
	echo "Failed to send mail to $email";
}

?>

