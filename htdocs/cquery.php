<?php
/**
 *
 * @author jay
 * @version
 */
require_once ("../include/kiaapi.php");
require_once ("../include/HttpClient.class.php");

function logData($str) {
    $fn = "gmpc.log";
    if (is_writable($fn)) {
        $f = fopen($fn, "a");
        if ($f) {
            $data = "=== [ Start Message ] : " . date("Y-m-d h:m:s") . "\n";
            $data .= $str;
            $data .= "\n=== [ End Message   ]\n\n";
            fwrite($f, $data);
            fclose($f);
        }
    }
}

function fetch_lock_code($serial) {
    $kia = $GLOBALS['kia'];
    if (lock_code_fetched($serial)) {
        // return one from the db
    } else {
        
    }
}

/**
 * Tells whether a lock code has previously been fetched
 * @param <type> $serial
 * @return <type> boolean
 */
function lock_code_fetched($serial) {
    $kia = $GLOBALS['kia'];
    $sql = "select * from gps_lock, gps_product where gps_lock.product_serial = gps_product.product_serial and gps_lock.product_serial = '$serial' and serial_fake = 'f'";
    return ($kia->resultCount($sql) > 0);
}

function dateDiff($startDate, $endDate) {
    // Parse dates for conversion
    $startArry = date_parse($startDate);
    $endArry = date_parse($endDate);

    // Convert dates to Julian Days
    $start_date = gregoriantojd($startArry ["month"], $startArry ["day"], $startArry ["year"]);
    $end_date = gregoriantojd($endArry ["month"], $endArry ["day"], $endArry ["year"]);

    // Return difference
    return round(($end_date - $start_date), 0);
}

$kia = new Kia ( );
$kia->setPrinterType(KIA_JSON);

$action = $_GET ["action"];

// Non order related actions
if ($action == "changedeviceserial") {
    $old_id = $_GET ["old_id"];
    $new_id = $_GET ["new_id"];

    if (empty($old_id) || empty($new_id)) {
        $kia->getPrinter()->kiaerror("Cannot change device serial. Ids not provided!");
    }

    // check to make sure that the old one exists
    if ($kia->resultCount("select * from gps_product where product_serial = '$old_id'") != 1) {
        $kia->getPrinter()->kiaerror("The old device does not exist in the system!");
    }

    // check to make sure that the new one does not exist
    if ($kia->resultCount("select * from gps_product where product_serial = '$new_id'") != 0) {
        $kia->getPrinter()->kiaerror("The new device already exists in the system!");
    }

    $sql = "update gps_product set product_serial = '$new_id', serial_fake = 'f' WHERE product_serial = '$old_id'";

    $res = $kia->runSQL($sql);

    if ($res) {
        $kia->getPrinter()->kiamsg("Serial number saved!");
    } else {
        $kia->getPrinter()->kiaerror("Could not save the serial number!");
    }
} else if ($action == "getunl") {
    $serial = $_GET ["serial"];
    $sql = "SELECT lock_code FROM gps_lock WHERE product_serial = '$serial'";
    if (empty($serial)) {
        $kia->getPrinter()->kiaerror("No serial number provided");
    } else {
        $res = $kia->runSQL($sql);
        while ($row = $kia->loopResult($res)) {
            $filename = "gmapsupp.unl";
            $code = $row["lock_code"] . "asdasiduhasdiuhasdiuashd";
            header("Content-type: text/plain");
            header("Content-disposition: filename=$filename");
            header("Cache-control: private");
            echo $code;
            exit;
        }
    }

    die();
} else if ($action == "getgma") {
    $serverType = $_GET["server_name"];
    $serial = $_GET["serial"];
    $filename = "$serial" . "_" . $serverType . ".gma";
    $filelocation = "../gma/$filename";
    if (file_exists($filelocation)) {
        $fp = fopen($filelocation, "r");
        if ($fp) {
            $size = filesize($filelocation);
            header("Content-type: application/octet-stream");
            header("Content-disposition: filename=$filename");
            header("Content-length: $size");
            header("Cache-control: private");
            while (!feof($fp)) {
                $buffer = fread($fp, 2048);
                echo $buffer;
            }
            fclose($fp);
            exit;
        }
    } else {
        echo "Could not locate GMA file!";
    }
    die();
} else if ($action == "getlockcode") {
    $serial = $_GET ["serial"];
    $serverType = $_GET["server_type"];
    if (empty($serial))
        $kia->getPrinter()->kiaerror("No product serial supplied. Cannot fetch lock code");

    // Check to ensure that the serial actually exists and that the serial is not fake
    $sql = "select * from gps_product where product_serial = '$serial' and serial_fake = 'f'";
    if ($kia->resultCount($sql) == 0) {
        $kia->getPrinter()->kiaerror("Serial number does not exist or is fake. Cannot retrieve lock code!");
    }

    $serialStored = false;

    // Check to make sure that the serial was not already fetched... check gps_lock
    $sql = "select * from gps_lock, gps_product where gps_lock.product_serial = gps_product.product_serial and gps_lock.product_serial = '$serial' AND serial_fake = 'f' AND server_name = '$serverType'";
//    echo $sql;qqq
    if ($kia->resultCount($sql) == 0) {
        if ($serverType == "prempc7") {
            // then we need to fetch the data
            $data_arr = array();
            $data_arr ["n"] = $serial;
            $data_arr ["loginname"] = "minformatix";
            $data_arr ["password"] = "1cnt04sp2u!";
            $data_arr ["n"] = $serial;
            $data_arr ["f"] = 1436;
            $data_arr ["p"] = 1;
            $data_arr ["r"] = 0;
            $data_arr ["s"] = 0;

            //        $host = "www.garmin.com";
            //        $script = "/cgi-bin/mpc/mpc_unlock_transact.cgi";

            $host = "projects.monagis.com";
            $script = "/gpsform/garmindummy.php";

            $client = new HttpClient($host);
            $client->setUserAgent('Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.3a) Gecko/20021207'); // fake a mozilla browser

            $url = "http://" . $host . $script . "?";
            $getparams = array();
            foreach ($data_arr as $key => $value) {
                $getparams [] = "$key=$value";
            }

            $url .= "&" . join("&", $getparams);


            if ($client->get($script, $data_arr)) {
                $str = $client->getContent();
                $format_str = htmlentities($str);

                $logSQL = "INSERT INTO server_log (log_url,log_response,log_ts) VALUES ('$url','$format_str',now())";
                $kia->runSQL($logSQL);

                logData("Server Contacted: \n$url");
                logData("Server Raw Response:\n$str");
                logData("Server Formatted Response:\n$format_str");

                if (ereg("[A-Z0-9]{25}", $str)) {
                    $matches = array();
                    if (preg_match_all("/[A-Z0-9]{25}/", $str, $matches)) {
                        if (sizeof($matches)) {
                            $lock_code = ($matches [0] [0]);
                            $ts = date("Y-M-d G:i:s");
                            //						$sql = "INSERT INTO gps_lock VALUES ('$serial','$lock_code','$ts','$format_str','$url')";
                            $sql = "INSERT INTO gps_lock (product_serial,lock_code,lock_timestamp,server_name) VALUES ('$serial','$lock_code','$ts','$serverType')";
                            $kia->runSQL($sql);
                            $serialStored = true;
                            logData("Serial found in Garmin response.\nUrl: $url\Lock Code: $lock_code\nSerial:$serial");
                        } else {
                            logData("Preg could not find a match in the response. \nUrl: $url\nResponse: $str");
                        }
                    } else {
                        if (ereg("password", $str)) {
                            $kia->getPrinter()->kiaerror("Credential error, please contact admin");
                        } else {
                            logData("Unknown error received from Garmin. \nResponse:\n$str");
                            $kia->getPrinter()->kiaerror("Unknown error received from Garmin, please consult the log.");
                        }
                    }
                } else {
                    logData("Ereg could not find a match in the response. \nUrl: $url\nResponse: $str");
                    $kia->getPrinter()->kiaerror("Garmin did not reply with a lock code, please try again later");
                }
            } else {
                logData("Could not reach Garmin server, ensure the server can see the outside world.");
                $logSQL = "INSERT INTO server_log (log_url,log_response,log_ts) VALUES ('$url','',now())";
                $kia->runSQL($logSQL);
                $kia->getPrinter()->kiaerror("Error contacting Garmin servers, please contact admin!");
            }
        } else if ($serverType == "mpc7") {
            $host = "www.garmin.com";
            $type = "0";

            $loginname = "testlogin";
            $password = "testpass";

            $esn = 9999;
            $familyID = 12345;
            $productID = 1;
            $regionID = 0;
            $subregionID = 0;

            $outfile = "../gma/$serial" . "_" . "$serverType" . ".gma";

            $version = "262145";

            $qs = "/cgi-bin/mapact/mapact_licensee.cgi?" . "type=" . $type .
                    "&loginname=" . $loginname . "&password=" . $password . "&n=" . $esn . "&f=" . $familyID .
                    "&p=" . $productID . "&r=" . $regionID . "&s=" . $subregionID . "&v=" . $version;

            $sock = fsockopen($host, 80, $errno, $errstr);
            if (!$sock) {
                $kia->getPrinter()->kiaerror("Unable to reach garmin host: $host");
//                print "[ERROR]: $host http connection error\n";
            } else {
                $request = "GET $qs HTTP/1.1\r\n";
                $request .= "Host: $host\r\n";
                $request .= "User-Agent: PHP Http_Open\r\n";
                $request .= "\r\n";

                $url = "http://$host/$qs";
                $result = file_get_contents($url, FALSE, NULL);
                fclose($sock);

                # byte 1: unlock code error ID
                $e = substr($result, 0, 1);
                $ebytes = unpack("Ce", $e);
                $errcode = sprintf("%d", $ebytes['e']);
//                echo "Error ID for unlock code = $errcode\n";

                if ($errcode == 0) {
                    # byte 2 - 26: the unlock code
                    $uc = substr($result, 1, 25);
//                    print "Unlock Code = $uc\n\n";
                    # byte 27: GMA error ID
                    $e = substr($result, 26, 1);
                    $ebytes = unpack("Ce", $e);
                    $errcode = sprintf("%d", $ebytes['e']);
//                    print "Error ID for GMA file = $errcode\n";
                    # byte 28 - 29: GMA data length
                    $l = substr($result, 27, 2);
                    $lbytes = unpack("Sl", $l);
                    $len = sprintf("%d", $lbytes['l']);
//                    print "Data length for GMA file = $len\n";
                    # byte 30 and up: GMA data
                    if ($errcode == 0 && $len > 0) {
                        $fp = fopen($outfile, "wb");
                        if (fwrite($fp, substr($result, 29)) == FALSE) {
                            $kia->getPrinter()->kiaerror("Unable to save the GMA file on the server, please contact your administrator!");
                        } else {
                            $ts = date("Y-M-d G:i:s");
                            $sql = "INSERT INTO gps_lock (product_serial,lock_code,lock_timestamp,server_name) VALUES ('$serial','$uc','$ts','$serverType')";
//                            echo "Running $sql<br>";
                            $kia->runSQL($sql);
                            $serialStored = true;
                            logData("Serial found in Garmin response.\nUrl: $url\Lock Code: $uc\nSerial:$serial");
                            fclose($fp);
                            logData("GMA saved onto file '$outfile'");
                        }
                    } else {
                        $kia->getPrinter()->kiaerror("The GMA file received from the server is invalid, please try again or contact Garmin");
                    }
                } else {
                    $kia->getPrinter()->kiaerror("No result received from the server!");
                }

                $logSQL = "INSERT INTO server_log (log_url,log_response,log_ts) VALUES ('$url','Lock Code=$uc Error Code=$errcode',now())";
                $kia->runSQL($logSQL);
//                echo "Trying to run: $logSQL";

                logData("Server Contacted: \n$url");
                logData("Server Raw Response:\n$str");
                logData("Server Formatted Response:\n$format_str");
            }
        } else {
            $kia->getPrinter()->kiaerror("Unimplemented action: We do not know how to fetch code from $serverType!");
        }
    } else {
        $serialStored = true;
    }

    if (!$serialStored) {
        logData("The system was unable to fetch a lock code from the server or from the database");
        $kia->getPrinter()->kiaerror("The system was unable to fetch lock code!");
    }

    // moving right along if we were able shall we?
    $kia->getPrinter()->kiamsg("Lock code retrieved");
} else if ($action == "getgmalockcode") {
    
    $serialStored = false;
    
    $serial = $_GET ["serial"];

    if (empty($serial)) {
        $serial = $_GET["esn"];
    }

    $serverType = "prempc7";

    $familyID = 1436;
    $productID = 1;
    $regionID = 0;
    $subregionID = 0;
    $unlFile = dirname(__FILE__) . "/codes/$serial.unl";
    $gmaFile = dirname(__FILE__) . "/codes/$serial.gma";

    // Login credentials
    $host = "legacy.garmin.com";
    $loginname = "minformatix";
    $password = "1cnt04sp2u!";
    $ucode = "";
    $type = "0";
    $version = "262145";

    // Check to ensure that the serial actually exists and that the serial is not fake
    $sql = "select * from gps_product where product_serial = '$serial' and serial_fake = 'f'";
    if ($kia->resultCount($sql) == 0) {
        $kia->getPrinter()->kiaerror("Serial number does not exist or is fake. Cannot retrieve lock code!");
    }

    $sql = "select * from gps_lock, gps_product where gps_lock.product_serial = gps_product.product_serial and gps_lock.product_serial = '$serial' AND serial_fake = 'f' AND server_name = '$serverType'";
    $serialStored = $kia->resultCount($sql) > 0;

    if ((file_exists($unlFile) && file_exists($gmaFile)) || $serialStored) {
        // do nothing....
    } else {
        try {
            // Soap Call to Web Service  
            ini_set('error_reporting', !E_WARNING);
            $client = new SoapClient('http://omt.garmin.com/LicenseeMapActivation_V1.0/LicenseeMapActivationService?wsdl', array("trace" => 1));
            //echo 'Created SOAP client';
            $authenticate->Username = $loginname;
            $authenticate->Password = $password;
            $authenticate->AccountTypeID = $type;

            $UnlockInfo0->Family = $familyID;
            $UnlockInfo0->Product = $productID;
            $UnlockInfo0->Region = $regionID;
            $UnlockInfo0->SubRegion = $subregionID;
            
            // Do some changes to support the serial...
             $serial = (float) $serial;
             $UnlockInfo0->UnitId = new SoapVar($serial, XSD_STRING, "string", "http://www.w3.org/2001/XMLSchema");
//             echo $UnlockInfo0->UnitId;
//            $UnlockInfo0->UnitId = bcmul($serial,1);
//            echo $UnlockInfo0->UnitId;
            $unlockInfos = array("0" => $UnlockInfo0);
            
            $request->Authenticate = $authenticate;
            $request->UnlockInfo = $unlockInfos;
            $request->GmaVersion = $version;

            //Retrieve data from Web Service
            $response = $client->GetUnlockAndGma(array("request" => $request));
            $ucode = $response->GetUnlockAndGmaResult->UnlockCode->string;
            $result = $response->GetUnlockAndGmaResult->GmaFile;

            //Write Unlock file
            $file = fopen($unlFile, 'w');
            if (fwrite($file, $ucode) == FALSE) {
                echo "[ERROR]: Cannot write to file '$unlFile' <br>\n";
            } else {
                fclose($file);
//            echo "Unlock saved onto file '$unlFile' <br>\n";
            }

            //Write GMA File
            $fp = fopen($gmaFile, "wb");
            if (fwrite($fp, $result) == FALSE) {
                echo "[ERROR]: Cannot write to file '$gmaFile' <br>\n";
            } else {
                fclose($fp);
                //echo "GMA saved onto file '$gmafile' <br>\n";
            }

            $ts = date("Y-M-d G:i:s");
            //						$sql = "INSERT INTO gps_lock VALUES ('$serial','$lock_code','$ts','$format_str','$url')";
            $sql = "INSERT INTO gps_lock (product_serial,lock_code,lock_timestamp,server_name) VALUES ('$serial','$ucode','$ts','$serverType')";
            $kia->runSQL($sql);
            $serialStored = true;
        } catch (Exception $e) {
            $kia->getPrinter()->kiaerror("The system was unable to fetch lock code! " . $e->getMessage());
        }
    }

    $kia->getPrinter()->kiamsg("Lock code retrieved");
} else if ($action == "addinventory") {
    $package_id = $_GET["package_id"];
    if (!isset($package_id) || empty($package_id)) {
        $kia->getPrinter()->kiaerror("No package id supplied");
    }

    // Next.. check to ensure that the package id exists
    $sql = "SELECT * FROM gps_package WHERE package_id = '$package_id'";
    if ($kia->resultCount($sql) == 1) {
        
    } else {
        $kia->getPrinter()->kiaerror("The package id does not exist");
    }

    // Now check the count
    $count = $_GET["count"];
    if (is_numeric($count) && $count > 0) {
        for ($c = 0; $c < $count; $c++) {
            $serial = "serial_" . time() . "$c";
            $kia->runSQL("BEGIN");
            $sql = "INSERT INTO gps_inventory_unit VALUES ('$serial','$package_id','f','','In House','Internal Memory','t',NULL)";
            $kia->runSQL($sql);
            $kia->runSQL("END");
        }
        $kia->getPrinter()->kiamsg("$count inventory items added.");
    } else {
        $kia->getPrinter()->kiaerror("The count given is invalid");
    }

    die();
} else if ($action == "getlockstatus") {
    $ser = $_GET["ser"];
    $ser_arr = explode(",", $ser);

    for ($i = 0; $i < sizeof($ser_arr); $i++) {
        $ser_arr[$i] = "'" . $ser_arr[$i] . "'";
    }

    $ser = join(",", $ser_arr);

    $ser_status = array();

    if (!$ser_arr || sizeof($ser_arr) == 0) {
        $kia->getPrinter()->kiaerror("No serials provided!");
    } else {
        for ($i = 0; $i < sizeof($ser_arr); $i++) {
            $key = trim($ser_arr[$i], "'");

            $ser_status [$key . "_mpc7"] = "unfetched";
            $ser_status [$key . "_prempc7"] = "unfetched";
        }

        $sql = "SELECT product_serial, server_name FROM gps_lock WHERE product_serial IN ( " . $ser . " ) ";
        //		echo $sql;

        $result = $kia->runSQL($sql);
        if ($result) {
            while ($row = $kia->loopResult($result)) {
                $server_name = $row["server_name"];
                $serial = $row["product_serial"];
//                				echo "Found $ser_row<br>";
                $key = $serial . "_" . $server_name;
                $ser_status[$key] = "fetched";
            }
        }

        $result_arr = array();
        foreach ($ser_status as $key => $value) {
            $result_arr [] = "$key:$value";
        }

        $kia->getPrinter()->kiamsg(join(",", $result_arr));
    }
} else if ($action == "saveunitrecords") {
    $record_str = $_GET["records"];
    if (empty($record_str) || !isset($record_str)) {
        $kia->getPrinter()->kiaerror("No changes made, nothing to be saved.");
    }

    $records = explode(",", $record_str);
    $recordsUpdated = 0;
    foreach ($records as $record) {
        $record_parts = explode(":", $record);
        $serial = $record_parts[0];
        $unit_id = $record_parts[1];
        $sql = "UPDATE gps_product SET unit_id = '$unit_id' WHERE product_serial = '$serial'";
        if ($unit_id == -1)
            $sql = "UPDATE gps_product SET unit_id = NULL WHERE product_serial = '$serial'";
        if ($kia->runSQL($sql)) {
            $recordsUpdated++;
        }
    }

    $kia->getPrinter()->kiamsg("$recordsUpdated record(s) updated!");
} else if ($action == "applyte") {
    $order_id = $_GET["order_id"];
    if (empty($order_id)) {
        $kia->getPrinter()->kiaerror("No order information supplied. Could not appply tax exemption!");
    } else {
        $sql = "SELECT order_tax_exemption, order_amount_owing, order_total FROM gps_order WHERE order_id = $order_id"; // check that there is no exemption there
        $res = $kia->runSQL($sql);

        if ($kia->resultCount($sql) == 0) {
            $kia->getPrinter()->kiaerror("Could not find order #$order_id to apply exemption");
        }

        while ($row = $kia->loopResult($res)) {
            $order_tax_exemption = $row["order_tax_exemption"];
            $amount_owing = $row["order_amount_owing"];
            $order_total = $row["order_total"];

            if ($order_tax_exemption == "t") {
                $kia->getPrinter()->kiaerror("Order #$order_id has already been exempted from tax");
            } else {

                // Check if the order_amount_owing = order_total
                $sql = "SELECT * FROM gps_order_payment WHERE order_id = $order_id";
                $number_payments = $kia->resultCount($sql);
                if ($number_payments > 0) {
                    $kia->getPrinter()->kiaerror("Please remove all payments from the order before applying tax exemption");
                } else {
                    $sql = "UPDATE gps_order SET order_total_orig = order_total_orig - order_tax WHERE order_id = $order_id";
                    $kia->runSQL($sql);
                    $sql = "UPDATE gps_order SET order_total = order_total_orig WHERE order_id = $order_id";
                    $kia->runSQL($sql);
                    $sql = "UPDATE gps_order SET order_tax_exemption = 't' WHERE order_id = $order_id";
                    $kia->runSQL($sql);

                    // Update the amount outstanding if it is equal to the total
                    if ($amount_owing == $order_total) {
                        $sql = "UPDATE gps_order SET order_amount_owing = order_total_orig WHERE order_id = $order_id";
                        $kia->runSQL($sql);
                    }

                    $kia->getPrinter()->kiamsg("Order #$order_id has been exempted from tax!");
                }
            }
        }
    }
} else if ($action == "removete") {
    $order_id = $_GET["order_id"];
    if (empty($order_id)) {
        $kia->getPrinter()->kiaerror("No order information supplied. Could not remove tax exemption!");
    } else {
        $sql = "SELECT order_tax_exemption, order_amount_owing, order_total FROM gps_order WHERE order_id = $order_id"; // check that there is an exemption there
        $res = $kia->runSQL($sql);

        if ($kia->resultCount($sql) == 0) {
            $kia->getPrinter()->kiaerror("Could not find order #$order_id to remove exemption");
        }

        while ($row = $kia->loopResult($res)) {

            $order_tax_exemption = $row["order_tax_exemption"];
            $amount_owing = $row["order_amount_owing"];
            $order_total = $row["order_total"];

            if ($order_tax_exemption == "f") {
                $kia->getPrinter()->kiaerror("Order #$order_id has is not exempted from tax");
            } else {
                // Check if the order_amount_owing = order_total
                $sql = "SELECT * FROM gps_order_payment WHERE order_id = $order_id";
                $number_payments = $kia->resultCount($sql);

                if ($number_payments > 0) {
                    $kia->getPrinter()->kiaerror("Please remove all payments from the order before removing the tax exemption");
                } else {
                    $sql = "UPDATE gps_order SET order_total_orig = order_total_orig + order_tax WHERE order_id = $order_id";
                    $kia->runSQL($sql);
                    $sql = "UPDATE gps_order SET order_total = order_total_orig WHERE order_id = $order_id";
                    $kia->runSQL($sql);
                    $sql = "UPDATE gps_order SET order_tax_exemption = 'f' WHERE order_id = $order_id";
                    $kia->runSQL($sql);

                    // Update the amount outstanding if it is equal to the total
                    if ($amount_owing == $order_total) {
                        $sql = "UPDATE gps_order SET order_amount_owing = order_total_orig WHERE order_id = $order_id";
                        $kia->runSQL($sql);
                    }

                    $kia->getPrinter()->kiamsg("Tax exemption has been removed from order #$order_id");
                }
            }
        }
    }
} else if ($action == "changediscount") {

    $order_id = $_GET["order_id"];
    $da = $_GET["da"];

    if ($da < 0 || $da > 1) {
        $kia->getPrinter()->kiaerror("Invalid discount amount provided!");
    }

    if (empty($order_id)) {
        $kia->getPrinter()->kiaerror("No order information supplied. Could not change the discount");
    } else {
        // Check that the order exists
        $sql = "SELECT apply_discount($order_id,$da)";
        if ($kia->resultCount($sql) == 1) {
            $kia->getPrinter()->kiamsg("Discount of " . $da * 100 . "% applied successfully to order # $order_id!");
        } else {
            $kia->getPrinter()->kiaerror("Could not find order #$order_id. No discount may be applied.");
        }
    }
} else if ($action == "fetchinventoryitems") {
    $sql = "SELECT product_serial, package_id, lock_retrieved, comments, status, serial_fake, unit_name FROM gps_inventory_unit LEFT JOIN garmin_unit ON gps_inventory_unit.unit_id = garmin_unit.unit_id";
    $rs = $kia->runSQL($sql);


    $messages = array();
    $messages [] = "({\"items\":[";

    $rowData = array();

    if ($rs) {
        while ($row = pg_fetch_assoc($rs)) {
            $cols = array();
            foreach ($row as $key => $value) {
                $cols [] = "\"$key\":\"$value\"";
            }

            $lastCustomerSql = "";

            $str = " { " . join(",", $cols) . " } ";
            $rowData [] = $str;
        }
    } else {
        
    }

    $messages [] = join(",", $rowData);

    $messages [] = "]})";

    die(join("", $messages));
}


$order_id = $_GET ["order_id"];
$cust_id = $_GET ["cust_id"];
$amount = $_GET ["amount"];

if ($action == "checkdelinquency") {
    $kia->setPrinterType(KIA_JSON);
    $sql = "select * from gps_customer where cust_id = $cust_id";
    if ($kia->resultCount($sql) == 0)
        $kia->getPrinter()->kiaerror("Customer $cust_id does not exist");

    // check number of user orders
    $sql = "select * from gps_order where cust_id = $cust_id";
    $res = $kia->runSQL($sql);
    if ($kia->resultCount($sql) == 0)
        $kia->getPrinter()->kiamsg("no orders");
    else {
        $order_statuses = array();
        // now we need to report on the status of each customer
        while ($row = $kia->loopResult($res)) {
            $db_order_id = $row ["order_id"];
            $db_order_date = $row ["order_date"];
            $status = $row ["order_complete"];
            if ($status == "f") {
                // check on all payments made
                $sql = "select * from gps_order_payment where cust_id = $cust_id and order_id = $db_order_id order by payment_date desc limit 1 ";
                //				echo $sql;
                $last_payment_date = Null;
                $today = date("Y-m-d");
                if ($kia->resultCount($sql) > 0) {
                    // get the last payment date
                    $payment_res = $kia->runSQL($sql);
                    while ($payment_row = $kia->loopResult($payment_res)) {
                        $last_payment_date = $payment_row ["payment_date"];
                        break;
                    }
                }

                if (($last_payment_date != null && (dateDiff($last_payment_date, $today) >= (12 * 7)) || // last payment over 12 weeks
                        ($last_payment_date == null && (dateDiff($db_order_date, $today) >= (12 * 7))))// no payment in over 12 weeks
                ) {
                    $order_statuses [] = "$db_order_id:delinquent";
                } else if (($last_payment_date != null && (dateDiff($last_payment_date, $today) >= (6 * 7)) || // last payment over 6 weeks
                        ($last_payment_date == null && (dateDiff($db_order_date, $today) >= (6 * 7))))// no payment in over 6 weeks
                ) {
                    $order_statuses [] = "$db_order_id:trouble";
                } else {
                    $order_statuses [] = "$db_order_id:cool";
                }
            } else {
                $order_statuses [] = "$db_order_id:cool";
            }
        }

        $kia->getPrinter()->kiaprintarr($order_statuses);
    }
}

function updateAmountOwning($order_id = null, $cust_id = null) {
    $kia = $GLOBALS ['kia'];

    if (!$order_id)
        $order_id = $GLOBALS ['order_id'];
    if (!$cust_id)
        $cust_id = $GLOBALS ['cust_id'];
    // Calculate the sum of all payments first
    $sql = "select sum(payment_amount) from gps_order_payment where order_id = $order_id AND cust_id = $cust_id";

    $res = $kia->runSQL($sql);

    if ($res) {
        while ($row = $kia->loopResult($res)) {
            $sum = $row ["sum"];
        }
    }

    if (empty($sum) || !is_numeric($sum))
        $sum = 0;

    $sql = "update gps_order set order_amount_owing =  (order_total - $sum) where order_id = $order_id and cust_id = $cust_id";

    $kia->runSQL($sql);

    // Check if the order is complete
    $sql = "update gps_order set order_complete = 't' where order_amount_owing = 0 and order_id = $order_id AND cust_id = $cust_id";
    //	echo $sql;


    $kia->runSQL($sql);
}

updateAmountOwning(); // update any amount that is owing


if (empty($order_id)) {
    $kia->getPrinter()->kiaerror("Invalid order id.");
}

if (empty($cust_id)) {
    $kia->getPrinter()->kiaerror("No customer information supplied.");
}

// Check that the order exists
$sql = "select order_id from gps_order where order_id = $order_id AND cust_id = $cust_id";

if ($kia->resultCount($sql) == 0) {
    $kia->getPrinter()->kiaerror("The order does not exist!");
}

if ($action == "updateorder") {
    if (empty($order_id)) {
        $kia->getPrinter()->kiaerror("Order information was not supplied");
    }

    if (empty($cust_id)) {
        $kia->getPrinter()->kiaerror("Customer information was not supplied");
    }

    // Calculate the sum of all payments first
    $sql = "select sum(payment_amount) from gps_order_payment where order_id = $order_id AND ";
    $sql .= "cust_id = $cust_id";

    $res = $kia->runSQL($sql);

    if ($res) {
        while ($row = $kia->loopResult($res)) {
            $sum = $row ["sum"];
        }
    }

    if (empty($sum) || !is_numeric($sum))
        $sum = 0;

    $sql = "update gps_order set order_amount_owing =  (order_total - $sum) where order_id = $order_id and cust_id = $cust_id";

    $res = $kia->runSQL($sql);
    if ($res) {
        $kia->getPrinter()->kiamsg("Order information was successfully updated");
    } else {
        $kia->getPrinter()->kiaerror("Error updating order information. " . ceh_last_error());
    }
} else if ($action == "paymentvalid") {
    $sql = "select order_total, order_complete,(select sum(payment_amount) from gps_order_payment where order_id = $order_id and cust_id = $cust_id) as total_payment from gps_order where order_id = $order_id and cust_id = $cust_id";

    if (!is_numeric($amount))
        $kia->getPrinter()->kiaerror("Invalid amount supplied");
    if ($amount <= 0) {
        $kia->getPrinter()->kiaerror("Invalid amount supplied");
    }

    $res = $kia->runSQL($sql);
    if ($res) {
        while ($row = $kia->loopResult($res)) {
            $order_complete = $row ["order_complete"];
            $order_total = $row ["order_total"];
            $total_payment = $row ["total_payment"];

            // Set the order as complete if the
            if (($total_payment + $amount) == $order_total) {
                $sql = "update gps_order set order_complete = t where order_id = $order_id AND cust_id = $cust_id";
                $kia->runSQL($sql);
            }

            if ($order_complete == "t" && $order_total == $total_payment) {
                $kia->getPrinter()->kiaerror("This order has been marked complete! No more payments may be made on this order.");
            }

            if ($total_payment > $order_total) {
                $kia->getPrinter()->kiaerror("The customer seems to have overpaid for this order. Contact the finance department concerning order # $order_id");
            }

            if (($total_payment + $amount) > $order_total) {
                $kia->getPrinter()->kiaerror("This amount will result in the customer overpaying for the order! Cannot accept this payment!");
            }

            $kia->getPrinter()->kiamsg("success");

            break;
        }
    }
} else if ($action == "generateproducts") {

    // Check if the order is complete
    $sql = "select order_complete from gps_order where order_id = $order_id  and cust_id = $cust_id";
    $res = $kia->runSQL($sql);
    if ($res) {
        while ($row = $kia->loopResult($res)) {
            if ($row ["order_complete"] == "f")
                $kia->getPrinter()->kiaerror("Cannot generate products for an incomplete order.");
            break;
        }
    }

    // Check to make sure the order is not already generated
    $sql = "select * from gps_order where order_id = $order_id and cust_id = $cust_id and order_generated = 'f'";
    if ($kia->resultCount($sql) == 0) {
        $kia->getPrinter()->kiaerror("The products for this order have already been generated!");
    }

    $sql = "select * from gps_order_item where order_id = $order_id AND cust_id = $cust_id";
    $res = $kia->runSQL($sql);

    $package_arr = array();

    if ($res) {
        while ($row = $kia->loopResult($res)) {
            $item_id = $row ["order_item_id"];
            $package_id = $row ["package_id"];
            $quantity = $row ["order_item_quantity"];
            if (!$package_arr [$package_id])
                $package_arr [$package_arr] = array();
            for ($k = 0; $k < $quantity; $k++) {
                $package_arr [$package_id] [] = $item_id;
            }
        }
    }

    $iter = 0;
    foreach ($package_arr as $package_id => $item_id_arr) {
        foreach ($item_id_arr as $item_id) {
            $id = (date("U") + date("u") + $iter); // ensure the id is NEVER the same as another
            $sql = "insert into gps_product (product_serial,cust_id,order_id,order_item_id,package_id) VALUES ('$id',$cust_id,$order_id,$item_id,'$package_id')";
            //			echo $sql;
            $res = $kia->runSQL($sql);
            //			echo $res;
            if (!$res) {
                $kia->getPrinter()->kiaerror("Could not generate all packages!");
            }
            $iter++;
        }
    }

    // Mark the order as having its products generated
    $sql = "update gps_order set order_generated = 't' where order_id = $order_id and cust_id = $cust_id";
    $kia->runSQL($sql);

    $kia->getPrinter()->kiamsg("All packages generated successfully");
} else if ($action == "addupdate") {

    $order_id = $_GET ["order_id"];
    $cust_id = $_GET ["cust_id"];
    $serial = $_GET["product_serial"];
    $dispatch_date = $_GET["dispatch_date"];
    $version_no = $_GET["version_no"];

    if (empty($order_id) || !isset($order_id))
        $kia->getPrinter()->kiaerror("No order information supplied");
    if (empty($cust_id) || !isset($cust_id))
        $kia->getPrinter()->kiaerror("No customer information supplied");
    if (empty($serial) || !isset($serial))
        $kia->getPrinter()->kiaerror("No product information supplied");
    if (empty($version_no) || !isset($version_no))
        $kia->getPrinter()->kiaerror("No version information supplied");

    if (empty($dispatch_date) || !isset($dispatch_date))
        $dispatch_date = date("Y-M-d");

    $sql = "INSERT INTO gps_customer_update VALUES ($cust_id,$order_id,$serial,'$version_no','$dispatch_date')";

    $result = $kia->runSQL($sql);

    if ($result) {
        $kia->getPrinter()->kiamsg("Notice of dispatch saved to the system");
    } else {
        $kia->getPrinter()->kiaerror("Dispatch could not be saved, consult your administrator.");
    }
} else if ($action == "mgigift") {
    $order_id = $_GET ["order_id"];
    if (empty($order_id)) {
        $kia->getPrinter()->kiaerror("No order information received! Could not convert order to MGI gift.");
    } else {
        $sql = "SELECT * FROM gps_order WHERE order_id = $order_id";
        $result = $kia->runSQL($sql);

        // Check to make sure that there's an amoutn owing

        while ($row = $kia->loopResult($result)) {
            $amount_outstanding = $row["order_amount_owing"];
            if ($amount_outstanding == 0) {
                $kia->getPrinter()->kiaerror("There is no amount owing on order #$order_id. This order cannot be saved as an MGI gift!");
                die;
            }
        }

        // set the mgi gift to zero
        $sql = "UPDATE gps_order set mgi_gift = order_total where order_id =  $order_id";
        $result = $kia->runSQL($sql);

        // set the amount owing to zero and totals to zero
        $sql = "UPDATE gps_order set order_amount_owing = 0, order_total = 0, order_total_orig = 0 where order_id = $order_id";
        $result = $kia->runSQL($sql);

        // remove all payments
        $sql = "DELETE FROM gps_order_payment WHERE order_id = $order_id";
        $result = $kia->runSQL($sql);

        if ($result) {
            $kia->getPrinter()->kiamsg("Order # $order_id saved as MGI gift!");
        } else {
            $kia->getPrinter()->kiaerror("Could not save order #$order_id as MGI gift!");
        }
    }
}