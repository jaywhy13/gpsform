<?php
include_once("../include/permguru.php");

$pg = new PermGuru("gpsform");
if(!$pg->isLoggedIn()){
	?>
	<html>
		<head><title>Unauthorized</title></head>
		<body>
		No unauthorized access is allowed on this page. Please click <a href="admin.php">here</a> to login.
		</body>
	</html>
	<?
	die;
}
?>


<html>
	<head>
		<title>Report Generator - JAMNAV</title>
		<script src="jquery-1.2.6.min.js"></script>
		<script src="jquery-ui-personalized-1.5.2.min.js"></script>
		<script src="jquery.blockUI.js"></script>
		<script src="jquery.tablesorter.min.js"></script>
		<script src="crudmachine-client.js"></script>
		<script src="calendar.js"></script>
		<link href="cal.css" type="text/css" rel="stylesheet"/>
		<style type="text/css">
		.div_relation {
			border-style:solid;
			border-width:1px;
			padding:5px;
			font-size:10px;
			font-family:Verdana;
			float:left;
			width:120px;
			margin:3px;
			background-color:rgb(220,220,235);
		}	
		
		DIV,SELECT {
			margin:3px;
		}
		
		BODY {
			font-size:11px;
			line-height:150%;
		}
		
		SELECT,INPUT {
			padding:3px;
			margin:3px;
			width:120px;
		}
		
		#div_customizeReport {
			background-color:rgb(140,150,160);
			padding:5px;
			overflow:auto;
			width:98%;
			display:none;
		}
		
		#div_relationList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		}
		
		
		#div_fieldList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		}
		
		#div_constraintList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		
		}
		
		
		.div_relFieldList {
			width:50%;
			padding:5px;
			margin:3px;
			border-style:solid;
			border-width:1px;
			overflow:auto;
		}
		
		.div_fieldConstraint {
			width:500px;
			padding:5px;
			margin:3px;
			border-style:solid;
			border-width:1px;
		}
		
		.div_fieldConstraint TABLE TD {
		}
		
		.div_relFieldList DIV {
			border-style:solid;
			border-width:1px;
			border-color:rgb(230,230,230);
			float:left;
			padding:3px;
			font-size:10px;
		}
		
		.div_relFieldList SPAN {
			width:98%;
			display:block;
			padding:3px;
			background-color:black;
			color:white;
			font-weight:bold;
			
		}
		
		#div_report {
			clear:all;
			width:98%;
			overflow:auto;
			background-color:rgb(230,240,250);
			padding:5px;
		}
		
		TABLE TD,TH {
			font-size:11px;
		}
		
		
		
		TABLE .headerRow TD {
			background-color:black;
			color:white;
			font-weight:bold;
			border-style:solid;
			border-width:1px;
			border-color:white;
			padding:5px;
		}
		
		.evenRow TD {
			background-color:white;
		}
		
		.oddRow TD {
			background-color:rgb(230,240,250);
		}
		
		.div_field {
			width:130px;
			overflow:auto;
		}
		
		.finalRow TD {
			border-top-style:solid;
			border-top-width:1px;
			padding:3px;
			background-color:white;
		}
		
		
		#calcontainer {
			border-style:solid;
			background-color:rgb(240,255,240);
			padding:5px;
			width:260px;
			right:10px;
			top:10px;
			position:absolute;
		}
		
		
		th.headerSortUp { 
    	background-image: url(images/small_asc.gif); 
    	background-color: #3399FF; 
		} 
		
		th.headerSortDown { 
   		 	background-image: url(images/small_desc.gif); 
    		background-color: #3399FF; 
		} 
		
		
		th.header { 
    	background-image: url(images/small.gif);     
    	cursor: pointer; 
    	font-weight: bold; 
    	background-repeat: no-repeat; 
    	background-position: center left; 
    	padding-left: 20px; 
    	border-right: 1px solid #dad9c7; 
    	margin-left: -1px; 
		} 
		
		
		#div_reportDescription {
			padding:5px;
			background-color:#eeeefe;
			width:400px;
			border-style:solid;
			border-width:1px;
		}
		
		</style>
		<script>
		function formatCurrency(num, prefix) {
			if (!prefix)
			var prefix = "J $";
			num = num.toString().replace(/\$|\,/g, '');
			if (isNaN(num))
			num = "0";
			sign = (num == (num = Math.abs(num)));
			num = Math.floor(num * 100 + 0.50000000001);
			cents = num % 100;
			num = Math.floor(num / 100).toString();
			if (cents < 10)
			cents = "0" + cents;
			for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
			num = num.substring(0, num.length - (4 * i + 3)) + ','
			+ num.substring(num.length - (4 * i + 3));
			return (((sign) ? '' : '-') + prefix + num + '.' + cents);
		}



		var processFields = {};

		function addProcessFunction(field,f){
			processFields[field] = f;
		}

		function processVar(field,val){
			if(processFields[field]){
				var f = processFields[field];
				return f(val);
			}
			return val;
		}


		var relationList = new Array("gps_customer:GPS Customer","gps_order:Order","gps_order_item:Order Item","gps_order_payment:Payment","gps_lock:GPS Lock","gps_package:GPS Package","gps_product:GPS Product","gps_commission:Sales Commission","usr:User","gps_data_version:Version","gps_customer_update:Customer Version","garmin_unit:Garmin Unit","gps_customer_update:GPS Customer Update");
		var relationListCopy = relationList;
		var fieldList = {};
		var savedReports = {
		"customers":{
		"title":"All Customers",
		"description":"Lists all customers and useful information",
		"relations":new Array("gps_customer"),
		"fields":new Array("gps_customer.cust_id","gps_customer.cust_lname","gps_customer.cust_fname")
		},
		"customerpackages":{
		"title":"Customer Orders",
		"description":"Lists all customer orders and showing order total, amount outstanding and MGI complimentary gift amount.",
		"relations":new Array("gps_customer","gps_order"),
		"fields":new Array("gps_customer.cust_lname","gps_customer.cust_fname","gps_order.order_total","gps_order.order_amount_owing","gps_order.mgi_gift","gps_order.order_id","gps_order.order_id","gps_order.order_note"),
		"postReport"	:
		(function(){
			return function(){
				var gifts = 0;
				var f = function(i,col){
					var amount = parseInt(col);
					if(!isNaN(amount) && amount > 0){
						gifts ++;
					}
				};
				mapRowByCol("mgi_gift",f);
				appendSummaryInfo("mgi_gift",formatCurrency(totalColumn("mgi_gift")) + " ( " + gifts + " gifts )");
			};
		}
		)()
		},
		"orderdetails":{
		"title"			:	"Order Details (Basic JAMNAVIS Report)",
		"description"	:	"Shows general information about customer orders",
		"relations"		:	new Array("gps_customer","gps_package","gps_order","gps_order_item"),
		"fields"		:	new Array("gps_customer.cust_fname","gps_customer.cust_lname","gps_package.package_name","gps_order.order_date","gps_order_item.order_item_quantity","gps_order_item.order_item_quantity"),
		"postReport"	:
		(function(){
			var summaries = new Array();
			return function(){
				var f = function(value,colArr){
					var totalPackages = 0;
					for(var i = 0; i < colArr.length; i++){
						var rowIndex = colArr[i];
						totalPackages += parseInt(getValue(rowIndex,"order_item_quantity")) ;
					}
					var summary = totalPackages + " " + value + " package(s)";
					appendSummaryInfo("package_name",summary);
					summaries.push(summary);
				};

				appendSummaryInfo("package_name","<br><br><b>Summary of Packages</b>");
				mapRowDistinctColumn("package_name",f);

				setSummaryInfo("order_item_quantity",totalColumn("order_item_quantity"));
				setSummaryInfo("order_amount_owing",formatCurrency(totalColumn("order_amount_owing")));
				setSummaryInfo("mgi_gift",formatCurrency(totalColumn("mgi_gift")));
				setSummaryInfo("order_total",formatCurrency(totalColumn("order_total")));
			};
		})()
		},
		"garminreport":{
		"title"			:	"Garmin Report",
		"description"	:	"Shows information about the locks performed.",
		"relations"		:	new Array("gps_lock"),
		"fields"		:	new Array("gps_lock.product_serial","gps_lock.lock_timestamp")
		},
		"packagesales":{
		"title"			:	"Packages Sold (w/o Customer Info)",
		"desciprtion"	:	"Shows information about the packages sold.",
		"relations"		:	new Array("gps_package","gps_order","gps_order_item"),
		"fields"		:	new Array("gps_package.package_name","gps_order_item.order_id","gps_order.order_date","gps_order_item.order_item_quantity",
		"gps_order_item.order_item_total")
		},
		"packagesaleswithcust":{
		"title"			:	"Packages Sold (w Customer Info)",
		"description"	:	"Shows information about the packages sold with customer information.",
		"relations"		:	new Array("gps_package","gps_order","gps_order_item","gps_customer"),
		"fields"		:	new Array("gps_package.package_name","gps_order_item.order_id","gps_order.order_date","gps_order_item.order_item_quantity",
		"gps_order_item.order_item_total","gps_customer.cust_fname","gps_customer.cust_lname")
		},
		"usercommissionreport":{
		"title"			:	"Commission Report",
		"description"	:	"Lists commission for users",
		"relations"		:	new Array("gps_commission","usr"),
		"fields"		:	new Array("gps_commission.order_id","usr.alias","gps_commission.amount")
		}
		,
		"allerrors":{
		"title"			:	"Error Report Review",
		"description"	:	"Lists all errors in the system",
		"relations"		:	new Array("gps_err","gps_parish","gps_area","gps_err_type"),
		"fields"		:	new Array("gps_err.err_id","gps_err_type.name","gps_parish.name","gps_area.name","description")
		}
		,
		"product_notes":{
		"title"			:	"Product Notes",
		"description"	:	"Lists all the products with their respective notes",
		"relations"		:	new Array("gps_product","gps_order","gps_customer"),
		"fields"		:	new Array("gps_product.order_id","gps_customer.cust_lname","gps_customer.cust_fname","gps_product.order_id","gps_product.product_serial","gps_product.comments")
		},
		"garmin_units":{
		"title"			:	"Customer Garmin Units",
		"description"	:	"Lists the names of all the garmin units customers are known to have",
		"relations"		:	new Array("gps_product","gps_order","gps_customer","garmin_unit","gps_package"),
		"fields"		:	new Array("gps_order.order_id","gps_customer.cust_lname","gps_customer.cust_fname","gps_package.package_name","gps_product.product_serial","garmin_unit.unit_name",""),
		"postReport"	:	function(){

			var units = {};
			var f = (function(){
				return function(i,value){
					if(!units[value]) units[value] = 1;
					else {
						units[value] = units[value]+1;
					}
				};
			})(units);
			mapRowByCol("unit_name",f);
			var summaries = new Array();
			for(var val in units){
				var amount = units[val];
				summaries.push(amount + " " + val + "(s)");
			}
			setSummaryInfo("unit_name",summaries.join(",<br>"));
		}
		}
		,
		"customer_updates":{
		"title"			:	"Customer Updates",
		"description"	:	"Lists all the customer updates and the dates that their updates were sent",
		"relations"		:	new Array("gps_customer","gps_order","gps_customer_update"),
		"fields"		:	new Array("gps_product.order_id","gps_customer.cust_lname","gps_customer.cust_fname","gps_customer_update.product_serial","gps_customer_update.version_no","gps_customer_update.dispatch_date")
		}		}

		;


		function report_init(){
			// Table names
			addAlias("gps_customer","Customer");
			addAlias("gps_order","Order");
			addAlias("gps_order_item","Order Item");
			addAlias("mgi_gift","MGI Complimentary Gift");
			addAlias("gps_order_payment","Payment");
			addAlias("gps_lock","Lock");

			addAlias("cust_id", "Customer");
			addAlias("cust_fname", "First Name");
			addAlias("cust_lname", "Surname");
			addAlias("cust_contact", "Contact");
			addAlias("cust_email", "Email");
			addAlias("cust_jam_id", "Id No.");
			addAlias("cust_jam_id_type", "Id Type");
			addAlias("cust_contact_sec", "Secondary Contact");
			addAlias("cust_email_sec", "Secondary Email");
			addAlias("cust_comment", "Other Info");

			addAlias("package_id", "Package");
			addAlias("package_name", "Package Name");
			addAlias("package_cost", "Package Cost US $");
			addAlias("package_icon", "Package Icon");
			addAlias("package_type", "Package Type");

			addAlias("order_id", "Order Id");
			addAlias("roe_id", "Date (YYYY-MM-DD)");
			addAlias("roe_curr_name", "Currency Name e.g. \"US\"");
			addAlias("roe_value", "Rate J$");

			addAlias("order_date", "Order Date");
			addAlias("order_note", "Order Note");
			addAlias("order_total", "Order Total J$");
			addAlias("order_item_total", "Order Item Total J$");
			addAlias("order_amount_owing", "Amount Owing");
			addAlias("order_item_quantity", "Order Item Qty");
			addAlias("order_complete", "Complete?");

			addAlias("payment_id", "Payment Number");
			addAlias("payment_date", "Payment Date");
			addAlias("payment_comment", "Notes");
			addAlias("payment_receipt", "Payment Receipt");
			addAlias("uid", "Cashier");
			addAlias("payment_amount", "Amount J$");
			addAlias("payment_type", "Payment Type");

			// Add processor functions
			addProcessFunction("package_cost", formatCurrency);
			addProcessFunction("amount", formatCurrency);
			addProcessFunction("roe_cost", formatCurrency);
			addProcessFunction("order_total", formatCurrency);
			addProcessFunction("order_item_total", formatCurrency);
			addProcessFunction("order_amount_owing", formatCurrency);
			addProcessFunction("mgi_gift", formatCurrency);
			addProcessFunction("payment_amount", formatCurrency);

			var cal = new Cal("calcontainer");
			$("#calcontainer").draggable();
			cal.hide();
		}


		function loadRelations(rel){
			if(!rel) var rel = relationListCopy;
			clearSelect();

			for(var i = 0 ; i < relationList.length; i++){
				var relationTmp = relationList[i];
				var relation = relationTmp.split(":")[0];
				var relationName = relationTmp.split(":")[1];
				addRelationToSelect(relation,relationName);
			}
		}



		function loadFields(rel,callback){
			if(!fieldList[rel]){
				var sel = $("#sel_relationList").get()[0];
				if(!sel) return;
				sel.disabled = true;

				$.get("crudmachine.php?sr=f&action=list&rel=" + rel,(function(){
					return function(data){
						if(!data) return;
						var obj = eval(data);
						if(!obj) return;
						if(obj.message.type == "notification"){
							var fields = obj.message.content.split(",");
							var fieldsObj = {};
							for(var i = 0; i < fields.length; i++){
								fieldsObj[fields[i]] = true;
							}
							fieldList[rel] = fieldsObj;
							addFields(rel,fieldsObj);

							var sel = $("#sel_relationList").get()[0];
							if(!sel) return;
							sel.disabled = false;

							if(callback) {
								callback();
							}
						}
					};
				})(rel,callback));
			}
			else {
				addFields(rel,fieldList[rel]);
				callback();
			}
		}



		function addFields(rel,fields){
			var divFieldList = $("#div_fieldList").get()[0];
			if(!divFieldList) return;

			var divName = "div_fieldList_" + rel;
			var divRelFieldList = null;
			if(document.getElementById(divName)){
				divRelFieldList = document.getElementById(divName);
				divRelFieldList.innerHTML = "";
			} else {
				divRelFieldList = document.createElement("div");
				divRelFieldList.id = divName;
				divRelFieldList.className = "div_relFieldList";
				divFieldList.appendChild(divRelFieldList);
			}

			divRelFieldList.innerHTML = "<span>" + getAlias(rel) + ":</span>";
			divRelFieldList.rel = rel;


			// Now to add the fields to the field list
			for(var field in fields){
				var div = document.createElement("div");
				div.id = "div_" + field + "_" + rel + "_container";
				div.className = "div_field";
				div.title = getAlias(field);
				div.field = field;
				var checkBox = document.createElement("input");
				checkBox.setAttribute("type","checkbox");
				checkBox.id = "cb_" + field + "_" + rel;
				div.cbId = "cb_" + field + "_" + rel;
				div.appendChild(checkBox);
				if(isTicked(rel,field)){
					checkBox.checked = false;
				}
				checkBox.rel = rel;
				checkBox.field = field;
				checkBox.onclick = function(){
					setTick(this.rel,this.field,this.checked);
				};

				var aAddConstraint = document.createElement("a");
				aAddConstraint.href = "javascript:void(0)";
				aAddConstraint.rel = rel;
				aAddConstraint.field = field;
				aAddConstraint.onclick = function(){
					addConstraint(this.rel,this.field);
				};
				aAddConstraint.appendChild(document.createTextNode("+"));
				aAddConstraint.title = "Add a constraint for " + getAlias(field);
				div.appendChild(document.createTextNode(getAlias(field)));
				div.appendChild(document.createTextNode(" "));
				div.appendChild(aAddConstraint);

				divRelFieldList.appendChild(div);
			}
		}


		function isTicked(rel,field){
			if(fieldList[rel]){
				var fieldsObj = fieldList[rel];
				if(fieldsObj[field] == true) return true;
				return false;
			}

			return false;
		}

		function setTick(rel,field,val){
			window.status = "Trying to set " + rel + "[" + field + "] to: " + val;
			try{
				fieldList[rel][field] = val;
			} catch(m){
				alert(m);
			}
		}



		function getConstraintCount(rel,field){
			var i = 0;
			$("#div_constraintList .div_fieldConstraint").each(function(){
				if(this.id.match("div_" + rel + "_" + field + "_[0-9]+")){
					i++;
				}
			});
			return i;
		}

		function addConstraint(rel,field){
			var divConstraintList = $("#div_constraintList").get()[0];
			if(!divConstraintList) return;

			var divRelFieldConstraint = document.createElement("div");
			divRelFieldConstraint.id = "div_" + rel + "_" + field + "_" + getConstraintCount(rel,field);
			divRelFieldConstraint.className = "div_fieldConstraint";
			divRelFieldConstraint.rel = rel;
			divRelFieldConstraint.field = field;

			// Create the table
			var consObj = constructConstraintTable(rel,field);
			divRelFieldConstraint.appendChild(consObj.tbl);

			divRelFieldConstraint.sel = consObj.sel;
			divRelFieldConstraint.inp = consObj.inp;

			divConstraintList.appendChild(divRelFieldConstraint);

		}


		function constructConstraintTable(rel,field){
			var tbl = document.createElement("table");
			var tbody = document.createElement("tbody");
			tbl.appendChild(tbody);

			var tr = document.createElement("tr");

			var td = document.createElement("td");
			td.innerHTML = getAlias(field);
			td.style.width = "150px";
			tr.appendChild(td);


			// Deal with the select bar
			td = document.createElement("td");
			td.style.width = "150px";
			var sel = document.createElement("select");
			var option;

			option = document.createElement("option");
			option.value = "equal";
			option.appendChild(document.createTextNode("Equals"));
			sel.appendChild(option);
			sel.style.width = "100%";


			option = document.createElement("option");
			option.value = "lt";
			option.appendChild(document.createTextNode("Less Than"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "gt";
			option.appendChild(document.createTextNode("Greater Than"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "bd";
			option.appendChild(document.createTextNode("Before"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "ad";
			option.appendChild(document.createTextNode("After"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "sw";
			option.appendChild(document.createTextNode("Starts With"));
			sel.appendChild(option);


			option = document.createElement("option");
			option.value = "ew";
			option.appendChild(document.createTextNode("Ends With"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "customlike";
			option.appendChild(document.createTextNode("Like"));
			sel.appendChild(option);



			td.appendChild(sel);
			tr.appendChild(td);

			// Input field
			td = document.createElement("td");
			var inp = document.createElement("input");
			td.appendChild(inp);
			tr.appendChild(td);

			td = document.createElement("td");
			var aRemoveConstraint = document.createElement("a");
			aRemoveConstraint.href = "javascript:void(0)";
			aRemoveConstraint.rel = rel;
			aRemoveConstraint.field = field;
			aRemoveConstraint.appendChild(document.createTextNode("Remove"));
			aRemoveConstraint.cId = getConstraintCount(rel,field);
			aRemoveConstraint.onclick = function(){
				removeConstraint(this.rel,this.field,this.cId);
			};
			td.appendChild(aRemoveConstraint);
			tr.appendChild(td);


			tbody.appendChild(tr);
			return {"tbl":tbl,
			"sel":sel,
			"inp":inp
			};
		}

		function removeConstraint(rel,field,index){

			if(!field){
				$("#div_constraintList DIV").each(function(){
					if(this.rel == rel){
						this.parentNode.removeChild(this);
					}
				});

			} else {
				var id = "div_" + rel + "_" + field + "_" + index;
				var div = $("#" + id).get()[0];
				if(div) div.parentNode.removeChild(div);
			}
		}

		function allConstraintsValid(){
			$("#div_constraintList .div_fieldConstraint").each(function(){
				if(this.inp.value == "") return false;
			});
			return true;
		}

		function getConstraints(){
			var constraints = new Array();
			$("#div_constraintList .div_fieldConstraint").each(function(){
				var constraintType = getComponentValue(this.sel);
				var constraintValue = this.inp.value;
				var ct = constraintType;
				var op = "";
				var constraintStr = "";

				if(ct == "equal") op = "=";
				if(ct == "lt") op = "<";
				if(ct == "gt") op = ">";
				constraintStr = this.rel + "." + this.field + op + "'" + constraintValue + "'";

				if(ct == "bd") constraintStr = this.rel + "." + this.field + " < " + "'#" + constraintValue + "#'";
				if(ct == "ad") constraintStr = this.rel + "." + this.field + " > " + "'#" + constraintValue + "#'";


				if(ct == "sw") constraintStr = this.rel + "." + this.field + " LIKE " + "'" + constraintValue + "%'";
				if(ct == "ew") constraintStr = this.rel + "." + this.field + " LIKE " + "'%" + constraintValue + "'";
				if(ct == "customlike") constraintStr = this.rel + "." + this.field + " LIKE " + "'%" + constraintValue + "%'";

				constraintStr = this.rel + "." + this.field + "::" + constraintStr;

				if(constraintStr) constraints.push(constraintStr);

			});

			return constraints;

		}


		function clearSelect(){
			var sel = $("#sel_relationList").get()[0];
			if(!sel) return;
			while(sel.options.length > 0) sel.removeChild(sel.options[sel.selectedIndex]);
		}

		function addRelationToSelect(relation,caption){
			var sel = $("#sel_relationList").get()[0];
			if(!sel) return;

			var option = document.createElement("option");
			option.value = relation;
			option.appendChild(document.createTextNode(caption));
			sel.appendChild(option);
		}

		function getIndexOfRelation(rel){
			var sel = $("#sel_relationList").get()[0];
			for(var i = 0; i < sel.options.length; i++){
				if(sel.options[i].value == rel) return i;
			}

			return null;
		}

		function addSelectedRelation(index,callback) {
			if(!index) var index = -1;
			var sel = $("#sel_relationList").get()[0];
			var divRelList = $("#div_relationList").get()[0];

			if(!sel) return;
			if(!divRelList) return;


			if(index != -1) if( (index+1) > sel.options.length) return;



			if(sel.options.length > 0) {
				if(index == -1) index = sel.selectedIndex;
				var relation = sel.options[index].value;
				var relationName = sel.options[index].text;
				sel.removeChild(sel.options[index]);
				// Loop through array
				for(var i = 0; i < relationList.length; i++){
					if(relationList[i] == relation){
						relationList.splice(i,1);
					}
				}
				divRelList.appendChild(constructRelationDiv(relation,relationName));
				loadFields(relation,callback);
			} else {
			}
		}

		function constructRelationDiv(relation,caption){
			var div = document.createElement("div");
			div.id = "div_relation_" + relation;
			div.className = "div_relation";
			div.rel = relation;
			div.caption = caption;
			var a = document.createElement("a");
			a.innerHTML = "Remove";
			a.relation = relation;
			a.caption = caption;
			a.href = "javascript:void(0)";
			a.onclick = function(){
				removeRelation(this.relation,this.caption);
			};

			div.innerHTML = caption + "&nbsp;&nbsp;";
			div.appendChild(a);

			return div;
		}

		function removeRelation(relation,caption){
			//			alert("Asked to remove: " + relation + " and " + caption);
			relationList.push(relation + ":" + caption);
			var divRelList = $("#div_relationList").get()[0];
			divRelList.removeChild($("#div_relation_" + relation).get()[0]);
			addRelationToSelect(relation,caption);
			// Remove from list of fields
			var divRelFieldList = $("#div_fieldList_" + relation).get()[0];
			if(divRelFieldList) divRelFieldList.parentNode.removeChild(divRelFieldList);

			removeConstraint(relation);
		}

		function removeAllRelations() {
			$("#div_relationList DIV").each(function(){
				removeRelation(this.rel,this.caption);
			});
		}

		function downloadReportAsXls(){
			var selSavedReportList = $("#sel_savedReports").get()[0];
			var val = selSavedReportList.options[selSavedReportList.selectedIndex].value;
			if(savedReports[val]){
				var savedReport = savedReports[val];
				generateCustomReport(savedReport.relations,savedReport.fields,savedReport.postReport,"xls");
			}
		}


		function generateReport(postReportFunction,printerType) {
			$("#btn_generateReport").get()[0].disabled = true;
			$("#btn_runCustomReport").get()[0].disabled = true;

			if(!printerType) var printerType = "j";

			var relations = new Array();
			var fields = new Array();

			$("#div_relationList DIV").each(function(){
				relations.push(this.rel);
			});


			$("#div_fieldList .div_relFieldList").each(function(){
				var relation = this.rel;
				$("#" + this.id + " .div_field").each(function(){
					var cb = $("#" + this.cbId).get()[0];
					if(cb.checked){
						fields.push(relation + "." + cb.field);
					}
				});
			});

			if(relations.length > 0){
				var relationStr = relations.join(",");
				var fieldStr = fields.join(",");
				var url = "crudmachine.php?action=read&rel=" + relationStr + "&cl=" + fieldStr + "&p=" + printerType;

				// Grab all the constraints
				if(!allConstraintsValid()){
					alert("All constraints are not valid!");
					$("#btn_generateReport").get()[0].disabled = false;
					$("#btn_runCustomReport").get()[0].disabled = false;
					return;
				}

				var constraints = getConstraints();
				if(constraints.length > 0){
					url += "&sc=" + constraints.join(":::");
				}


				if(printerType == "xls"){ // excel printer
					window.open(url); // open a window where the file can be downloaded
					$("#btn_generateReport").get()[0].disabled = false;
					$("#btn_runCustomReport").get()[0].disabled = false;

				} else {
					//				alert(url);
					$.get(url,(function(){
						return function(data){
							$("#btn_generateReport").get()[0].disabled = false;
							$("#btn_runCustomReport").get()[0].disabled = false;
							if(!data) { alert("No data returned, sorry"); return; }

							var obj;

							try { obj = eval(data); } catch(e) {
								alert("Error parsing data. Will attempt to filter data.");
								return;
							}

							for(var item in obj){
								// should be the first item
								var listData = obj[item];
								// should be an array
								printData(listData);
								summarizeTbl(lastReportId);

								if(postReportFunction){
									postReportFunction();
								} else {
									// Check each column now
									var node = getRow(0);
									if(node){
										var rowFormatted = encapsulateRow(node,true,true);
										var row = encapsulateRow(node,true);
										for(var item in row){
											// determine what kind of data is there ...
											if(!isNaN(row[item])){
												if(item.indexOf("_id") == (item.length - 3)) continue; //do not total id columns
												// total that column
												// check if we used currency
												if(rowFormatted[item].indexOf("$") > 0){
													setSummaryInfo(item,formatCurrency(totalColumn(item)));
												} else {
													setSummaryInfo(item,totalColumn(item));
												}
											} else {

											}
										}
									}

								}

								break;
							}
						};
					})(postReportFunction));

				}
			} else {
				alert("Nothing to report on!");
				$("#btn_generateReport").get()[0].disabled = false;
				$("#btn_runCustomReport").get()[0].disabled = false;

			}
		}

		function summarizeTbl(tblId){
			var tblDataCacheObj = {
			"rowCount":0,
			"colCount":0,
			"distinctcolumns":{}
			};

			var i = 0;
			jQuery("#" + tblId + " TR").each(function(){
				if(this.className == "headerRow" || this.className == "finalRow") return;
				if(this.hasChildNodes()){
					var kiddies = this.childNodes;
					for(var j = 0; j < kiddies.length; j++){
						var kid = kiddies[j];
						if(kid.nodeName.toLowerCase() == "td"){
							tblDataCacheObj["colCount"]++;
							// Start storing unique values
							var rawTitle = kid.rawTitle;
							var rawValue = kid.rawValue;

							if(!tblDataCacheObj["distinctcolumns"][rawTitle]){
								tblDataCacheObj["distinctcolumns"][rawTitle] = {};
							}

							if(!tblDataCacheObj["distinctcolumns"][rawTitle][rawValue]) tblDataCacheObj["distinctcolumns"][rawTitle][rawValue] = new Array();
							tblDataCacheObj["distinctcolumns"][rawTitle][rawValue].push(i);
						}
					}
				}
				i++;
				tblDataCacheObj["rowCount"]++;
			});

			tblDataCache[tblId] = tblDataCacheObj;
		}

		/**
		* Calls f on every distinct value in the column "column"
		*/
		function mapRowDistinctColumn(column,f){
			if(tblDataCache[lastReportId]){
				var tblDataCacheObj = tblDataCache[lastReportId];
				var distinctCols = tblDataCacheObj["distinctcolumns"];
				if(distinctCols[column]){
					for(var val in distinctCols[column]){
						f(val,distinctCols[column][val]);
					}
				}
			}
		}


		/**
		* Apply function f to every row in column "column"
		*/
		function mapRowByCol(column,f){
			for(var i = 0; i < getRowCount(); i++){
				var node = getRow(i);
				if(node){
					var row = encapsulateRow(node,true);
					f(i,row[column]);
				}
			}
		}

		function totalColumn(column){
			var total = 0;
			mapRowByCol(column,function(i,value){
				if(!isNaN(parseInt(value))) total += parseInt(value);
			});
			return total;
		}

		function getRowCount(){
			if(lastReportId){
				return tblDataCache[lastReportId]["rowCount"];
			}
			return 0;
		}

		function mapRowAssoc(f){
			if(lastReportId){
				var i = 0;
				jQuery("#" + lastReportId + " TR").each(function(){
					f(i,encapsulateRow(this,true));
					i++;
				});
			}
		}

		function getRow(i,doNotIncrement){
			if(!doNotIncrement) i++;
			if(lastReportId){
				var node = jQuery("#" + lastReportId + " TR").get()[i];
				if(node) return node;
			}
			return null;
		}


		function getValue(row,columnName){
			var node = getRow(row);

			if(node){
				var row = encapsulateRow(node,true);
				if(row[columnName]) return row[columnName];
			} else {
			}
			return null;
		}

		function setSummaryInfo(column,str){
			if(lastReportId){
				jQuery("#" + lastReportId + " .finalRow TD").each(function(){
					if(this.rawTitle == column){
						this.innerHTML = str;
					}
				});
			}
		}


		function clearSummaryInfo(column){
			if(lastReportId){
				jQuery("#" + lastReportId + " .finalRow TD").each(function(){
					if(this.rawTitle == column){
						this.innerHTML = "";
					}
				});
			}
		}


		function appendSummaryInfo(column,str){
			if(lastReportId){
				jQuery("#" + lastReportId + " .finalRow TD").each(function(){
					if(this.rawTitle == column){
						var space = "<br>";
						if(this.innerHTML == "&nbsp;" || this.innerHTML == "" || this.innerHTML == " ") { this.innerHTML = ""; space =""; }
						this.innerHTML = this.innerHTML  + space + str;
					}
				});
			}
		}


		function getSumamryInfo(column){
			if(lastReportId){
				jQuery("#" + lastReportId + " .finalTr TD").each(function(){
					if(this.rawTitle == column){
						return this.innerHTML;
					}
				});
			}
			return null;
		}


		function encapsulateRow(trObj,associative,useFormatted){
			var row = {};
			var arr = new Array();
			if(trObj.hasChildNodes()){
				var j = 0;
				for(var i = 0; i < trObj.childNodes.length; i++){
					var kid = trObj.childNodes[i];
					if(kid.nodeName.toLowerCase() == "td"){
						var value = kid.rawValue;
						if(useFormatted) value = kid.innerHTML;
						row[kid.rawTitle] = value;
						arr[j] = value;
						j++;
					}
				}
			}
			if(associative) return row;
			else return arr;
		}


		var tblDataCache = {};

		var customReports = {};

		function generateCustomReport(relationArr,fieldArr,postReportFunction,printerType){
			$("#btn_runCustomReport").get()[0].disabled = true;

			removeAllRelations();

			var relCount = relationArr.length;
			var relationIndices = {};
			var fieldCount = fieldArr.length;

			var d = new Date();
			var utc = Date.UTC(d.getFullYear(),d.getMonth(),d.getDate());
			customReports[utc] = {
			"completed":0,
			"total":relCount,
			"fields":fieldArr,
			"relations":relationArr
			};


			for(var i = 0; i < relCount; i++) {
				var relation = relationArr[i];
				var index = getIndexOfRelation(relation);
				addSelectedRelation(index,(function(){
					return function(){
						customReports[utc]["completed"] = customReports[utc]["completed"] + 1;

						if(customReports[utc]["completed"] == customReports[utc]["total"]) {


							var fields = customReports[utc]["fields"];
							var relations = customReports[utc]["relations"];

							for(var r = 0; r < relations.length; r++){
								var relation = relations[r];
								// Uncheck all first
								var cssStr = "#div_fieldList_" + relation + " INPUT";
								//								alert(cssStr);
								$(cssStr).each(function(){
									if(this.type == "checkbox") this.checked = false;
								});
							}

							for(var j = 0; j < fields.length; j++){
								var field = fields[j];
								var fieldName = field.split(".")[1];
								var relation = field.split(".")[0];
								var cbId = "cb_" + fieldName + "_" + relation;
								var cb = $("#" + cbId).get()[0];
								if(cb) cb.checked = true;
							}

							generateReport(postReportFunction,printerType);


						}

					};
				})(utc));
			}
		}

		var reportIndex = 0;
		var lastReportId = null;

		function printData(arr){ // takes an array of objects
			if(arr.length == 0){
				alert("No results found for your query, sorry.");
				return;
			}

			var divReport = $("#div_report").get()[0];
			if(divReport) {
				divReport.innerHTML = "";

				var tbl = document.createElement("table");
				var tblId = "jis_report_" + reportIndex;
				tbl.id = tblId;
				reportIndex++; // increase the index
				lastReportId = tbl.id;
				tbl.setAttribute("cellpadding",5);
				tbl.setAttribute("cellspacing",0);


				var thead = document.createElement("thead");
				var tbody = document.createElement("tbody");
				var tfoot = document.createElement("tfoot");


				tbl.appendChild(thead);
				tbl.appendChild(tbody);
				tbl.appendChild(tfoot);

				// Get the first row
				var row = arr[0];
				var tr = document.createElement("tr");
				var finalTr = document.createElement("tr");
				finalTr.className = "finalRow";

				tr.className = "headerRow";
				for(var item in row){
					var td = document.createElement("th");
					td.innerHTML = getAlias(item);
					tr.appendChild(td);

					// Add a row to the final row tr
					td = document.createElement("td");
					td.rawTitle = item;
					td.setAttribute("valign","top");
					//					td.setAttribute("cellpadding",5);
					td.innerHTML = "&nbsp;";
					finalTr.appendChild(td);

				}

				thead.appendChild(tr);

				// Now for the actual rows ...
				for(var i = 0; i < arr.length; i++){
					var tr = document.createElement("tr");
					if(parseInt(i/2) == (i/2)){
						tr.className = "evenRow";
					} else {
						tr.className = "oddRow";
					}
					var row = arr[i];
					for(var item in row){
						var td = document.createElement("td");
						td.rawValue = row[item];
						td.rawTitle = item;
						td.innerHTML = processVar(item,row[item]);
						tr.appendChild(td);
					}
					tbody.appendChild(tr);
				}


				tfoot.appendChild(finalTr);
				tbl.tbody = tbody;
				tbl.summaryRow = finalTr;

				divReport.appendChild(tbl);
				divReport.appendChild(document.createTextNode(arr.length + " rows in your report."));

				var formatter = function(node){
					var strHTML = node.innerHTML;
					var result = strHTML;
					if(strHTML.indexOf("\$")){
						result = trim(strHTML.substring(strHTML.indexOf("\$"),result.length));
					}
					return result;
				};
				jQuery("#" + tblId).tablesorter({"textExtraction":formatter});
				$("#" + tblId).bind("sortEnd",function() {
					var tbody = this.tbody;
					for(var i = 0; i < tbody.childNodes.length; i++){
						var tr = tbody.childNodes.item(i);
						if(i % 2 == 0){
							tr.className = "evenRow";
						} else {
							tr.className = "oddRow";
						}
					}
				});

			}
		}

		function loadSavedReports(){
			var selSavedReportList = $("#sel_savedReports").get()[0];
			if(!selSavedReportList) return;

			for(var item in savedReports){
				var savedReport = savedReports[item];
				var option = document.createElement("option");
				option.value = item;
				var title = (savedReport["title"]);
				option.appendChild(document.createTextNode(title));
				selSavedReportList.appendChild(option);
			}


			showReport(selSavedReportList.options[selSavedReportList.selectedIndex].value);
		}

		function showReport(val){
			var divReportDescription = $("#div_reportDescription").get()[0];
			if(divReportDescription){
				var description = savedReports[val]["description"];
				if(!description) {
					description = "No description available";
				}
				divReportDescription.innerHTML = description;
			} else {

			}
		}

		function runCustomReport(){
			var selSavedReportList = $("#sel_savedReports").get()[0];
			var val = selSavedReportList.options[selSavedReportList.selectedIndex].value;
			if(savedReports[val]){
				var savedReport = savedReports[val];
				generateCustomReport(savedReport.relations,savedReport.fields,savedReport.postReport);
			}
		}

		function getComponentValue(obj, proc) {
			if(!obj) return null;
			var val = obj.type == "select" ? obj.options[obj.selectedIndex].value
			: obj.value;

			if (proc) {
				var item = grabColumnName(obj.id);
				val = process(item, val);
			}

			return val;
		}

		</script>
	</head>
<body onLoad="loadRelations(); report_init(); loadSavedReports();">
<div id="calcontainer">
</div>
<div id="div_customizeReport">

<select id="sel_relationList">
</select>
<a href="javascript:void(0);" onclick="addSelectedRelation();">Add Relation</a>

<div id="div_relationList">
<b>Relation List:</b><br>
</div>

<div id="div_fieldList">
<b>List of Fields:</b><br>
</div>

<div id="div_constraintList">
<b>List of Constraints:</b><br>

</div>
<br>
<input type="button" value="Generate Report" onclick="generateReport();" id="btn_generateReport"/>
<br>
<a href="javascript:void(0);" onclick="$('#div_customizeReport').hide('slow',function(){$('#btn_runCustomReport').get()[0].disabled = false;});" style="color:white; font-weight:bold;">Close Customization Window</a>

</div>



Pick a saved report format from below or click <a href="javascript:void(0);" onclick="$('#div_customizeReport').show('slow',function(){$('#btn_runCustomReport').get()[0].disabled = true;}); " style="font-weight:bold;">here</a> to 
customize the report.
<br><a href="admin.php">Admin Page</a>
<br>
<select id="sel_savedReports" style="width:150px;" onchange="showReport(this.options[this.selectedIndex].value);">
</select>&nbsp;&nbsp;<input type="button" value="Run Custom Report" onClick="runCustomReport()" id="btn_runCustomReport"/>
&nbsp;&nbsp;<input type="button" value="Download Report" onClick="downloadReportAsXls()"/>
<br>
<div id="div_reportDescription">

</div>

<div id="div_report" style="float:none;">
</div>

</body>
</html>
