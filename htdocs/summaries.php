<html>
    <head>
        <title>JAMNAV Summaries</title>
        <style>
            body {
                background-image:url("images/dark_dotted.png");
                background-repeat: repeat;
            }

            .navigation {
                position:fixed;
                top:0px;
                left:0px;
            }

            .navigation .link {
                font-family:"Century Gothic";
                font-size:15px;
                color:#e7ba3b;
                padding:14px;
                background-color:#333333;
                cursor:pointer;
                margin-bottom: 4px;
                border-bottom:1px #000 solid;
            }

            .charts {
                margin-left:100px;
            }

            TABLE {
                width:300px;
            }


            .chart_container {
                margin:10px;
                padding:5px;
            }

            .data {
                margin-top:14px;
            }

            .year {
                padding:10px;
                font-weight: bold;
                font-size:16px;
                background-color: #e7ba3b;
                border-bottom:2px #2b877c solid;
            }


            BODY,TR,TD {
                font-size:11px;	
                font-family:"Century Gothic";
            }

            TD {
                background-color:#333333;
                border-top:none;
                border-left:none;
                border-right:none;
                text-align: center;
                font-weight: bold;
                color:#e7ba3b;
                font-size:13px;
            }

            TH {
                cursor:pointer;
                color:#e7ba3b;
                font-weight:bold;
                font-size:15px;
                border-bottom:2px solid #2b877c;
            }

            .odd TD {
                background-color:rgb(240,240,255);
            }

            .header TH {
                font-weight:bold;
            }

        </style>
        <script LANGUAGE="Javascript" SRC="js/FusionCharts.js"></script>

        <script>
            
            var chartDataUrl = "cd.php?id=";
            
            function ChartConfig(id){
                this.load = function(domId,year){
                    var self =  this;
                    domId = domId + "_" + year;
                    var url = chartDataUrl + id + "&year=" + year;
                    $("#" + domId).remove();
                    $(".charts #year_" + year).after($("<div id='" + domId + "'/>"));
                    $("#" + domId).html("Loading ...");
                    $.get(url,(function(){
                        return function(data){
                            if(data){
                                $("#" + domId).empty();
                                var width = $(document).width();
                                var chart = new FusionCharts("fc/FCF_Column3D.swf",domId, width-250, "400", "0", "1");
                                chart.setDataXML(data);
                                
                                var chartDiv = $("<div id='chart_" + domId + "'/>").appendTo("#" + domId);
                                var chartData = $("<div class='data'/>").appendTo("#" + domId);
                                chart.render("chart_" + domId);
                                $("#" + domId + " .data").load(url + "&f=html",function(){
                                    $("#" + domId + " table").tablesorter();
                                });
                                $("#" + domId).addClass("chart_container");
                            }
                        };
                    })(self,domId));
                };
            }
            
            
        </script>
        <script src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
        <script src="http://autobahn.tablesorter.com/jquery.tablesorter.min.js"></script>
        <script src="js/jquery.scrollTo-1.4.2-min.js"></script>
        <script>
            
            
            function loadCharts(year){
                var updates = new ChartConfig("updates");
                updates.load("chart_updates",year);
                var orders = new ChartConfig("orders");
                orders.load("chart_orders",year);
            }
            
            $(document).ready(function(){
                // add the year markers...
                var d = new Date();
                var year = d.getFullYear();
                var numberYears = 4;
                
                for(var i = 0; i < numberYears; i++){
                    var y = year - i;
                    var div = $("<div class='year' id='year_" + y + "'>Year "+ y + "</div>");
                    div.appendTo(".charts");
                    div.append("<a name='" + y + "'></a>");
                    
                    var link = $("<div class='link'>" + y + "</div>").appendTo(".navigation");
                    link.click((function(){
                        var _y = y;
                        return function(){
                            alert("Going to " + _y);
                            $("#year_" + _y).scrollTo();
                        };
                    })(y));
                    
                }
                
                for(var i = 0; i < numberYears; i++){
                    loadCharts(year - i);
                }
            });
            
            
            String.prototype.toTitleCase = function () {
                var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|of|on|or|the|to|vs?\.?|via)$/i;

                return this.replace(/([^\W_]+[^\s-]*) */g, function (match, p1, index, title) {
                    if (index > 0 && index + p1.length !== title.length &&
                        p1.search(smallWords) > -1 && title.charAt(index - 2) !== ":" && 
                        title.charAt(index - 1).search(/[^\s-]/) < 0) {
                        return match.toLowerCase();
                    }

                    if (p1.substr(1).search(/[A-Z]|\../) > -1) {
                        return match;
                    }

                    return match.charAt(0).toUpperCase() + match.substr(1);
                });
            };
            
        </script>
    </head>
    <body>
        <div class="navigations"></div>
        <div class="charts"></div>
    </body>


</html>
<?php
?>
