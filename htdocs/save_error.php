<?php

include_once ("../include/dbal.php");
include_once ("error_mailer.php");

$dbal = new DbAbstractionLayer ( );
define ( "GPS_ROAD_ERROR", "road" );
define ( "GPS_POI_ERROR", "poi" );
define ( "GPS_ROUTING_ERROR", "routing" );

if (! $dbal->connect ())
die ( "Cannot connect to the database!" );

//print_r ( $_GET );
$parish_id = $_GET ["sel_parish"];
if (empty ( $parish_id ))
$parish_id = 69;

$area_id = $_GET ["sel_area"];
if (empty ( $area_id )) {
	$area_id = "unknown";
} else {
	if (ereg ( "[0-9]+:[0-9]+", $area_id )) {
		$pieces = explode ( ":", $area_id );
		$area_id = $pieces [0];
	} else if (ereg ( "^unknown", $area_id )) {
		$area_id = "unknown";
	}
}

$new_area = $_GET ["new_area"];

$err_type = $_GET ["err_type"];

$feature_name = $_GET ["feature_name"];
if (empty ( $feature_name ))
unset ( $feature_name );

if (! empty ( $_GET ["feature_value"] ))
$feature_value = $_GET ["feature_value"];

$err_description = $_GET ["err_description"];

$email_addr = $_GET ["email_addr"];

$email_sent = false;

$submitBtn = $_GET ["submitBtn"];

if ($submitBtn == "Submit") {

	// Versioning and coords
	$db_lat = 0;
	$db_lon = 0;

	// Deal with the northing
	if(is_numeric($_GET["latd"]) && is_numeric($_GET["latm"])) {
		$db_lat = floatval($_GET["latd"]) + floatval($_GET["latm"]) / 60;
	}

	if(is_numeric($_GET["lond"]) && is_numeric($_GET["lond"])){
		$db_lon = floatval($_GET["lond"]) + floatval($_GET["lonm"]) / 60;
	}
	
	$db_version = $_GET["version"];

	if(!is_numeric($db_lat)) $db_lat = 0;
	if(!is_numeric($db_lon)) $db_lon = 0;
	if(!is_numeric($db_version)) $db_version = 0;

	if(empty($db_lat)) $db_lat = 0;
	if(empty($db_lon)) $db_lon = 0;
	if(empty($db_version)) $db_version = 'NULL';



	// All vars will be prefixed with "db" are for entry into the database
	$db_parish_id = $parish_id;

	// Do we have a good area selection?
	// Create the area if one is supplied
	if ($area_id == "unknown" && ! empty ( $new_area )) {
		// Create the new area that the user claims does not exist
		$sql = "SELECT nextval('gps_area_gid_seq')";
		$result = $dbal->queryDb ( $sql );
		if ($result) {
			while ( $row = $dbal->loopResult ( $result ) ) {
				$db_area_id = $row ["nextval"];
				break;
			}

			$sql = "INSERT INTO gps_area (area_id,name,parish_id,custom) VALUES ($db_area_id,'$new_area',$db_parish_id,True)";
			$dbal->queryDb ( $sql );
		}

	} else if ($area_id == "unknown" && empty ( $new_area )) {
		$db_area_id = - 1;
	} else {
		$db_area_id = $area_id;
	}

	// What did the user tell us about the error?
	$db_err_type = trim ( $err_type ); // acceptable to take this as it is


	// Do we have a good point of interest selection
	if (isset ( $feature_value )) {
		$db_feature_value = $feature_value;
	} else {
		$db_feature_name = $feature_name;
		// We need to create a custom poi for this ... this is either a road or poi
		if ($db_err_type == GPS_ROAD_ERROR) {
			$sql = "SELECT nextval('gps_road_gid_seq')";
			$result = $dbal->queryDb ( $sql );
			if ($result) {
				while ( $row = $dbal->loopResult ( $result ) ) {
					$db_feature_value = $row ["nextval"];
					break;
				}
			}

			$sql = "INSERT INTO gps_road (gid,road_id,parish_id,area_id,name,custom) VALUES ($db_feature_value,$db_feature_value,$db_parish_id,$db_area_id,'$db_feature_name',True)";
			$dbal->queryDb ( $sql );

		} else if ($db_err_type == GPS_POI_ERROR) {

			$sql = "SELECT nextval('gps_poi_gid_seq'::regclass)";
			$result = $dbal->queryDb ( $sql );
			if ($result) {
				while ( $row = $dbal->loopResult ( $result ) ) {
					$db_feature_value = $row ["nextval"];
				}
			}

			// Create the poi
			$sql = "INSERT INTO gps_poi (gid,name,poi_id,parish_id,area_id,custom) VALUES ($db_feature_value,'$db_feature_name',$db_feature_value,$db_parish_id,$db_area_id,TRUE)";
			$result = $dbal->queryDb ( $sql );
		} else {

		}
	}



	if (empty ( $db_feature_value )) {
		// This happens if a POI was selected
		$sql = "SELECT nextval('gps_routing_seq')";
		$result = $dbal->queryDb($sql);
		while($row = $dbal->loopResult($result)){
			$db_feature_value = $row["nextval"];
			break;
		}
	} else {
	}

	// Get all the tags
	$db_tags = array ();
	foreach ( $_GET as $key => $value ) {
		if ($value == "on")
		$db_tags [] = $key;
	}

	$db_err_description = $err_description;

	$db_email_addr = $email_addr;

	// Create the main row for the gps_err table
	$db_sql = "SELECT nextval('gps_err_seq')";
	$result = $dbal->queryDb ( $db_sql );
	if ($result) {
		while ( $row = $dbal->loopResult ( $result ) ) {
			$db_err_gid = $row ["nextval"];
			break;
		}
	}
	
	$log_ts = date("Y-m-d h:i:s a");

	$db_sql = "INSERT INTO gps_err (err_id,fid,description,err_type,area_id,parish_id,lat,lon,version,email_addr,log_ts) VALUES ($db_err_gid,$db_feature_value,'$db_err_description','$db_err_type',$db_area_id,$db_parish_id,$db_lat,$db_lon,$db_version,'$email_addr','$log_ts')";
	$result = $dbal->queryDb ( $db_sql );

	// Now create the tags
	for($i = 0; $i < count ( $db_tags ); $i ++) {
		$db_tag = $db_tags [$i];
		$sql = "INSERT INTO gps_err_has_tag VALUES ($db_err_gid,'$db_tag')";
		try {
			@$dbal->queryDb ( $sql );
		} catch ( Exception $e ) {
		}
	}
	
	if(isset($email_addr) && !empty($email_addr)){
		$email_sent = notify_customer($email_addr,$db_err_gid);
	}
	
}

?>
<html>
<head>
<title>Thank you for reporting an error</title>
<style type="text/css">
BODY,TD {
	font-size: 11px;
	font-family: "Verdana,Georgia,Sans Serif,Trebuchet MS";
}
</style>
<script>
window.setTimeout("window.location='index.php'",50000);
</script>
</head>
<body>
<table width="400px">
	<tr>
		<td><img src="jaanav.gif"><br />
		<span style="text-align:center;">Thank you for making this report.
		<?php
		echo " You can track the status of this error by clicking <a href='track_error.php?id=$db_err_gid'>here</a>.";
		
		if($email_sent){
			echo " We have also sent an email to your address ($email_addr) with the error number and a link by which you can track the status of the error. ";
		} else {
			echo " If you left your email address, you will be contacted when the error is fixed. ";
		}
		?>
		<br><br>You will now be redirected to the main page, you can report more errors there. Click <a href='index.php'>here</a> if this does not happen.</span></td>
	</tr>
</table>
</body>
</html>

