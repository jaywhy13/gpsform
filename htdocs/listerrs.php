<?php
include_once("../include/permguru.php");

$pg = new PermGuru("gpsform");


?>
<html>
<head>
<title>Paul List</title>
<style>
TABLE,TD {
	 font-size:11px;
	 font-family:Verdana;
}

</style>
</head>

<body>

<?php

// ROAD

$sql = "SELECT err_id, gps_err_type.name as typ, gps_parish.name as parish, gps_area.name as area, gps_err.description FROM gps_err, gps_err_type, gps_area, gps_parish where gps_err.err_type = gps_err_type.id and gps_err.parish_id = gps_parish.parish_id and gps_err.area_id = gps_area.area_id and  gps_err.err_type = 'road' order by gps_err.err_id ";

$result = $pg->runSQL($sql);

echo "<h1>Road Errors</h1>";
echo "<table width='700px' border='1'>";
echo "<tr><td>Error No.</td><td>Type</td><td>Parish</td><td>Area</td><td>Message</td>";
while($row = $pg->loopResult($result)){
	   $err_id = $row["err_id"];
	   $area = $row["area"];
	   $parish  = $row["parish"];
	   $typ = $row["typ"];
	   $description = $row["description"];

	echo "<tr>";  
	echo "<td>$err_id</td>";
	echo "<td>$typ</td>";
	echo "<td>$parish</td>";
	echo "<td>$area</td>";
	echo "<td>$description</td>";
	
	echo "</tr>";  


}

echo "</table>";
echo $pg->sqlRowCount($sql) . " errors<br>";
echo "<br><br>";

// POI 

$sql = "SELECT err_id, gps_err_type.name as typ, gps_parish.name as parish, gps_area.name as area, gps_err.description FROM gps_err, gps_err_type, gps_area, gps_parish where gps_err.err_type = gps_err_type.id and gps_err.parish_id = gps_parish.parish_id and gps_err.area_id = gps_area.area_id and  gps_err.err_type = 'poi' order by gps_err.err_id ";

$result = $pg->runSQL($sql);

echo "<h1>POI Errors</h1>";
echo "<table width='700px' border='1'>";
echo "<tr><td>Error No.</td><td>Type</td><td>Parish</td><td>Area</td><td>Message</td>";
while($row = $pg->loopResult($result)){
	   $err_id = $row["err_id"];
	   $area = $row["area"];
	   $parish  = $row["parish"];
	   $typ = $row["typ"];
	   $description = $row["description"];

	echo "<tr>";  
	echo "<td>$err_id</td>";
	echo "<td>$typ</td>";
	echo "<td>$parish</td>";
	echo "<td>$area</td>";
	echo "<td>$description</td>";
	
	echo "</tr>";  


}

echo "</table>";
echo $pg->sqlRowCount($sql) . " errors<br>";
echo "<br><br>";


// ROUTING

$sql = "SELECT err_id, gps_err_type.name as typ, gps_parish.name as parish, gps_area.name as area, gps_err.description FROM gps_err, gps_err_type, gps_area, gps_parish where gps_err.err_type = gps_err_type.id and gps_err.parish_id = gps_parish.parish_id and gps_err.area_id = gps_area.area_id and  gps_err.err_type = 'routing' order by gps_err.err_id ";

$result = $pg->runSQL($sql);

echo "<h1>Routing Errors</h1>";
echo "<table width='700px' border='1'>";
echo "<tr><td>Error No.</td><td>Type</td><td>Parish</td><td>Area</td><td>Message</td>";
while($row = $pg->loopResult($result)){
	   $err_id = $row["err_id"];
	   $area = $row["area"];
	   $parish  = $row["parish"];
	   $typ = $row["typ"];
	   $description = $row["description"];

	echo "<tr>";  
	echo "<td>$err_id</td>";
	echo "<td>$typ</td>";
	echo "<td>$parish</td>";
	echo "<td>$area</td>";
	echo "<td>$description</td>";
	
	echo "</tr>";  


}

echo "</table>";
echo $pg->sqlRowCount($sql) . " errors<br>";
echo "<br><br>";





?>

</body>
</html>