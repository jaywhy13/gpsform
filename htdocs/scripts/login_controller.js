// JavaScript Document
var loggedIn = false;
var user_alias_ = null;

function updateLink(isLoggedIn)
{
	if(isLoggedIn == true)
	{
		document.getElementById('ll').style.display="none";
		//var user = document.getElementById('loginUser').value;
		//document.getElementById("logoutLink").innerHTML = 'Logout ' + user + "...?" ;
		document.getElementById('lo').style.display="block";
		loggedIn = true;
		document.getElementById('info').style.display="none";
		/*document.getElementById('ll').innerHTML = '<a id="loginLink" onmouseover='this.style.cursor="pointer" ' style='font-size: 12px;' onfocus='this.blur();' onclick="doLogout();this.style.display='none';"><span style="text-decoration: none;">Logout...?</span></a>';*/
		
		document.getElementById('loggedInUserDisplay').innerHTML = '<b>' + '\'' + user_alias_ + '\'' + ' Signed In</b>';
		
		//document.getElementById('loggedInUserDisplay').style="text-transform:uppercase;color:red";
		//document.getElementById('loggedInUserDisplay').innerHTML = '<br /> Welcome ' + document.getElementById('loginUser').value + '!';
		refreshDataSets();
	}else
	{
		document.getElementById('info').style.display="block";
	}
}


function updateLoginStatus(response){
	/*if(response.match("\\ssuccess") != null || response == "success")
	{
		updateLink(true);
		hideLogin();
		return true;
	}*/
	
	if(response.match("success\\s@[a-b]*") != null /*|| response == "success"*/)
	{
		autoLogout();
		user_alias_ = response.split("@")[1];
		updateLink(true);
		hideLogin();
		return true;
	}
	
	if(response.match("\\sfailure") != null || response == "failure")
	{
		alert("Login Failed!");
	}
	
	if(response.match("\\sloggedout") != null || response == "loggedout")
	{
		updateLink(false);
	}
	
	/*if(response.match("\\sloggedin") != null || response == "loggedin")
	{
		updateLink(true);
	}*/
	if(response.match("loggedin\\s@[a-b]*") != null || response == "loggedin")
	{
		user_alias_ = response.split("@")[1];
		updateLink(true);
	}

	return false;
}


/***********************************************************************/



/*************Login Object*************/
function Login(url) {
	this.url = url;
}

Login.prototype.checkLoginStatus = function()
{
	var xmlHttp = this.getXMLHttp();
	
	xmlHttp.open('GET', this.url+"?check=true", true);	
	
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4){
			return updateLoginStatus(xmlHttp.responseText);
		}
	}
	xmlHttp.send(null);	
}

Login.prototype.getXMLHttp = function(){

  var xHttp;

  if(window.XMLHttpRequest)
  {
    xHttp = new XMLHttpRequest();
  }
  else if(window.ActiveXObject)
  {
    xHttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xHttp;
}

Login.prototype.login = function(username, password) {
	var params = "username="+username+"&password="+password;
		
	this.sendLogin(this.url, params);
}

Login.prototype.logout = function(){
	var xmlHttp = this.getXMLHttp();
	
	xmlHttp.open('GET', "scripts/login.php?logout=true", true);	
	
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4){
			return updateLoginStatus(xmlHttp.responseText);
		}
	}
	xmlHttp.send(null);	
}

Login.prototype.sendLogin = function(url, params){
	var xmlHttp = this.getXMLHttp();
	
	xmlHttp.open('POST', url, true);
	
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-length", params.length);
	xmlHttp.setRequestHeader("Connection", "close");
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4){
			return updateLoginStatus(xmlHttp.responseText);
		}
	}
	xmlHttp.send(params);
}

Login.prototype.isLoggedIn = function()
{
	return this.checkLoginStatus();
//	return this.loggedIn;
}

loginObj = new Login("scripts/login.php");
var loginLink = "";

function doLogin(username, password)
{
	loginLink = document.getElementById('ll').innerHTML;
	loginObj.login(username, password);		
}

function doLogout()
{
	loginObj.logout();
	window.location.reload();
}

function doCheckLogin()
{
	loginObj.checkLoginStatus();	
}

function submitEnter(field, e, username, password)
{
			var keycode;
			if(window.event) 
				keycode = window.event.keyCode;
			else if (e) 
				keycode = e.which;
			else 
				return true;

			if (keycode == 13)
 	 	 	{// switch no different fields	
				if(field.id == "loginPopUp")
					doLogin(username, password);
   				return false;
   			}
			else
   				return true;
}