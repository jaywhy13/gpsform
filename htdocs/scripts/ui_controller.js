// JavaScript Document

var userUrl = "scripts/user.php";
var groupUrl = "scripts/group.php";
var permissionUrl = "scripts/permission.php";
var ur = null;
var gr = null;
	
function sendGroup(params, code)
{
	doGet(groupUrl, params, code);
}

function sendUser(params, code)
{
	doGet(userUrl, params, code);
}

function sendPerm(params, code)
{
	doGet(permissionUrl, params, code);
}


function updateResponse(response, code)
{
	refreshDataSets();
	
	//error: -3: Insufficient Permissions!
		
	if(response != "")
	{
		if(response.match("error:\\s-\\d{1}:\\s[a-b]*") != null )
		{
			if(response.split(": ")[1] == "-3"){
				alert("You do not have sufficient permissions to perform this operation.");
				return;
			}
		}
		alert(response);
		return;
	}		
	
	if (code != "" && code != null)
	{
		eval(code);
	}
	
}

function getXMLHttp(){

  var xHttp;

  if(window.XMLHttpRequest)
  {
    xHttp = new XMLHttpRequest();
  }
  else if(window.ActiveXObject)
  {
    xHttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xHttp;
}

function doPost(url, params, code)
{
	var xmlHttp = getXMLHttp();
	
	xmlHttp.open('POST', url, true);
	
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-length", params.length);
	xmlHttp.setRequestHeader("Connection", "close");
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4){
			return updateResponse(xmlHttp.responseText, code);
		}
	}
	xmlHttp.send(params);
}

function doGet(url, params, code)
{
	var xmlHttp = getXMLHttp();
	
	xmlHttp.open('GET', url+"?"+params, true);	
	
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4){
			return updateResponse(xmlHttp.responseText, code);
		}
	}
	xmlHttp.send(null);	
}

function hideLogin()
{
	document.getElementById('loginPopUp').style.display = 'none';
}

function showLogin()
{
	document.getElementById('loginPopUp').style.display = 'block';
}

function hideLoginLink()
{
	document.getElementById('loginLink').style.display = 'none';
}

function showLoginLink()
{
	document.getElementById('loginLink').style.display = 'block';
}

function showDiv(div)
{
	element(div).style.display = 'block';
}

function sD(div)
{
	div.style.display = 'block';		
}

function hideDiv(div)
{
	element(div).style.display = 'none';
}

function hD(div)
{
	div.style.display = 'none';
}

var bodies = new Array()
bodies[0]="userDetails";
bodies[1]="groupDetails";
bodies[2]="permissionDetails";
bodies[3]="info";


var userText = "System users are configured here. Click on '+' to add a new user. <br/><br/>Click on a user from the list of users to edit or delete.";

var groupText = "Groups are configured here. Groups are a collection of users. <br/><br/>Click on '+' to add a group. Click on a group from the list of groups to add/remove users from a group and edit or delete a group. When adding users to a group, only users that are not already in the group will be available and displayed for adding. Users are deleted from a group by clicking 'X'.";

var permText = "Access permissions are configured here. An access permission is the right for the users or members of a user group to perform a certain operation on pieces of information: view (read), update, delete, or create the information.<br/><br/> Use the tabs to switch between user and group permissions. Under a selected tab, click the user or group to view its list of permissions. A detailed description of the permission is shown if the mouse is placed over it. Click on the check boxes to mark or unmark (i.e. enable or disable) permissions. It's possible to go to the next or previous user/group by pressing the 'NEXT' and 'PREVIOUS' buttons. Press the 'UPDATE !' button to save the current user's or group's permissions. ";

var desc = new Array();
desc[0] = userText;
desc[1] = groupText;
desc[2] = permText;


function switchBody(num){
	if(!loggedIn){
		return;
	}
		
		switch(num)
		{
			case 0: element(bodies[0]).style.display = 'block';
					element('sideInfoPara').innerHTML = desc[0];
					hideRest(bodies, bodies[0]);
					break;
			case 1: element(bodies[1]).style.display = 'block';
					element('sideInfoPara').innerHTML = desc[1];
					hideRest(bodies, bodies[1]);
					break;
			case 2: element(bodies[2]).style.display = 'block';
					hideRest(bodies, bodies[2]);
					element('sideInfoPara').innerHTML = desc[2];
					break;
			case 3: element(bodies[3]).style.display = 'block';
					hideRest(bodies, bodies[3]);
					break;
		}
}

function hideRest(bodies, name)
{
	var b;
	for(b in bodies)
	{
		if(bodies[b] != name)
		{
			element(bodies[b]).style.display = 'none';
		}
	}
}

function element(id)
{
	return document.getElementById(id);
}


////////////////USER

function addUser(username, password, code)
{
	if(username == "" || password == "")
	{
		alert("Username and Password should not be empty!");
	}
	var params= "create&alias="+username+"&password="+password;	
	sendUser(params, code);
}

function updateUser(uid, groupname, ds)
{
	if(groupname == "" || groupname == null || groupname == "--NONE--")
	{
		return;
	}
	var g = ds.getData();
	var gid = null;
	
	for(var i = 0; i < g.length; i++)
	{
		if(g[i]["alias"] == groupname)
		{
			gid = g[i]["gid"];
			break;
		}
	}
	// add to group
	var params = "update&add&uid="+uid+"&gid="+gid;
	sendGroup(params);
}

function updateUserPassword(uid, old_password, password, code)
{
	if(password=="" || old_password =="")
	{
		alert("Old and new password should not be empty!");
		return;
	}
	var params= "update&uid="+uid+"&old_password="+old_password+"&password="+password;
	sendUser(params, code);
}

function deleteUser(uid, code)
{
	var yes = confirm("Do you really want to delete the selected user?");
	
	if(!yes)
	{
		return;
	}
	
	if(uid == null || uid == "")
	{
		alert("User Id Empty");
		// shouldn't occur!
		return;
	}
	
	var params= "delete&uid="+uid;
	sendUser(params, code);
}


/////////////////////GROUP
function addGroup(alias, code)
{
	if(alias == "")
	{
		alert("Group name should not be empty!");
	}
	var params= "create&alias="+alias;	
	sendGroup(params, code);
}

function renameGroup(gid, alias, code)
{
	if(alias == "")
	{
		alert("Group name should not be empty!");
	}
	var params="update&rename&gid="+gid+"&alias="+alias;
	sendGroup(params, code);
}

function addUserToGroup(uid, gid, code)
{
	var params="update&add&uid="+uid+"&gid="+gid;
	sendGroup(params, code);
}

function removeUserFromGroup(uid, gid, code)
{
	var yes = confirm("Do you really want to delete the selected user from this group?");
	
	if(!yes)
	{
		return;
	}
	
	var params="update&remove&uid="+uid+"&gid="+gid;
	sendGroup(params, code);
}

function deleteGroup(gid, code)
{
	var yes = confirm("Do you really want to delete the selected group?");
	
	if(!yes)
	{
		return;
	}
	
	if(gid == null || gid == "")
	{
		alert("Group Id Empty");
		// shouldn't occur!
		return;
	}
	
	var params= "delete&gid="+gid;
	sendGroup(params, code);
}



///////////////PERMISSION

function addUserPermission(uid, perm, bits)
{
	var yes = confirm("Do you really want to update the selected user's permissions?");
	
	if(!yes)
	{
		return;
	}
	
	var params="update&add&uid="+uid+"&perm="+perm+"&bits="+bits;
	//alert(params);
	sendPerm(params, "");
}

function addGroupPermission(gid, perm, bits)
{
	var yes = confirm("Do you really want to update the selected group's permissions?");
	
	if(!yes)
	{
		return;
	}
	
	var params="update&add&gid="+gid+"&perm="+perm+"&bits="+bits;
	sendPerm(params, "");
}

function clearUserPermission(uid, perm, code)
{
	var params="update&clear&uid="+uid+"&perm="+perm;
	sendPerm(params, code);
}

function clearGroupPermission(gid, perm, code)
{
	var params="update&clear&gid="+gid+"&perm="+perm;
	sendPerm(params, code);
}

function definePerm(alias, scope)
{
	var params="";
	
	if(scope == 'user')
	{
		params="update&init&ualias="+alias;
	}
	
	if(scope == 'group')
	{
		params="update&init&galias="+alias;
	}
	
	sendPerm(params, "");
}

////////////////////MISC
function refreshDataSets()
{
	ur = getRowMarker(dsUsers, "uid");
	gr = getRowMarker(dsGroups, "gid");
		
	dsUsers.loadData();
	dsGroups.loadData();
	dsUsers.setCurrentRow(ur);
	dsGroups.setCurrentRow(gr);
	dsUsersInGroup.loadData();
	dsUsersNotInGroup.loadData();
	dsUserGroup.loadData();
	dsGroupNot.loadData();
	dsPTags.loadData();
	dsPermissions.loadData();
	dsPermList.loadData();
	dsUserPermissionsDetail.loadData();
	dsGroupPermissionsDetail.loadData();
	
	//Spry.Data.updateAllRegions();
}

function getRowMarker(ds, id)
{
	var ret = null;
	if(ds != null){
		var row = ds.getCurrentRow();
		if(!row)
		{
			return;
		}
		ret = row[id];
	}
	return ret;
}

function setRowMarker(ds, id, val)
{
	if(ds == null){
		return;
	}
	
	/*
	if(ds.getRowCount() == 0)
	{
		return;
	}*/
	
	var rr = ds.getData();
	var i = 0;
		
	if(val != null){
		for(i = 0; i < rr.length; i++)
		{
			if(rr[i][id] == val)
			{
				ds.setCurrentRowNumber(i);
				break;
			}	
		}
	}
}

//////////SESSION AUTOLOGOUT
sessionTimeout = 900000;
sessionTimer = null; 

function doSessionLogout()
{
	doLogout();
	alert("Your session expired because of inactivity.");
}

function autoLogout()
{
	if(sessionTimer){
		window.clearTimeout(sessionTimer);
	}
	sessionTimer = window.setTimeout("doSessionLogout();", sessionTimeout);
}

function resetSessionTimer()
{
	if(sessionTimer){
		window.clearTimeout(sessionTimer);
	}
	autoLogout();
}
/////////////////////////////////////////////////////