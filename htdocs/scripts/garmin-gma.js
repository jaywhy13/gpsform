/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



var display;

/**
 * This is the init function
 */
function gmaLoad()
{
    display = new listener();
}

function getUnlockCode() 
{ 
    var url = 'cquery.php?action=getgmalockcode&order_id=27&cust_id=17';			
			
    new Ajax.Request(url, 
    {			
        method: 'GET',
        parameters:
        {
            esn: esn,
            selected: $('fileSelect').selectedIndex
        },
        onSuccess: showResponse,
        onFailure: showError
    });
}
		
function showResponse(response)
{			
    $('unlockCode').innerHTML = response.responseText;
    $('downloadFile').disabled = false;						
}           

function showError(response)
{
    $('unlockCode').innerHTML = 'Unable to obtain unlock code.  Error: ' + response.responseText;
} 