<?php

include_once("../../include/controller.php");


//$create = $_GET['create'];
$read = $_GET['read'];
$update = $_GET['update'];
//$delete = $_GET['delete'];

$add = $_GET['add'];
//$overwrite = $_GET['overwrite'];
$clear = $_GET['clear'];

$uid = $_GET['uid'];
$gid = $_GET['gid'];

$ualias = $_GET['ualias'];
$galias = $_GET['galias'];

$perm = $_GET['perm'];
$tag = $_GET['tag']; 
$tags = $_GET['tags']; 
$description = $_GET['description'];
//$permType = $_GET['permType']; 
$bits = $_GET['bits'];
$init = $_GET['init'];
$ctrl = new Controller();

/*
if(isset($create))
{
	if(!isset($perm) || $perm == "" || $perm == NULL || !isset($tag) || $tag == "" || $tag == NULL /*|| !isset($description) || $description == "" || $description == NULL)
	
	{
		return FALSE;
	}
	return @$ctrl->createPermission($perm, $tag, $description);
}*/

if(isset($update) && isset($add) && isset($uid))
{
	if(!isset($bits) || $bits=="" || $bits==NULL || !isset($uid) || $uid=="" || $uid==NULL || !isset($perm) || $perm=="" || $perm==NULL )
	{
		return FALSE;
	}
	
	$perms = explode(",", $perm);
	$bitz = explode(",", $bits);
	$cp = 0;
	
	if(($cp = count($perms)) != count($bitz))
	{
		return FALSE;
	}
	try{
	for($i = 0; $i < $cp; $i++)
	{
		@$ctrl->addUserPermission($uid, $perms[$i], $bitz[$i]);
	}
	}catch(Exception $e)
	{
		// should be rare
		echo "Error occurred during Permission update!";
	} 
	//echo "Permissions Updated.";
	return TRUE;
}

if(isset($update) && isset($add) && isset($gid))
{
	if(!isset($bits) || $bits=="" || $bits==NULL || !isset($gid) || $gid=="" || $gid==NULL || !isset($perm) || $perm=="" || $perm==NULL )
	{
		return FALSE;
	}
	
	$perms = explode(",", $perm);
	$bitz = explode(",", $bits);
	$cp = 0;
	
	if(($cp = count($perms)) != count($bitz))
	{
		return FALSE;
	}
	try{
	for($i = 0; $i < $cp; $i++)
	{
		@$ctrl->addGroupPermission($gid, $perms[$i], $bitz[$i]);
	}
	}catch(Exception $e)
	{
		echo "Error occurred during Permission update!";
	} 
	//echo "Permissions Updated.";
	
	return TRUE;
}


/*
if(isset($update) && isset($overwrite) && isset($uid))
{
	if(!isset($bits) || $bits=="" || $bits==NULL || !isset($uid) || $uid=="" || $uid==NULL || !isset($permType) || $permType=="" || $permType==NULL )
	{
		return FALSE;
	}
	return @$ctrl->overwriteUserPermission($uid, $permType, $bits);
}

if(isset($update) && isset($overwrite) && isset($gid))
{
	if(!isset($bits) || $bits=="" || $bits==NULL || !isset($gid) || $gid=="" || $gid==NULL || !isset($permType) || $permType=="" || $permType==NULL )
	{
		return FALSE;
	}
	return @$ctrl->overwriteGroupPermission($gid, $permType, $bits);
}
*/
if(isset($update) && isset($clear) && isset($uid))
{
	if(!isset($uid) || $uid=="" || $uid==NULL || !isset($perm) || $perm=="" || $perm==NULL )
	{
		return FALSE;
	}
	return @$ctrl->clearUserPermissions($uid, $perm);
}

if(isset($update) && isset($clear) && isset($gid))
{
	if(!isset($gid) || $gid=="" || $gid==NULL || !isset($perm) || $perm=="" || $perm==NULL )
	{
		return FALSE;
	}
	return @$ctrl->clearGroupPermissions($gid, $perm);
}


if(isset($update) && isset($init) && isset($ualias))
{
	if($ualias == "" || $ualias == NULL)
	{
		return FALSE;
	}
	return @$ctrl->initPermissions($ualias, 'user');
	
}

if(isset($update) && isset($init) && isset($galias))
{
	if($galias == "" || $galias == NULL)
	{
		return FALSE;
	}
	return @$ctrl->initPermissions($galias, 'group');
}

/*
if(isset($delete))
{
	if(!isset($perm) || $perm == "" || $perm==NULL)
	{
		return FALSE;
	}
	return @$ctrl->deletePermission($perm);
}
*/

if(isset($read))
{
	/*create xml/array for spry TRUE => XML FALSE => ARRAY*/
	if(isset($perm) && isset($tag) && $perm != "" && $tag != "")
	{
		$args = array("pmt_id" => $perm, "pmt_tag" => $tag);
		return $ctrl->readPermission(TRUE, $args);
	}
	else if(isset($perm) && perm != "")
	{
		return $ctrl->readPermission(TRUE, "pmt_id", $perm);
	}
	else if(isset($tag) && tag != "")
	{
		return $ctrl->readPermission(TRUE, "pmt_tag", $tag);
	}
	else if(isset($tags) && tags !=  "")
	{
		return $ctrl->readPermissionTags(TRUE);
	}
	else if(isset($gid) && $gid != "")
	{
		return $ctrl->readGroupPermissions(TRUE, $gid);
	} 
	else if(isset($uid) && $uid != "")
	{
		return $ctrl->readUserPermissions(TRUE, $uid);
	}
	else{
		return $ctrl->readPermission(TRUE);
	}
}
?>
