<?php

include_once("../../include/controller.php");

$create = $_GET['create'];
$read = $_GET['read'];
$update = $_GET['update'];
$delete = $_GET['delete'];
$not = $_GET['not'];

$rename = $_GET['rename'];
$add = $_GET['add'];
$remove = $_GET['remove'];

$uid = $_GET['uid'];
$gid = $_GET['gid'];
$alias = $_GET['alias'];


$ctrl = new Controller();

if(isset($create))
{
	if(!isset($alias) || $alias=="")
	{
		return FALSE;
	}
	try{
		@$ctrl->createGroup($alias);
	}catch(Exception $e)
	{
		echo "Group name '$alias' already exists. Please enter a different group name";
		return FALSE;
	}
	return TRUE;
}

if(isset($update) && isset($rename))
{
	if(!isset($gid) || $gid=="" || $gid==NULL || !isset($alias) || $alias=="")
	{
		return FALSE;
	}
	try{
		@$ctrl->renameGroup($gid, $alias);
	}catch(Exception $e)
	{
		echo "Group name '$alias'already exists. Please enter a different group name";
	}
}

if(isset($update) && isset($add))
{
	if(!isset($gid) || $gid=="" || $gid==NULL || !isset($uid) || $uid=="" || $uid==NULL)
	{
		return FALSE;
	}
	return @$ctrl->addUserToGroup($uid, $gid);
}

if(isset($update) && isset($remove))
{
	if(!isset($gid) || $gid=="" || $gid==NULL || !isset($uid) || $uid=="" || $uid==NULL)
	{
		return FALSE;
	}
	return @$ctrl->removeUserFromGroup($uid, $gid);
}

if(isset($delete))
{
	if(!isset($gid) || $gid == "" || $gid==NULL)
	{
		return FALSE;
	}
	return @$ctrl->deleteGroup($gid);
}

if(isset($read))
{
	/*create xml/array for spry TRUE => XML FALSE => ARRAY*/
	if(isset($gid) && isset($alias) && $gid != "" && $alias != "")
	{
		$args = array("gid" => $gid, "alias" => $alias);
		return $ctrl->readGroup(TRUE, $args);
	}
	else if(isset($gid) && isset($not) && $gid != "")
	{
		return $ctrl->readGroupNot(TRUE, $gid);
	}
	else if(isset($gid) && $gid != "")
	{
		return $ctrl->readGroup(TRUE, "gid", $gid);
	} 
	else if(isset($alias) && $alias != "")
	{
		return $ctrl->readGroup(TRUE, "alias", $alias);
	}
	else{
		return $ctrl->readGroup(TRUE);
	}
}
?>
