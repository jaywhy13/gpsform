<?php
    define("LOGIN_SUCCESS",2);
    define("LOGIN_FAILED",3);
    define("LOGIN_NO_SUCH_USER",4);
    define("ALREADYLOGGEDIN",5);
    define("TRACKINGUSER",'mgituser');
    define("USERALIAS","guest");
    define("DBCONNECTFAILED",6);
	include_once("../../include/db.php");
	
    class LoginModule {
        private $db = null;
        private $isConnected = false; // is the db connected
        private $uid = "";	//	uid, user in the system
        
		private $loggedIn = false;

        function __construct(){
            session_start();
            $this->dbal = new DbAbstractionLayer();

            // Check if ser already logged in a cached session
            /*if(isset($_SESSION[TRACKINGUSER])){
                $this->loggedIn = true;
                $this->uid = $_SESSION[TRACKINGUSER];
		echo "Isset: " . $_SESSION[TRACKINGUSER] . "";
            }*/
                
            $this->dbal->loadConfig("/var/www/AdminConsole/scripts/include/dbconfig.xml"); // file stored in same location
        }
        
        public function login($usr,$psw){
            if(!$this->connect()){
                return DBCONNECTFAILED;
            }

            if(!$this->isLoggedIn($usr)){
                $grabUsr = "select uid,usr_alias,usr_salt from mgis_usr where usr_alias = '$usr'";

                $result = pg_query($grabUsr);
                $userMatched = false;

                if($result) {
                    while($row = pg_fetch_row($result)){
                        $usr_uid = $row[0];
                        $checkThisHash = $psw . $row[2];
                        $checkHash = "select uid from mgis_usr where usr_hash = (select md5('$checkThisHash')) and uid = $usr_uid";
                        $resultOfHashCheck = pg_query($checkHash);
                        if(pg_numrows($resultOfHashCheck) > 0){
                            $userMatched = true;
                            $this->saveUser($usr_uid,$usr);
			    $this->loggedIn = true;
			    $this->uid = $usr_uid;
                        }
                        break;
                    }

                    if($userMatched == true){
                        $this->isLoggedIn = true;
                        return LOGIN_SUCCESS;
                    } else {
                        return LOGIN_FAILED;
                    }

                } else {
                    return LOGIN_FAILED;
                }
            }
            return ALREADYLOGGEDIN;
        }

        private function connect(){
            $host = $this->host;
            $dbname = $this->dbname;
            $usr = $this->usr;
            $psw = $this->psw;

            $this->conn = $this->dbal->connect();
            if($this->conn){
                return true;
            } 
            return false;
        }
        
        private function saveUser($usr,$alias){
            $_SESSION[TRACKINGUSER] = $usr;
            $_SESSION[USERALIAS] = $alias;
        }
        
        public function getUser(){
            return $_SESSION[TRACKINGUSER];
        }
        
        public function getUserAlias(){
            return  $_SESSION[USERALIAS];
        }

        private function clear_session_vars(){
            $_SESSION[TRACKINGUSER] = null;
	    $_SESSION[USERALIAS] = null;
            unset($_SESSION[TRACKINGUSER]);
	    unset($_SESSION[USERALIAS]);
            $this->loggedIn = false;
        }

	public function isAnyOneLoggedIn()
	{
		if(isset($_SESSION[TRACKINGUSER]))
		{
			return true;
		}
		return false;
	}
	
        public function isLoggedIn($usr){
		if($usr == $_SESSION[TRACKINGUSER] || $usr == $_SESSION[USERALIAS])
			return true;
		return false;
            //return $this->loggedIn;
        }

        public function logout(){
            $this->clear_session_vars();
            session_destroy();
        }
        
        public function getConnection(){
            return $this->conn;		
        }
        
        public function isConnected(){
            return $this->isConnected;
        }
    }
?>
