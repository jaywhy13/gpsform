<?php

include_once("../../include/controller.php");

$create = $_GET['create'];
$read = $_GET['read'];
$update = $_GET['update'];
$delete = $_GET['delete'];
$group = $_GET['group'];
$uid = $_GET['uid'];
$alias = $_GET['alias'];
$password = $_GET['password'];
$old_password = $_GET['old_password'];

$ctrl = new Controller();

if(isset($create))
{
	if(!isset($alias) || $alias=="" || !isset($password) || $password=="")
	{
		return FALSE;
	}
	try{
		@$ctrl->createUser($alias, $password);
	}catch(Exception $e)
	{
		echo "Username '$alias' already exists. Please enter a different username";
		return FALSE;
	}
	return TRUE;
}

if(isset($update))
{
	if(!isset($uid) || $uid=="" || $uid==NULL || !isset($password) || $password=="" || !isset($old_password) || $old_password=="")
	{
		return FALSE;
	}
	try{
	return @$ctrl->updateUserPassword($uid, $old_password, $password);
	}catch(Exception $e)
	{
		echo "The old password is incorrect. Please enter your correct password.";
		return FALSE;
	}
	return TRUE;
}

if(isset($delete))
{
	if(!isset($uid) || $uid == "")
	{
		return FALSE;
	}
	return @$ctrl->deleteUser($uid);
}

if(isset($read))
{
	/*create xml/array for spry TRUE => XML FALSE => ARRAY*/
	if(isset($uid) && isset($alias) && $uid != "" && $alias != "")
	{
		$args = array("uid" => $uid, "alias" => $alias);
		return $ctrl->readUser(TRUE, $args);
	}
	else if(isset($uid) && isset($group) && $uid != "" && $group != "")
	{
		return $ctrl->readUserGroup(TRUE, $uid);
	}
	else if(isset($uid) && $uid != "")
	{
		return $ctrl->readUser(TRUE, "uid", $uid);
	} 
	else if(isset($alias) && $alias != "")
	{
		return $ctrl->readUser(TRUE, "alias", $alias);
	}
	else{
		return $ctrl->readUser(TRUE);
	}
}
?>
