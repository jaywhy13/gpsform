<?php

include_once("../../include/controller.php");

$create = $_GET['create'];
$read = $_GET['read'];
$delete = $_GET['delete'];
$not = $_GET['not'];
$uid = $_GET['uid'];
$gid = $_GET['gid'];

$ctrl = new Controller();

if(isset($create))
{
	if(!isset($uid) || $uid=="" || $uid == NULL || !isset($gid) || $gid=="" || $gid == NULL)
	{
		return FALSE;
	}
	return @$ctrl->createUserInGroupInGroup($gid, $uid);
}

if(isset($delete))
{
	if(!isset($uid) || $uid=="" || $uid == NULL || !isset($gid) || $gid=="" || $gid == NULL)
	{
		return FALSE;
	}
	return @$ctrl->deleteUserInGroupInGroup($gid, $uid);
}


if(isset($read))
{
	/*create xml/array for spry TRUE => XML FALSE => ARRAY*/
	if(isset($uid) && isset($gid) && $gid != "" && $uid != "")
	{
		$args = array("uid" => $uid, "gid" => $gid);
		return $ctrl->readUserInGroup(TRUE, $args);
	}
	else if(isset($uid) && $uid != "")
	{
		return $ctrl->readUserInGroup(TRUE, "uid", $uid);
	}
	else if(isset($gid) && $gid != "" && isset($not))
	{
		return $ctrl->readUsersNotInGroup(TRUE, $gid);
	} 
	else if(isset($gid) && $gid != "")
	{
		return $ctrl->readUserInGroup(TRUE, "gid", $gid);
	}
	else{
		return $ctrl->readUserInGroup(TRUE);
	}
}
?>
