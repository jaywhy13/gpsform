<?php
require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");
require_once ("../include/HttpClient.class.php");

$kia = new Kia();

$sql = "SELECT cust_email from gps_customer WHERE cust_email IS NOT NULL AND cust_email <> ''";


$res = $kia->runSQL ( $sql );


$emails = array();

while($row = $kia->loopResult($res)) {

    $email = $row["cust_email"];

    if(ereg("@",$email)) {
        $emails [] = "'$email'";
    }
}

$pg = new PermGuru ( "gpsform" );

if(!$pg->isLoggedIn()) {
    echo "Please login first.";
}

?>
<html>
    <head>
        <title>Customer Emails</title>
        <script type="text/javascript">
            <?php
                echo "var emails = new Array(" . implode(",", $emails) . ");";
            ?>

            function gen(){
                var _sep = document.getElementById("separator");
                var _tA = document.getElementById("tA");

                var _separator = _sep.value;
                if(_separator == null) {
                    _separator = "";
                }

                _tA.value = emails.join(_separator);
            }
        </script>

    </head>
    <body>
        Choose your separator:&nbsp;<input value="," id="separator"><br/>
        <input type="button" value="Generate" onclick="gen()"/>
        <textarea id="tA" rows="10" cols="90"></textarea>

    </body>
</html>