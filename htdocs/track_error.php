<?php
require_once ("../include/kiaapi.php");

$kia = new Kia();

function fetch_name($table,$column,$value){
    $name = "Unknown";
    if(isset($value) && !empty($value)) {
        $sql = "SELECT * FROM $table WHERE $column = $value";
        $res = $GLOBALS['kia']->runSQL($sql);
        while($row_inner = $GLOBALS['kia']->loopResult($res)){
            $name = $row_inner["$column"];
        }
    }
    return $name;
}

?>

<html>
    <head>
        <title>Track Error</title>
        <style>
            TD,INPUT {
                font-size:11px;
                font-family:Verdana, Sans Serif;
            }

            INPUT {
                font-weight:bold;
                padding:5px;
                border-style:solid;
                border-width:1px;
                border-color:black;
            }

            .date {
                width:150px;
                font-size:10px;
            }

            tr.init td {
                background-color:#cccccc;
            }

        </style>
    </head>
    <body>


    <?php
    $err_no = $_GET["id"];
    $track_mode = !empty($err_no) && isset($err_no);
    if($track_mode){
        ?>
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left"><img src="jamnav_logo.jpg" width="250px"/></td>
        </tr>

        <tr>
            <td bgcolor="Black" style="color:white; font-weight:bold;">Details on error # <? print $err_no ?></td>
        </tr>


    <?php
} else { // no error is being tracked
    ?>

        <table width="100%" cellspacing="0" cellpadding="5">
            <tr>
                <td align="center"><img src="jamnav_logo.jpg" width="300px"/></td>
            </tr>
            <tr>
                <td bgcolor="Black" style="color:white;" align="center">
                    Please enter the number of the error you wish to track to get information about it's status.
                </td>
            </tr>

            <tr>
                <td align="center">
                    <form action="track_error.php" method="GET">
                        Error Number:&nbsp;&nbsp;
                        <input type="text" name="id" size="5"/>
                        &nbsp;&nbsp;
                        <input type="submit" value="Track"/>
                    </form>
                </td>
            </tr>


            <?php
        }
        ?>
            <tr>
                <td> <!-- start a new td cell for the table that will hold the log of events -->
                    <table width="100%" cellpadding="5" cellspacing="0">
                        <?php

                        // Get the log messages for the error
                        $sql = "SELECT * FROM gps_err_log WHERE err_id = $err_no ORDER BY log_timestamp DESC";
                        $result = $kia->runSQL($sql);
                        while($row = $kia->loopResult($result)){
                            $err_id = $row["err_id"];
                            $message = $row["log_message"];
                            $ts = $row["log_timestamp"];
                            $status = $row["log_status"];
                            $date_readable = date("Y-m-d h:i:s A",strtotime($ts));

                            echo "<tr><td>$date_readable</td><td>$message</td></tr>";
                            
                        }

                        // Give details about the message
                        $sql = "SELECT gps_err.description as desc, err_id, err_type, area_id, parish_id, lat, lon, version, status, log_ts FROM gps_err, gps_err_type WHERE err_id = $err_no AND gps_err_type.id = gps_err.err_type ORDER BY log_ts ASC ";

                        if($kia->resultCount($sql) == 0){
                            echo "<tr><td><b>ERROR: </b> Could not find any information on the error number given.</td></tr>";
                        } else {
                            $result = $kia->runSQL($sql);
                            while($row = $kia->loopResult($result)){
                                $err_id = $row["err_id"];
                                $description = $row["desc"];
                                $err_type = $row["err_type"];
                                $area_id = $row["area_id"];
                                $parish_id = $row["parish_id"];
                                $lat = $row["lat"];
                                $lon = $row["lon"];
                                $version = $row["version"];
                                $status = $row["status"];
                                $timestamp = $row["log_ts"];


                                // Start putting the data together
                                $date_readable = date("Y-m-d h:i:s A",strtotime($timestamp));


                                $parish_name = fetch_name("gps_parish","name",$parish_id);
                                $area_name = fetch_name("gps_area","name",$area_id);


                                // Provide details on when the error was logged
                                echo "
                                        <tr class='init'><td class='date' valign='top'>$date_readable</td>
                                        <td>
                                        Error logged as # $err_id. <br><br>
                                        <b>Message Details:</b><br>
                                        <b>Area: </b>$area_name<br>
                                        <b>Parish:</b> $parish_name<br>
                                        <b>Content:</b><br>
                                         $description
                                        </td>
                                        </tr>
                                    ";

                            }
                        }





                        ?>

                    </table>
                </td>
            </tr>
        </table>
    </body>

</html>