<?php
include_once ("../include/permguru.php");
include_once ("../include/kiaapi.php");
$pg = new PermGuru ( "gpsform" );

$jsonPrinter = new JsonKiaPrinter ( );

$usr = $_GET ["login_usr"];
$psw = $_GET ["login_psw"];

if(isset($_GET["check"])){
	if($pg->isLoggedIn()){
		$jsonPrinter->kiamsg( "loggedin:" . $pg->getUserAlias() );
	} else {
		$jsonPrinter->kiamsg( "unauthorized" );
	}
}


if(isset($_GET["logout"])){
	$pg->logout();	
}

if (! $pg->isLoggedIn ()) {

	try {
		$isLoggedIn = $pg->login ( $usr, $psw );
		if ($isLoggedIn) {
			$jsonPrinter->kiamsg ( "Successfully logged in!" );
		} else {
			$jsonPrinter->kiaerror ( "Username or password incorrect!" );
		}
	} catch ( NonExistentEntityException $e ) {
		$jsonPrinter->kiaerror ( $e->getMessage( ) );
	}

} else {
	$jsonPrinter->kiamsg ( "Already logged in!" );

}

?>