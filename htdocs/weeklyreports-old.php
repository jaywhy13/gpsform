<?php
require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");
require_once("../include/blabber.php");

$pg = new PermGuru("gpsform");

$kia = new Kia();
?>
<html>
    <head>
        <script src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
        <script src="http://tablesorter.com/jquery.tablesorter.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#report").tablesorter();
            });

            $("th").hover(function(){
                this.style.backgroundColor = "rgb(230,230,255)";
            },null);




        </script>
        <title>Weekly Reports</title>
        <style type="text/css">
            BODY,TR,TD {
                font-size:11px;	
                font-family:Verdana;
            }
            TD {
                border-style:solid;
                border-width:1px;
                border-top:none;
                border-left:none;
                border-right:none;
            }

            TH {
                cursor:pointer;
            }

            .odd TD {
                background-color:rgb(240,240,255);
            }

            .header TH {
                font-weight:bold;
            }

        </style>
    </head>
    <body>
        <?
        $firstSaleDate = "SELECT min(order_date) FROM gps_order";

        $res = $kia->runSQL($firstSaleDate);


        $firstDate = "";

        while ($row = $kia->loopResult($res)) {
            $firstDate = strtotime($row["min"]);
        }

        $weekday = date("w", $firstDate);
        $y = date("Y", $firstDate);
        $m = date("m", $firstDate);
        $d = date("d", $firstDate);

        $inc = 0;
        if ($weekday == 0)
            $inc = 1;
        else if ($weekday > 1) {
            $inc = - ($weekday - 1);
        }

        $firstWeek = mktime(0, 0, 0, $m, $d + $inc, $y);


// Get the last week
        $lastSaleDate = "SELECT max(order_date) FROM gps_order";

        $res = $kia->runSQL($lastSaleDate);

        $lastDate = "";

        while ($row = $kia->loopResult($res)) {
            $lastDate = strtotime($row["max"]);
        }

        $weekday = date("w", $lastDate);
        $y = date("Y", $lastDate);
        $m = date("m", $lastDate);
        $d = date("d", $lastDate);

        $inc = 0;
        if ($weekday == 0)
            $inc = 1;
        else if ($weekday > 1) {
            $inc = - ($weekday - 1);
        }

        $lastWeek = mktime(0, 0, 0, $m, $d + $inc, $y);


// Start looping now
        $curWeek = $firstWeek;

        $weeks = array();
        $weeksIs = array();

        while ($curWeek < $lastWeek) {
            $y = date("Y", $curWeek);
            $m = date("m", $curWeek);
            $d = date("d", $curWeek);

            $curWeek = mktime(0, 0, 0, $m, $d + 7, $y);
            $weeks[$curWeek] = 0;
            $weeksIs[$curWeek] = 0;
        }

// Now loop over all sales
        function getWeekStart($ts) {
            $m = date("m", $ts);
            $d = date("d", $ts);
            $y = date("y", $ts);
            $w = date("w", $ts);
            $inc = 0;
            if ($w < 1) {
                $inc = 1;
            } else if ($w > 1) {
                $inc = - ($w - 1);
            }

            $weekStart = mktime(0, 0, 0, $m, $d + $inc, $y);
            return $weekStart;
        }

        $total = 0;
        $totalItems = 0;

        // normal updates
        $uSql = "select date_part('week',dispatch_date), count(cust_id) from gps_customer_update 
WHERE date_part('year',dispatch_date) = $y 
group by date_part('week',dispatch_date);";
        echo $uSql;
        $uRes = $kia->runSQL($uSql);
        $uArr = pg_fetch_all($uRes);
        $uWeeks = array();
        foreach ($uArr as $row) {
            $week = $row["date_part"];
            $count = $row["count"];
            $uWeeks[$week] = $count;
        }
        
        echo "<pre>";
        print_r($uWeeks);
        echo "</pre>";
        

// get special package week count details...
        $spSql = "select date_part('week',order_date), count(order_id) from gps_order 
        group by date_part('week',order_date) 
        order by date_part('week',order_date) where package_id='gps_1307461442007fis29810is' AND date_part('year',order_date) = $y";
        $spRes = $kia->runSQL($spSql);
        $spArr = pg_fetch_all($spRes);
        $spWeeks = array();
        foreach ($spArr as $row) {
            $week = $row["date_part"];
            $count = $row["count"];
            $spWeeks[$week] = $count;
        }

      

        $sql = "SELECT order_id, order_date, order_total, (select sum(order_item_quantity) FROM gps_order_item where order_id = gps_order.order_id) as foo FROM gps_order";
        $res = $kia->runSQL($sql);
        while ($row = $kia->loopResult($res)) {
            $order_date = strtotime($row["order_date"]);
            $order_item_count = $row["foo"];

            $weeks[getWeekStart($order_date)]++;
            $weeksIs[getWeekStart($order_date)] += $order_item_count;
            $totalItems += $order_item_count;
            $total++;
        }



        echo "Report for period " . date("M d, Y", $firstWeek) . " to " . date("M d, Y", $lastWeek);
        echo "<table id='report' cellpadding='5' cellspacing='0' border='0'><thead><tr class='header'><th>Week No.</th><th>Week Starting</th><th>Order count</th><th>Packages Sold</th><th>Normal Updates</th><th>Special Updates</th></tr></thead>";

        echo "<tbody>";
        $weekCount = 1;
        foreach ($weeks as $key => $value) {
            $weekNo = date("W",$key);
            $spWeekCount = $spWeeks[$weekNo] ? $spWeeks[$weekNo] : 0;
            $uWeekCount = $uWeeks[$weekNo] ? $uWeeks[$weekNo] : 0;
            $totalSpCount+= $spWeekCount;
            $totalUCount+= $uWeekCount;
            $class = ($weekCount % 2 == 1) ? "odd" : "even";
            echo "<tr class='$class'><td>$weekCount</td>";
            echo "<td>" . date("Y-m-d", $key) . "</td>";
            echo "<td align='center'>" . $value . "</td>";
            echo "<td align='center'>" . $weeksIs[$key] . "</td>";
            echo "<td align='center'>" . $uWeekCount . "</td>";
            echo "<td align='center'>" . $spWeekCount . "</td>";
            echo "</tr>";
            $weekCount++;
        }
        echo "</tbody>";

        echo "<tfoot><tr><td></td><td></td><td align='center'>$total</td><td align='center'>$totalItems</td><td align='center'>$totalUCount</td><td align='center'>$totalSpCount</td></tr></tfoot>";

        echo "</table>";

        echo "<br><br>";
        echo date("Y-m-d", getWeekStart(strtotime("2009-12-11")));
        ?>


    </body>
</html>