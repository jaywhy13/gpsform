<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GPS Error Reporting Form</title>
<style type="text/css">
.inputWidget {
	width: 270px;
	height: 30px;
	padding: 5px;
	font-size: 12px;
}

.sideHeading {
	font-weight: bold;
	font-size: 11px;
	vertical-align: top;
	padding: 10px;
}

.errHint {
	font-size: 12px;
	color: rgb(50, 80, 220);
	border-left-style: solid;
	background-color: rgb(230, 230, 245);
	padding: 5px;
}

TEXTAREA {
	font-family: "Verdana,Georgia,Sans Serif,Trebuchet MS";
	font-size: 12px;
	padding: 5px;
}

BODY,TD,SPAN {
	font-size: 12px;
	line-height: 150%;
	font-family: "Verdana,Georgia,Sans Serif,Trebuchet MS";
}

.step1 {
	background-color: rgb(250, 255, 250);
}

.step2 {
	background-color: rgb(250, 250, 254);
}

.step3 {
	background-color: rgb(245, 250, 254);
}

.stepHeading {
	font-weight: bold;
	font-size: 12px;
}

.pageHeader {
	font-family: "Verdana,Georgia,Sans Serif,Trebuchet MS";
	font-size: 22px;
	padding: 10px;
	font-weight: bold;
	margin-left: 20px;
	color: rgb(40, 70, 30);
}

.quickTips { /*position: absolute;*/
	right: 0px;
	width: 250px;
	border-left-style: solid;
	border-left-color: rgb(240, 240, 255);
	background-color: rgb(250, 250, 255);
	height: 100%;
	top: 0px;
	font-size: 12px;
	font-family: "Verdana,Georgia,Sans Serif,Trebuchet MS";
	top: 0px;
	margin-left: -10px;
	padding-right: 15px;
	height: 100%;
}

.quickTips .heading {
	font-size: 14px;
	font-weight: bold;
	background-color: rgb(150, 150, 200);
	color: rgb(30, 30, 50);
	padding: 5px;
	width: 102%;
	display: block;
}

.quickTips LI {
	line-height: 150%;
	margin-bottom: 15px;
}

#latlondiv SPAN {
	border-style:solid;
	border-width:1px;
	padding-left:3px;
	padding-right:3px;
}

#latLonTbl TD {
	padding:5px;
	line-height:150%;
}

#latLonTbl INPUT {
	width:50px;
	border-style:solid;
	border-width:1px;
	border-color:black;
}

#latLonTbl .coord {
	background-color:rgb(240,240,252);
}

</style>
<script src="spry/xpath.js" language="javascript"></script>
<script src="spry/SpryAutoSuggest.js" language="javascript"></script>
<script src="spry/SpryData.js" language="javascript"></script>
<link href="spry/SpryAutoSuggest.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
var dsParish = new Spry.Data.XMLDataSet("parish.php","parish_list/parish");
var dsArea = new Spry.Data.XMLDataSet("area.php?parish_id={dsParish::id}", "area_list/area");
var dsErrType = new Spry.Data.XMLDataSet("gps_err_type.php","gps_err_type_list/gps_err_type");
var dsFeature = new Spry.Data.XMLDataSet("feature.php?ft={dsErrType::id}&parish_id={dsParish::id}&area_id={dsArea::id}","feature_list/feature");
var dsErrTag = new Spry.Data.XMLDataSet("err_tag.php","err_tag_list/err_tag");

function grabParishId(str){
	var parts = str.split(":");
	if(parts[1]){
		return parts[1];
	}
}

function grabAreaId(str){
	var parts = str.split(":");
	if(parts[0]){
		return parts[0];
	}
}


var areaRestorationNeeded = false;
var areaRestorationValue = null;

function updateUnknownParish(str){

	var parishId = grabParishId(str);
	var areaId = grabAreaId(str);
	if(parishId == 69) return;

	var selParish = document.getElementById("sel_parish");
	var selectedParishValue = selParish.options[selParish.selectedIndex].value;
	if(selectedParishValue == 69){

		if(parishId){ // search the selParish for an option that has a value == parishId
			for(var i = 0; i < selParish.options.length; i++){
				var option = selParish.options[i];
				if(option.value == parishId){
					selParish.selectedIndex = i;
					areaRestorationNeeded = true;
					areaRestorationValue = areaId;
					dsParish.setCurrentRowNumber(i);
				}
			}

		}
	}
}


function restoreUserParishChoice(dataSet,data){
	var selArea = document.getElementById("sel_area");
	var selParish = document.getElementById("sel_parish");
	if(!areaInitialized){
		selArea.selectedIndex = 0;
		dsArea.setCurrentRowNumber(0);
		selParish.selectedIndex = 0;
		dsParish.setCurrentRowNumber(0);

		areaInitialized = true;
	}

	if(areaRestorationNeeded){


		for(var i = 0; i < selArea.options.length; i++){
			var optionValue = grabAreaId(selArea.options[i].value);
			if(optionValue == areaRestorationValue){
				selArea.selectedIndex = i;
				areaRestorationNeeded = false;
				areaRestorationValue = null;
			}
		}
	}
}

var req = ["feature_name","err_description"];
function validate(){
	var isValid = true;
	var frm = document.forms[0];
	for(var i = 0; i < req.length; i++){
		var elemName = req[i];
		var elem = frm.elements[elemName];
		if(elem.value == "" || elem.value == null){
			isValid = false;
			colorRed(elem);
		} else {
			colorNormal (elem);
		}
	}

	if(!isValid){
		alert("Please fill out all the fields highlighted in red");
	}
	return isValid;
}

var areaInitialized = false;

function colorRed(elem){
	elem.style.backgroundColor = "rgb(250,120,120)";
}

function colorNormal(elem){
	elem.style.backgroundColor = "white";
}

var areaObserver = {};
areaObserver.onPostUpdate = restoreUserParishChoice;
Spry.Data.Region.addObserver("area_region",areaObserver);



</script>
</head>
<body>
<br />

<table>
	<tr>
		<td valign="top" style="position: relative;"><br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div class="quickTips"><span class="heading">Quick Tips</span><br>
		<ul>
			<li>Report <b><u>only one error</u></b> at a time. You will need to
			fill out the entire form for each error that you have.</li>
			<li>If you leave your email address, we can contact you when there
			are updates to the system.</li>
			<li>
			How do I save get coordinates from my Garmin Nuvi?<br>Simply touch the car icon while at any location
			and the coordinates will be displayed, for example: <br>N 18°02.101’ W 076°49.857’
			</li>

		</ul>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		</div>

		</td>

		<td valign="top"><img src="images/new_jamnav_text.gif" width="400px" />
		<form method="get" action="save_error.php"
			onSubmit="return validate();"><span class="pageHeader">GPS Navigation
		Error Form</span>
		<table width="500px" cellpadding="10" cellspacing="0"
			style="margin-left: 30px; border-left-style: solid; border-bottom-style: solid;">
			<tr class="step1">
				<td></td>
				<td class="stepHeading">Where did you experience this error?<br>
				</td>
			</tr>

			<tr class="step1">
				<td class="sideHeading">Parish:</td>
				<td spry:region="dsParish"><select name="sel_parish" id="sel_parish"
					class="inputWidget"
					onchange="dsParish.setCurrentRowNumber(this.selectedIndex);">
					<option spry:repeat="dsParish" value="{id}">{name}</option>
				</select></td>
			</tr>

			<tr class="step1">
				<td class="sideHeading">Area:</td>
				<td id="area_region" spry:region="dsArea"><select name="sel_area"
					class="inputWidget" id="sel_area"
					onchange="dsArea.setCurrentRowNumber(this.selectedIndex); updateUnknownParish(this.options[this.selectedIndex].value);">
					<option spry:repeat="dsArea" value="{id}:{parish_id}">{name}</option>
				</select> &nbsp; &nbsp; <a href="javascript:void(0)"
					onclick="document.getElementById('new_area_div').style.display='';">Not
				Listed</a>

				<div id="new_area_div" style="display: none"><br />
				Type the name of the area here:<br />
				<input type="text" id="new_area" name="new_area" class="inputWidget" />
				</div>

				<br>
				<br>
				<b><a href="javascript:void(0)"
					onClick="document.getElementById('latlondiv').style.display='';">I
				know the GPS coords</a></b>
				
				<div id="latlondiv"
					style="display: none; background-color: white; padding: 5px; width: 390px; margin-top: 5px; position: relative; border-style: solid; border-width: 1px;">
				<b>Coordinates:</b><br>
				Enter the coordinates in Degrees Minute Seconds format. <br>
				<table id="latLonTbl" cellspacing="0">
<!--					<tr>
						<td>North</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>West</td>
					</tr>
-->					<tr>
						<td class="coord"><b>N</b>&nbsp;&nbsp;<input name="latd" maxlength="2" style="width:30px;"/>&nbsp;&deg;&nbsp;<input name="latm">&nbsp;'</td>
						<td></td>
						<td class="coord"><b>W</b>&nbsp;&nbsp;<input name="lond" maxlength="2" style="width:30px;"/>&nbsp;&deg;&nbsp;<input name="lonm">&nbsp;'</td>
					</tr>
				</table>
				<b>Example:</b>
				<br>N <span>18</span>&nbsp;°&nbsp;<span>02.101</span>&nbsp;’ &nbsp;&nbsp;&nbsp;&nbsp;W <span>076</span>&nbsp;°&nbsp;<span>49.857</span>&nbsp;’
				<br>
				<br>
				<i>NB: See the 3rd tip on the left to find out how to retrieve position information from your device.</i>
				</div>

				</td>

			</tr>

			<tr class="step2">
				<td></td>
				<td class="stepHeading">Tell us more about the error<br>
				</td>
			</tr>
			<tr class="step2">
				<td></td>
				<td><span spry:region="dsErrType"> What kind of error was it?<br />
				<select class="inputWidget" name="err_type" id="err_type"
					onchange="dsErrType.setCurrentRowNumber(this.selectedIndex);">
					<option spry:repeat="dsErrType" value="{id}">{name}</option>
				</select> <br>
				</span> <br />
				<span spry:detailregion="dsErrType" class="errHint"> {description} </span></td>
			</tr>
			<tr class="step2">
				<td></td>
				<td>* What was the name of the feature?<br />
				<div id="featureContainer"><input class="inputWidget" type="text"
					name="feature_name" id="featureName" />&nbsp;&nbsp;<a
					href="javascript:void(0)"
					onclick="document.getElementById('featureName').value='';">Clear</a>
				<div id="featureSearchResults" spry:region="dsFeature"><span
					spry:repeat="dsFeature" spry:suggest="{name}">{name}<br />
				</span></div>
				</div>

				<span spry:region="dsFeature"> <input type="hidden"
					name="feature_value" value="{id}" /> </span></td>
			</tr>

			<tr class="step2">
				<td></td>
				<td spry:region="dsErrTag">Tick all the following that apply:<br />
				<span spry:repeat="dsErrTag"><input type="checkbox" id="{id}"
					name="{id}">&nbsp;{description}<br />
				</span> <br>
				<b>Do you know your version number?</b> &nbsp;<u>Example:</u> "1.5"
				<br>
				<input name="version" class="inputWidget" value=""></td>
			</tr>

			<tr class="step2">
				<td class="sideHeading">Message:</td>
				<td>* Tell us about the error that you experienced below:<br />
				<textarea cols="100" rows="5" name="err_description"
				 id="err_description"></textarea></td>

			</tr>


			<tr class="step3">
				<td></td>
				<td class="stepHeading">How can we contact you?</td>
			</tr>


			<tr class="step3">
				<td class="sideHeading">Email:</td>
				<td>You may leave your email so we can contact you for more
				information. This will help us to resolve the problem more quickly.
				<br />
				<input class="inputWidget" type="text" value="" name="email_addr" /></td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" value="Submit" name="submitBtn" />
			
			</tr>
		</table>
		<br />
		<br />
		<script language="javascript">
		var asFeature = new Spry.Widget.AutoSuggest("featureContainer","featureSearchResults","dsFeature","name",{loadFromServer:true,urlParam:"query",minCharsType:4});
</script></form>

		</td>
	</tr>
</table>
<!--
Select the spanner icon > Settings > Look for "Map" in the listing > Select Map infor at the bottom centre of the screen. YOu should see two items listed, the first in the list should indicate the versino, for example "Jamaica Navigates Version 1.5"

-->

</body>
</html>
