<?php

require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");
require_once("../include/blabber.php");



function getWeekStr($year,$weekNumber){
    if($weekNumber < 10){
        $weekNumber = "0" . $weekNumber;
    }
    return date(
    "M-d",
    strtotime($year . "W" . $weekNumber));
}

$pg = new PermGuru("gpsform");

$kia = new Kia();

$monthNames = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
$chartData = "";
$htmlData = "";
$year = 2012;

if (isset($_GET["year"]) && is_numeric($_GET["year"])) {
    $year = $_GET["year"];
}

$f = "xml";

if (isset($_GET["f"])) {
    $f = $_GET["f"];
}

$id = $_GET["id"];
if ($id == "orders") {
    $graphTitle = "JAMNAV Orders by week ($year)";
    $xTitle = "Week";
    $yTitle = "Number of orders";

    $dp = "date_part('week',order_date)";
    $sql = "SELECT $dp as week, count(gps_order) as count FROM gps_order WHERE date_part('year',order_date) = $year GROUP BY $dp ORDER BY $dp";
    $res = $kia->runSQL($sql);

    $htmlData = "<table><thead><tr><th>Week</th><th>Number of orders</th></tr></thead><tbody>";
    while ($row = $kia->loopResult($res)) {
        $week = $row["week"];
        $weekDateStr = getWeekStr($year, $week);
        $weekStr = "Week $week (" . $weekDateStr . ")";
        $count = $row["count"];
        $chartData .= "<set name='$week' value='$count'/>";
        $htmlData .= "<tr><td>$weekStr</td><td>$count</td></tr>";
    }
    $htmlData .= "</tbody></table>";
} else if ($id == "updates") {
    $graphTitle = "JAMNAV Updates by week ($year)";
    $xTitle = "Week";
    $yTitle = "Number of updates";

    $dp = "date_part('week',created_at)";
    $sql = "SELECT $dp as week, count(order_expense) as count FROM order_expense WHERE date_part('year',created_at) = $year GROUP BY $dp  ORDER BY $dp";
    $res = $kia->runSQL($sql);

    $htmlData = "<table><thead><tr><th>Week</th><th>Number of updates</th></tr></thead><tbody>";
    while ($row = $kia->loopResult($res)) {
        $week = $row["week"];
        $weekDateStr = getWeekStr($year, $week);
        $weekStr = "Week $week (" . $weekDateStr . ")";
        $count = $row["count"];
        $chartData .= "<set name='$week' value='$count'/>";
        $htmlData .= "<tr><td>$weekStr</td><td>$count</td></tr>";
    }
    $htmlData .= "</tbody></table>";
}




$chartXML = "<graph caption='$graphTitle' xAxisName='$xTitle' yAxisName='$yTitle' decimalPrecision='0' formatNumberScale='0'>";
//$chartData = "<set name='Jan' value='462' color='AFD8F8'/><set name='Feb' value='857' color='F6BD0F'/><set name='Mar' value='671' color='8BBA00'/><set name='Apr' value='494' color='FF8E46'/><set name='May' value='761' color='008E8E'/><set name='Jun' value='960' color='D64646'/><set name='Jul' value='629' color='8E468E'/><set name='Aug' value='622' color='588526'/><set name='Sep' value='376' color='B3AA00'/><set name='Oct' value='494' color='008ED6'/><set name='Nov' value='761' color='9D080D'/><set name='Dec' value='960' color='A186BE'/>";

$chartXML .= $chartData;
$chartXML .= "</graph>";

if ($f == "xml") {
    echo $chartXML;
} else if ($f == "html") {
    echo $htmlData;
}
?>
