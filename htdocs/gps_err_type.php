<?php
include_once ("../include/dbal.php");

$dbal = new DbAbstractionLayer();

$err_type_list = array();

header("Content-type: text/xml");
if($dbal->connect()){
	$sql = "SELECT * FROM gps_err_type";
	$result = $dbal->queryDb($sql);
	if($result){
		while($row = $dbal->loopResult($result)){
			$id = $row["id"];
			$name = $row["name"];
			$description = $row["description"];
			$err_type_list [] = "<gps_err_type><id>$id</id><name>$name</name><description>$description</description></gps_err_type>";
		}
	}
}
echo "<gps_err_type_list>" . join(",",$err_type_list) . "</gps_err_type_list>";
?>