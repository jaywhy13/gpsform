<?php
require_once ("../include/permguru.php");
require_once ("../include/kiaapi.php");
require_once("../include/blabber.php");

$pg = new PermGuru ( "gpsform" );

$kia = new Kia ( );

$sr = $_GET["sr"]; // skip required keys
if(empty($sr)){
	$sr = true;
} else {
	if($sr == "f"){
		$sr = false;
	}
}

function manage_sql_calls($action, $relation) {

	if (! $action)
	return false;
	if (! $relation)
	return false;
	$pg = $GLOBALS ['pg'];
	if (! $pg->isLoggedIn ())
	return false;

	$bot = $pg->getPermissionBot ();

	if ($action == "read")
	$ans = $bot->canRead ( $relation );
	if ($action == "create")
	$ans = $bot->canCreate ( $relation );
	if ($action == "update")
	$ans = $bot->canUpdate ( $relation );
	if ($action == "delete")
	$ans = $bot->canDelete ( $relation );

//		if($ans) echo "User can $action relation: $relation<br>";
    
	return $ans;
}

function screenValues($relation,$field,$value){
	return $value;
	//	return "xxxxx";

}

$kia->setScreener("screenValues");

function snoopRunSql($sql){
	$pg = $GLOBALS['pg'];
	if($pg){
		if($pg->isLoggedIn()){
			$pg->runSQL($sql);
		}
	}
}

$blabber_function = "snoopRunSql"; // set the blabber function


function log_sql ($action,$relation,$fields,$values){
	$usrAlias = "guest";
	$pg = $GLOBALS ['pg'];
	$lg = $GLOBALS['lg'];
	if($pg->isLoggedIn()) $usrAlias = $pg->getUserAlias();
	$msg = "$usrAlias $action";

	if($action == "create" || $action == "delete" || $action == "update") $msg .= "d";
	$msg .= " a record in $relation. ";

	if($fields && !$values){
		$msg .= " Fields selected: " . join(",",$fields);
	}
	else if($fields && $values){
		$messages = array();
		foreach($fields as $field){
			$messages [] = "$field = " . $values[$field];
		}
		$msg .= join(",",$messages);
	}

	blabber($msg,$relation);

}

$kia->setSupervisor ( "manage_sql_calls" );
$kia->setSpyFunction("log_sql");


if ($_GET ["p"] == "x") {
	$kia->setPrinterType ( KIA_XML );
} else if($_GET["p"] == "xls"){
	$kia->setPrinterType ( KIA_XLS );
}
else {
	$kia->setPrinterType ( KIA_JSON );
}

$action = $_GET ["action"];
$rel = $_GET ["rel"];
$view_name = $_GET ["vn"];
$ob = explode(",",$_GET["ob"]);
if(sizeof($ob) == 0){
	$ob = null;
}
if($_GET["cl"]) $cl = explode(",",$_GET["cl"]);
else $cl = null;

if(empty($action)){
	$kia->getPrinter()->kiaerror("No action specified");
}


if(empty($_GET["sc"]) || !isset($_GET["sc"])) $special_conds = array();
else {

	$special_conds_tmp = explode(":::",$_GET["sc"]);	// arrays of special conds
	for($i = 0; $i < sizeof($special_conds_tmp); $i++){
		$tmp_arr = explode("::",$special_conds_tmp[$i]);
		$special_conds[$tmp_arr[0] . ":$i"] = $tmp_arr[1];
	}
}

//echo "Special Conds:<br>";
//print_r($special_conds);
//echo "<br>";

if ($_GET ["uid"] == "undefined") unset ( $_GET ["uid"] );

foreach($_GET as $key=>$value){
	if($value == "undefined"){
		$kia->getPrinter()->kiaerror("Cannot proceed, $key has value: $value");
	}
}


if ($action == "list" && ! empty ( $rel )) {
	$arr = $kia->getColumnNames ( $rel, $sr );
	$kia->getPrinter ()->kiaprintarr ( $arr );
} else if ($action == "create" || $action == "update" || $action == "delete") {
	$kia->magic ( $action, $rel, null, $_GET );
} else if ($action == "read") {
	if (ereg ( ",", $rel )) {
		$kia->magic_xtq ( $action, explode ( ",", $rel ), $cl, $_GET, $view_name, $special_conds , $ob);
	} else {
		$kia->magic ( $action, $rel,$cl, $_GET, $view_name, $special_conds, $ob );
	}
} else if ($action == "listkeys") {
	$arr = $kia->getColumnKeys ( $rel );
	$kia->getPrinter ()->kiaprintarr ( $arr );
} else if ($action == "listfkeys") {
	$arr = $kia->getColumnKeys ( $rel, true );
	$kia->getPrinter ()->kiaprintarr ( $arr );
} else if ($action == "createseq") {
	$key_field = $_GET ["kf"];
	$seq = $_GET ["seq"];

	if (! isset ( $seq ))
	$seq = NULL;
	if (empty ( $seq ))
	$seq = NULL;

	$kia->magic_seq ( "create", $rel, $key_field, $_GET, $seq );
} else if ($action == "createmultiple") {
	$delim = $_GET ["delim"];
	$count = $_GET ["count"];

	$multiples = array ();
	$rowData = array ();

	for($i = 0; $i < $count; $i ++) {
		foreach ( $_REQUEST as $key => $value ) {
			if (ereg ( "\\" . $delim, $value ) && $delim != $value) {
				$tmp_arr = explode ( $delim, $value );
                $val = $tmp_arr [$i];
				$row [$key] = $val;
                if($val == $delim) $val = "";
			} else {
                $val = $value;
                if($val == $delim) $val = "";
				$row [$key] = $val;
                
			}

        }
		$rowData [] = $row;
	}

	$old_printer = $kia->getPrinterType();

	$kia->setPrinterType ( KIA_BOOL ); // use the bool printer to prevent the dying and prevent the execution of all the iterations

    $kia->runSQL("BEGIN");
    for($i = 0; $i < $count; $i ++) {
		$row = $rowData [$i];
		if (!$kia->magic ( "create", $rel, NULL, $row )) {
			$kia->setPrinterType($old_printer);
            $kia->getPrinter()->kiaerror("Could not save your data! All incomplete data has been removed!");
		} 
	}
    $kia->runSQL("END");

	$kia->setPrinterType($old_printer);
	$kia->getPrinter()->kiamsg("Your data was successfully saved to the system");
}

?>
