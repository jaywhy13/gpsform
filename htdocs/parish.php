<?php
include_once ("../include/dbal.php");
$dbal = new DbAbstractionLayer ( );

if (! empty ( $_GET ["parish_id"] )) {
	$parish_id = $_GET ["parish_id"];
}

header("Content-type: text/xml");

$parishList = array ();
$parishList [] = "<parish><name>I don't know</name><id>69</id></parish>";

if ($parish_id != 69) {
	if ($dbal->connect ()) {
		$sql = "select parish_id, name from gps_parish ";
		if (isset ( $parish_id )) {
			$sql = $sql . " where gid = $parish_id AND custom = false ";
		} else {
		$sql  = $sql . " WHERE custom = false ";
		  }   
		$sql = $sql . " order by name";
		$result = $dbal->queryDb ( $sql );
		if ($result) {
			while ( $row = $dbal->loopResult ( $result ) ) {
				$gid = $row ["parish_id"];
				$name = $row ["name"];
				$parishList [] = "<parish><name>$name</name><id>$gid</id></parish>";
			}
		}
	}
}

echo "<parish_list>" . join ( ",", $parishList ) . "</parish_list>";

?>