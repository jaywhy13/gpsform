<?php
include_once("../include/db.php");
include_once("../include/permguru.php");
include_once("../include/accountability.php");

lg("Some test message");

echo "this is a test";
echo getParam("dbconfigfile-psql");

$arr = array();
$arr["onez"] = 1;
$arr["two2"] = 2;
echo "the count was found to be: " . count($arr);

?>
<html>
	<head>
		<script src="jquery-1.2.6.min.js"></script>
		<script src="flagger-client.js"></script>
		<title>SQL Template .. Testing</title>
		<style>
		TABLE {
			font-family:Verdana;
			font-size:11px;
		}
		
		BODY {
			font-size:12px;		
		}
		</style>
		<script>
		function highlightErrors(divId){
			var domObj = document.getElementById(divId);
			if(domObj){
				var str = domObj.innerHTML;
				if(str.toLowerCase().indexOf("error") > -1){
					domObj.style.color = "red";
					domObj.style.fontSize = 11;
					domObj.style.fontWeight = "bold";
				}
			}
		}


		</script>
	</head>
	<body onLoad="highlightErrors('divLog'); initEvaluator('evaluator');">
<?	

function printStuff($i,$arr){
	echo "$i: ";
	print_r($arr);
	echo "<br>";
}

$permGuru = new PermGuru();

$ptype = $_GET["perm_type"];
$ptag = $_GET["perm_tag"];
$pdescription = $_GET["perm_description"];
$usr = $_GET["usr"];
$uid = $_GET["uid"];
$psw = $_GET["psw"];
$newpsw = $_GET["newpsw"];
$gid = $_GET["gid"];
$galias = $_GET["galias"];
$usrgrp = $_GET["usr_grp"];
$action = $_GET["action"];
$id = $_GET["id"];
$pgid = $_GET["pgid"];
$puid = $_GET["puid"];
if($ptype == ""){
	$ptype = $_GET["pmt_type"];
}
$bits = $_GET["permission"];

// Pre

echo "<div style='font-family:Verdana; font-size:11px; background-color:rgb(200,220,255); width:75%; padding:10px;' id='divLog'>";
?>
		<a href="test.php">Reload</a><br>

<?
if($action == "CreatePerm") {

	if($ptype != ""){
		try {
			$permGuru->addPermissionType($ptype,$ptag,$pdescription);
			echo "Notice: Permission Type: \"$ptype\" created";
		}
		catch(Exception $e){
			echo "Error occurred while attempting to create the permission type: " . $e->getMessage() . "<br>";
		}
	} else {
		echo "Error: Could not create permssion type, fieids missing...<br> ";
		print_r($_GET);
	}
} else if ($action == "DeletePerm"){
	if($ptype != ""){
		$permGuru->removePermissionType($ptype);
	}
} else if($action == "CreateUser"){
	if($usr != "" && $psw != ""){
		try {
			$permGuru->addUser($usr,$psw);
		}
		catch (Exception  $e)
		{
			echo "Error: Could not create the user. " . $e->getMessage() . "<br>";
		}
	}
} else if($action == "DeleteUser"){
	if($usr != "" && $psw != ""){
		try{$permGuru->removeUserByAlias($usr);} catch (Exception $e){
			echo "Error: Create not remove the user. " . $e->getMessage() . "<br>";
		}
	}
} else if($action == "UpdatePsw"){
	if($newpsw != "" && $psw != "" && $usr != "") {
		$permGuru->changeUserPassword($uid,$psw,$newpsw);
	}
} else if($action == "DeleteByUid"){
	if(($uid != '')){
		try {
			$permGuru->removeUser($uid);
		} catch(Exception $e){
			echo "Error: Cannot remove the user. " . $e->getMessage() . "<br>";
		}
	} else {
		
	}
} else if($action == "CreateGroup"){
	if($galias != ""){
		try{
			$permGuru->addGroup($galias);
		} catch (Exception $e){
			echo "Error: Cannot create the group. " . $e->getMessage() . "<br>";
		}
	}
} else if($action == "DeleteGroup"){
	if($gid != ""){
		try {
			$permGuru->removeGroup($gid);
		} catch(Exception $e){
			echo "Error: Cannot remove the group. " . $e->getMessage() . "<br>";

		}
	}
} else if($action == "AddUserToGroup"){
	if($usrgrp != ""){
		$pieces = explode(":",$usrgrp);
		try {
			$permGuru->addUserToGroup($pieces[0],$pieces[1]);
		} catch (Exception $e){
			echo "Error: Could not add the user to the group ... " . $e->getMessage() . "<br>";
		}
	}
} else if($action == "RemoveUserFromGroup"){
	if($usrgrp != ""){
		$pieces = explode(":",$usrgrp);
		try {
			$permGuru->removeUserFromGroup($pieces[0],$pieces[1]);
		} catch (Exception $e){
			echo "Error: Could not remove the user from the group ... " . $e->getMessage() . "<br>";
		}
	}
} else if($action == "AssignToGid"){
	if($pgid != "" && $bits != "" && $ptype != "" ){
		try {
			$permGuru->addGroupPermission($pgid,$ptype,$bits);
		} catch (Exception $e){
			echo "Error: Could not assign permissions. " . $e->getMessage();
		}
	} else {
		echo "Error : Please specify a gid, permission type and permission";
	}
} else if($action == "AssignToUid"){
	if($puid != "" && $bits != "" && $ptype != ""){
		try {
			$permGuru->addUserPermission($puid,$ptype,$bits);
		} catch(Exception $e){
			echo "Error: Could not assign permissions. " . $e->getMessage();
		}
	}
} else if($action == "ShowGroupPerms"){
	if($id != ""){
		$groupArr = $permGuru->getGroupPermissions($id);
		echo "<br>";
		$p = new PermObj($groupArr,$permGuru->getDb());
		echo $p;
	}
} else if($action == "ShowUserPerms"){
	if($id != ""){
		$userResult = $permGuru->getUserPermissions($id);
		echo "<br>";
		$p = new PermObj($userResult,$permGuru->getDb());
		echo $p;
	}
} else if($action == "regularizePerms"){
	$permGuru->regularizePermissions(true);
}

echo "</div>";
echo "<hr>";
echo "<b>Gibberish</b><br/>";

echo "<br/><br/>";

//$permGuru->mapSQL("(SELECT * FROM rule WHERE rule_scope = 'group' AND rule_ref IN (SELECT gid FROM usr_in_grp WHERE uid = 13 )) ","printStuff");
//
//$result = $permGuru->runSQL("(SELECT * FROM rule WHERE rule_scope = 'group' AND rule_ref IN (SELECT gid FROM usr_in_grp WHERE uid = 13 )) ");
//echo "From fetch<br>";
//$arr = $permGuru->getDb()->fetchArray($result);
//print_r($arr);
//
//echo "<br>Loop through<br>";
//while($row = $permGuru->getDb()->fetchArray($result)){
//	print_r($row);
//}
//

echo "<hr/>";

echo "<b>Permission Types:</b><br/>";
$permGuru->mapSQL("SELECT * FROM "	. PERM_TBL . " ORDER BY pmt_tag, pmt_id","printStuff");

echo "<br><b>Users:</b><br/>";
$permGuru->mapSQL("select * from usr","printStuff");

echo "<br><b>Groups:</b><br>";
$permGuru->mapSQL("select * from grp","printStuff");

echo "<br><b>Groups and Members:</b><br>";
$permGuru->mapSQL("select grp.gid, grp.alias as group_name, usr.uid, usr.alias as username from usr, grp, usr_in_grp where usr_in_grp.uid = usr.uid and usr_in_grp.gid = grp.gid  order by grp.alias, usr.alias","printStuff");

echo "<br><b>Permissions:</b><br>";
$permGuru->mapSQL("select * from rule","printStuff");

echo "<br><br>";


?>

	
	
		<form action="test.php" method="GET">
		<table>
			<tr>
				<td>
				<table>
					<tr>
						<td colspan="2">Add Permission Type:</td>
					</tr>
					<tr><td>Type:</td><td><input name="perm_type"></td></tr>
					<tr><td>Tag:</td><td><input name="perm_tag"></td></tr>
					<tr><td>Description:</td><td><input name="perm_description"></td></tr>
					<tr><td></td><td><input type="submit" name="action" value="CreatePerm">
								<input type="submit" name="action" value="DeletePerm">
					</td></tr>
				</table>
				</td>
			</tr>
		</table>
		
		<br>
		<br>
		
		<table>
			<tr><td>Uid:</td><td><input name="uid"></td></tr>
			<tr><td>Username:</td><td><input name="usr"></td></tr>
			<tr><td>Password:</td><td><input type="password" name="psw"></td></tr>
			<tr><td>New Password:</td><td><input type="password" name="newpsw"></td></tr>
			<tr><td></td><td><input name="action" value="CreateUser" type="submit"><input type="submit" name="action" value="DeleteUser"><input type="submit" name="action" value="UpdatePsw"><input type="submit" name="action" value="DeleteByUid"/></td></tr>
			<tr><td></td><td></td></tr>
		
		</table>
		
		<br>
		<br>
		
		<table>
		<tr><td>Gid:</td><td><input name="gid"/></td></tr>
		<tr><td>Group Alias:</td><td><input name="galias"/></td></tr>
		<tr><td>Usr:Group</td><td><input name="usr_grp" value="uid:gid"/></td></tr>
		<tr><td></td><td>
			<input type="submit" name="action" value="CreateGroup"/>
			<input type="submit" name="action" value="DeleteGroup"/>
			<input type="submit" name="action" value="AddUserToGroup"/>
			<input type="submit" name="action" value="RemoveUserFromGroup"/>
		</td></tr>
		
		</table>
		
		
		<br><br>
		
		<table>
		<tr><td>Perm Type:</td><td><input name="pmt_type"/></td></tr>
		<tr><td>Permission:</td><td><input name="permission"/></td></tr>
		<tr><td>Uid:</td><td><input name="puid"/></td></tr>
		<tr><td>Gid:</td><td><input name="pgid"/></td></tr>
		<tr><td></td>
		<td>
		<input type="submit" value="AssignToUid" name="action"/>
		<input type="submit" value="AssignToGid" name="action"/>
		</td></tr>
		
		</table>

		
		
		<br><br>
		
		<table>
		<tr><td>Show Permissions:</td><td><input name="id"/></td></tr>
		<td>
		<input type="submit" value="ShowGroupPerms" name="action"/>
		<input type="submit" value="ShowUserPerms" name="action"/>
		</td></tr>
		
		</table>		
		
		
		<table>
		<tr><td>Regularize Permissions:</td><td><input name="action" value="regularizePerms" type="submit"/></td></tr></tr>
		
		</table>		

		
		</form>
		
		<div id="evaluator">
		</div>
		
	</body>

</html> 