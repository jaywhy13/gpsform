<?php
include_once("../include/dbal.php");
$err_tag_list = array();

header("Content-type: text/xml");
$dbal = new DbAbstractionLayer();
$sql = "SELECT * FROM gps_err_tag";
if($dbal->connect())
{
	$result = $dbal->queryDb($sql);
	if($result){
		while($row = $dbal->loopResult($result)){
			$id = $row["err_tag_name"];
			$description = $row["err_tag_description"];
			$err_tag_list [] = "<err_tag><id>$id</id><description>$description</description></err_tag>";	
		}
	}
}
echo "<err_tag_list>" . join(",",$err_tag_list) . "</err_tag_list>";
?>
