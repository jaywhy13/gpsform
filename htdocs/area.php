<?php
include_once("../include/dbal.php");
$areaList = array();
$dbal = new DbAbstractionLayer();
if(!empty($_GET["parish_id"])){
	if($_GET["parish_id"] != 69) $parish_id = $_GET["parish_id"]; // value of 69 is "I don't know"
}



header("Content-type: text/xml");

$areaList [] =   "<area><id>unknown</id><name>I don't know</name><parish_id>69</parish_id></area>";

if($dbal->connect()){
	$sql = "select area_id, name, parish_id from gps_area ";
	if(isset($parish_id)){
		$sql = $sql . " where parish_id = $parish_id and custom = False";
	} else {
		$sql = $sql . " where custom = false";	
	}
	
	$sql = $sql . " order by name";
	
	$result = $dbal->queryDb($sql);
	if($result){
		while($row = $dbal->loopResult($result)){
			$gid = $row["area_id"];
			$name = htmlentities($row["name"]);
			if($name == "") continue;
			$parishgid = $row["parish_id"];
			$areaList [] =   "<area><id>$gid</id><name>$name</name><parish_id>$parishgid</parish_id></area>";
		}
	}
}

echo "<area_list>" . join(",",$areaList) . "</area_list>";


?>