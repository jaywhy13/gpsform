<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html>
    <head>

        <script src="../js/jquery-1.5.1.min.js"></script>
        <script src="../js/jquery-ui-1.8.12.custom.min.js"></script>
        <script type="text/javascript">
            var jQ = jQuery.noConflict();
        
        </script>
        <script type="text/javascript" src="scripts/prototype/prototype.js"></script> 
        <script type="text/javascript" src="scripts/util/Util-Broadcaster.js"></script> 
        <script type="text/javascript" src="scripts/util/Util-BrowserDetect.js"></script> 
        <script type="text/javascript" src="scripts/util/Util-DateTimeFormat.js"></script> 
        <script type="text/javascript" src="scripts/util/Util-PluginDetect.js"></script> 
        <script type="text/javascript" src="scripts/util/Util-XmlConverter.js"></script> 
        <script type="text/javascript" src="scripts/device/GarminObjectGenerator.js"></script> 
        <script type="text/javascript" src="scripts/device/GarminPluginUtils.js"></script> 
        <script type="text/javascript" src="scripts/device/GarminDevice.js"></script> 
        <script type="text/javascript" src="scripts/device/GarminDevicePlugin.js"></script> 
        <script type="text/javascript" src="scripts/device/GarminDeviceControl.js"></script> 
        <script type="text/javascript" src="scripts/ContentDelivery.js"></script>         
        <link href="../css/sunny/jquery-ui-1.8.12.custom.css" rel="stylesheet"/>
        <style type="text/css">
            #pbar {
                width:300px;
                height:25px;
            }

            .animated {
                background-image: url(../images/pbar-ani.gif); 
            }

        </style>
        <script type="text/javascript">
            // Garmin keys ...
            var hostKeys = {};
            hostKeys["http://10.0.40.151"] = "839c4c1a6f30f6edae6c10f01f34ea8a";
            hostKeys["http://demo.monagis.com"] = "89f6bf534f83fc492170c3cd96a61617";
            
            // FileInfo Array
            // Add to this array if you want to support multiple files
            //
            // 0: DataType Name - The data type of the file, e.g. VCTVoices
            // 1: Device Path - This will automatically be found so please ignore
            // 2: File Name - The file name that will be on the device after the download
            // 3: Source - The source location to download the file from
            // 4: UNLSource - The UNL source location to download the file from
            // 5: Dropdown DisplayName - The name to display in the dropdown menu
            var fileInfo = new Array(
            ["CustomPOI", "Device Path", "gpiTest.gpi", "http://10.0.40.151/gpsform/delivery/public/gpiTest.gpi", "http://10.0.40.151/gpsform/delivery/gmapsupp.unl", "Test GPI File"],    
            ["VCTVoices", "Device Path", "vctTest.vpm", "http://10.0.40.151/gpsform/delivery/public/vctTest.vpm", "http://localhost/ContentDelivery/public/vctTest.unl", "Test VCT File"]);
 
            var fileInfo = new Array(["GPSData","Device Path","gmapsupp.img","http://10.0.40.151/gpsform/delivery/data.img","http://10.0.40.151/gpsform/delivery/gmapsupp.unl","JAMNAV Data"]);
            
            function getFileInfoParamByIndex(index){
                return fileInfo[0][index];
            }
            
            function getHostKey(){
                var host = window.location.toString();
                var arr = new Array();
                for(var hostName in hostKeys){
                    if(host.indexOf(hostName) == 0){
                        arr.push(hostName);
                        arr.push(hostKeys[hostName]);
                        return arr;
                    }
                }
                return arr;
            }
            
            
            var listener = null;
            
            jQ(document).ready(function(){
                jQ("#pbar").progressbar();
                jQ("#pbar").progressbar({"value":50});
            });
            
            function progressIndeterminate(){
                jQ(".ui-progressbar-value").addClass("animated");
            }
            
            //            ContentDelivery.js notes
            function testGarminStuff(){
                if(listener == null){
                    listener = new gListener();
                }
                var control = new Garmin.DeviceControl();
                if(control.isPluginInitialized()){
                    var version = control.getPluginVersionString();
                    log("Version No: " + version);
                    if (control.unlock(getHostKey())){
                        log("Plugin unlocked!");
                        control.register(listener);
                        control.findDevices();
                    } else {
                        logError("Could not unlock plugin");
                    }

                } else {
                    logError("Plugin is not initialized!!!");
                }

            }
            
            /**
             * Check to see if the GPS device supports GPSData 
             */
            function isDeviceSupported(device){
                var typesSupported = device.getDeviceDataTypes().values();
                for(var t = 0; t < typesSupported.length; t++){
                    var typeSupported = typesSupported[t];
                    var typeName = typeSupported.typeName;
                    var writeAccess = typeSupported.writeAccess;
                    var readAccess = typeSupported.readAccess;
                    var filePath = typeSupported.filePath;
                    if(typeName == "SupplementalMaps" && writeAccess){
                        return true;
                    }
                }                
                return false;
            }
            
            function describe(obj){
                var result = "";
                for(var key in obj){
                    result += key + " = " + obj[key];
                }
                return result;
            }
            
            function gListener(){
                
                // When we start looking for devices
                this.onStartFindDevices = function(json){
                    log("Looking for devices!");
                };
                
                // After we are through looking for the devices 
                this.onFinishFindDevices = function(json){
                    var deviceCount = json.controller.getDevicesCount();
                    if(deviceCount > 0){
                        log("Found " + deviceCount + " devices");
                        var devices = json.controller.getDevices();
                        for(var i = 0; i < devices.length; i++){
                            var device = devices[0];
                            var deviceSerial = device.getId();
                            var deviceNo = device.getNumber();
                            // control.deviceNumber stores current device number 
                            var deviceName = device.getDisplayName();
                            log("Examining device: " + deviceName);
                            if(isDeviceSupported(device)){
                                log("Device is supported");
                                var path = getFileInfoParamByIndex(1);
                                var fileName = getFileInfoParamByIndex(2);
                                var source = getFileInfoParamByIndex(3);
                                var destination = path + "/" + fileName;
                                
                                var unlSource = getFileInfoParamByIndex(4);
                                var unlFileName = fileName.substring(0,fileName.lastIndexOf(".")) + ".unl";
                                var unlDestination = path + "/" + unlFileName;
                                
                                var gmaSource = unlSource.substring(0,unlSource.lastIndexOf(".")) + ".gma";
                                var gmaFileName = fileName.substring(0,fileName.lastIndexOf(".")) + ".gma";
                                var gmaDestination = path + "/" + gmaFileName;
                                
                                log("<hr>");
                                log("Path: " + path);
                                log("Filename: " + fileName);
                                log("Source: " + source);
                                log("Destination: " + destination);
                                log("UNL Source: " + unlSource);
                                log("UNL Filename: " + unlFileName);
                                log("UNL Dest: " + unlDestination);
                                

                                log("GMA Source: " + gmaSource);
                                log("GMA Filename: " + gmaFileName);
                                log("GMA Dest: " + gmaDestination);
                                
                                var descriptionArray = ([source, destination, unlSource, unlDestination, gmaSource, gmaDestination]);    
                                var xml = Garmin.GpiUtil.buildMultipleDeviceDownloadsXML(descriptionArray);
                                //                                json.controller.
                                this.download(xml,json);


                            } else {
                                logError("This device is not supported, sorry");
                            }
                    
                        }
                    } else {
                        log("No devices found");
                    }
                };
                
                this.onProgressWriteToDevice = function(json){
                    var p = json.progress.getPercentage();
                    $("#percentage").html("Loading data " + p);
                };
                
                this.onFinishWriteToDevice = function(json){
                    $("#percentage").html("Data download through!!!");
                };
                
                this.download = function(xml,json)
                {                                            
                    // Sets the download status
                    log("Downloading data, do not disconnect device");
                    // We download to the device depending on the xml file
                    // The filename parameter that downloadToDevice needs can be ignored and will be removed in a later API release
                    try
                    {                                                        
                        json.controller.downloadToDevice(xml, "");
                    }
                    catch (e)
                    {
                        logError("Failed to download the data to the device! " + e.message);
                    }
                };
        
                
                
            }
            
            function logError(msg){
                jQuery("<div class='error'>" + msg + "</div>").appendTo("#info");
            }

            function log(msg){
                jQuery("<div class='info'>" + msg + "</div>").appendTo("#info");
            }
            
            function describe(obj,asString){
                if(!asString){
                    var asString = false;
                }
                
                var arr = new Array();
                for(var key in obj){
                    var val = obj[key];
                    arr.push(key + "=" + val);
                }
                
                result = arr.join(", ");
                if(asString){
                    return result;
                } else {
                    alert(result);
                }
                
            }

        </script>
        <style type="text/css">
            .info {
                color:green;
            }

            .error {
                color:red;
            }
        </style>

    </head>
    <body>
        <div id="pbar">

        </div>
        <input value="Go" onclick="testGarminStuff()" type="button"/>
        <div id="info">
            Searching...
        </div>
        <div id="percentage">
        </div>
    </body>


</html>