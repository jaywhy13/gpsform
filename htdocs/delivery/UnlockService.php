<?php

$esn = $_GET["esn"];
$selected = $_GET["selected"];

// The existing parameter values are used for testing
// Please modify the following with your own parameter values
// Replace www.garmin.com with legacy.garmin.com when going live
$host = "legacy.garmin.com";
//$host = "www.garmin.com";
$loginname = "testlogin";
$password = "testpass";
$ucode = "";

// "0" for MPC
// "1" for Content Toolkit
$type = "1";

// GMA signature version which you probably never need to modify at all
$version = "262145";

switch ($selected)
  {
  case 0:
    /***** USER DEFINED VARIABLES *****/
    $familyID = 12345;
    $productID = 2;
    $regionID = 0;
    $subregionID = 0;
    $fullname = "C:\\Inetpub\\wwwroot\\ContentDelivery\\public\\gpiTest.unl";
  
    // GMA signature filename co-existing with the unlock code
    // filename is flexible but extension needs to be "*.gma"
    $gmafile = "C:\\Inetpub\\wwwroot\\ContentDelivery\\public\\gpiTest.gma";
    break;
  case 1:
    /***** USER DEFINED VARIABLES *****/
    $familyID = 12345;
    $productID = 2;
    $regionID = 0;
    $subregionID = 0;
    $fullname = "C:\\Inetpub\\wwwroot\\ContentDelivery\\public\\vctTest.unl";
  
    // GMA signature filename co-existing with the unlock code
    // filename is flexible but extension needs to be "*.gma"
    $gmafile = "C:\\Inetpub\\wwwroot\\ContentDelivery\\public\\vctTest.gma";
    break;
	
  // If you add to the client dropdown list, make sure to add the corresponding index here
  default:
    break;
  }

// Constructs request string
$qs = "cgi-bin/mapact/mapact_licensee.cgi?loginname=" . $loginname . 
  "&password=" . $password . 
  "&n=" . $esn . 
  "&f=" . $familyID . 
  "&p=" . $productID . 
  "&r=" . $regionID . 
  "&s=" . $subregionID .
  "&type=" . $type . 
  "&v=" . $version;

$sock = fsockopen($host, 80, $errno, $errstr);

if (!$sock)
  {
  echo "[Error]: $host http connection error\n";
  } 
else 
  {
  $result = file_get_contents("http://$host/$qs", FALSE, NULL);
  fclose($sock);  

  // byte 1: unlock code error ID
  $e = substr($result, 0, 1);
  $ebytes = unpack("Ce", $e);
  $errcode = sprintf("%d", $ebytes['e']);
  //echo "Error ID for unlock code = $errcode <br>\n";

  if ($errcode == 0)
    {
    // byte 2 - 26: the unlock code
    $ucode = substr($result, 1, 25);
    //echo "Unlock Code = $ucode <br>\n\n";
    
    // write unlock code to a file	
    $file = fopen($fullname, 'w');
    fwrite($file, $ucode);
    fclose($file);

    // byte 27: GMA error ID
    $e = substr($result, 26, 1);
    $ebytes = unpack("Ce", $e);
    $errcode = sprintf("%d", $ebytes['e']);
    //echo "Error ID for GMA file = $errcode <br>\n";

    // byte 28 - 29: GMA data length
    $l = substr($result, 27, 2);
    $lbytes = unpack("Sl", $l);
    $len = sprintf("%d", $lbytes['l']);
    //echo "Data length for GMA file = $len <br>\n";
       
    // byte 30 and up: GMA data
    if ($errcode == 0 && $len > 0)
      {
      $fp = fopen($gmafile, "wb");
      if (fwrite($fp, substr($result, 29)) == FALSE)
        {
        echo "[ERROR]: Cannot write to file '$gmafile' <br>\n";
        }
      else
        {
        fclose($fp);
        //echo "GMA saved onto file '$gmafile' <br>\n";
        }
      }
    else
      {
      echo "[ERROR]: GMA data length error (error=$errcode) <br>\n";
      }
    }
  else
    {
    echo "[ERROR]: no result from server (error=$errcode) <br>\n";
    }          
  }


echo $ucode;

?>