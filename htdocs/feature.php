<?php

include_once ("../include/dbal.php");
$featureList = array ();
$query = strtolower ( $_GET ["query"] );
$parish_id = $_GET ["parish_id"];
$area_id = $_GET ["area_id"];

if ($parish_id == 69)
	unset ( $parish_id );
if ($area_id == "unknown")
	unset ( $area_id );

$dbal = new DbAbstractionLayer ( );
header ( "Content-type: text/xml" );

if (! empty ( $query )) {
	if ($dbal->connect ()) {
		if (! empty ( $_GET ["fid"] )) {
			$fid = $_GET ["fid"];
		}
		
		$feature_type = "road";
		if (! empty ( $_GET ["ft"] )) {
			$feature_type = $_GET ["ft"];
		}
		
		$key_id = "";
		$tbl_name = "";
		echo "<!-- $feature_type -->";
		
		if ($feature_type == "road") { // Road
			
			$key_id = "road_id";
			$tbl_name = "gps_road";
		
		} else if ($feature_type == "poi") // POI
		{
			$key_id = "poi_id";
			$tbl_name = "gps_poi";
		}
		
		$sql = "select $key_id, parish_id, area_id, name from $tbl_name";
		
		if (isset ( $fid )) {
			$sql = $sql . " where $key_id = $fid";
		} else {
			$sql = $sql . " where name != 'road' AND LOWER(name) LIKE '%$query%'";
			if (! empty ( $parish_id )) {
				$sql = $sql . " AND parish_id = $parish_id ";
			}
			
			if (! empty ( $area_id )) {
				$sql = $sql . " AND area_id = $area_id ";
			}
		}
		
		$sql = $sql . " AND custom = False ";
		
		$sql = $sql . " order by name limit 400";
		
//		echo "<!-- $sql -->";
		
		if (isset ( $feature_type ) && $feature_type != "routing") {
			$result = $dbal->queryDb ( $sql );
			if ($result) {
				while ( $row = $dbal->loopResult ( $result ) ) {
					$id = $row [$key_id];
					$name = htmlentities ( $row ["name"] );
					$parish = $row ["parish_id"];
					$area = $row ["area_id"];
					$featureList [] = "<feature><id>$id</id><name>$name</name><parish_id>$parish</parish_id><area_id>$area</area_id></feature>";
				}
			}
		}
	}
}
echo "<feature_list>" . join ( ",", $featureList ) . "</feature_list>";

?>