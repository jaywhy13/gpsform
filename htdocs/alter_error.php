<?php

if(!empty($_GET["action"])) $action = $_GET["action"];
if(!empty($_GET["err_id"])) $err_id = $_GET["err_id"];
if(!empty($_GET["status"])) $status = $_GET["status"];
if(!empty($_GET["repeat_id"])) $repeat = $_GET["repeat_id"];
if(!empty($_GET["version"])) $version = $_GET["version"];

$message = "";

include_once ("../include/dbal.php");
$dbal = new DbAbstractionLayer ( );

if(!$dbal->connect()) die("error:No connection to database");

function query($sql){
    return $GLOBALS['dbal']->queryDb($sql);
}

function update_error_status($err_id,$status="unfixed",$repeat=-1,$version=""){
    $clauses = array();
    $clause_str = "";
    
    if(is_numeric($repeat) && $repeat >= 0){
        $clauses [] = "repeat_err_id = $repeat";
    }

    if($version != ""){
        $clauses [] = "version = '$version'";
    }

    if(sizeof($clauses) > 0){
        $clause_str = "," .  join(",",$clauses);
    }
    
    
    $sql = "UPDATE gps_err SET status = '$status' $clause_str WHERE err_id = $err_id";
    if(query($sql)){
        if($message == "") $message = "Error # " . $err_id . " status changed to $status";
        
        if($status == "repeat"){
            $message .= " Click <a target=''_blank'' href=''track_error.php?id=$repeat''>here<a> to see the repeat error.";
        }
        
        $sql = "INSERT INTO gps_err_log VALUES ($err_id,'$message','now()','$status')";
        $result = query($sql);
        echo "success:Successfully updated error # $err_id";
    } else {
        echo "error:Could not change the error status of error # $err_id";
    }
}

if($action == "delete"){
    if(isset($err_id)){
        $sql = "DELETE FROM gps_err WHERE err_id IN ($err_id)";
        $result = $dbal->queryDb($sql);

        $arr = explode(",",$err_id);
        if(sizeof($arr) > 1){
            for($i = 0; $i < sizeof($arr); $i++){
                $err_id_single = $arr[$i];
                if($message == "") $message = "Error # " . $err_id_single . " was deleted";
                $sql = "INSERT INTO gps_err_log VALUES ($err_id_single,'$message','now()','deleted')";
                $result = $dbal->queryDb($sql);

            }
        } else {
            if($message == "") $message = "Error # " . $err_id . " was deleted";
            $sql = "INSERT INTO gps_err_log VALUES ($err_id,'$message','now()','deleted')";
            $result = $dbal->queryDb($sql);
        }


        if($result){
            echo "success:Successfully deleted the errors";
        } else {
            echo "error:Could not delete the errors";
        }
    } else {
        echo "error:Nothing to delete";
    }
}
else if($action == "update"){
    if(!isset($status) || empty($status)){
        echo "error:No status set for error # $err_id";
    } else {
        update_error_status($err_id,$status,$repeat,$version); // update the error status

    }
}

?>