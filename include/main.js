/**
* TODO: getKeysFromServer ... add functionality to exclude keys from initial
* fetch!!!! For use when we doing pseudo edits, keys will be added later.
* Check back override total ... if cancel is pressed... no override should persist
*
* @author jaydub
* @return
*/

var tax = 1.165;
var markup = 1.1;
var commish = 0.1;
var dataCost = 115;
var sdDataCost = 145;
var restrictedHosts = new Array("localhost","10.0.40","192.168.1");

String.prototype.reverse = function() {
	splitext = this.split("");
	revertext = splitext.reverse();
	reversed = revertext.join("");
	return reversed;
};

function formatCurrency(num, prefix) {
	if (!prefix)
	var prefix = "J $";
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
	cents = "0" + cents;
	for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
	num = num.substring(0, num.length - (4 * i + 3)) + ','
	+ num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + prefix + num + '.' + cents);
}

var icoPaymentOutstanding 	= 	"warning.png";
var icoGenerateProducts		=	"images/pda.png";
var icoOrderComplete		=	"images/accept-24x24.png";
var icoViewOrderDetails		=	"images/Documents-32x32.png";
var icoManagePayments		=	"images/credit-cards-32x32.png";
var icoDeleteOrder			=	"images/delete-32x32.png";
var icoOrderDetails			=	"images/visa-72x72.png";
var icoAddPayment			=	"images/add-16x16.png";
var icoRemovePayment 		=	"images/delete-16x16.png";
var icoOrderNote			=	"images/comment-16x16.png";




/**
* Prefix for the customer operation panel. It is suffixed by the customer id.
*/
var operationPanelPrefix = "cust_operate_";

/**
* Prefix for the customer order details div. It is suffixed by the customer id.
*/
var orderDivPrefix = "order_info_";

/**
* Prefix for the customer product details div. It is suffixed by the customer
* id.
*/
var productDivPrefix = "product_info_";

var msdPaymentAddLinkPrefix = "add_payment_";


function toggleOrderInfo(nodeId){
	var node = jQuery("#" + nodeId).get()[0];
	if(node){
		if(node.style.display == "none"){
			jQuery("#" + nodeId).show("slow");
		} else {
			jQuery("#" + nodeId).hide("fast");
		}
	}
}

/**
* Opens or closes the customer operations panel. This panel shows contact
* details (phone, email) and allows the user to edit/delete the customer. It
* also shows customer orders
*
* @param id
* @return
*/
function toggleCustomerOperationPanel(id, regenerate) {

	if(isNaN(id)) return;

	if (!regenerate)
	var regenerate = false;


	jQuery("#" + operationPanelPrefix + id).each( function() {

		if (this.style.display == "none") {
			// Close ALL others FIRST
			jQuery(".customer").each(function(){
				var nodeId = this.id;
				var custId =  getTrailingNumber(nodeId);
				var node = jQuery("#" + operationPanelPrefix + custId).get()[0];
				if(node) node.style.display = "none";
			});
			this.style.display = "block";
		} else {
			this.style.display = "none";
			if (!regenerate) {
				return;
			}
		}
	});

	fetchCustomerOrderData(id);
	fetchCustomerProductData(id);
}

function selectProductVersion(ser){
	var url = "crudmachine.php?action=read&rel=gps_product&product_serial=" + ser + "&vn=product";
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			var products = obj.products;
			if(products.length > 0){
				var product = products[0];
				var cust_id  = product["cust_id"];
				var order_id = product["order_id"];
				var serial   = product["product_serial"];

				var row = {
				"cust_id":cust_id,
				"order_id":order_id,
				"product_serial":serial
				};
				editForm("gps_customer_update","Customer Data Version",row,true,true);
			}
		};
	})(ser));
}

function promptDeviceSerial(oldId, custId) {
	var newId = prompt("Enter the real serial number", "");
	if (!newId) {
		return;
	}

	if (newId == oldId) {
		alert("The value has not changed");
		return;
	}

	if (confirm("By clicking OK you confirm that the serial number: " + newId
	+ " is correct?"))
	changeDeviceSerial(oldId, newId, custId);
	else {
		promptDeviceSerial(oldId, custId);
	}
}

function changeDeviceSerial(oldId, newId, custId) {
	jQuery.get("cquery.php?action=changedeviceserial&old_id=" + oldId
	+ "&new_id=" + newId, null, ( function() {
		return function(data) {
			var obj = eval(data);
			alert(obj.message.content);
			if (obj.message.type == "notification")
			if (custId) {
				fetchCustomerProductData(custId);
				alert("You will now be prompted to choose the version the customer will be getting.");
				selectProductVersion(newId);
			}
		};
	})(oldId, newId, custId));
}

function fetchCustomerProductData(id) {
	var divId = productDivPrefix + id;
	jQuery("#" + divId).html("Loading product data ...");
	var url = "crudmachine.php?action=read&vn=product&rel=gps_product,gps_package&cust_id="
	+ id + "&ob=gps_product.product_serial";
	jQuery
	.get(
	url,
	null,
	( function() {
		return function(data, status) {
			var obj = eval(data);
			var products = obj.products;
			var count = products.length;

			if (count == 0) {
				jQuery("#" + divId).html("No products.");
			} else {
				var tbl = document.createElement("table");
				try { var tblId = "product_list_" + products[0].cust_id; tbl.id = tblId; } catch (e) {}
				var tbody = document.createElement("tbody");
				tbl.appendChild(tbody);

				tbody.appendChild(produceTr( {
				"product_type":"Type",
				"id" :"<b>Serial Number</b>",
				"package" :"<b>Package Name</b>",
				"lockcode" :"<b>Lock Code</b>",
				"versioninfo":"<b>Version Info</b>"
				}));

				var productArr = new Array();
				
				for ( var p = 0; p < count; p++) {
					var product = products[p];
					var serial = product.product_serial;
					productArr.push(serial);
					if (product.serial_fake == "t") {
						serial = "<a href='javascript:void(0)' onClick='promptDeviceSerial("
						+ product.product_serial
						+ ","
						+ id
						+ ")' style='color:red; font-decoration:underline; font-weight:bold;' title='Please enter the serial number!'>"
						+ "Enter Serial #" + "</a>";
					}

					var lockCodeUrl = "<a href='javascript:void(0);' title='Fetch lock code' id='serial_"
					+ product.product_serial
					+ "' onClick='fetchLockCode("
					+ product.product_serial
					+ ")'>[ Get Lock Code ]</a>";

					for(var i = 0; i < restrictedHosts.length; i++){
						var host = restrictedHosts[i];
						if(window.location.toString().indexOf(host) > -1){
							lockCodeUrl = "[ Fetching Disabled ]";
						}
					}
					
					lockCodeUrl += "<span id='lock_status_" + serial + "'></span>";
					

					if (product.serial_fake == "t") {
						lockCodeUrl = "";
					}

					var productIcon = product.product_type;

					if(product.product_type == "Internal Memory"){
						productIcon = "<a href='javascript:void(0)' title='Click to change this product type to Card' onClick=updateCardType('" + product.product_serial + "','Card'," + product.cust_id + ");><img src='images/iPod-Touch-32x32.png' width='24px' border='0'></a>";
					}
					else if(product.product_type == "Card"){
						productIcon = "<a href='javascript:void(0)' title='Click to change this product type to Internal Memory' onClick=updateCardType('" + product.product_serial + "','Internal'," + product.cust_id + ");><img src='images/Sandisk-6-in-1-32x32.png' width='24px' border='0'></a>"
					}


					var versionStr = "<a href='javascript:void(0)' title='Click here to see version history for this product' onClick=showVersionHistory('" + product.product_serial + "')>View Versions</a>";
					versionStr += "&nbsp;&nbsp;<a  href='javascript:void(0)' title='Click here to indicate that a new version has been sent out to a customer' onClick=selectProductVersion('" + product.product_serial + "')>+</a>";

					if(product.serial_fake == "t"){
						versionStr = "";
					}



					var product_details = {
					"product_type":productIcon,
					"id" :serial,
					"name" : (product.package_name + "<a name='link_" + product.product_serial + "'></a>"),
					"lockcode" :lockCodeUrl,
					"versioninfo":versionStr
					};

					var tr = produceTr(product_details);

					tbody.appendChild(tr);
				}

				
				// Setup the filter
				var div = jQuery("#" + divId).get()[0];
				div.innerHTML = "";
				var inp = document.createElement("input");
				inp.style.padding = 3;
				inp.style.width = 100;
				inp.style.margin = 3;
				inp.id = "filter_" + products[0].cust_id;
				
				div.appendChild(inp);
				if(tblId){
					inp.tblId = tblId;
					inp.onkeyup = function(){
						filterSerial(this.value,this.tblId);
					};
					inp.onfocus = function(){
						filterSerial(this.value,this.tblId);
					};
					inp.onblur = function(){
						filterSerial(this.value,this.tblId);
					};
					inp.onclick = function(){
						filterSerial(this.value,this.tblId);
					}
				}
				div.appendChild(document.createTextNode(" Filter Serial"));
				div.appendChild(document.createElement("br"));
				var divResults = document.createElement("div");
				div.appendChild(divResults);
				divResults.id = "div_product_list_" + product.cust_id;
				divResults.style.paddingTop = 7;
				divResults.style.paddingBottom = 7;
				divResults.style.width = 600;
				divResults.style.overflow = "auto";
				divResults.style.lineHeight = 1.5;
				
				div.appendChild(tbl);

				fetchLockStatus(productArr);
			}
		};
	})(id, divId));

	checkDelinquency(id);
}

function filterSerial(query,tblId){
	clearSerialResults(getTrailingNumber(tblId));
	var trs = jQuery("#" + tblId + " TR").each(function(){
		var tr = this;
		var td = tr.childNodes[1];
		var val = td.innerHTML.toLowerCase();
		if(query.length < 4) return;
		if(!isNaN(val)){
			if(val.toString().match("^" + query.toLowerCase())){
				jQuery(td).effect("highlight",{"color":"#ffaabb"},1500);
				addSerialResult(val,getTrailingNumber(tblId));
			}
		}
	});
}

function addSerialResult(ser,custId){
	var divId = "div_product_list_" + custId;
	var div = jQuery("#" + divId).get()[0];
	var span = document.createElement("span");
	span.innerHTML = ser;
	span.style.fontDecoration = "underline";
	span.style.padding = 3;
	span.style.margin = 5;
	span.style.lineHeight = 1.5;
	span.style.backgroundColor = "white";
	span.style.width = 70;
	span.style.cursor = "pointer";
	span.setAttribute("title","Click to jump to the result");
	span.aLink = "link_" + ser;
	span.custId = custId;
	span.onclick = function(){
		filterSerial(this.innerHTML,"product_list_" + this.custId);
		jQuery("#" + "filter_" + this.custId).get()[0].value = this.innerHTML;
		window.location.hash = this.aLink;
	};
	
	var spanNodeCount = jQuery("#" + divId + " SPAN").get().length;
	if((spanNodeCount > 0) && (parseInt(spanNodeCount / 5) == spanNodeCount / 5)){
		div.appendChild(document.createElement("br"));
	}
	
	div.appendChild(span);
}

function clearSerialResults(custId){
	var divId = "div_product_list_" + custId;
	jQuery("#" + divId).empty();
}


function restoreSerialColors(tblId){
	var trs = jQuery("#" + tblId + " TR").get();
	if(trs){
		if(trs.length == 1) return;
		for(var i = 1; i < trs.length; i++){
			var tr = trs[i];
			var td = tr.childNodes[1];
			td.style.backgroundColor = "white";
		}
	}

}

function fetchLockStatus(arr){
	if(arr.length < 110){
		var str = arr.join(",");
		jQuery.get("cquery.php?action=getlockstatus&ser=" + str,(function(){
			return function(data){
				var obj = eval(data);
				if(obj.message.type == "notification"){
					var str = obj.message.content;
					var statusArr = str.split(",");
					window.status = statusArr.length + " serials.";
					for(var i = 0; i < statusArr.length; i++){
						var statusInfo = statusArr[i];
						var parts = statusInfo.split(":");
                                                var serAndServ = parts[0].split("_");
						var ser = serAndServ[0];
                                                var serverType = serAndServ[1];
						var statusCode = parts[1];
						var nodeId = "lock_status_" + ser + "_" + serverType;
						var caption = "";
						jQuery("#" + nodeId).each(function(){
							var img = document.createElement("img");
							img.style.border = 0;
							var imgLink = null;
							if(statusCode == "fetched"){
								imgLink = "001_36.png";
								caption = "Lock code has been fetched. Click \"Get lock code\" to get it again!";
							} else if(statusCode == "unfetched"){
								imgLink = "001_34.png";
								caption = "Lock code has NOT been fetched. Click \"Get lock code\" to see it!";
							}
							img.src = imgLink;
							img.title = caption;
							if(imgLink) this.appendChild(img);
						});
					}
				}
			};
		})(arr));
	}
}

function showVersionHistory(serial){
	var url = "crudmachine.php?rel=gps_customer_update&vn=version&action=read&product_serial=" + serial;
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			var versions = obj.versions;
			var versionArr = new Array();
			if(versions.length == 0) {
				alert("No version history");
				return;
			}
			for(var i = 0; i < versions.length; i++){
				var version = versions[i];
				versionArr.push(version["version_no"] + " - " + version["dispatch_date"]);
			}
			alert(versionArr.join("\n"));
		};
	})(serial));
}


function updateCardType(serial,pType,cId){
	if(!cId) {
		alert("No CID supplied");
		return;
	}


	if(pType == "Internal") pType = "Internal Memory";
	var url = "crudmachine.php?action=update&rel=gps_product&product_serial=" + serial + "&product_type=" + pType;
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			if(obj.message.type == "notification"){
				fetchCustomerProductData(cId);

			} else {
				alert(obj.message.content);
			}
		};
	})(serial,pType,cId));
}

function fetchLockCode(product_serial) {

	if(window.location.toString().indexOf("localhost") > -1){
		alert("No lock code fetching allowed on localhost!");
		return;
	}

	jQuery
	.get(
	"cquery.php?action=getlockcode&serial=" + product_serial,
	null,
	( function() {
		return function(data) {
			var obj = eval(data);
			if (obj.message.type == "notification") {

				jQuery
				.get(
				"crudmachine.php?rel=gps_lock&action=read&product_serial="
				+ product_serial,
				null,
				( function() {
					return function(data) {
						var obj = eval(data);
						if (obj.gps_locks) {
							if (obj.gps_locks.length == 0) {
								alert("You are not privileged enough to see lock codes!");
								return;
							}
							var gps_lock = obj.gps_locks[0];
							alert("Serial: "
							+ obj.gps_locks[0].product_serial
							+ "\nLock Code: "
							+ gps_lock.lock_code
							+ "\nDate retrieved: "
							+ gps_lock.lock_timestamp);
						}
					};
				})(product_serial));
			} else {
				alert(obj.message.content);
			}
		};
	})(product_serial));
}

function fetchCustomerOrderData(id) {
	var divId = orderDivPrefix + id;
	var url = "crudmachine.php?action=read&vn=order&rel=gps_customer,gps_order&cust_id="
	+ id + "&ob=gps_order.order_id";
	jQuery("#" + divId).html("Loading order data ...");
	jQuery
	.get(
	url,
	null,
	( function() {
		return function(data, status) {
			var obj = eval(data);

			var orders = obj["orders"];
			if(!orders){
				if(obj.message){
					alert(obj.message.content);
				} else {
					alert(data);
				}
				return;
			}
			if (orders.length == 0) {
				jQuery("#" + orderDivPrefix + id).html(
				"No orders made");
			} else {


				var strHtml = "<table class='orderDetail' cellpadding=5 cellspacing=0>";

				for ( var i = 0; i < orders.length; i++) {
					var order = orders[i];

					var order_title = process("order_id",order.order_id);
					+" ";

					var statusImg = "<img src='" + icoPaymentOutstanding + "' title='Payments outstanding'>";

					if (order.order_complete == "t"
					&& order.order_generated == "f") {
						statusImg = "<a href='javascript:void(0);' onClick='generateProducts("
						+ order.order_id
						+ ","
						+ order.cust_id
						+ ")'><img  border='0' src='" + icoGenerateProducts + "' width='24px' height='24px' title='Order complete, click to generate products!'/></a>";
					} else if (order.order_complete == "t"
					&& order.order_generated == "t") {
						statusImg = "<img src='" + icoOrderComplete + "' width='24px' height='24px' title='Order Complete, Products Generated'>";
					}

					var order_date = orders[i].order_date;
					if(order_date.indexOf("00:00:00") > -1){
						order_date = order_date.substring(0,order_date.indexOf("00:00:00"));
					}

					strHtml += "<tr><td valign='bottom' class='orderDetailTd' width='25px'>"
					+ statusImg
					+ "</td><td valign='top' class='orderDetailTd'>";




					var orderDetailTblStr = "<table border='0' cellpadding=0 cellspacing=0>";


					if(order.order_total_orig != order.order_total && order.order_total_orig > 0){

						orderDetailTblStr += "<tr><td>Override Info:</td><td class='orderNo'>";
						var overrideNote = "This order total was overriden. The original figure is: " + formatCurrency(order.order_total_orig) + ". Reason: " + order.order_note ;
						orderDetailTblStr += "&nbsp;&nbsp;<img src='" + icoOrderNote + "' title='" + overrideNote + "' border='0' style='cursor:hand'>";
						orderDetailTblStr += "</td></tr>";

					}

					orderDetailTblStr += "<tr><td>Order Date:</td><td>"
					+ order_date
					+ "</td></tr>";



					var redHighlight = "";
					var amountOwing = formatCurrency(order.order_amount_owing);

					if(order.order_amount_owing > 0){
						redHighlight = "style='color:red; font-weight:bold;'";
					} else {
						amountOwing = " - ";
					}


					// Add the order total
					orderDetailTblStr += "</td></tr><tr><td>Order Total:</td><td>"
					+ formatCurrency(order.order_total)
					+ "</td></tr>";


					// Add the discount
					orderDetailTblStr += "<tr><td>Order Discount:</td><td>" + (order.order_discount) * 100 + "&nbsp;%</td><tr>";

					// Add the tax
					orderDetailTblStr += "<tr><td>Order Tax:</td><td>" + formatCurrency(order.order_tax) + "</td><tr>";


					// Add the amount outstanding
					orderDetailTblStr += "</td></tr><tr><td " + redHighlight + ">Outstanding:</td><td " + redHighlight + ">"
					+ formatCurrency(order.order_amount_owing)
					+ "</td></tr>";



					orderDetailTblStr += "<tr><td valign='top'>Order Note:</td>";

					if(order.order_note != ""){
						orderDetailTblStr += "<td><img src='images/reminder.png' width='24px' title='" + order.order_note + "'>&nbsp;";
						orderDetailTblStr += "<a href='javascript:void(0)' onClick=editOrderNote(" + order.order_id + "," + order.cust_id + ");>Edit</a>&nbsp;";
						orderDetailTblStr += "<a href='javascript:void(0)' onClick='removeOrderNote(" + order.order_id + "," + order.cust_id + ")';>Remove</a>";
						orderDetailTblStr += "</td>";
					} else {
						orderDetailTblStr += "None&nbsp;&nbsp;<a href='javascript:void(0)' onClick='addOrderNote(" + order.order_id + "," + order.cust_id + ")';>Add</a>";
						orderDetailTblStr += "</td>";
					}

					orderDetailTblStr += "</tr>";

					// Add non-product expenses
					orderDetailTblStr += "<tr><td valign='top'>Non-Product Expenses:<br>(Updates, Subscriptions, etc...)</td><td><a href='javascript:void(0);' onClick=addNPE(" + order.cust_id + "," + order.order_id + ")><img border=0 src='images/Button-Add-16x16.png'>Add</a><br><div style='padding:3px;' id='oe_list_" + order.order_id + "'>Loading ...</div></td></tr>";


					orderDetailTblStr += "</table>";


					// Do up the summary header
					var header = "<div class='orderSummaryHeader'><span onClick=toggleOrderInfo('order_info_" + order_title + "');  title='Click to see more information about Order # " + order_title + "'>Order # " + order_title + "</span>";

					header += ("<a href='javascript:void(0);' onclick=viewOrderDetails({\"order_id\":"
					+ order.order_id + ",\"cust_id\":" + order.cust_id);

					header += ("}) title='View Order Details'><img border='0' src='" + icoViewOrderDetails + "' width='20px'/>View Order</a>&nbsp;");

					var underScoreReplacedPackageId = order_title
					.replace(/ /g, "_");

					header += ("<a href='javascript:void(0);' onclick=managePayments({\"order_id\":"
					+ order.order_id + ",\"cust_id\":" + order.cust_id);

					header += (",\"roe_id\":'" + order.roe_id
					+ "'},\""
					+ underScoreReplacedPackageId + "\") title='Manage Payments'><img width='20px' border='0' src='" + icoManagePayments + "'>Manage Payments</a>&nbsp;");

					// replace spaces with underscores coz
					// javascript thinks everything after the
					// space is a new arg, prob coz the string
					// is so long??? not sure... no time to
					// figure that out

					var deleteLink = "";
					deleteLink += "<a href='javascript:void(0);' onclick=deleteAction({\"order_id\":"
					+ order.order_id
					+ ",\"cust_id\":"
					+ order.cust_id
					+ ",\"roe_id\":'"
					+ order.roe_id
					+ "'},'gps_order',function(){fetchCustomerOrderData("
					+ order.cust_id
					+ ");}) border=0 'Delete Order'><img  width='20px' border='0' src='" + icoDeleteOrder + "'/>Delete Order</a>";

					header += deleteLink;
					header += "&nbsp;";


					strHtml += header;

					strHtml += "<div style='display:none;' class='orderInfo' id='order_info_" + order_title + "'>" + orderDetailTblStr + "</div>";

					strHtml += "</td>";


					strHtml += "</tr>";

				}

				strHtml += "</table>";
				// alert(strHtml);

				jQuery("#" + divId)
				.empty()
				.html(
				"<img src='" + icoOrderDetails + "' width='48px' height='48px' style='margin:-5px;'><b style='font-size:14px;'>Order Details:</b><br>")
				.html("<b>Order Details:</b><br>");
				jQuery(strHtml).appendTo("#" + divId);

				for(var i = 0; i < orders.length; i++){
					window.setTimeout("fetchOrderExpense(" + orders[i].order_id + ")",1500+800*i);
				}
			}

		};
	})(id, divId));

	checkDelinquency(id);
}

function fetchOrderExpense(id){
	var url = "crudmachine.php?rel=order_expense,order_expense_type&action=read&vn=order_expense&p=j&order_id=" + id;
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			var nodeId = "oe_list_" + id;
			var node = document.getElementById(nodeId);
			if(!node) return;
			if(obj.order_expenses){
				if(obj.order_expenses.length == 0){
					node.innerHTML = "No expenses.";
					return;
				}

				jQuery("#" + nodeId).empty(); // clear out the node
				var tbl = document.createElement("table");
				tbl.setAttribute("cellpadding",2);
				tbl.setAttribute("cellspacing",0);
				var tbody = document.createElement("tbody");
				tbl.appendChild(tbody);
				node.appendChild(tbl);

				var total = 0;
				for(var i = 0; i < obj.order_expenses.length+1; i++){
					var tr = document.createElement("tr");

					var qtyTd = document.createElement("td");
					var descriptionTd = document.createElement("td");
					var totalTd = document.createElement("td");
					var delTd = document.createElement("td");
					var statusTd = document.createElement("td");


					tr.appendChild(statusTd);
					tr.appendChild(delTd);
					tr.appendChild(qtyTd);
					tr.appendChild(descriptionTd);
					tr.appendChild(totalTd);


					if(i >= obj.order_expenses.length){
						totalTd.innerHTML = formatCurrency(total);
						descriptionTd.innerHTML = "<b>Total:</b>";
						descriptionTd.setAttribute("align","right");
						tbody.appendChild(tr);
						continue;
					}

					var order_expense = obj.order_expenses[i];
					qtyTd.innerHTML = order_expense.oe_quantity;
					descriptionTd.innerHTML = order_expense.oe_title;
					totalTd.innerHTML = formatCurrency(order_expense.oe_total);
					var a = document.createElement("a");
					a.href = "javascript:void(0)";
					a.orderId = order_expense.order_id;
					a.oeId = order_expense.oe_id;
					a.custId = order_expense.cust_id;
					a.oeNo = order_expense.oe_no;
					a.onclick = function(){
						removeOrderExpense(this.orderId,this.oeId,this.custId,this.oeNo);
					};
					a.innerHTML = "<img border='0' src='images/Button-Delete-16x16.png' title='Remove'>";
					delTd.appendChild(a);


					var caption = "";
					a = document.createElement("a");
					a.href = "javascript:void(0)";
					a.orderId = order_expense.order_id;
					a.oeId = order_expense.oe_id;
					a.custId = order_expense.cust_id;
					a.oeNo = order_expense.oe_no;
					if(order_expense.oe_paid == "t"){
						caption = "Click to mark as not paid!";
						a.innerHTML = "<img border='0' src='images/Tick-48x48.png' width='21px' title='" + caption + "'>";
						a.stat = true;
					} else {
						caption = "Click to mark as paid!";
						a.innerHTML = "<img border='0' src='images/important-32x32.png' width='21px' title='" + caption + "'>";
						a.stat = false;
					}

					a.onclick = function(){
						updateOrderExpenseStatus(this.orderId,this.oeId,this.custId,this.oeNo,this.stat);
					};


					delTd.appendChild(a);



					total += parseFloat(order_expense.oe_total);
					tbody.appendChild(tr);
				}



			} else {
				if(obj.type == "error")
				{
					alert(obj.message.content);
				}
			}
		};
	})(id))

}

function updateOrderExpenseStatus(orderId,oeId,custId,oeNo,stat){
	if(!orderId) return;
	if(!oeId) return;
	if(!oeNo) return;
	if(!custId) return;

	var msg;
	if(stat == true) msg = "Do you want to mark this payment as unpaid?";
	else msg = "Do you want to mark this payment as paid?";

	if(!confirm(msg)) return;

	var status = stat == true ? "f"  : "t";
	var url = "crudmachine.php?rel=order_expense&action=update&order_id=" + orderId + "&oe_id=" + oeId + "&oe_paid=" + status + "&cust_id=" + custId + "&oe_no=" + oeNo;
	jQuery.get(url,((function(){
		return function(data){
			var obj = eval(data);
			if(obj.message){
				alert(obj.message.content);
				if(obj.message.type == "notification"){
					fetchOrderExpense(orderId);
				}
			}
		};
	}))(orderId,oeId,status));

}

function removeOrderExpense(orderId,oeId,custId,oeNo){
	if(!orderId) return;
	if(!oeId) return;
	if(!oeNo) return;
	if(!custId) return;

	if(!confirm("Are you sure you want to delete this expense? This cannot be undone!")) return;
	var url = "crudmachine.php?rel=order_expense&action=delete&order_id=" + orderId + "&oe_id=" + oeId + "&cust_id=" + custId + "&oe_no=" + oeNo;
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			alert(obj.message.content);
			if(obj.message.type == "notification"){
				fetchOrderExpense(orderId);
			}
		};
	})(orderId,oeId));
}

function addOrderNote(oid,cid,note,acceptNull){
	if(!note && !acceptNull) var note = prompt("Enter the order note");

	if(!note){
		return;
	}

	jQuery.get("crudmachine.php?action=update&p=j&rel=gps_order&order_id=" + oid + "&order_note=" + note,(function(){
		return function(data){
			var obj = eval(data);
			if(!obj) {
				alert(data);
				return;
			}
			if(obj.message.type == "notification"){
				alert("Note updated");
				fetchCustomerOrderData(cid);
				fetchCustomerProductData(cid);

			}
		}
	})(oid,cid));
}

function editOrderNote(oid,cid,note){
	if(!note) var note = "";
	var newNote = prompt("Enter the new note",note);
	if(newNote){
		if(confirm("Are you sure you want to update the note?")) addOrderNote(oid,cid,newNote);
	}
}

function removeOrderNote(oid,cid){
	if(confirm("Are you sure you want to delete the note?")) addOrderNote(oid,cid,"",true);
}

function addMessage(title,content){
	if(!title) var title = "";
	if(!content) return;

	var d = new Date();
	var dateStr = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	//	var dateStr = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	var url = "crudmachine.php?rel=message&action=create&msg_content=" + content + "&msg_title=" + title + "&msg_timestamp=" + dateStr;
	alert(url);
	jQuery.get(url,(function(){
		return function(data){
			alert(data);
		};
	})());
}


function getDateString() {
	var d = new Date();
	var dateStr = d.getFullYear() + "-" + (d.getMonth() + 1) + "-"
	+ d.getDate();
	return dateStr;
}

function generateProducts(orderId, custId) {
	if (orderId) {

		jQuery.get("cquery.php?action=generateproducts&order_id=" + orderId
		+ "&cust_id=" + custId, null, ( function() {
			return function(data) {
				var obj = eval(data);
				alert(obj.message.content);
				if (obj.message.type == "notification")
				fetchCustomerOrderData(custId);
				fetchCustomerProductData(custId);

			};
		})(custId));

	}
}

/**
* The call back function used by the crud machine
*
* @param obj
*            obj returned from the crud machine
* @return
*/

function cm_callback(obj) {
	if (!obj)
	return;

	var relation = obj.relation;
	var action = obj.action;
	var components = obj.components;
	if (!components) {
		components = new Array();
	}
	var fi = obj.formIncrement;
	var cbId = obj.createButtonId;
	var formId = obj.formId;
	var objData = obj.objdata;

	// The following are only relevant for the action methods like updateAction
	// or createAction
	var data = obj.data;
	var params = obj.params;

	if (relation == "gps_customer")
	dsCustomer.loadData();
	else if (relation == "gps_package"
	&& (action == "create" || action == "update" || action == "delete"))
	dsPackage.loadData();

	// Loop through our components and make some changes
	for ( var i = 0; i < components.length; i++) {
		var component = components[i];

		if (grabColumnName(component) == "cust_id"
		&& (action == "createform" || action == "editform")
		&& relation != "gps_customer") {
			doSpryDsAliasing(component, "gps_customer",
			"{cust_fname} {cust_lname}", "cust_id",null,null,null,new Array("gps_customer.cust_fname"));
		} else if (grabColumnName(component) == "package_id"
		&& relation == "gps_package"
		&& (action == "createform" || action == "editform")) {
			var id = "gps_" + (new Date().getTime());
			var randomChars = "ofj8902309asdiou124uasdoij089124u09asdokj0912";
			for ( var c = 0; c < 10; c++) {
				var rndIndex = parseInt(Math.random()
				* (randomChars.length - 1));
				id += randomChars.charAt(rndIndex);
			}

			document.getElementById(component).value = id;
			hideRowInForm(component);


			var costNode = jQuery("#package_cost_" + fi).get()[0];
			//			costNode.disabled = true;

			var unitCostNode = jQuery("#package_unit_cost_" + fi).get()[0];
			unitCostNode.fi = fi;
			unitCostNode.onkeyup = function(){
				var fi = this.fi;
				var val = Math.round(this.value);
				calculatePackageRetailCost(fi);
			};


			var unitCostParent = unitCostNode.parentNode;
			unitCostParent.appendChild(document.createElement("br"));
			// create a table where we will choose the type
			var dtTbl = document.createElement("table");
			dtTbl.style.width = 200;
			dtTbl.setAttribute("width",200);
			var dtTbody = document.createElement("tbody");
			dtTbody.id = "data_type_" + fi;

			dtTbody.appendChild(produceTr({
			"dType":"<input type='radio' name='dtype' value='sdcard' onClick='calculatePackageRetailCost(" + fi + ")';>",
			"icon":"<img src='images/Sandisk-6-in-1-32x32.png'>",
			"caption":"SD Card Data"
			},false,{"dType":15,"icon":35,"caption":150}));

			dtTbody.appendChild(produceTr({
			"dType":"<input type='radio' name='dtype' value='unitdata' onClick='calculatePackageRetailCost(" + fi + ")';>",
			"icon":"<img src='images/iPod-Touch-32x32.png'>",
			"caption":"Unit + Data"
			}));


			dtTbody.appendChild(produceTr({
			"dType":"<input type='radio' name='dtype' value='dataonly' onClick='calculatePackageRetailCost(" + fi + ")';>",
			"icon":"<img src='images/text-file-32x32.png'>",
			"caption":"Data Only"
			}));

			dtTbody.appendChild(produceTr({
			"dType":"<input type='radio' name='dtype' value='other' onClick='calculatePackageRetailCost(" + fi + ")';>",
			"icon":"<img src='images/question-32x32.png'>",
			"caption":"Other"
			}));


			dtTbody.appendChild(produceTr({
			"dType":"",
			"icon":"",
			"caption":"<span id='data_formula_" + fi + "'>"
			}));

			dtTbl.appendChild(dtTbody);
			unitCostParent.appendChild(dtTbl);


			disableComponent("package_cost_" + fi);
			disableComponent("package_unit_cost_" + fi);


			stipulateRequirements(components, {
			"package_unit_cost" :"isnumber",
			"package_cost" :"isnumber",
			"package_name":"notnull"
			});


		} else if (grabColumnName(component) == "package_id"
		&& relation != "gps_package" && action == "createform") {
			doSpryDsAliasing(component, "gps_package",
			"{package_name} US$ {package_cost}", "package_id");
		} else if (grabColumnName(component) == "roe_id"
		&& relation != "rate_of_exchange" && action == "createform") {
			if (relation == "gps_order") {

				// Register node for calendar pop up
				var inpNode = jQuery("#order_date_" + fi).get()[0];
				if (inpNode) {
					registerNodeForCalendarInput(inpNode);
				}

				// Do rate of exchange for rate of exchange
				doSpryDsAliasing(component, "rate_of_exchange",
				"{roe_id} {roe_curr_name}$ {roe_value}", "roe_id",
				null, ( function() {
					return function(obj) {
						var componentId = obj["componentId"];
						var selectNode = jQuery("#" + componentId)
						.get()[0];
						if (selectNode) {

							selectNode.containerId = "order_contents_"
							+ getTrailingNumber(componentId);
							selectNode.onchange = function() {
								calculateTotal(this.containerId);
							};
						}
					};
				})());
			} else {
				doSpryDsAliasing(component, "rate_of_exchange",
				"{roe_id} {roe_curr_name}$ {roe_value}", "roe_id");
			}
		} else if (grabColumnName(component) == "order_date"
		&& action == "createform") {
			var node = jQuery("#" + component).get()[0];
			node.value = getDateString();
		} else if (grabColumnName(component) == "cust_jam_id_type") { // removed
			// createaction
			// check
			var node = jQuery("#" + component).get()[0];
			if (node) {
				var previousValue = null;
				var index = -1;
				if (node.value != null && node.value != "") {
					previousValue = node.value;
				}
				var selectNode = document.createElement("select");
				var options = [ "passport:Passport", "voter:Voter\'s Id",
				"license:Driver\'s License" ];

				for ( var j = 0; j < options.length; j++) {
					var option = document.createElement("option");
					var optionStr = options[j];
					var value = optionStr.split(":")[0];
					var caption = optionStr.split(":")[1];
					if (previousValue != null && value == previousValue)
					index = j;
					option.value = value;
					option.appendChild(document.createTextNode(caption));
					selectNode.appendChild(option);
				}

				var parent = node.parentNode;
				parent.removeChild(node);
				selectNode.id = component;
				parent.appendChild(selectNode);
				if (index > -1)
				selectNode.selectedIndex = index;
			}
		} else if (grabColumnName(component) == "cust_comment") {
			var inpNode = jQuery("#" + component).get()[0];
			if (inpNode) {
				var parent = inpNode.parentNode;
				var val = inpNode.value;
				var tArea = document.createElement("textarea");
				parent.removeChild(inpNode);
				tArea.id = component;
				if (val)
				tArea.value = val;
				parent.appendChild(tArea);
			}
		} else if (grabColumnName(component) == "payment_comment") {
			var inpNode = jQuery("#" + component).get()[0];
			if (inpNode) {
				var parent = inpNode.parentNode;
				var tArea = document.createElement("textarea");
				var val = inpNode.value;
				if (val)
				tArea.value = val;
				parent.removeChild(inpNode);
				tArea.id = component;
				parent.appendChild(tArea);
			}
		}

	} // Finish looping over components

	// Specific requirements for editing and creation of forms
	if (action == "editform" || action == "createform") { // action editform
		if (relation == "gps_order") {
			transformFormToInvoice(fi, formId, cbId); // create an invoice out
			// of a
			// normal gps_order form

			// stipulate guidelines for this form
			stipulateRequirements(components, {
			"cust_id" :"notnull",
			"roe_id" :"notnull",
			"order_date" :"notnull",
			"uid":"notnull"
			});

			doSpryDsAliasing("usr_commission_" + fi, "usr",
			"{alias}", "uid",null,function(obj){
				var nodeId = obj["componentId"];
				var node = jQuery("#" + nodeId).get()[0];
				if(!node) return;

				var option = document.createElement("option");
				option.value = -1;
				option.appendChild(document.createTextNode("No one"));

				node.insertBefore(option,node.firstChild);
				node.selectedIndex = 0;

			});

			discardComponentRowInForm("order_tax_" + fi);

		} else if (relation == "gps_customer") {
			stipulateRequirements(components, {
			"cust_lname" :"notnull"
			});
		} else if (relation == "gps_package") {
			var selectNode = document.createElement("select");
			var inpNode = jQuery("#package_type_" + fi).get()[0];
			var inpNodeId = inpNode.id;

			var options = [ "Basic:Basic","Premium:Premium","Standard:Standard", "Update:Update" ];
			changeNodeToSelect(inpNodeId, options);

			// Stipulate requirements
			stipulateRequirements(components, {
			"package_cost" :"isnumber",
			"package_name" :"notnull",
			"package_type" :"notnull"
			});

		} else if (relation == "rate_of_exchange") {
			// Stipulate requirements
			stipulateRequirements(components, {
			"roe_id" :"notnull",
			"roe_curr_name" :"notnull",
			"roe_value" :"isnumber"
			});

			var roeNode = jQuery("#roe_id_" + fi).get()[0];
			if (roeNode) {
				registerNodeForCalendarInput(roeNode);
			}
		}

		else if (relation == "gps_order_payment") {
			doSpryDsAliasing("cust_id_" + fi, "gps_customer",
			"{cust_lname}, {cust_fname}", "cust_id");
			hideRowInForm("order_id_" + fi); // hide order id
			discardComponentRowInForm("payment_id_" + fi); // this is a primary
			// key field ...
			// discard it
			// also change the payment type field to a drop down
			var pType = jQuery("#payment_type_" + fi).get()[0];
			if (pType) {
				var parent = pType.parentNode;
				var pTypeId = pType.id;

				var selectNode = document.createElement("select");
				parent.removeChild(pType);
				selectNode.id = pTypeId;
				selectNode.fi = fi;
				selectNode.onchange = function() {
					var fi = this.fi;
					var paymentNoId = "payment_no_" + fi;
					unhideRowInForm(paymentNoId);
					var val = this.options[this.selectedIndex].value;
					if (val == "Cash") {
						hideRowInForm(paymentNoId);
					} else if (val == "Wire") {
						changeComponentCaption(paymentNoId, "Wire Trans No.");
					} else if (val == "Deposit") {
						changeComponentCaption(paymentNoId, "Voucher No.");
					} else if (val == "Cheque") {
						changeComponentCaption(paymentNoId, "Cheque No.");
					}
				};

				var option;
				var typeArr = [ "Cash:Cash", "Wire:Wire Transfer",
				"Deposit:Bank Deposit", "Cheque:Cheque No." ];

				for ( var j = 0; j < typeArr.length; j++) {
					var paymentTypeStr = typeArr[j];
					var paymentType = paymentTypeStr.split(":")[0];
					var paymentCaption = paymentTypeStr.split(":")[1];
					option = document.createElement("option");
					option.value = paymentType;
					option.appendChild(document.createTextNode(paymentCaption));
					selectNode.appendChild(option);
				}

				parent.appendChild(selectNode);
				selectNode.onchange();

			}

			// Also, set the current date
			var pDate = jQuery("#payment_date_" + fi).get()[0];
			if (pDate) {
				pDate.value = getDateString();
			}

			var createBtnId = "jis_create_" + fi;
			var createBtn = jQuery("#jis_create_" + fi).get()[0];
			if (createBtn) {
				hookCreateAction(createBtn, relation, formId);
				createBtn.postCheck = createBtn.onclick;
				var postCheck = createBtn.onclick;
				createBtn.onclick = ( function() {
					var fid = formId;
					var callback = postCheck;

					return function() {
						var args = getInputAsArgs(fid);
						if (!args)
						return;
						var orderId = args["order_id"];
						var custId = args["cust_id"];
						var amount = args["payment_amount"];

						jQuery
						.get(
						"cquery.php?action=paymentvalid&order_id="
						+ orderId + "&cust_id="
						+ custId + "&amount=" + amount,
						null,
						( function() {
							return function(data) {

								// we know callback
								var obj = eval(data);
								if (obj.message) {
									if (obj.message.type == "notification") {
										if (obj.message.content == "success") {
											var createBtn = jQuery(
											"#"
											+ createBtnId)
											.get()[0];
											if (createBtn) {
												callback
												.call(createBtn);
											}
										} else {
											alert("Unrecognized response from server. Contact admin. General cquery error.");
										}
									} else {
										alert(obj.message.content);
									}
								} else {
									alert("Error occurred updating data. Contact admin! Cquery error.");
								}
							};
						})(callback, createBtnId));
					};
				})(formId, postCheck, createBtnId);
			}

			// Stipulate requirements
			stipulateRequirements(components, {
			"cust_id" :"notnull",
			"payment_amount" :"isnumber",
			"payment_date" :"notnull"
			});

		}
		else if(relation == "order_expense"){
			var f = (function(){
				return function(){
					calculateUpdateCost(fi);
				};
			})(fi);

			var g = (function(){
				return function(obj){
					if(obj["componentId"]){
						var componentId = obj["componentId"];
						if(document.getElementById(componentId))
						document.getElementById(componentId).onchange = f;
						calculateUpdateCost(fi);
					}
				}
			})(f,fi);


			var nodeId = "roe_id_" + fi;
			doSpryDsAliasing(nodeId, "rate_of_exchange","{roe_id} {roe_curr_name}$ {roe_value}", "roe_id",null,g);
			stipulateRequirements(components,{
			"oe_quantity" :"isnumber",
			"roe_id" :"notnull",
			"oe_total":"isnumber"
			});

			//			addComponentRowInForm("order_expense",fi,"oe_id",true);
			doSpryDsAliasing("oe_id_" + fi,"order_expense_type","{oe_title} ${oe_cost}","oe_id",null,g);

			// Set the quantity to one
			var qtyNode = document.getElementById("oe_quantity_" + fi);
			qtyNode.value = 1;
			qtyNode.onkeyup = f;


			// Hide the total ... add a span to show the total
			var span = document.createElement("span");
			span.id = "oe_total_span_" + fi;

			var totalNode = document.getElementById("oe_total_" + fi);
			totalNode.parentNode.appendChild(span);
			totalNode.setAttribute("type","hidden");

			// Change the oe_paid input to a hidden and add a checkbox that controls it's value
			var oePaidNode = jQuery("#oe_paid_" + fi).get()[0];
			var checkboxNode = document.createElement("input");
			checkboxNode.type = "checkbox";
			oePaidNode.parentNode.appendChild(checkboxNode);
			oePaidNode.value = "f";
			oePaidNode.type = "hidden";
			checkboxNode.id = "oe_paid_cb_" + fi;
			checkboxNode.onclick = (function(){
				return function(){
					var checked = false;
					checked =  jQuery("#oe_paid_cb_" + fi).get()[0].checked;
					var node = jQuery("#oe_paid_" + fi).get()[0];
					if(checked){
						node.value = "t";
					} else {
						node.value = "f";
					}
				};
			})(fi);



			//			hookCreateAction(node, tbl, containerId)
			hookCreateAction(document.getElementById("jis_create_" + fi),"order_expense",formId);

		} else if(relation == "gps_customer_update"){

			hookCreateAction(document.getElementById("jis_create_" + fi),"gps_customer_udpate",formId);
			var nodeId = "version_no_"  + fi;
			registerNodeForCalendarInput(jQuery("#dispatch_date_" + fi).get()[0]);
			doSpryDsAliasing(nodeId, "gps_data_version","{version_name} ({version_release_date})", "version_no",null,null);
			var createButton = jQuery("#jis_create_" + fi).get()[0];
			createButton.fi = fi;
			createButton.formId = formId;
			createButton.onclick = function(){
				var obj = getInputAsArgs(this.formId);
				var formId = this.formId;
				var url = "cquery.php?action=addupdate&";
				var params = new Array();
				for(var param in obj){
					params.push(param + "=" + obj[param]);
				}

				url += params.join("&");

				jQuery.get(url,(function(){
					return function(data){
						var o = eval(data);
						if(o.message){
							alert(o.message.content);
							if(o.message.type == "notification"){
								killForm(formId);
							} 						}
					};
				})(obj,formId));
			};

		}

	} else if (action == "create") { // action create
		if (relation == "gps_order_payment") {
			if (params) {
				var order_id = params["order_id"];
				var cust_id = params["cust_id"];
				if (!order_id) {
					alert("No order information supplied, cannot update order payment information!");
					return;
				}
				if (!cust_id) {
					alert("No customer information supplied, cannot update order payment information!");
					return;
				}

				updateOrderDetails(order_id, cust_id, ( function() {
					return function(data) {
						refreshPaymentWindow(params, data);
					};

				})(params));
			}
		}
		else if(relation == "order_expense"){
			var orderId = params["order_id"];
			fetchOrderExpense(orderId);
		}


	} else if (action == "delete") {

		if (relation == "gps_order_payment") {

			if (params) {
				var order_id = params["order_id"];
				var cust_id = params["cust_id"];
				if (!order_id) {
					alert("No order information supplied, cannot update order payment information!");
					return;
				}
				if (!cust_id) {
					alert("No customer information supplied, cannot update order payment information!");
					return;
				}

				updateOrderDetails(order_id, cust_id, ( function() {
					return function(data) {
						refreshPaymentWindow(params, data);
					};

				}) (params));
			}
		}
	}
}


function getRBSelection(fi){
	var id = "data_type_" + fi;
	var value = null;
	jQuery("#" + id + " INPUT").each(function(){
		if(this.type == "radio"){
			if(this.checked) value = this.value;
		}
	});

	return value;
}

function disableComponent(id){
	jQuery("#" + id).get()[0].disabled = true;
}

function enableComponent(id){
	jQuery("#" + id).get()[0].disabled = false;
}



function calculatePackageRetailCost(fi){

	disableComponent("package_cost_" + fi);
	disableComponent("package_unit_cost_" + fi);

	if(!getRBSelection(fi)) {
		//		alert("Please select a data type below!");
		return;
	}

	enableComponent("package_unit_cost_" + fi);

	var unitCostNode = jQuery("#package_unit_cost_" + fi).get()[0];
	var val = Math.round(unitCostNode.value);

	var totalNode = jQuery("#package_cost_" + fi).get()[0];
	if(isNaN(val)){
		totalNode.value = 0;
		return;
	}


	var dType = getRBSelection(fi);
	var cost = 0;
	var formula = "";
	if(dType == "sdcard"){
		disableComponent("package_cost_" + fi);
		disableComponent("package_unit_cost_" + fi);
		cost = sdDataCost;
		unitCostNode.value = cost;
	} else if(dType == "unitdata"){
		enableComponent("package_unit_cost_" + fi);
		disableComponent("package_cost_" + fi);
		cost = Math.round(val * markup) + dataCost;
		totalNode.focus();
		formula = " " + val + " * " + markup + " + " + dataCost + " = " + cost;
	} else  if(dType == "dataonly") {
		disableComponent("package_cost_" + fi);
		disableComponent("package_unit_cost_" + fi);
		cost = dataCost;
		unitCostNode.value = cost;
	} else if(dType == "other"){
		enableComponent("package_cost_" + fi);
		enableComponent("package_unit_cost_" + fi);
	}

	totalNode.value = cost;
	if(formula) {
		formula = (escape(formula));
		jQuery("#data_formula_" + fi).get()[0].innerHTML = "<a href='javascript:void(0)' onClick=alertE('" + formula + "');>View Formula</a>";
	} else {
		jQuery("#data_formula_" + fi).get()[0].innerHTML = "";
	}
}

function alertE(str){
	alert(unescape(str));
}

function transformFormToInvoice(fi, formId, cbId) {
	jQuery("#" + formId).css("width",750);
	var tblId = "tbl_gps_order_" + fi;
	jQuery("#" + tblId).css("width",720);
	var tbodyId = "tbody_gps_order_" + fi;
	var tbody = jQuery("#" + tbodyId).get()[0];
	// get the fourth row and insert a tr before that
	var trNumberFour = tbody.childNodes[3];
	if (trNumberFour) {
		var tr, td;

		// Create a row for the cashier
		tr = document.createElement("tr");
		td = document.createElement("td");
		td.innerHTML = "Cashier";
		tr.appendChild(td);

		td = document.createElement("td");
		var inpId = "uid_" + fi;
		td.innerHTML = "<input type='hidden' id='" + inpId + "'>";
		tr.appendChild(td);
		tbody.insertBefore(tr, trNumberFour);
		doSpryDsAliasing(inpId, "usr", "{alias}", "uid", null, null);

		tr = document.createElement("tr");
		td = document.createElement("td");
		td.innerHTML = "Order Details";
		tr.appendChild(td);

		td = document.createElement("td");
		td.id = "order_contents_" + fi;

		tr.appendChild(td);
		tbody.insertBefore(tr, trNumberFour);

		createInvoice(td.id); // create the invoice within the td

		// Change the on click event for the create button
		var createButton = jQuery("#" + cbId).get()[0];
		if (createButton) {
			createButton.fi = fi;
			createButton.formId = formId;

			createButton.onclick = function() { // on click for create
				// button
				// make the sale
				var containerId = "order_contents_" + this.fi;
				var order_note = "";
				var custName = "";
				var addNotificationMessage = false;
				// Check if the user ticked the checkbox and didn't put in a comment
				if(jQuery("#order_note_cb_" + this.fi).get()[0].checked){
					var ta = jQuery("#order_note_" + this.fi).get()[0];
					if(ta){
						if(ta.value == "" || ta.value == null) {
							alert("Please provide a reason for marking the order as complete!");
							return;
						} else {
							order_note = ta.value;
							var sel = jQuery("#cust_id_" + this.fi).get()[0];
							if(sel) custName = sel.options[sel.selectedIndex].text;
							addNotificationMessage = true;

						}
					}
				}

				var currentTotal = calculateTotal(containerId);

				if(currentTotal == 0){
					alert("No items are on this invoice!");
					return;
				}
				if(parseInt(currentTotal) != currentTotal){
					alert("Error: Cannot make this sale; Please ensure that the cost for all packages and the exchange rate is rounded off to the nearest dollar!");
					return;
				}

				var numRows = getInvoiceNumRows(containerId);
				if (numRows == 0) {
					alert("Please add items to the order before submitting!");
					return;
				}

				// args from the order form ... this does not include
				// order items

				var args = getInputAsArgs(this.formId);
				if (!args)
				return;

				this.order_html = document.getElementById("order_contents_"
				+ this.fi).innerHTML;

				jQuery("#order_contents_" + this.fi).html(
				"Saving order ... Please wait!!!!");

				// Now get the order items
				var invoiceDataObj = invoiceData["order_contents_" + this.fi];
				if (invoiceDataObj) {
					// Do up arrays
					var package_ids = new Array();
					var quantity_ids = new Array();
					var total_costs = new Array();

					for ( var i = 0; i < numRows; i++) {
						var invoiceDataRowObj = invoiceDataObj["itemArray"]["row"
						+ i];
						if (!invoiceDataRowObj)
						continue;

						// describe(invoiceDataRowObj);

						package_ids.push(invoiceDataRowObj["package_id"]);
						quantity_ids.push(invoiceDataRowObj["quantity"]);

						total_costs.push(invoiceDataRowObj["total"]);
					}

					var delim = "|";
					var urlParams = {};
					urlParams["package_id"] = package_ids.join(delim);
					urlParams["order_item_quantity"] = quantity_ids.join(delim);
					urlParams["order_item_total"] = total_costs.join(delim);

					// Add the total cost
					args["order_total"] = invoiceData[containerId]["order_total"];
					args["order_amount_owing"] = invoiceData[containerId]["order_total"];
					args["order_total_orig"] = invoiceData[containerId]["order_total_orig"];
					args["order_tax"] = invoiceData[containerId]["order_tax"];

					args["order_note"] = order_note;
					args["order_discount"] = invoiceData[containerId]["discount"];
					args["order_discount_amount"] = invoiceData[containerId]["discount_amount"];

					if(!args["usr_commission"] || args["usr_commission"] <= 0){
						delete args["usr_commission"];
					}
				}

				// Contact the server and create the order
				jQuery
				.get(
				"crudmachine.php?action=createseq&rel=gps_order&kf=order_id&seq=gps_order_seq",
				args, ( function() {
					return function(data) {

						var obj = eval(data);
						if (obj.message.content
						.indexOf("success") >= 0) {
							var pk = obj.message.content
							.split(":")[1];

							// describe(args);
							var order_id = pk;
							urlParams["order_id"] = pk;
							urlParams["cust_id"] = args["cust_id"];

							var cust_id = args["cust_id"];

							if(addNotificationMessage){
								addMessage("Payment pending for Order No." + order_id,"Order No. " + order_id + " for " + custName + " has been marked complete, however no payments have been received. You may dismiss this message when payment has been made.<br><a href='javascript:void(0)' onClick=managePaymentsCurr(" + cust_id + "," + order_id + ");>Add Payments</a> <a href='javascript:void(0);' onclick=viewOrderDetailsCurr("
								+ cust_id + "," + order_id+ ")>View Order</a>");
							}

							// Now save the order items
							// for the order
							jQuery
							.get(
							"crudmachine.php?action=createmultiple&rel=gps_order_item&delim=|&count="
							+ numRows,
							urlParams,
							( function() {
								return function(data) {
									var obj = eval(data);
									if (obj.message.type == "notification") {
										killForm(formId);
										alert("Your order has been saved!");
										var id = urlParams["cust_id"];
										fetchCustomerOrderData(id);
										fetchCustomerProductData(id);
									}
								};
							})(formId));


							if(args["usr_commission"] && parseInt(args["usr_commission"]) > 0){
								addUserCommission(args["usr_commission"],urlParams["order_id"],args["order_total"]);
							}

						} else {
							alert(obj.message.content);
						}
					};

				})(args, urlParams, numRows, formId, custName, addNotificationMessage ));
			};
			//				var custName = "";
			//				var addNotificationMessage = false;

		}

		// Hide the row for the order total ...
		hideRowInForm("order_total_" + fi);
		// Hide the row for the order total ...
		hideRowInForm("order_amount_owing_" + fi);
		// Hide the row for the order total ...
		discardComponentRowInForm("order_complete_" + fi);
		// Discard the original total
		discardComponentRowInForm("order_total_orig_" + fi);


		addOrderCompletionSection(fi);


		discardComponentRowInForm("order_discount_" + fi);
		discardComponentRowInForm("order_discount_amount_" + fi);

		// discard the order_generated field
		discardComponentRowInForm("order_generated_" + fi);
	}
}

function addUserCommission(uid,oid,total){
	if(uid <= 0) return;
	if(!uid) return;
	if(!oid) return;

	var amount = Math.round(total * commish);

	var url = "crudmachine.php?rel=gps_commission&action=create&uid=" + uid + "&order_id=" + oid + "&amount=" + amount + "&p=j";
	jQuery.get(url,(function(){
		return function(data){
			var obj = eval(data);
			if(obj.message){
				if(obj.message.type == "notification"){
					// Get the user's alias and say who got the commission
					var url = "crudmachine.php?action=read&rel=usr&uid=" + uid + "&p=j";
					jQuery.get(url,(function(){
						return function(data){
							var obj = eval(data);
							if(obj.usrs){
								var usr = obj.usrs[0];
								alert(usr.alias + " got commission of value: " + formatCurrency(amount) + " for order # " + oid);
							}
						};
					})(uid,amount,oid));
				} else {
					alert(obj.message.content);
				}
			}
		};
	})(uid,oid,amount));
}


function calculateUpdateCost(fi){
	var roe = 0;
	var cost = 0;
	var qty = 0;
	try {
		// Get the cost of the update
		var selUpdate = document.getElementById("oe_id_" + fi);
		var value = selUpdate.options[selUpdate.selectedIndex].text;
		cost = value.split("$")[1];

		var roeNode = document.getElementById("roe_id_" + fi);
		var roeValue = roeNode.options[roeNode.selectedIndex].text;
		roe = roeValue.split("$")[1];

		// Get the quantity
		var qtyNode = document.getElementById("oe_quantity_" + fi);
		qty = qtyNode.value;

		if(isNaN(qty)) { alert(qty + " is not a number!"); qty = 1; qtyNode.value = 1; }
	} catch (e){

	}

	var total = Math.round(roe * qty * cost);
	var totalNode = document.getElementById("oe_total_" + fi);
	totalNode.value = total;

	var totalDisplay = document.getElementById("oe_total_span_" + fi);
	totalDisplay.innerHTML = formatCurrency(total);
}


function addOrderCompletionSection(fi){
	// Change the order_note thingy into a text area
	var order_note = jQuery("#order_note_" + fi).get()[0];
	var parent = order_note.parentNode;
	parent.removeChild(order_note);

	// Add a checkbox and a message
	var cb = document.createElement("input");
	cb.setAttribute("type","checkbox");
	cb.id = "order_note_cb_" + fi;
	parent.appendChild(cb);
	parent.appendChild(document.createTextNode(" Mark order as complete"));
	parent.appendChild(document.createElement("br"));
	parent.appendChild(document.createElement("br"));

	var spanNode = document.createElement("span");
	spanNode.innerHTML = "Please provide a reason<br>";
	spanNode.id = "order_note_span_" + fi;
	parent.appendChild(spanNode);
	spanNode.style.display = "none";

	// Add a hidden node for order complete
	var order_complete = document.createElement("input");
	order_complete.setAttribute("type","hidden");
	order_complete.id = "order_complete_" + fi;
	parent.appendChild(order_complete);

	cb.fi = fi;
	cb.onclick = function(){
		var fi = this.fi;
		if(this.checked){
			jQuery("#order_note_" + fi).get()[0].style.display = "";
			jQuery("#order_note_span_" + fi).get()[0].style.display = "";
			jQuery("#order_complete_" + fi).get()[0].value = "t";
		} else {
			jQuery("#order_note_" + fi).get()[0].style.display = "none";
			jQuery("#order_note_span_" + fi).get()[0].style.display = "none";
			jQuery("#order_complete_" + fi).get()[0].value = "f";
		}
	};

	var ta = document.createElement("textarea");
	ta.id = "order_note_" + fi;
	parent.appendChild(ta);
	ta.style.display = "none";
	changeComponentCaption("order_note_" + fi,"Order Information");

}

function updateOrderDetails(order_id, cust_id, callback) {
	jQuery.get("cquery.php?action=updateorder&cust_id=" + cust_id
	+ "&order_id=" + order_id, null, callback);
}

function refreshPaymentWindow(params, data) {
	var obj = eval(data);
	// describe(params);
	alert(obj.message.content);
	if (obj.message.type == "notification") {
		var order_id = params["order_id"];
		var cust_id = params["cust_id"];
		killPaymentWindows(order_id, cust_id);
		var paymentParams = {
		"order_id" :params["order_id"],
		"cust_id" :params["cust_id"],
		"roe_id" :params["roe_id"]
		};
		managePayments(paymentParams, order_id);
		fetchCustomerOrderData(params["cust_id"]);
	}
}

/**
* Increment to keep track of main screen dialogs hence "msd" ... everytime a
* msd dialog is created, msdIncrement is incremented
*/
var msdIncrement = 0;

/**
* This is the prefix for the div that contains the list of payments for a
* customer. It is suffixed by msdIncrement
*/
var msdPaymentListPrefix = "mpl_";

/**
* This is the prefix for the container for the list of payments. It is a div
*/
var msdPaymentDialogPrefix = "mpayments_";

var paymentDialogWindows = {};

function managePaymentsCurr(custId,oId){
	managePayments({"cust_id":custId,"order_id":oId},oId);
}

/**
* This function brings up a screen that allows the user to manage payments for
* a user that were made on an order
*
* @param row
*            associative array containing all the keys for the
*            gps_order_payment table
* @param title
*            title to head the dialog
* @return
*/
function managePayments(row, title) {
	try { // remove roe_id... it does not make a diff
		delete row["roe_id"];
		delete row.roe_id;
	} catch (e) {

	}

	var msg = "Manage Payments: [ Title: " + title + ", Data: "
	+ describe(row, true) + " ]";

	scribble(msg);
	killPaymentWindows(row["order_id"], row["cust_id"]);
	title = new String(title);
	if (title) {
		if (title.indexOf("_") >= 0) {
			title = title.replace(/_/g, " ");
		}
	}

	title = "Order #" + title;

	var divId = msdPaymentDialogPrefix + msdIncrement;

	var order_id = row["order_id"];
	var cust_id = row["cust_id"];

	if (!paymentDialogWindows[order_id + ":" + cust_id]) {
		paymentDialogWindows[order_id + ":" + cust_id] = new Array();
	}

	paymentDialogWindows[order_id + ":" + cust_id].push(divId);

	jQuery(
	"<div class='paymentDialog' id='"
	+ divId
	+ "'><img src='" + icoManagePayments + "'>&nbsp;<b>Manage Payments:&nbsp;&nbsp;[&nbsp;" + title
	+ "&nbsp;]</b><br><br><div id='"
	+ msdPaymentListPrefix
	+ msdIncrement
	+ "' class='paymentListing'></div><br><input type='button' value='Close' onClick='killForm(\""
	+ divId + "\")'></div>").appendTo("body");

	makeDraggable(divId);

	loadPayments(row, title, msdIncrement);
	msdIncrement++;

}

/**
* This function (called after managePayments) loads payment details for an
* order into div with id: msdPaymentListPrefix + msdIncrement. Once all details
* are supplied. The function hits the server and grabs all the payment info and
* places it in a div.
*
* @param row
*            associative array containing all the keys needed to access payment
*            records for the user
* @param title
*            title to be used for the screen
* @param increment
*            the msdIncrement so the function can compose the div id where it
*            content should go. See main function description for id
*            composition
* @return
*/
function loadPayments(row, title, increment) {

	jQuery
	.get(
	"crudmachine.php?rel=gps_order_payment&action=read&vn=payment",
	row,
	( function() {
		return function(data, status) {
			var obj = eval(data);

			if (obj.payments) {
				var container = jQuery(
				"#" + msdPaymentListPrefix + increment)
				.empty().get()[0];

				var tblStr = "<table cellspacing='0' width='100%' cellpadding='3' class='paymentTable'><tbody><tr class='paymentTableHeader'><td width='40px'></td><td width='100px'>Payment Date</td><td width='120px'>Amount</td><td width='100px;'>Type</td></tr>";
				for ( var i = 0; i < obj.payments.length; i++) {

					var payment = obj.payments[i];
					// describe(payment);
					tblStr += "<tr>";

					var removePaymentLink = "<a href='javascript:void(0)' onClick='removePayment("
					+ payment.order_id
					+ ","
					+ payment.cust_id
					+ ","
					+ payment.payment_id
					+ ");' title='Delete Payment'><img border='0' src='" + icoRemovePayment + "'/></a>";

					tblStr += "<td>" + removePaymentLink
					+ "</td>";
					tblStr += ("<td>"
					+ payment.payment_date
					+ "</td><td>"
					+ formatCurrency(payment.payment_amount)
					+ "</td><td>"
					+ payment.payment_type + "</td></tr>");
				}
				tblStr += "</tbody></table>";

				if (obj.payments.length == 0) {
					tblStr = "No payments made for this order.<br/>";
				} else {

				}

				var addPaymentLink = "<br>&nbsp;<a href='javascript:void(0);' id='"
				+ msdPaymentAddLinkPrefix
				+ increment
				+ "'><img border='0' src='" + icoAddPayment + "'>Add Payment</a>";

				tblStr += addPaymentLink;
				container.innerHTML = tblStr;

				// retrieve the link and add some more
				// functionality
				var addPaymentNode = jQuery(
				"#" + msdPaymentAddLinkPrefix
				+ increment).get()[0];

				if (addPaymentNode) {
					addPaymentNode.onclick = ( function(
					increment, order_row) {
						return function() {
							editForm("gps_order_payment",
							"Make Payment", order_row,
							true, true);
						};
					})(increment, row);
				}
			} else {

			}
		};
	})(row, title, increment));
}

function removePayment(order_id, cust_id, payment_id) {

	var params = {
	"order_id" :order_id,
	"cust_id" :cust_id,
	"payment_id" :payment_id
	};

	deleteAction(params, "gps_order_payment", ( function() {

		return function() {

			killPaymentWindows(params["order_id"], params["cust_id"]);
			var paymentParams = {
			"order_id" :params["order_id"],
			"cust_id" :params["cust_id"],
			"roe_id" :params["roe_id"]
			};
			var order_id = params["order_id"];
			managePayments(paymentParams, order_id);
			updateOrderDetails(order_id, cust_id);
		};
	})(params));
}

function viewOrderDetailsCurr(custId,orderId){
	viewOrderDetails({"cust_id":custId,"order_id":orderId});
}

function viewOrderDetails(orderObj) {
	// orderInfoDialog
	var orderId = orderObj["order_id"];
	var custId = orderObj["cust_id"];

	var divId = "order_details_" + msdIncrement;
	var headerId = "order_details_header_" + msdIncrement;
	jQuery(
	"<div id='" + divId + "' class='orderDetailDialog'><img src='" + icoViewOrderDetails + "'/><b>View Order Details:</b>&nbsp;<span id='"
	+ headerId
	+ "'></span><br><input type='button' onClick=killForm('" + divId
	+ "'); value='Close Order'><br><br></div>").appendTo("body");
	makeDraggable(divId);
	var div = jQuery("#" + divId).get()[0];

	// jQuery("<b>Order Details:</b><br>").appendTo("#" + divId);
	doSpryDsAliasing(headerId, "gps_order", "{order_id}", "order_id", null,
	null, ( function() {
		var mi = msdIncrement;
		return function(obj, row) {
			var order = row[0];
			jQuery("#order_details_header_" + mi).html(
			"<b>[ Order # " + orderId + " ]</b>");
		};
	})(msdIncrement));

	readAction(orderObj, "gps_order_item,gps_package", ( function() {
		var msdi = msdIncrement;
		return function(obj) {
			var dataObj = obj.dataobj;
			var relation = obj.relation;
			var action = obj.action;

			// begin the table
			var div = document.createElement("div");
			div.className = "orderDetailDialogTblContainer";
			var tbl = document.createElement("table");
			tbl.setAttribute("cellpadding",3);
			tbl.setAttribute("cellspacing",0);
			tbl.style.width = "100%";
			var tbody = document.createElement("tbody");

			tbl.appendChild(tbody);

			var tr, td;

			// Create the headers
			tr = document.createElement("tr");
			tr.className = "orderDetailTblHeader";

			td = document.createElement("td");
			td.innerHTML = "Quantity";
			tr.appendChild(td);

			td = document.createElement("td");
			td.innerHTML = "Package Name";
			tr.appendChild(td);

			// td = document.createElement("td");
			// td.innerHTML = "Package Type";
			// tr.appendChild(td);

			td = document.createElement("td");
			td.innerHTML = "Cost";
			tr.appendChild(td);

			tbody.appendChild(tr);

			var items = dataObj.order_items;
			if (items.length > 0) {
				for ( var i = 0; i < items.length; i++) {
					var item = items[i];
					tr = document.createElement("tr");

					// Quantity
					td = document.createElement("td");
					td.innerHTML = item.order_item_quantity;
					tr.appendChild(td);

					// Package name
					td = document.createElement("td");
					td.innerHTML = item.package_name;
					tr.appendChild(td);

					// Package Type
					// td = document.createElement("td");
					// td.innerHTML = item.package_type;
					// tr.appendChild(td);

					// Total for package
					td = document.createElement("td");
					td.innerHTML = formatCurrency(item.order_item_total);
					tr.appendChild(td);

					tbody.appendChild(tr);
				}

				div.appendChild(tbl);
				div.appendChild(document.createElement("br"));

				div.appendChild(document.createTextNode("NB: These totals only show the costs of each item on the invoice before tax/discount are applied. For tax and discount information please see the \"Order Details\" section."));

				jQuery("#" + divId).get()[0].appendChild(div);
			} else {

			}

		};
	})(divId, headerId, msdIncrement, orderId, custId), {
	"viewname" :"order_item"
	});
	msdIncrement++;
}

/**
* Prefix for the table that holds the invoice... suffixed by invoiceCount
*/
var invoiceTblPrefix = "inv_tbl_";
/**
* Prefix for the tbody that holds the invoice items... suffixed by invoiceCount
*/
var invoiceTbodyPrefix = "inv_tbody_";

var invoiceDataIndex = "inv_data_";

var invoiceTotalPrefix = "inv_total_";

var invoiceRawTotalPrefix = "inv_total_raw_";

var invoiceSubTotalPrefix = "inv_total_sub_";

var discountAmntPrefix = "inv_discount_amnt_";

var discountDescriptionPrefix = "inv_discount_descrip_";

var taxAmntPrefix = "inv_tax_amnt_";

/**
* Stores all the data on the invoices <br>
* <br>
* invoiceData [ containerId ] = { itemArray : { },<br>
* itemCount : number of rows in the invoice<br> }
*/
var invoiceData = {};

/**
* Increment used to keep track of number of invoices.
*/
var invoiceCount = 0;
// Invoice functions

/**
* Creates an invoice within a container with container id: container id.
* Currently we store the invoice in a td.
*
* @see addInvoiceRow
* @param containerId
*            the id of the containing element. Now we use a td.
*/
function createInvoice(containerId) {

	var container = jQuery("#" + containerId);
	var overrideLink  = "<a href='javascript:void(0);'  style='float:left;' onClick=overrideTotal('" + containerId + "')>Override</a>&nbsp;&nbsp;";

	overrideLink = ""; // disable overriding

	var discountOverrideLink = "&nbsp;<a href='javascript:void(0)' onClick=\"overrideDiscount('" + containerId + "');\">[+]</a>";
	discountOverrideLink = "";

	var firstTotalStr = "<tr><td><b>Sub Total:</b></td><td id='" + invoiceRawTotalPrefix + containerId + "'>J$ 0</td></tr>";

	var discountStr = "<tr><td id='" + discountDescriptionPrefix + containerId + "'><b><u>Discount:</u></b>" + discountOverrideLink + "</td><td id='" + discountAmntPrefix + containerId + "'>J$ 0</td></tr>";

	var subTotalStr = "<tr><td><b>Sub Total:</b></td><td id='" + invoiceSubTotalPrefix + containerId + "' >J$ 0</td></tr>";

	var taxStr = "<tr><td><b><u>Tax:</u></b></td><td id='" + taxAmntPrefix + containerId + "' >J$ 0</td></tr>";

	var grandTotalStr = "<tr><td><b>Grand Total</b></td><td id='"	+ invoiceTotalPrefix + containerId	+ "'>J$ 0</td></tr>";

	jQuery(
	"<a href='javascript:void(0);' onclick=\"addInvoiceRow('"
	+ containerId
	+ "');\"><img border='0' src='images/Button-Add-16x16.png'>&nbsp;Add Item</a>")
	.appendTo("#" + containerId);

	var nodeStr = "<table border=0 cellpadding='5' cellspacing='0' class='invoiceTbl' id='"
	+ invoiceTblPrefix
	+ containerId
	+ "'><tbody id='"
	+ invoiceTbodyPrefix;

	nodeStr += containerId
	+ "'><tr class='invoiceTblHeader'><td></td><td>Quantity</td><td>Item</td><td>Unit Cost</td><td>Total</td></tr></tbody></table><br>";


	// Start adding the totals here

	nodeStr += "<table width='200px' align='right'><tbody>" + firstTotalStr + discountStr + subTotalStr + taxStr + overrideLink + grandTotalStr + "</tbody></table>";

	jQuery(nodeStr).appendTo(
	"#" + containerId);

	invoiceCount++;
}

/**
* Prepares the invoiceData object to store information about a new invoice.
* Does nothing if already initialized
*
* @param containerId
* @return
*/
function initializeInvoiceData(containerId) {
	if (invoiceData[containerId])
	return;

	orderOverrides[containerId] = {};
	invoiceData[containerId] = {};
	invoiceData[containerId]["increment"] = 0;
	invoiceData[containerId]["itemCount"] = 0;
	invoiceData[containerId]["itemArray"] = {};
	invoiceData[containerId]["order_total"] = 0;
	invoiceData[containerId]["order_total_orig"] = 0;
	invoiceData[containerId]["order_tax"] = 0;
	invoiceData[containerId]["order_note"] = "";
	invoiceData[containerId]["discount"] = 0;
	invoiceData[containerId]["discount_amount"] = 0;

}

function removeInvoiceRow(containerId, rowNumber) {
	var itemId = "row" + rowNumber;
	if (!invoiceData[containerId])
	return;

	if (!invoiceData[containerId]["itemArray"][itemId])
	return;
	delete invoiceData[containerId]["itemArray"][itemId];

	// Get the tbody then get the tr and remove it
	var tbody = document.getElementById(invoiceTbodyPrefix + containerId);
	var tr = document.getElementById(getInvoiceRowId(containerId, rowNumber));
	tbody.removeChild(tr);

	invoiceData[containerId]["itemCount"] = invoiceData[containerId]["itemCount"] - 1;

	calculateTotal(containerId);

}

/**
* Grabs and returns the invoice count from an id.
*
* @param id
*            id of a component that was suffixed by invoiceCount
* @return {String} id
*/
function getInvoiceCount(id) {
	return id.reverse().substring(0, id.reverse().indexOf("_")).reverse();
}

function getTrailingNumber(id) {
	return id.reverse().substring(0, id.reverse().indexOf("_")).reverse();
}

/**
* Composes ids to be used by invoice rows
*
* @param containerId
* @param rowNumber
* @return
*/
function generateInvoiceRowId(containerId) {
	return containerId + "__" + "ir_"
	+ (++invoiceData[containerId]["increment"]);
}

function getInvoiceRowId(containerId, rowNumber) {
	return containerId + "__" + "ir_" + rowNumber;
}

function getInvoiceNumRows(containerId) {
	try {
		return invoiceData[containerId]["itemCount"];
	} catch (e) {
		return 0;
	}
}

/**
* Returns the id for the select that contains the package listing. This is row
* specific. Container id and row number must be supplied. PS: This row number
* will not necessarily flow logically. If items are deleted there may be jumps
* in the row number.
*
* @param containerId
* @param rowNumber
* @return
*/
function getInvoicePackageSelectId(containerId, rowNumber) {
	var invoiceRowId = getInvoiceRowId(containerId, rowNumber);
	var packageListInpId = "package_list_inp_" + invoiceRowId;
	return packageListInpId;
}

/**
* Returns the id for the td that contains the package unit cost. This is row
* specific. The container id and row number must be supplied.
*
* @param containerId
* @param rowNumber
* @return
*/
function getInvoicePackageUnitCostId(containerId, rowNumber) {
	var invoiceRowId = getInvoiceRowId(containerId, rowNumber);
	var packageUnitCostId = "package_unit_cost_td_" + invoiceRowId;
	return packageUnitCostId;
}

function getInvoicePackageTotalCostId(containerId, rowNumber) {
	var invoiceRowId = getInvoiceRowId(containerId, rowNumber);
	return "package_total_cost_td_" + invoiceRowId;
}

function getInvoicePackageQuantityId(containerId, rowNumber) {
	var invoiceRowId = getInvoiceRowId(containerId, rowNumber);
	return "package_quantity_td_" + invoiceRowId;
}

/**
* Creates a row on the invoice that provides quantity, item and cost...
* allowing the user to edit these values and see the cost.
*
* @param containerId
*            the id of the container that contains the invoice. This is used as
*            a key in invoiceData
*
*/
function addInvoiceRow(containerId) {
	initializeInvoiceData(containerId);

	var invoiceRowObj = {};
	invoiceRowObj["quantity"] = 1;
	invoiceRowObj["package_id"] = null;
	invoiceRowObj["unit_cost"] = 0;
	invoiceRowObj["total"] = 0;

	var ic = getInvoiceCount(containerId);

	var rowNumber = invoiceData[containerId]["increment"];

	invoiceData[containerId]["itemArray"]["row" + rowNumber] = invoiceRowObj;

	// Create the tr
	var tr = document.createElement("tr");
	tr.id = getInvoiceRowId(containerId, rowNumber);
	var td;

	td = document.createElement("td");
	tr.appendChild(td);
	td.style.cursor = "pointer";
	td.innerHTML = "<img src='images/Button-Delete-16x16.png'>";
	td.onclick = ( function() {
		return function() {
			removeInvoiceRow(containerId, rowNumber);
		};

	})(containerId, rowNumber);

	// Quantity
	td = document.createElement("td");
	tr.appendChild(td);
	td.id = getInvoicePackageQuantityId(containerId, rowNumber);

	makeEditableOnClick(td, 1, {
	"onblur" :( function() {
		return function() {
			calculateCost(containerId, rowNumber);
			calculateTotal(containerId);
		};
	})(containerId, rowNumber)
	});

	// Item
	td = document.createElement("td");
	tr.appendChild(td);
	var nodeInput = document.createElement("input");
	nodeInput.value = "Please wait ...";
	nodeInput.type = "hidden";
	td.appendChild(nodeInput);
	var packageListInpId = getInvoicePackageSelectId(containerId, rowNumber);
	nodeInput.id = packageListInpId;

	// Unit Cost
	td = document.createElement("td");
	tr.appendChild(td);
	td.id = getInvoicePackageUnitCostId(containerId, rowNumber);

	// Total
	td = document.createElement("td");
	td.id = getInvoicePackageTotalCostId(containerId, rowNumber);
	tr.appendChild(td);

	var tbody = jQuery("#" + invoiceTbodyPrefix + containerId).get()[0];
	tbody.appendChild(tr);

	var groupBy = [ "package_type", "package_name" ];
	// Do spry aliasing
	doSpryDsAliasing(packageListInpId,
	"gps_package",
	"{package_name}",
	"package_id",
	groupBy, // group by type, then by name
	( function(package_unit_cost_id, cid, rn) {

		return function(obj) {
			var selectId = obj["componentId"];
			var dsName = obj["dsName"];
			var selectNode = jQuery("#" + selectId).get()[0];
			if (!selectNode)
			return;
			selectNode.dsName = dsName;
			selectNode.unitCostTd = package_unit_cost_id;
			selectNode.containerId = cid;
			selectNode.rowNumber = rn;
			selectNode.style.width = "100%";
			selectNode.onchange = function() {
				if (eval(dsName)) {
					var ds = eval(dsName);
					var row = ds.getData()[this.selectedIndex];
					var cost = (row["package_cost"]);
					invoiceData[containerId]["itemArray"]["row"
					+ rowNumber]["package_id"] = row["package_id"];
					var node = jQuery("#" + this.unitCostTd).get()[0];
					if (node)
					node.innerHTML = cost;
					calculateCost(this.containerId, this.rowNumber);
					calculateTotal(this.containerId);
				}
			};
			selectNode.onchange(); // call the onchange function
		};
	})(getInvoicePackageUnitCostId(containerId, rowNumber),
	containerId, rowNumber));

	invoiceData[containerId]["increment"] = rowNumber + 1;
	invoiceData[containerId]["itemCount"] = rowNumber + 1;

}

/**
* Calculates the total cost for a row... formula = qty * unit_cost * exchange
* rate
*
* @param containerId
*            the id of the container
* @param rowNumber
*            the row number
* @return
*/
function calculateCost(containerId, rowNumber) {

	var increment = getTrailingNumber(containerId);
	var roeSelect = document.getElementById("roe_id_" + increment);
	if (roeSelect) {
		var val = roeSelect.options[roeSelect.selectedIndex].text;
		var amount = parseFloat(val.split("\$")[1]);

		// Grab the quantity
		var qtyTd = document.getElementById(getInvoicePackageQuantityId(
		containerId, rowNumber));

		var qty = parseFloat(qtyTd.getValue());

		// Grab the unit cost
		var unit_cost_node = document
		.getElementById(getInvoicePackageUnitCostId(containerId,
		rowNumber));

		if (unit_cost_node) {
			var cost = parseFloat(unit_cost_node.innerHTML);
			if (!isNaN(cost * amount * qty)) {
				document.getElementById(getInvoicePackageTotalCostId(
				containerId, rowNumber)).innerHTML = formatCurrency(cost
				* amount * qty);

				// Update the invoiceData record
				invoiceData[containerId]["itemArray"]["row" + rowNumber]["quantity"] = qty;
				invoiceData[containerId]["itemArray"]["row" + rowNumber]["unit_cost"] = cost;
				invoiceData[containerId]["itemArray"]["row" + rowNumber]["total"] = cost
				* amount * qty;
			}
		} else {

		}
	}
}

function overrideTotal(containerId) {
	alert("This feature is currently disabled.");
	return;

	if(!containerId) return;


	var cost = getCalculatedTotal(containerId);
	if(cost == 0){
		alert("Cannot override now!");
		return;
	}

	var overrideValue = prompt("Enter the value.");
	if(overrideValue) {
		if(overrideValue.indexOf(",") > -1){
			overrideValue = overrideValue.replace(",","");
		}
		if(!isNaN(overrideValue)){
			if(confirm("By clicking OK you confirm that you are changing the total from " +
			formatCurrency(getCalculatedTotal(containerId)) + " to " + formatCurrency(overrideValue))) {
				var msg = prompt("Please expalin the reason for the override");
				if(msg != null && msg != "") {
					invoiceData[containerId]["order_note"] = msg;
					calculateTotal(containerId,overrideValue);
				}
				else {
					alert("Will not override without an explanation.");
				}
			}
		} else {
			alert(overrideValue + " is not a number!");
		}
	}
}

function getCalculatedTotal(containerId){
	if(!containerId){
		return 0;
	}

	if(!invoiceData[containerId]) return 0;

	calculateTotal(containerId);
	return invoiceData[containerId]["order_total"];
}


function getDiscount(qty){

	if(qty >= 10 && qty < 20){
		return 0.05;
	}
	else if(qty >= 20 && qty < 50){
		return 0.1;
	}
	else if(qty >= 50 && qty < 100){
		return 0.15;
	}
	else if(qty >= 100) {
		return 0.2;
	}

	return 0;
}


var orderOverrides = {};

function overrideDiscount(containerId,discount){
	if(!discount){
		var discount = 0;
		var discount_tmp = prompt("Enter the discount percentage","5%");
		if(!discount_tmp) return;
		if(discount_tmp.indexOf("%") >= 1){
			discount = discount_tmp.substring(0,discount_tmp.length-1);
		} else {
			alert("System does not understand \"" + discount_tmp + "\", please use the format: xx%, e.g. 5%");
			return;
		}

		discount = discount * 0.01;
	}

	if(!orderOverrides[containerId]) orderOverrides[containerId] = {};
	if(!isNaN(discount)) orderOverrides[containerId]["discount"] = discount; else alert(discount +  " is an invalid discount amount");
	calculateTotal(containerId);

}

function resetDiscount(containerId){
	orderOverrides[containerId]["discount"] = null;
	delete orderOverrides[containerId]["discount"];
	calculateTotal(containerId);
}


function calculateTotal(containerId) {
	var discountOverride = false;
	var overrideParams = orderOverrides[containerId];

	if(overrideParams) {
		// Deal with discount overrides
		if(!isNaN(overrideParams["discount"])){
			discountOverride = true;
		}
	}

	if (!invoiceData[containerId])
	return;

	var invoiceRows = invoiceData[containerId]["itemArray"];
	var total = 0;

	for ( var row in invoiceRows) {
		var rowIndex = row.substring(3, row.length);
		calculateCost(containerId, rowIndex);
		var invoiceRow = invoiceRows[row];
		total += parseFloat(invoiceRow["total"]);
	}

	if (!total)
	total = 0;



	var totalAfterDiscount = total;
	setRawTotal(containerId,total); // the first total


	// calculate the number of items in the invoice
	var qty = 0;
	for ( var row in invoiceRows){
		var invoiceRow = invoiceRows[row];
		qty += invoiceRow["quantity"];
	}

	// Calculate the discount
	var discount = getDiscount(qty);
	var discountAmount = 0;

	if(discountOverride){ // handle discount overrides
		discount = overrideParams["discount"];
	}

	if(discount > 0){
		discountAmount = Math.round(total * discount);
		invoiceData[containerId]["discount_amount"] = discountAmount;
		invoiceData[containerId]["discount"] = discount;
		totalAfterDiscount = total - discountAmount;
	}




	// Set the discount amount
	setDiscountTotal(containerId,discount,discountAmount,discountOverride);


	// Set the total after the discount
	setSubTotal(containerId,totalAfterDiscount); // the total after the discount


	// Calculate the tax
	var taxAmnt = Math.round((tax-1) * totalAfterDiscount);
	invoiceData[containerId]["order_tax"] = taxAmnt;
	setTaxTotal(containerId,taxAmnt);

	var totalAfterTax = totalAfterDiscount + taxAmnt;

	invoiceData[containerId]["order_total"] = totalAfterTax;
	invoiceData[containerId]["order_total_orig"] = totalAfterTax;

	setGrandTotal(containerId,totalAfterTax);


	return total;
}

function setRawTotal(containerId,total){
	jQuery("#" + invoiceRawTotalPrefix + containerId).each( function() {
		this.innerHTML = formatCurrency(total);
	});
}

function setDiscountTotal(containerId,discount,discountAmount,overriden){
	var spanStart  = "<span style='color:red'>";
	var spanClose  = "</span>";
	if(discount <= 0){
		spanStart = "";
		spanClose = "";
	}

	var discountOverrideLink = "&nbsp;<a href='javascript:void(0)' title='Override discount' onClick=\"overrideDiscount('" + containerId + "');\">[+]</a>";
	if(overriden){
		discountOverrideLink = "&nbsp;<a href='javascript:void(0)' title='Remove overrided discount' onClick=\"resetDiscount('" + containerId + "');\">[-]</a>";
	}

	jQuery("#" + discountAmntPrefix + containerId).each( function() {
		this.innerHTML = spanStart + formatCurrency(discountAmount * -1) + spanClose;
	});

	jQuery("#" + discountDescriptionPrefix + containerId).each( function() {
		if(discount > 0){
			this.innerHTML = "<b><u>Discount:</u></b>&nbsp;(&nbsp;" + (discount * 100) + "%&nbsp;)" + discountOverrideLink;
		} else {
			this.innerHTML = "<b><u>Discount:</u></b>" + discountOverrideLink;
		}
	});


}


function setTaxTotal(containerId,amnt){
	jQuery("#" + taxAmntPrefix + containerId).each( function() {
		this.innerHTML = formatCurrency(amnt);
	});
}



function setSubTotal(containerId,total){
	jQuery("#" + invoiceSubTotalPrefix + containerId).each( function() {
		this.innerHTML = formatCurrency(total);
	});
}

function setGrandTotal(containerId,total){
	jQuery("#" + invoiceTotalPrefix + containerId).each( function() {
		this.innerHTML =formatCurrency(total);
	});
}

var calLastControlSelected = null;

function registerNodeForCalendarInput(node) {
	if (node) {
		node.onclick = function() {
			cal.show();
			calLastControlSelected = this.id;
		};
	}
}

function daySelected(eventType, args) {
	var d = (new Date(args));
	if (document.getElementById(calLastControlSelected)) {
		var domObj = document.getElementById(calLastControlSelected);
		var year = d.getFullYear();
		var month = d.getMonth() + 1;
		month = month < 10 ? "0" + month : month;
		var day = d.getDate();
		day = day < 10 ? "0" + day : day;
		domObj.value = year + "-" + month + "-" + day;
	}
}

function repeatChar(chr,times){
	var str = "";
	for(var i = 0; i < times; i++){
		str += chr;
	}
	return str;
}

/**
* Called at startup ... runs all startup functions
*
* @return
*/
function doStartupStuff() {
	// jQuery("#customer_list").draggable();
	// Add aliases
	addAlias("cust_id", "Customer");
	addAlias("cust_fname", "First Name");
	addAlias("cust_lname", "Surname");
	addAlias("cust_contact", "Contact");
	addAlias("cust_email", "Email");
	addAlias("cust_jam_id", "Id No.");
	addAlias("cust_jam_id_type", "Id Type");
	addAlias("cust_contact_sec", "Secondary Contact");
	addAlias("cust_email_sec", "Secondary Email");
	addAlias("cust_comment", "Other Info");

	addAlias("package_id", "Pacakge");
	addAlias("package_name", "Pacakge Name");
	addAlias("package_cost", "Retail Price US $");
	addAlias("package_unit_cost", "Package Unit Cost US $");
	addAlias("package_icon", "Package Icon");
	addAlias("package_type", "Package Type");

	addAlias("order_id", "Order Id");
	addAlias("roe_id", "Date (YYYY-MM-DD)");
	addAlias("roe_curr_name", "Currency Name e.g. \"US\"");
	addAlias("roe_value", "Rate J$");

	addAlias("order_date", "Order Date");
	addAlias("order_note", "Order Note");
	addAlias("order_total", "Order Total");
	addAlias("order_tax", "Order Tax");
	addAlias("order_amount_owing", "Amount Owing");
	addAlias("order_complete", "Complete?");
	addAlias("usr_commission", "Sales Commission");

	addAlias("payment_id", "Payment Number");
	addAlias("payment_date", "Payment Date");
	addAlias("payment_comment", "Notes");
	addAlias("payment_receipt", "Payment Receipt");
	addAlias("uid", "Cashier");
	addAlias("payment_amount", "Amount J$");
	addAlias("payment_type", "Payment Type");

	addAlias("oe_quantity","Quantity");
	addAlias("oe_total","Total");
	addAlias("oe_id","Order Expense");
	addAlias("oe_paid","Paid");
	addAlias("oe_title","Order Expense Name");
	addAlias("oe_description","Order Expense Description");
	addAlias("oe_cost","Order Expense Cost (US$)");

	addAlias("product_serial","Product Serial");

	addAlias("version_no","Version No");
	addAlias("dispatch_date","Date Sent");

	// Apply global processors
	var removeCommas = function(val) {
		try {
			return val.replace(",", "");
		} catch (m) {
			return val;
		}
	};

	var padOrderNo = function(val){
		var str = "";
		str = repeatChar("0",6-val.length) + val;
		return str;
	}

	addGlobalProcessor("package_cost", removeCommas);
	addGlobalProcessor("package_cost", removeCommas);
	addGlobalProcessor("roe_cost", removeCommas);
	addGlobalProcessor("order_total", removeCommas);
	addGlobalProcessor("order_tax", removeCommas);
	addGlobalProcessor("order_amount_owing", removeCommas);
	addGlobalProcessor("payment_amount", removeCommas);
	addGlobalProcessor("order_id",padOrderNo);

	loginScreen();

	cal = new Cal("cal_div");
	cal.registerForEvent(CAL_DAY_SELECTED, daySelected);
	makeDraggable("cal_div");
	cal.hide();
}

var cal = null;
var usr_uid = null;

function login() {
	var args = getInputAsArgs("loginDiv");
	jQuery.get("login.php", args, ( function() {
		return function(data) {
			var obj = eval(data);
			if (obj.message) {
				var msg = obj.message.content;
				var parts = msg.split(":");
				if(parts.length == 2){
					usr_uid = parts[1];
				}
				if (obj.message.type == "notification") {
					jQuery.unblockUI();
					var alias = args["login_usr"];
					if (alias)
					jQuery("#loginInfo").html(getGreeting(alias));
					reloadDatasets();
				}
			} else {

			}
		};
	})(args));
}

function reloadDatasets() {
	dsCustomer.loadData();
	dsOrder.loadData();
	dsPackage.loadData();
}

function loginScreen() {
	jQuery.blockUI( {
		message :"Checking login status ... Please wait"
	});

	jQuery
	.get(
	"login.php?check",
	null,
	function(data) {
		var obj = eval(data);
		var msg = obj.message.content;
		if (msg.indexOf("loggedin") >= 0) {
			var alias = msg.split(":")[1];
			usr_uid = msg.split(":")[2];
			jQuery("#loginInfo").html(getGreeting(alias));
			jQuery.unblockUI();
		} else {
			var loginStr = "<h1>Login</h1><br>Username:&nbsp;<div id='loginDiv'><form onsubmit='login(); return false;'><input name='usr' id='login_usr'>&nbsp;Psw:&nbsp;<input id='login_psw'  type='password' name='psw'>&nbsp;<div id='loginErr'></div><input type='button' value='Login' onClick='login();'></form></div>";
			jQuery.blockUI( {
				message :loginStr
			});
		}
	});
}

function logout() {
	jQuery.get("login.php?logout", null, function() {
		jQuery("#loginInfo").html("");
		loginScreen();
		reloadDatasets();
	});
}

function getGreeting(alias) {
	return alias
	+ " is currently logged in. <a href='javascript:void(0);' onclick='logout();'>Logout</a>";
}

function killPaymentWindows(order_id, cust_id) {

	var key = order_id + ":" + cust_id;
	var windowList = paymentDialogWindows[key];
	if (!windowList)
	return;
	while (windowList.length > 0) {
		var id = windowList.pop();
		killForm(id);
	}
}

function checkDelinquency(id) {
	if (!jQuery("#delinquency_" + id).get()[0])
	return;

	jQuery("#deliquency_" + id).html(".....");
	jQuery
	.get(
	"cquery.php?action=checkdelinquency&cust_id=" + id,
	null,
	( function() {
		return function(data) {
			var obj = eval(data);
			if (obj.message.type == "notification") {
				jQuery("#delinquency_" + id).empty();
				var content = obj.message.content;
				if (content) {
					var parts = content.split(",");
					var orderStatus = new Array();
					if (parts.length > 0) {
						for ( var i = 0; i < parts.length; i++) {
							var part = parts[i];
							var orderId = part.split(":")[0];
							var status = part.split(":")[1];
							if (!orderStatus[status])
							orderStatus[status] = new Array();
							orderStatus[status].push(orderId);
						}

						for ( var status in orderStatus) {
							var arr = orderStatus[status];
							var msg = "";
							var icon = "";
							if (status == "trouble") {
								msg = "No payments received in the last 6 weeks from order(s): "
								+ arr.join(",") + ".";
								icon = "<img src='info-24x24.png' title='"
								+ msg
								+ "' width='16px' height='16px'>&nbsp;";
							} else if (status == "delinquent") {
								msg = "No payments received in the last 3 months from order(s): "
								+ +arr.join(",") + ".";
								icon = "<img src='error.png' title='"
								+ msg
								+ "' width='16px' height='16px'>&nbsp;";
							}

							if (icon) {
								jQuery(icon).appendTo(
								"#delinquency_" + id);
							}
						}
					}
				}
			}
		};
	})(id));
}


function searchBySerial(){
	var ser = prompt("Enter the serial number you want to search for.");
	if(ser){
		var url  ="crudmachine.php?p=j&action=read&rel=gps_customer,gps_order,gps_product&vn=customer&product_serial=" + ser;
		jQuery.get(url,(function(){
			return function(data){
				var obj = eval(data);
				if(obj.customers){
					if(obj.customers.length == 0){
						alert("No record found of serial number " + ser);
					} else {
						var customer = obj.customers[0];
						alert("Serial number # " + ser + " belongs to " + customer.cust_fname + ", " + customer.cust_lname);
					}
				}
			};
		})(ser));
	}
}

function scheduleNotificationChecker(){
	window.setTimeout("checkForNotifications()",3 * 1000);
	window.setInterval("checkForNotifications()",40 * 1000); // check every 40 seconds
}

var nMessages = new Array();

function checkForNotifications(){
	jQuery("#notification").hide("slow");

	var d = new Date();
	d.setHours(d.getHours() + 24*2);

	var dateStr = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();

	var url = "crudmachine.php?rel=message&action=read&msg_dismissed=f";
	url += "&sc=message.msg_timestamp::message.msg_timestamp<='" + dateStr + "'";


	jQuery.get(url,function(data){
		var obj;
		try {
			obj = eval(data);
		} catch(e){
			alert(e + "\n" + data);
			return;
		}

		if(obj.messages){
			var messageCount = obj.messages.length;
			if(messageCount == 0) return;
			var notificationDiv = document.getElementById("notification");
			if(notificationDiv){
				notificationDiv.style.fontSize = 14;
				var newMsgStr = "New Messages!!!";
				if(messageCount ==  1) newMsgStr = "New Message!!!";
				var nofocusMessage = "<img src='images/edit_note.png' width='24px;'/>&nbsp;<a href='javascript:void(0)' onClick='printNotifications();'>" + messageCount + " " + newMsgStr + "</a>";
				notificationDiv.innerHTML = nofocusMessage;
				notificationDiv.nofocusMessage =  nofocusMessage;
				nMessages = obj.messages;
				jQuery("#notification").show("slow");
			}
		}
	});
}

function showNotifications(){
	jQuery("#notification").hide("slow",function(){
		printNotifications();
		window.setTimeout("jQuery('#notification').show('slow')",400);
	});
}


function printNotifications(){
	var htmlStr = "";
	for(var i = 0; i < nMessages.length; i++){
		var message = nMessages[i];
		var messageId = message.msg_id;
		var title = message.msg_title;
		if(title) title += "<br>";
		var content = message.msg_content;
		content = content.replace("&gt;",">");
		content = content.replace("&tt;","<");
		htmlStr += "<div id='msg_note_" + messageId + "' class='notificationMessage'><span class='title' style='font-weight:bold; color:yellow;'>" + title + "</span>" + content + "<br><br><input type='button' value='Dismiss' onClick=dismiss(" + messageId + ")></div>";
	}

	var notificationDiv = document.getElementById("notification");

	jQuery("#notification").get()[0].innerHTML = notificationDiv.nofocusMessage + "<br>" + htmlStr + "<br><input type='button' value='Close Notifications' onClick='hideNotifications();'>";
}

function dismiss(msgId){
	if(confirm("Are you sure you want to dismiss this notice? After a notice is dismissed it will not be seen ever again.")){
		jQuery.get("crudmachine.php?rel=message&msg_id=" + msgId + "&msg_dismissed=t&action=update",(function(){
			return function(data){
				var obj = eval(data);
				if(obj.message.type == "notification"){
					var parent = jQuery("#notification").get()[0];
					var node = jQuery("#msg_note_" + msgId).get()[0];
					if(parent) if(node) parent.removeChild(node);
					alert("Notification removed!");
					checkForNotifications();
				}
			};
		})(msgId));
	}
}

function hideNotifications(){
	var notificationDiv = document.getElementById("notification");
	notificationDiv.innerHTML =  notificationDiv.nofocusMessage;
}


function addNPE(custId,orderId){
	if(!custId) return;
	if(!orderId) return;

	var row = {
	"order_id":orderId,
	"cust_id":custId
	}

	editForm("order_expense","Add Non-Product Expense",row,true,true);
}


function filterCustomers(str){
	if(!str) return;
	if(str.length < 3){
		if(dsCustomer.url != custUrl)
		dsCustomer.url = custUrl;
		dsCustomer.loadData();
		return;
	}
	if(dsCustomer){
		str = str.toLowerCase();
		var url =  custUrl + "&sc=gps_customer.cust_lname::lower(gps_customer.cust_lname) like '%" + str + "%':::gps_customer.cust_fname::lower(gps_customer.cust_fname) like '%" + str + "%'";
		dsCustomer.url =url;
		var filterNode = jQuery("#filterCust").get()[0];
		filterNode.disabled = true;
		dsCustomer.loadData();
	}
}


function sellPackage(){
	var titleStr = "<center><div style='width:95%; background-color:black; padding:10px; font-size:13px; color:white; font-weight:bold;'>Mona Informatix Ltd. Invoice</div></center>";
	createForm('gps_order',"Mona Informatix Ltd. JAMNAV Invoice");
}

function styleFocus(inp){
	if(inp.type != "text") return;
	inp.style.borderWidth = 2;
	inp.style.borderColor = "black";
}

function styleBlur(inp){
	if(inp.type != "text") return;
	inp.style.borderWidth = 1;
	inp.style.borderColor = "rgb(150,150,250)";


}
