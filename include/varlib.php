<?php

if(array_key_exists("mode", $_GET)){
    $mode = $_GET["mode"];
}
else {
    $mode = "";
}

$dom = new DOMDocument();
$configFile = dirname(__FILE__) . "/params.xml"; 
if(file_exists($configFile)) {
	$dom->load($configFile);
	$params = $dom->getElementsByTagName("param");
	foreach($params as $param){
		$name = $param->getAttribute("name");
		$value = $param->getAttribute("value");
		$_SESSION[$name] = $value;
		if($mode == "verbose"){
			echo "Adding $name = $value<br/>";
		}
	}
} else {
	echo "File $configFile does not exist!";
}


function addParam($name,$value){
	if(isset($name)){
		$_SESSION[$name] = $value;
	}
}

function getParam($name){
	return $_SESSION[$name];
}

if(strpos($_SERVER['SERVER_SOFTWARE'],"Win")){
	addParam("ostype","windows");
	addParam("owtchartcgi","owtchart.exe");

} else {
	addParam("ostype","linux");
	addParam("owtchartcgi","owtchart");
}

?>
