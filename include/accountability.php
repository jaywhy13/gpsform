<?php 

include_once("db.php");
include_once("varlib.php");
require_once("permguru.php");

$acc_pg = null;

function useConnection($conn){
	$GLOBALS['logConn'] = $conn;
}


function lg($msg,$usr="guest",$tag="MISC"){
	$pg = $GLOBALS['acc_pg'];
	if(!$pg) return;
	if($pg->isLoggedIn()){
		$now = date("Y-m-d H:i:s");
		$sql = "INSERT INTO  minlog (msg_date,msg_by,msg_description,msg_tag) VALUES ('$now','$usr','$msg','$tag')";
		if(trim($msg) != "" ){
			$pg->runSQL($sql);
		}
	} else {
	}
}


/*
 * This function searches the log
 */
function searchLog($term,$params) {
	if($term != ""){
		$conds = array();
		if($params["date"])	{ $conds [] = "msg_date = " . $params["date"]; }
		if($params["date_start"]) { $conds [] = "msg_date > " . $params["date_start"]; }
		if($params["date_end"]) { $conds [] = "msg_date < " . $params["date_start"]; }
		if($params["tags"]) { $conds [] = "msg_tag IN (" . $params["tags"] . ")"; }
		if($params["user"]) { $conds [] = "msg_by = " . $params["user"]; }
		if($params["users"]) { $conds [] = "msg_by IN ( " . $params["users"] . ")"; }

		$sql = "SELECT * FROM minlog ";

		if(count($conds) == 1){
			$sql  =  " WHERE " . $conds[0];
		} else {
			$sql = " WHERE " . join(" AND ",$conds);
		}
		
		$result = $GLOBALS['logConn']->query($sql);

		return $result;

	} else {
		return null;
	}
}





?>