<?php

abstract class DB {
	
	public function loadConfig($config) {
		$comment = "#";
		
		if (isset ( $config )) {
			
			if (strlen ( $config ) > 0 && ! file_exists ( $config )) {
				echo ("File: $config does not exist!");
				return NULL;
			}
			
			$fp = fopen ( $config, "r" );
			
			while ( ! feof ( $fp ) ) {
				$line = trim ( fgets ( $fp ) );
				if ($line && ! ereg ( "^$comment", $line )) {
					$pieces = explode ( "=", $line );
					$option = trim ( $pieces [0] );
					$value = trim ( $pieces [1] );
					$config_values [$option] = $value;
				}
			}
			fclose ( $fp );
		}
		return $config_values;
	}
	
	abstract protected function connect();
	
	abstract protected function disconnect();
	
	abstract protected function query($sql);
	
	abstract protected function fetchArray($rs);
	
	abstract protected function fetchAssocArray($rs);
	
	abstract protected function fetchAll($rs);
	
	abstract protected function applyPerRowAssoc($rs, $func); // pass the name of the function
	

	abstract protected function applyPerRowArray($rs, $func); // pass the name of the function
	

	abstract protected function applyPerColumn($rs, $func); // pass the name of the function
	

	abstract protected function isConnected();
	
	abstract protected function rowCount($rs);
	
	abstract protected function freeResult($rs);
	
	private $conn;
	
	private $DEFAULT_CFG = 'db_config.conf'; // TODO: EDIT TO DEFINED CONSTANT LOCATION
}

class PGDB extends DB {
	function __construct($config) {
		if (! isset ( $config ) || $config == '' || $config == null) {
			$config = $this->DEFAULT_CFG;
		}
		
		$values = parent::loadConfig ( $config );
		
		if ($values != NULL) {
			
			$this->conn_str = "host=" . $values ['HOST'] . " dbname=" . $values ['DB_NAME'] . " user=" . $values ['USER'] . " password=" . $values ['PASSWORD'];
			$this->conn = $this->connect ();
			
		}
		//			$this->conn_str = "host=10.0.40.202 dbname=mindrill user=mindriller password=drillthath0le";
		//			$this->conn = $this->connect();
		

		
	
	}
	
	public function connect() {
		if ($this->conn_str == NULL) {
			die ( "Path to PSQL DB connection unset" );
		}
		return pg_pconnect ( $this->conn_str ) or die("Could not connect using " . $this->conn_str);
	}
	
	public function disconnect() {
		if ($this->conn != NULL) {
			pg_close ( $this->conn );
		}
	}
	
	public function query($sql) {
		//print_r($this->conn);
		//		echo "SQL: $sql";
		return pg_query ( $sql );
	}
	
	public function fetchArray($rs) {
		return pg_fetch_array ( $rs );
	}
	
	public function fetchAssocArray($rs) {
		return pg_fetch_assoc ( $rs );
	}
	
	public function fetchAll($rs) {
		return pg_fetch_all ( $rs );
	}
	
	public function applyPerRowAssoc($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$i = 0;
		
		while ( ($row_assoc_arr = pg_fetch_assoc ( $rs )) !== FALSE ) // note associative array
{
			$func ( $i, $row_assoc_arr );
			$i ++;
		}
	}
	
	public function applyPerRowArray($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$row = array ();
		
		$i = 0;
		
		while ( ($row_arr = pg_fetch_array ( $rs )) !== FALSE ) {
			$func ( $i, $row_assoc_arr );
			$i ++;
		}
	}
	
	public function applyPerColumn($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$row = NULL;
		$tot = array ();
		
		while ( $row = pg_fetch_array ( $rs ) ) {
			array_push ( $tot, $row );
		}
		
		$i = 0;
		$j = 0;
		foreach ( $tot as $line ) {
			foreach ( $line as $col ) {
				$func ( $i, $j, $col );
				$j ++;
			}
			$i ++;
		}
	}
	
	public function isConnected() {
		if ($this->conn !== FALSE) {
			return TRUE;
		}
		return FALSE;
	}
	
	public function rowCount($rs) {
		if ($rs == FALSE) {
			return NULL;
		}
		return pg_num_rows ( $rs );
	}
	
	public function freeResult($rs) {
		return pg_free_result ( $rs );
	}
	
	private $conn_str = NULL;
}

class ODBCDB extends DB {
	
	function __construct($config) {
		if (! isset ( $config ) || $config == '' || $config == null) {
			$config = $this->DEFAULT_CFG;
		}
		
		$values = parent::loadConfig ( $config );
		
		if ($values != NULL) {
			$this->conn_vals = $values;
			$this->conn = $this->connect ();
		}
	}
	
	public function connect() {
		if ($this->conn_vals == NULL) {
			die ( "Path to ODBC DB connection unset" );
		}
		$dsn = $this->conn_vals ['DSN'];
		$user = $this->conn_vals ['USERNAME'];
		$password = $this->conn_vals ['PASSWORD'];
		return odbc_pconnect ( $dsn, $user, $password, SQL_CUR_USE_ODBC );
	}
	
	public function disconnect() {
		if ($this->conn != NULL) {
			odbc_close ( $this->conn );
		}
	}
	
	public function query($sql) {
		return odbc_exec ( $this->conn, $sql );
	}
	
	public function fetchArray($rs) {
		return odbc_fetch_array ( $rs );
	}
	
	public function applyPerRowAssoc($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$i = 0;
		
		while ( ($row_assoc_arr = odbc_fetch_array ( $rs )) !== FALSE ) // note associative array
{
			$func ( $i, $row_assoc_arr );
			$i ++;
		}
	}
	
	public function applyPerRowArray($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$row = array ();
		
		$i = 0;
		
		while ( odbc_fetch_into ( $rs, $row ) ) {
			$func ( $i, $row );
			$i ++;
		}
	}
	
	public function applyPerColumn($rs, $func) // pass the name of the function
{
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$row = array ();
		$tot = array ();
		
		while ( odbc_fetch_into ( $rs, $row ) ) {
			array_push ( $tot, $row );
		}
		
		$i = 0;
		$j = 0;
		foreach ( $tot as $line ) {
			foreach ( $line as $col ) {
				$func ( $i, $j, $col );
				$j ++;
			}
			$i ++;
		}
	}
	
	public function isConnected() {
		if ($this->conn !== FALSE) {
			return TRUE;
		}
		return FALSE;
	}
	
	public function rowCount($rs) {
		if ($rs == FALSE) {
			return NULL;
		}
		if (($count = odbc_num_rows ( $rs )) != - 1) {
			return $count;
		}
		
		$row = array ();
		
		while ( odbc_fetch_into ( $rs, $row ) ) {
		}
		
		return count ( $row );
	}
	
	public function freeResult($rs) {
		return odbc_free_result ( $rs );
	}
	
	private $conn_vals = NULL;
	
	public function fetchAll($rs) {
		if (! $rs) {
			echo "Result set empty!";
			return;
		}
		
		$row = array ();
		$tot = array ();
		
		while ( odbc_fetch_into ( $rs, $row ) ) {
			array_push ( $tot, $row );
		}
		
		return $tot;
	}
	
	public function fetchAssocArray($rs) {
		return odbc_fetch_array ( $rs );
	}
}

function getDBInstance($type, $config) {
	if ($type == "odbc") {
		return new ODBCDB ( $config );
	}
	
	if ($type == "pg") {
		return new PGDB ( $config );
	}
	
	return NULL;
}

?>