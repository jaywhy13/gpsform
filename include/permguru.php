<?php
// TODO: Check whether conn is pgsql or odbc to decide on whether TEXT or VARCHAR character is correct

session_start();
include_once("db.php");
include_once("varlib.php");

// Define session related values
define("SESSIONUID","suid");										//	session var storing the user's uid
define("SESSIONUSR","sessionalias"); 								//	session var storing the user's alias
define("SESSIONPERMOBJ","sessionpermobj"); 							//	session var storing the user's permission object

// Database table values
define("PERM_TBL","perm_type");
define("PERM_RULES_TBL","rule");
define("USR_TBL","usr");
define("GRP_TBL","grp");
define("USR_GRP_TBL","usr_in_grp");

// list of permissions that will be used in the database
// the list will need to be expanded, probably refined and so on as we go along
DEFINE("PERM_GPSCOORDS","gpscoords");
DEFINE("PERM_SURVEYCOORDS","surveycoords");
DEFINE("PERM_DRILLHOLE","drillhole");
DEFINE("PERM_DRILLSAMPLES","drillsamples");
DEFINE("PERM_LABREPORTUPLOAD","labreportupload");
DEFINE("PERM_AREA","area");
DEFINE("PERM_DEPOSIT","deposit");
DEFINE("PERM_CREW","crew");
DEFINE("PERM_DRILLMACHINE","drillmachine");
DEFINE("PERM_SYSTEMREPORTS","systemreports");
DEFINE("PERM_DATACLERK","dataclerk");

DEFINE("PERM_USERSGROUPS", "usersgroups");
DEFINE("PERM_PERMISSIONS", "permissions");

class PermGuru {
	private $db = null;
	private $isVerbose = false;
	private $loggedIn = false;
	private $id = "";

	function __construct($idd){

		if($idd == "" || $idd == null)
		{
			throw new Exception("Id cannot be empty");
			die();
		}
		$this->id = $idd;
		// Initialize and connect

		$this->db = getDBInstance("pg",getParam("dbconfigfile-psql"));
		//		$this->db = new DB(getParam("dbconfigfile"));	// initialize the new config file
		if(!$this->db->isConnected()){
			throw new Exception("Could not connect to database");
		}
		$this->loggedIn = $this->sessionDataExists();				//	check if the user is logged in
	}

	function loopResult($result){
		return $this->db->fetchArray($result);
	}

	//	===========================================================
	//	SQL RELATED FUNCTIONS
	function runSQL($str){
		// Runs a sql string
		if($this->db){
			return $this->db->query($str);
		}
		return null;
	}

	// Returns the FIRST ROW
	function getSQLAssocArr($sql){
		if($this->db){
			$result = $this->db->query($sql);
			if($result) return $this->db->fetchArray($result);
		}
		return null;
	}

	function mapSQL($sql,$f){
		if($this->db){
			$result = $this->db->query($sql);
			$this->db->applyPerRowAssoc($result,$f);
		}
	}

	function sqlRowCount($sql){
		if($this->db){
			$result = $this->runSQL($sql);
			return $this->db->rowCount($result);
		}
		return -1;
	}

	// 	============================================================
	//	USER FUNCTIONALITY

	// Adds a new user to the database
	function addUser($alias,$psw){
		if(!$this->userAliasExists($alias)){
			$salt = createRandomPassword();
			$hash = md5($psw . $salt);
			$sql = "INSERT INTO " . USR_TBL . " (alias,salt,psw_hash) VALUES ('$alias','$salt','$hash')";
			return $this->runSQL($sql);
		} else {
			throw new Exception("User $usr already exists. Use a different username");
		}
		return null;
	}

	// Removes a user by their uid
	function removeUser($uid){
		if($this->userUidExists($uid)){
			$sql = "DELETE FROM " . USR_TBL . " WHERE uid = $uid";
			return $this->runSQL($sql);
		} else {
			throw new NonExistentEntityException("The uid $uid was not found");
		}
	}

	// Removes a user by their alias
	function removeUserByAlias($alias){
		if($this->userAliasExists($alias)){
			$sql = "DELETE FROM " . USR_TBL . " WHERE alias = '$alias'";
			return $this->runSQL($sql);
		} else {
			throw new NonExistentEntityException("The uid $uid does not exist");
		}
	}

	// Check if a user alias exists
	function userAliasExists($usr,$throwException=false){
		$sql = "select uid from " . USR_TBL . " WHERE alias = '$usr'" ;
		$result = $this->runSQL($sql);
		if($this->db->rowCount($result) > 0){
			return true;
		} else {
			if($throwException){
				throw new NonExistentEntityException("User with alias \"$usr\" does not exist");
			}
			return false;
		}
	}

	// Check if a user uid exists
	function userUidExists($uid,$throwException=false){
		$sql = "select uid from " . USR_TBL . " WHERE uid = $uid" ;
		$result = $this->runSQL($sql);
		if($this->db->rowCount($result) > 0){
			return true;
		} else {
			if($throwException){
				throw new NonExistentEntityException("User with uid $uid does not exist");
			}
			return false;
		}
	}

	// Gets a user alias given their uid
	function getUserAlias($uid=null){
		if($this->isLoggedIn()){
			//return $_SESSION[SESSIONUSR];
			$sd = $_SESSION[$this->id];
			return $sd[SESSIONUSR];
		}

		if(empty($uid)){
			return null;
		}

		$sql = "select alias from " . USR_TBL . " WHERE uid = $uid";
		$arr = $this->getSQLAssocArr($sql);
		if($arr){
			return $arr["alias"];
		}
		return null;
	}

	// Changes the user password given the uid, the old and new password
	function changeUserPassword($uid,$oldPsw,$newPsw){
		if($this->userUidExists($uid)){
			$sql = "SELECT salt,psw_hash from " . USR_TBL . " where uid = $uid";
			$result = $this->runSQL($sql);
			if($result){
				$arr = $this->db->fetchArray($result);
				$salt = $arr["salt"];
				$hash = $arr["psw_hash"];

				if(md5($oldPsw . $salt) == $hash) {
					$this->resetUserPsw($uid,$newPsw);
				} else {
					throw new Exception("The password is incorrect.");
				}

			} else {
				echo "Change Psw error";
			}
		} else {
			throw new NonExistentEntityException("Uid '$uid' was not found");
		}
	}


	// Resets the user password to summin else
	private function resetUserPsw($uid,$psw){
		if($this->db->isConnected()){
			$salt = createRandomPassword();
			$hash = md5($psw . $salt);
			if(!$this->userUidExists($uid)) throw new NonExistentEntityException("Uid $uid not found");
			return $this->runSQL("UPDATE " . USR_TBL . " SET psw_hash = '$hash',salt='$salt' WHERE uid = $uid");
		}
	}

	//	============================================================
	//	GROUP MANAGEMENT

	// Add a group by the name
	function addGroup($alias){
		if($this->groupAliasExists($alias)){
			throw new Exception("Group with alias \"$alias\" already exists");
		}

		$sql = "INSERT INTO " . GRP_TBL . " (alias) VALUES ('$alias')";
		return $this->runSQL($sql);
	}

	// Check if a group gid exists
	function groupGidExists($gid,$throwException=false){
		$sql = "SELECT gid FROM " . GRP_TBL . " WHERE gid = $gid";
		if($this->sqlRowCount($sql) > 0){
			return true;
		}
		return false;
	}

	// Check if a group alias exists
	function groupAliasExists($alias){
		$sql = "SELECT gid FROM " . GRP_TBL . " WHERE alias = '$alias'";
		if($this->sqlRowCount($sql) > 0){
			return true;
		}
		return false;
	}

	// Removes a group by their gid
	function removeGroup($gid){
		$sql = "delete from " . GRP_TBL . " where gid = $gid";
		if(!$this->groupGidExists($gid)) throw new NonExistentEntityException("The group does not exist");
		return $this->runSQL($sql);
	}

	// Changes the alias of a group, given that alias does not already exist
	function changeGroupAlias($gid,$newAlias){
		if(!$this->groupGidExists($gid)) throw new NonExistentEntityException("The group does not exist");
		if($this->groupAliasExists($newAlias)) throw new Exception("A group already exists with alias \"$newAlias\"");

		$sql = "UPDATE " . GRP_TBL . " SET alias = '$newAlias' WHERE gid = $gid";
		return $this->runSQL($sql);
	}

	// Adds a user to a group, given they don't already belong to the group
	function addUserToGroup($uid,$gid){
		if(!$this->userUidExists($uid)) throw new NonExistentEntityException("Uid $uid not found");
		if(!$this->groupGidExists($gid)) throw new NonExistentEntityException("Gid $gid not found");

		if(!$this->userBelongsToGroup($uid,$gid)){
			$sql = "insert into " . USR_GRP_TBL . " VALUES ($gid,$uid)";
			return $this->runSQL($sql);
		} else {
			throw new Exception("User already belongs to the group");
		}
	}

	// Removes a user from a group, given they belong and exist
	function removeUserFromGroup($uid,$gid){
		$this->userBelongsToGroup($uid,$gid,true);

		$sql = "delete from " . USR_GRP_TBL . " where gid = $gid AND uid = $uid ";
		return $this->runSQL($sql);
	}


	function userBelongsToGroup($uid,$gid,$throwException=false){
		$sql = "SELECT * FROM " . USR_GRP_TBL . " WHERE uid = $uid AND gid = $gid";
		if($this->sqlRowCount($sql) > 0){
			return true;
		}

		if($throwException){
			throw new Exception("User $uid does not exists in group $gid");
		}

		return false;
	}

	//	============================================================
	//	PERMISSION MANAGEMENT
	// 	There are these things called "permission types" - they are just "phrases" essentially are activities for which permission
	//	can be defined for either users or groups
	//	"gpscoords" is an example of a permission type
	//	a user "jay" may have the following permission on it
	//	C R U D
	// 	1 1 0 0
	//	Meaning jay can create and read gpscoords but cannot update or delete gpscoords within the system
	//	"gpscoords" would be added with addPermissionType, removed with removePermissionType



	// Adds a permission type from the database
	function addPermissionType($perm,$tag,$description,$default="0000") {

		if($this->permissionExists($perm)){
			throw new Exception("Permission tag already exists");
		}

		$sql = "INSERT INTO " . PERM_TBL . " (pmt_id,pmt_tag,pmt_description)  VALUES ('$perm','$tag','$description')";
		$result = $this->runSQL($sql);

		if($result){
			$sql = "ALTER TABLE " . PERM_RULES_TBL . " ADD COLUMN $perm varchar(10) ";
			$result = $this->runSQL($sql);

			$sql = "UPDATE " . PERM_RULES_TBL . " SET $perm = '$default'";
			$result = $this->runSQL($sql);

			$this->regularizePermissions(false); // ensure that the permissions are regularized
		}
		return $result;
	}

	// Removes a permission type from the database
	function removePermissionType($perm,$throwException=false) {
		$sql = "DELETE FROM " . PERM_TBL . " WHERE pmt_id = '" . $perm  . "'";
		$this->runSQL($sql);

		// this one might run us into errors
		try{
			$sql = "ALTER TABLE " . PERM_RULES_TBL . " DROP COLUMN $perm";
			$this->runSQL($sql);
			$this->regularizePermissions(false); // ensure that the permissions are regularized
		} catch(Exception $e){
			if($throwException){
				throw new Exception("Database inconsistency. Column did not exist");
			}
		}
	}

	// Prints the different permission types there are
	function printPermissionTypes(){

		$sql = "SELECT * FROM "	. PERM_TBL . " ORDER BY pmt_tag, pmt_id";
		$result = $this->runSQL($sql);
		$this->db->applyPerRowAssoc($result,"printStuff");
	}

	// Check if a permission by that name exists
	private function permissionExists($perm,$throwException=false){
		$sql = "SELECT pmt_id FROM " . PERM_TBL . " WHERE pmt_id = '" . $perm . "'";
		$result = $this->runSQL($sql);

		if($result){
			if($this->db->rowCount($result) > 0){
				return true;
			}
		}

		if($throwException){
			throw new NonExistentEntityException("Permission type does not exist");
		}

		return false;
	}

	// Checks if a permission exists for a user, throws exception if the permission type does not exist
	function userPermissionExists($uid,$permType,$throwException=false) {
		$this->userUidExists($uid,$throwException);
		$sql = "SELECT $permType FROM " . PERM_RULES_TBL .  " WHERE rule_scope = 'user' AND rule_ref = $uid ";
		try {
			$numrows = $this->sqlRowCount($sql);
			if($numrows == 1){
				return true;
			}
			else {
				return false;
			}
		} catch (Exception $e){
			if($throwException) throw new NonExistentEntityException("Could not find permission type: $perm");
			return false;
		}
	}

	// Checks if a permission exists for a group, throws exception if the permission type does not exist
	function groupPermissionExists($gid,$permType,$throwException=false) {
		$this->groupGidExists($gid,$throwException);
		$sql = "SELECT $permType FROM " . PERM_RULES_TBL .  " WHERE rule_scope = 'group' AND rule_ref = $gid ";
		try {
			$numrows = $this->sqlRowCount($sql);
			if($numrows == 1){
				return true;
			}
			else {
				return false;
			}
		} catch (Exception $e){
			if($throwException) throw new NonExistentEntityException("Could not find permission type: $perm");
			return false;
		}
	}

	// Overwrites a user's permission
	private function overwriteUserPermission($uid,$permType,$bits){
		$this->userUidExists($uid,true);
		$this->permissionExists($permType,true);

		// Check that there is actually a row for the user, if not... initialize it
		if(!$this->userRowInitialized($uid)) $this->initializeUserRow($uid);

		$sql = "UPDATE " . PERM_RULES_TBL . " SET $permType = '$bits' WHERE rule_scope = 'user' AND rule_ref = $uid";
		return $this->runSQL($sql);
	}

	private function overwriteGroupPermission($gid,$permType,$bits){
		$this->groupGidExists($gid,true);
		$this->permissionExists($permType,true);

		// Check that there's actually a row for the gorup... if not ... intiialize it
		if(!$this->groupRowInitialized($gid)) $this->initializeGroupRow($gid);

		$sql = "UPDATE " . PERM_RULES_TBL . " SET $permType = '$bits' WHERE rule_scope = 'group' AND rule_ref = $gid";
		return $this->runSQL($sql);
	}

	// Clears all permissions for a user for a particular perm type
	function clearPermissions($id,$permType,$type){
		$this->permissionExists($permType,true);
		if($type == "user"){
			$this->userUidExists($id,true);
			$this->overwriteUserPermission($id,$permType,null);
		} else if($type == "group"){
			$this->groupGidExists($id,true);
			$this->overwriteGroupPermission($id,$permType,null);
		}
	}


	// Enter permissions for a user
	function addUserPermission($uid,$permType,$bits){
		$this->userUidExists($uid,true); // check if user uid exists, it'll throw an exception if it does not exist
		$this->permissionExists($permType,true);
		$this->overwriteUserPermission($uid,$permType,$bits);
	}

	// Enter permissions for a user
	function addGroupPermission($gid,$permType,$bits){
		$this->groupGidExists($gid,true); // check if user uid exists, it'll throw an exception if it does not exist
		$this->permissionExists($permType,true);
		$this->overwriteGroupPermission($gid,$permType,$bits);
	}

	// Puts in a row for the user
	function initializeUserRow($uid){
		if(!$this->userRowInitialized($uid)){
			$sql = "INSERT INTO " . PERM_RULES_TBL . " (rule_scope,rule_ref) VALUES ('user',$uid)";
			return $this->runSQL($sql);
		}
	}

	// Returns true if a row exists for the user in the rules table
	function userRowInitialized($uid){
		$sql = "SELECT rule_id FROM " . PERM_RULES_TBL . " WHERE rule_scope = 'user' AND rule_ref = $uid";
		if($this->sqlRowCount($sql) == 1){
			return true;
		} else {
			return false;
		}
	}


	function groupRowInitialized($gid){
		$sql = "SELECT rule_id FROM " . PERM_RULES_TBL . " WHERE rule_scope = 'group' AND rule_ref = $gid";
		if($this->sqlRowCount($sql) == 1){
			return true;
		} else {
			return false;
		}
	}

	// Puts in a row for the group
	function initializeGroupRow($gid){
		if(!$this->groupRowInitialized($gid)){
			$sql = "INSERT INTO " . PERM_RULES_TBL . " (rule_scope,rule_ref) VALUES ('group',$gid)";
			return $this->runSQL($sql);
		}
	}


	// Gets all permissions for a group
	function getGroupPermissions($gid) {
		$sql = "SELECT * FROM " . PERM_RULES_TBL .  " WHERE rule_scope = 'group' AND rule_ref = $gid";
		$result = $this->runSQL($sql);
		return $result;
	}

	// Gets all permissions for a user, this includes all the permissions for the group the user is in
	function getUserPermissions($uid) {
		$sql = "SELECT * FROM " . PERM_RULES_TBL .  " WHERE rule_scope = 'user' AND rule_ref = $uid UNION ";
		$sql = $sql . " (SELECT * FROM " . PERM_RULES_TBL . " WHERE rule_scope = 'group' AND rule_ref ";
		$sql = $sql . " IN (SELECT gid FROM " . USR_GRP_TBL . " WHERE uid = $uid ))";
		$result = $this->runSQL($sql);
		return $result;
	}


	function getUserPermissionsInXml($uid=null){
		$sql = "SELECT * FROM " . PERM_RULES_TBL . " WHERE rule_scope = 'user'";
		if($uid !== NULL){
			$sql = $sql . " AND rule_ref = $uid";
		}

		$result = $this->runSQL($sql);

		$returnStr = array();
		$returnStr [] =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		$returnStr [] = "<user_permissions>";

		if($result){
			$returnStr []  = "<user_permission>";
			while($userPermRow = $this->db->fetchArray($result)){

				$uid = $userPermRow["rule_ref"];
				$returnStr [] = "<uid>$uid</uid>";

				$allperms = "SELECT * FROM " . PERM_TBL;
				$allperms_result = $this->runSQL($allperms);
				$returnStr [] = "<permissions>";
				if($allperms_result) {

					while($permRow = $this->db->fetchArray($allperms_result)) {
						$pmt_id = strtolower($permRow["pmt_id"]);
						$crud = $userPermRow[$pmt_id];
						if(empty($crud)) $crud = "0000";

						$returnStr []  = "<permission>";
						$returnStr []  = "<id>";
						$returnStr []  = $pmt_id;
						$returnStr []  = "</id>";

						$returnStr []  = "<crud>";
						$returnStr []  = $crud;
						$returnStr []  = "</crud>";

						$returnStr []  = "</permission>";
					}
				}

				$returnStr [] = "</permissions>";
			}
			$returnStr []  = "</user_permission>";
		}

		$returnStr [] = "</user_permissions>";
		return join("",$returnStr);
	}


	function getGroupPermissionsInXml($gid=null){
		$sql = "SELECT * FROM " . PERM_RULES_TBL . " WHERE rule_scope = 'group'";
		if($gid !== NULL){
			$sql = $sql . " AND rule_ref = $gid";
		}

		$result = $this->runSQL($sql);

		$returnStr = array();
		$returnStr [] =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		$returnStr [] = "<group_permissions>";

		if($result){

			$returnStr []  = "<group_permission>";
			while($groupPermRow = $this->db->fetchArray($result)){

				$gid = $groupPermRow["rule_ref"];
				$returnStr [] = "<gid>$gid</gid>";

				$allperms = "SELECT * FROM " . PERM_TBL;
				$allperms_result = $this->runSQL($allperms);
				$returnStr [] = "<permissions>";
				if($allperms_result) {

					while($permRow = $this->db->fetchArray($allperms_result)) {
						$pmt_id = strtolower($permRow["pmt_id"]);
						$crud = $groupPermRow[$pmt_id];
						if(empty($crud)) $crud = "0000";

						$returnStr []  = "<permission>";
						$returnStr []  = "<id>";
						$returnStr []  = $pmt_id;
						$returnStr []  = "</id>";

						$returnStr []  = "<crud>";
						$returnStr []  = $crud;
						$returnStr []  = "</crud>";

						$returnStr []  = "</permission>";
					}
				}

				$returnStr [] = "</permissions>";
			}
			$returnStr []  = "</group_permission>";
		}

		$returnStr [] = "</group_permissions>";
		return join("",$returnStr);
	}

	function getDb(){
		return $this->db;
	}

	// Ensure that all the columns in the rule table exist in permission type table and delete all others
	function regularizePermissions($report=false){
		$sql = "SELECT pmt_id FROM " . PERM_TBL;
		$result = $this->runSQL($sql);
		if($result){
			while($row = $this->db->fetchArray($result)) {
				$permType = $row["pmt_id"];
				// Check to see if the column exists ...
				$sql = "SELECT $permType FROM " . PERM_RULES_TBL . " WHERE 1=2";
				try {
					$result = @$this->runSQL($sql); // if this does not throw an exception it can stay
					if($result)	{
						if($report) echo "Keeping \"$permType\"<br>";
					} else {
						if($report){
							echo "Deleting \"$permType\"<br>";
						}
						$sql = "DELETE FROM " . PERM_TBL . " WHERE pmt_id = '$permType'";
						$this->runSQL($sql);
					}
				} catch(Exception $e){
					// Delete this one from the permission table
					if($report){
						if($report) echo "Deleting \"$permType\"<br>";
					}
					$sql = "DELETE FROM " . PERM_TBL . " WHERE pmt_id = '$permType'";
					$this->runSQL($sql);
				}
			}
		}
	}

	//	============================================================
	//	AUTHENTICATION METHODS

	public function login($usr,$psw,$descriptive=false) {

		//if($_SESSION[SESSIONUID]){ throw new UserAlreadyLoggedInException(); } // throw exception is some one is already logged in
		if($_SESSION[$this->id] && $_SESSION[$this->id] != ""){ throw new UserAlreadyLoggedInException(); }

		$sql = "SELECT uid,salt,psw_hash,alias FROM " . USR_TBL . " WHERE alias = '$usr'";
		if($this->sqlRowCount($sql) == 0) {
			if($descriptive){
				throw new NonExistentEntityException("The username \"$usr\" does not exist");
			}
			return false; // alias not known
		}

		$result = $this->runSQL($sql); // run the query and get the results
		if($result)	{
			while($row = $this->db->fetchArray($result)){
				$hash = $row["psw_hash"];
				$salt = $row["salt"];
				$uid = $row["uid"];
				$alias = $row["alias"];

				$generatedHash = $psw . $salt;
				if(md5($generatedHash) == $hash){
					if($descriptive) echo "Hash match";
					$this->saveSessionData($uid,$alias);
					return true;
				} else {
					if($descriptive) echo "Hash mismatch";
					return false;
				}
			}
		} else {
			if($descriptive) echo "Uknown error";
			return false;
		}
		return false;
	}

	// Logs out the user
	public function logout(){
		$this->clearSessionData();
		$this->loggedIn = false;
	}

	// Tells whether the user is logged in or not
	public function isLoggedIn(){
		return $this->loggedIn;
	}

	// Tells if someone is logged in at all, used internally for preventing hijackin of sessions
	private function isAnyOneLoggedIn() {
		return $this->sessionDataExists();
	}

	private function sessionDataExists(){
		//return ($_SESSION[SESSIONUID] != "" && $_SESSION[SESSIONUSR] != "");
		return ($_SESSION[$this->id] != "" && $_SESSION[$this->id] != null);
	}

	// Clears all the session data stored for a user ... called by the logout function
	private function clearSessionData(){
		/*
		$_SESSION[SESSIONUID] = null;
		$_SESSION[SESSIONUSR] = null;
		$_SESSION[SESSIONPERMOBJ] = null;
		*/

		$_SESSION[$this->id] = null;
		session_destroy();
	}

	// Saves the user's uid and alias in the session... also stores the user's permission object in the session
	private function saveSessionData($uid,$alias){
		/*$_SESSION[SESSIONUID] = $uid;
		$_SESSION[SESSIONUSR] = $alias;
		$userpermissions = $this->getUserPermissions($uid);
		$permObj = new PermObj($userpermissions,$this->getDb());
		$_SESSION[SESSIONPERMOBJ] = $permObj;
		*/

		$userpermissions = $this->getUserPermissions($uid);
		$permObj = new PermObj($userpermissions,$this->getDb());
		$sd = array(SESSIONUID => $uid, SESSIONUSR => $alias, SESSIONPERMOBJ => $permObj);
		$_SESSION[$this->id] = $sd;
		$this->loggedIn = true;
	}


	public function getSessionUser()
	{
		//return $_SESSION[SESSIONUSR];
		$sd = $_SESSION[$this->id];
		return $sd[SESSIONUSR];
	}

	public function getSessionUserUid(){
		$sd = $_SESSION[$this->id];
		return $sd[SESSIONUID];

	}

	// Returns the permission object so that it can be queried
	public function getPermissionBot($throwException=false) {
		if($this->loggedIn) {
			//return $_SESSION[SESSIONPERMOBJ];
			$sd = $_SESSION[$this->id];
			return $sd[SESSIONPERMOBJ];
		} else {
			if($throwException) throw new Exception("User is not logged in!");
			return null;
		}
	}
}


class PermObj {

	private $perms = array();
	private $db = null;
	private $verbose = false;
	private $initialized = false;

	function __construct($permResult=null,$db){
		$this->db = $db;

		if($permResult) {
			while($arr = $db->fetchArray($permResult)) {
				foreach($arr as $key=>$value) {
					if(is_numeric($key)) continue;

					if($key == "rule_id" ) continue;

					if($key == "rule_scope") {
						if($this->verbose) echo "[ $value permissions ";
						continue;
					}

					if($key == "rule_ref"){
						if($this->verbose) echo " $value ]<br>";
						continue;
					}

					if(!$this->perms[$key]) $this->perms[$key] = "0000"; // initialize permissions if not initialized
					if($value){
						if($this->verbose) echo "Updating permission for type: \"$key\" ... combining " . $this->perms[$key] . " and " . $value . " ";
						$newVal = $this->combine($value,$this->perms[$key]); // update the permissions object
					} else {
						if($this->verbose) echo "Updating permission for type: \"$key\" ... uninitialized, setting to 0000 ";
						$newVal = "0000";
					}
					if($this->verbose) echo "Result: " . $newVal . "<br>";
					$this->perms[$key] = $newVal;
				}
			}
			$this->locked = true;
			$this->initialized = true;
		} 
	}

	private function combine($bit1,$bit2) {
		$orredStr = "";
		if(strlen($bit1) != strlen($bit2)) {
			return null;
		}
		for($i = 0; $i < strlen($bit1); $i++){
			$orredStr[$i] = $bit1[$i] | $bit2[$i];
		}
		return implode("",$orredStr);
	}

	private $locked = false;


	function canCreate($permType){
		if(!$this->perms[$permType]) return false;
		return $this->perms[$permType][0] == "1";

	}

	function canRead($permType){
		if(!$this->perms[$permType]) return false;
		return $this->perms[$permType][1] == "1";
	}

	function canUpdate($permType){
		if(!$this->perms[$permType]) return false;
		return $this->perms[$permType][2] == "1";
	}

	function canDelete($permType){
		if(!$this->perms[$permType]) return false;
		return $this->perms[$permType][3] == "1";
	}

	function __toString() {
		$str = "<table border=1><tr><td>Permission Type</td><td>C</td><td>R</td><td>U</td><td>D</td></tr>";
		foreach($this->perms as $key=>$value){
			if(is_numeric($key)) continue;
			$str = $str . "<tr>";
			$str = $str . "<td>$key</td>";
			if($value == ""){
				$str = $str . "<td>No</td><td>No</td><td>No</td><td>No</td>";
			} else if(strlen($value) == 4){
				for($i = 0; $i < strlen($value); $i++){
					if($value[$i] == "1"){
						$str = $str . "<td>Yes";
						$str .= "</td>";
					} else {
						$str = $str . "<td>No</td>";
					}
				}
			}
			$str = $str . "</tr>";
		}
		$str = $str . "</table>";
		return $str;
	}
}

function createRandomPassword() {
	$chars = "_.#abcdefghijkmnopqrstuvwxyz023456789";
	srand((double)microtime()*1000000);
	$i = 0;
	$pass = '' ;
	while ($i <= 10) {
		$num = rand() % 33;
		$tmp = substr($chars, $num, 1);
		$pass = $pass . $tmp;
		$i++;
	}
	return $pass;
}

class NonExistentEntityException extends Exception {
	function __construct($message,$code){
		parent::__construct($messsage,$code);
	}

	function __toString(){
		return __CLASS__ . " [ " . $this->message . " ]";
	}
}

class UserAlreadyLoggedInException extends Exception { }

/*
class SessionData {
function __construct(){}
$suid = '';
$sessionalias = '';
$sessionpermobj = '';
}*/

?>
