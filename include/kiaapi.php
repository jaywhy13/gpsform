<?php
/** TODO:
 *	Check for require columns and check to ensure that those are provided prior to attempting insertion into database
 * 	Implement kiaprint for JsonKiaPrinter
 *  Extend functionality of key matching for cases where keys have different names in tables
 *  Check getReferredTable sql
 */

require_once ("db.php");
require_once ("excelwriter.inc.php");
define ( "KIA_XML", "kiaxml" );
define ( "KIA_JSON", "kiajson" );
define ( "KIA_BOOL", "kiabool" );
define ( "KIA_XLS","kiaxls");
define ( "KIA_SUCCESS_MSG", "Your data was successfully saved to the system.");
define ( "KIA_ERROR_MSG", "Error occurred while inserting your data! " );

/**
 * Very nice function for printing out a file obtained from: http://w-shadow.com/blog/2007/08/12/how-to-force-file-download-with-php/
 *
 * @param String $file location of the file
 * @param String $name the filename to be printed out to the browser
 * @param String $mime_type optional mime type of the file
 */
function output_file($file, $name, $mime_type='')
{
	/*
	This function takes a path to a file to output ($file),
	the filename that the browser will see ($name) and
	the MIME type of the file ($mime_type, optional).

	If you want to do something on download abort/finish,
	register_shutdown_function('function_name');
	*/
	if(!is_readable($file)) die('File not found or inaccessible!');

	$size = filesize($file);
	$name = rawurldecode($name);

	/* Figure out the MIME type (if not specified) */
	$known_mime_types=array(
	"pdf" => "application/pdf",
	"txt" => "text/plain",
	"html" => "text/html",
	"htm" => "text/html",
	"exe" => "application/octet-stream",
	"zip" => "application/zip",
	"doc" => "application/msword",
	"xls" => "application/vnd.ms-excel",
	"ppt" => "application/vnd.ms-powerpoint",
	"gif" => "image/gif",
	"png" => "image/png",
	"jpeg"=> "image/jpg",
	"jpg" =>  "image/jpg",
	"php" => "text/plain"
	);

	if($mime_type==''){
		$file_extension = strtolower(substr(strrchr($file,"."),1));
		if(array_key_exists($file_extension, $known_mime_types)){
			$mime_type=$known_mime_types[$file_extension];
		} else {
			$mime_type="application/force-download";
		};
	};

	@ob_end_clean(); //turn off output buffering to decrease cpu usage

	// required for IE, otherwise Content-Disposition may be ignored
	if(ini_get('zlib.output_compression'))
	ini_set('zlib.output_compression', 'Off');

	header('Content-Type: ' . $mime_type);
	header('Content-Disposition: attachment; filename="'.$name.'"');
	header("Content-Transfer-Encoding: binary");
	header('Accept-Ranges: bytes');

	/* The three lines below basically make the
	download non-cacheable */
	header("Cache-control: private");
	header('Pragma: private');
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

	// multipart-download and download resuming support
	if(isset($_SERVER['HTTP_RANGE']))
	{
		list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
		list($range) = explode(",",$range,2);
		list($range, $range_end) = explode("-", $range);
		$range=intval($range);
		if(!$range_end) {
			$range_end=$size-1;
		} else {
			$range_end=intval($range_end);
		}

		$new_length = $range_end-$range+1;
		header("HTTP/1.1 206 Partial Content");
		header("Content-Length: $new_length");
		header("Content-Range: bytes $range-$range_end/$size");
	} else {
		$new_length=$size;
		header("Content-Length: ".$size);
	}

	/* output the file itself */
	$chunksize = 1; //you may want to change this
	$bytes_send = 0;
	if ($file = fopen($file, 'r'))
	{
		if(isset($_SERVER['HTTP_RANGE']))
		fseek($file, $range);

		while(!feof($file) &&
		(!connection_aborted()) &&
		($bytes_send<$new_length)
		)
		{
			$buffer = fread($file, $chunksize);
			print($buffer);
			//			echo($buffer); // is also possible
			flush();
			$bytes_send += strlen($buffer);
		}
		fclose($file);
	} else {
		$GLOBALS['kia']->getPrinter->kiaerror('Error - can not open file.');
	}

	die("");
}

// Setup custom error handler
$ceh_messages = array ();
function ceh($num, $str, $file, $line) {
	$GLOBALS ['ceh_messages'] [] = $str;
}

$ceh_processing = true;

function ceh_last_error() {
	$arr = $GLOBALS ['ceh_messages'];
	if (sizeof ( $arr ) > 0) {
		$msg = $arr [sizeof ( $arr ) - 1];

		if ($GLOBALS ['ceh_processing']) {
			if (ereg ( "pg-query", $msg )) {
				if (ereg ( "duplicate", $msg )) {
					$msg = "Your data would create duplicates.";
				}
			}
			else if(ereg("Undefined index",$msg)) {
				$msg = "Your data would create duplicates.";
			}
		}
		return $msg;
	}
	return null;
}

set_error_handler ( "ceh" );

class Kia {

	private $conn = null;
	private $db = null;
	private $printerType = KIA_XML;
	private $printer = null;
	private $quoteData = array ();
	private $tableOids = array ();
	private $tableRequirements = array ();
	private $supervisor = null;
	private $screener = null;
	private $spyFunction = null;
	private $debug = false;
	private $columnAlias = null;

	function __construct() {
		$this->connect ();
		$this->setPrinterType ();
	}

	// Manage the connection to the database
	function connect() {
		$this->db = getDBInstance ( "pg", "../include/db_config_pgsql.conf" );
	}

	function isConnected() {
		return $this->db->isConnected ();
	}

	// Printer functions
	function setPrinterType($printerType = KIA_XML) {
		if ($printerType == KIA_XML)
		$this->printer = new XmlKiaPrinter ( $this );
		if ($printerType == KIA_JSON)
		$this->printer = new JsonKiaPrinter ( $this );
		if ($printerType == KIA_BOOL)
		$this->printer = new BoolKiaPrinter ( $this );
		if ($printerType == KIA_XLS)
		$this->printer = new XlsKiaPrinter( $this );

		$this->printerType = $printerType;
	}

	function setSupervisor($supervisor) {
		$this->supervisor = $supervisor;
	}

	function getSupervisor() {
		return $this->supervisor;
	}

	function setScreener($func){
		$this->screener = $func;
	}

	function getScreener($func){
		return $this->screener;
	}

	function screen($relation,$field,$value){
		if($this->screener){
			return call_user_func($this->screener,$relation,$field,$value);
		} else {
			return $value;
		}
	}

	function setSpyFunction($spyFunction){
		$this->spyFunction = $spyFunction;
	}

	function getSpyFunction(){
		return $this->spyFunction;
	}
	
	function setColumnAliasFunction($colAliasFunction){
		$this->columnAlias = $colAliasFunction;
	}
	
	function getColumnAliasFunction(){
		return $this->columnAlias;
	}
	
	function getColumnAlias($column_name){
		if($this->columnAlias == null) return $column_name;
		else return call_user_func($this->columnAlias,$column_name);
	}
		
	function spy($action,$relation,$criteria=null,$values=null){
		if($this->spyFunction){
			call_user_func($this->spyFunction,$action,$relation,$criteria,$values);
		}
	}

	function askSupervisor($action,$permission) {
		if ($this->supervisor == null)
		return true;
		return call_user_func ( $this->supervisor, $action, $permission );
	}

	function getPrinterType() {
		return $this->printerType;
	}

	function getPrinter() {
		return $this->printer;
	}

	// Query functions
	function runSQL($sql, $allowDebug = false) {
		if ($allowDebug)
		return $this->db->query ( $sql );
		else
		return @$this->db->query ( $sql );
	}

	function loopResult($res) {
		return $this->db->fetchArray ( $res );
	}

	function resultCount($sql){
		$res = $this->runSQL($sql);
		if($res) return pg_num_rows($res);
		return 0;
	}

	function magic_seq($action, $relation, $key_field, $data_arr, $seq = null) {
		if ($seq == NULL) {
			$seq = "$relation" . "_seq";
		}

		$id = 0;
		$sql = "SELECT nextval('$seq')";
		$res = $this->runSQL ( $sql );
		if ($res) {
			while ( $row = $this->loopResult ( $res ) ) {
				$id = $row ["nextval"];
			}
		}


		// Now call magic
		$data_arr [$key_field] = $id;

		// Save old printer
		$old_printer = $this->getPrinterType ();

		// Use a bool printer
		$this->setPrinterType ( KIA_BOOL );
		if ($this->magic ( "create", $relation, null, $data_arr )) {
			$this->setPrinterType ( $old_printer );
			$this->getPrinter ()->kiamsg ( "success:$id" );
		} else {
			$this->setPrinterType ( $old_printer );
			$this->getPrinter ()->kiaerror ( "Could not create the data" );
		}

	}

	function magic_xtq($action, $relation_arr, $col_list = null, $criteria_arr = null, $view_name = null, $special_criteria=null,$order_by=null) {
		if (! is_array ( $relation_arr )) {
			$this->printer->kiaerror ( "No array supplied" );
		}

		$order_by_accepted = array();


		if ($action == "read") {
			// Loop through tables and add order bys
			for($l = 0; $l < sizeof($relation_arr); $l++){
				$col_list_rel = $this->getColumnNames($relation_arr[$l]);
				// check the order by clauses
				if($order_by){
					for($i = 0; $i < sizeof($order_by); $i++){
						$order_by_clause = explode(":",$order_by[$i]);
						$field_name_parts = explode(".",$order_by_clause[0]);
						if(sizeof($field_name_parts) != 2) $field_name = $field_name_parts[0];
						else $field_name = $field_name_parts[1];

						$order = "";
						if(sizeof($order_by_clause) > 1) $order = $order_by_clause[1];
						for($j = 0; $j < sizeof($col_list_rel); $j++){

							if($field_name == $col_list_rel[$j] || $field_name == ($relation . "." . $col_list_rel[$j])) {
								$col_str = $relation_arr[$l] . ".$field_name $order";
								if(!in_array($col_str,$order_by_accepted)) $order_by_accepted [] = $col_str;
								//								echo "$field_name <u>MATCHES</u> " . $col_list_rel[$j] . "<br>";
							} else {
								//								echo "$field_name does not match " . $col_list_rel[$j] . "<br>";
							}
						}
					}
				}
			}

			//			print_r($order_by_accepted);

			$fkeyClauses = array (); // array to store the foreign key clauses
			for($r = 0; $r < sizeof ( $relation_arr ); $r ++) {
				for($s = 0; $s < sizeof ( $relation_arr ); $s ++) {

					$orig_rel = $relation_arr [$r];
					$for_rel = $relation_arr [$s];

					if ($r == $s)
					continue;

					if ($fkeyClauses ["$r:$s"] || $fkeyClauses ["$s:$r"]){
						continue; // if condition added already.. skip this cond
					}

					$orig_keys = $this->getColumnKeys ( $orig_rel );
					$for_fkeys = $this->getColumnFKeys ( $for_rel );


					// Add foreign key clauses
					for($i = 0; $i < sizeof ( $for_fkeys ); $i ++) {
						$for_fkey = $for_fkeys [$i];
						$for_fkey_index = $this->getColumnIndex ( $for_rel, $for_fkey );

						if (in_array ( $for_fkey, $orig_keys )) {
							if (in_array($for_fkey_index,$this->getReferredColumnIndex ( $orig_rel, $for_fkey, $for_rel))) {
								$fkeyClause = "$for_rel.$for_fkey = $orig_rel.$for_fkey";
								//								echo "&nbsp;&nbsp;>>&nbsp;&nbsp;Adding clause: $fkeyClause<br>";
								$fkeyClauses [] = $fkeyClause;
							}
						}
					}
				}
			}

			$sql = "SELECT * ";

			if ($col_list) {
				$sql = "SELECT " . join ( ",", $col_list );
			}

			$sql .= " FROM " . join ( ",", $relation_arr );

			if (sizeof ( $fkeyClauses ) > 0) {
				$sql .= " WHERE " . join ( " AND ", $fkeyClauses );
			}

			if (sizeof ( $this->filterFields ( $criteria_arr, $relation_arr ) ) > 0) {

				if (sizeof ( $fkeyClauses ) == 0)
				$sql .= " WHERE ";
				else
				$sql .= " AND ";

				$sql .= " " . $this->joinConds ( $this->filterFields ( $criteria_arr, $relation_arr ) );
			}

			if (sizeof( $this->filterFields($special_criteria,$relation_arr)) > 0){
				if (sizeof ( $fkeyClauses ) == 0)
				$sql .= " WHERE ";
				else
				$sql .= " AND ";

				$filteredConds = $this->filterFields ( $special_criteria, $relation_arr );
				$sql .= " " . $this->joinConds ( $filteredConds ,true, " OR ");

			}

			if(sizeof($order_by_accepted) > 0){
				$sql .= " ORDER BY " . join(",",$order_by_accepted);
			}

			$sql = str_replace("\\","",$sql);

//													die("$sql");

			$canReadAll = true;
			for($i = 0; $i < sizeof($relation_arr); $i++){
				$canReadAll = $canReadAll && $this->askSupervisor("read",$relation_arr[$i]);
			}

			if ($canReadAll) {
				$res = $this->runSQL ( $sql );
				$this->spy("read",$relation_arr,$col_list,null);
			} else {
				$res = NULL;
			}
			if (empty ( $view_name ) || (! isset ( $view_name )))
			$view_name = join ( "__", $relation_arr );

			echo $this->printer->kiaprint ( $res, $view_name );

		}
	}

	// Magical expressions


	function magic($action, $relation, $col_list = null, $criteria_arr = null, $view_name = null, $special_criteria = null, $order_by = null) {
		$columns = $this->getColumnNames ( $relation );
		$keys = $this->getColumnKeys ( $relation ); // get a list of the keys
		$order_by_accepted = array();

		// check the order by clauses
		if($order_by){
			for($i = 0; $i < sizeof($order_by); $i++){
				$order_by_clause = explode(":",$order_by[$i]);
				$field_name = $order_by_clause[0];
				$order = "";
				if(sizeof($order_by_clause) > 1) $order = $order_by_clause[1];
				for($j = 0; $j < sizeof($columns); $j++){
					if($field_name == $columns[$j] || $field_name == ($relation . "." . $columns[$j])) {
						$order_by_accepted [] = "$field_name $order";
					}
				}
			}
		}

		if ($action == "read") { //	READ FROM THE DATABASE ===================
			$sql = "SELECT ";
			if ($col_list) {
				$sql .= join ( ", ", $col_list );
			} else {
				$sql .= " * ";
			}

			$sql .= " FROM $relation ";

			if ($criteria_arr && sizeof ( $criteria_arr ) > 0) { // check to make sure that all the column names are actually valid
				$conditions = array ();
				foreach ( $criteria_arr as $field => $value ) {
					if (in_array ( $field, $columns )) {
						$quoted_expression = $this->quoteExpression ( $relation, $field, $value );
						$conditions [$field] = $quoted_expression;
					}
				}

				if (sizeof ( $conditions ) > 0)
				$sql .= " WHERE " . $this->joinConds ( $conditions );

			}


			if (sizeof( $this->filterFields($special_criteria,$relation)) > 0){
				if (sizeof ( $conditions ) == 0)
				$sql .= " WHERE ";
				else
				$sql .= " AND ";

				$filteredConds = $this->filterFields ( $special_criteria, $relation );
				//				echo "<br>Filtered Conds:<br>";
				//				print_r($this->filterFields ( $special_criteria, $relation ));
				//				echo "<br>";
				//				echo "<br>";
				$sql .= " " . $this->joinConds ( $filteredConds ,true, " OR ");
			}


			if(sizeof($order_by_accepted) > 0){
				$sql .= " ORDER BY " . join(",",$order_by_accepted);
			}

			$sql = str_replace("\\","",$sql);

			$sql = trim($sql);
            
//																die ($sql);

			if ($this->askSupervisor ( "read" , $relation )) {
				$rs = $this->runSQL ( $sql );
				$this->spy("read",$relation,$col_list,null);
			} else {
				$rs = NULL;
			}


			if (empty ( $view_name ) || ! isset ( $view_name ))
			$view_name = $relation;
			echo $this->printer->kiaprint ( $rs, $view_name );

		} else if ($action == "update") { //	UPDATE FROM THE DATABASE ===================
			$sql = "UPDATE $relation SET ";

			$updateClauses = array ();
			$fields = array();
			$values = array();

			foreach ( $criteria_arr as $key => $value ) {
				if (in_array ( $key, $keys ))
				continue;
				if (! in_array ( $key, $columns ))
				continue;

				$updateClauses [] = "$key = " . $this->quoteExpression ( $relation, $key, $value );
				$fields [] = $key;
				$values [$key] = $value;
			}

			if (sizeof ( $updateClauses ) > 0) {
				$sql .= join ( ",", $updateClauses );
			}

			// Now add the where clause
			$conditionClauses = array ();
			for($i = 0; $i < sizeof ( $keys ); $i ++) {
				$key = $keys [$i];
				if ($criteria_arr [$key]) {
					$conditionClauses [] = "$key = " . $this->quoteExpression ( $relation, $key, $criteria_arr [$key] );
				}
			}
			if (sizeof ( $conditionClauses ) > 0) {
				$sql .= " WHERE " . join ( " AND ", $conditionClauses );
			}

			if (sizeof ( $updateClauses ) <= 0) {
				$this->printer->kiaerror ( "No update specified. Cannot perform update" );
			}

			if ($this->askSupervisor ( "update" , $relation )) {
				$result = $this->runSQL ( $sql );
				$this->spy("update",$relation,$fields,$values);
			} else {
				$result = NULL;
			}

			if ($result) {
				if (pg_affected_rows ( $result ) > 0) {
					if ($this->printerType == KIA_BOOL) {
						return true;
					} else {
						$this->printer->kiamsg ( "Data updated successfully" );
					}

				} else {
					if ($this->printerType == KIA_BOOL) {
						return false;
					} else
					$this->printer->kiamsg ( "No data matching the criteria specified could be updated" );
				}
			} else {
				if ($this->printerType == KIA_BOOL) {
					return false;
				} else {
					if (! $this->askSupervisor ( "update" , $relation ))
					$this->printer->kiaerror ( "You do not have permissions to update this data!" );
					$this->printer->kiaerror ( "Unable to update data. Error occurred!" );
				}

			}

		} else if ($action == "delete") { //	DELETE FROM THE DATABASE ===================
			$sql = "DELETE FROM $relation ";
			$deleteClauses = array ();
			$fields = array();
			$values = array();

			foreach ( $criteria_arr as $key => $value ) {
				if (! in_array ( $key, $columns ))
				continue;
				$deleteClauses [] = "$key = " . $this->quoteExpression ( $relation, $key, $value );
				$fields [] = $key;
				$values [$field] = $value;
			}

			if (sizeof ( $deleteClauses ) > 0) {
				$sql .= " WHERE " . join ( " AND ", $deleteClauses );
			}

			if ($this->askSupervisor ( "delete", $relation )) {
				$result = $this->runSQL ( $sql );
				$this->spy("delete",$relation,$fields,$values);
			} else {
				$result = NULL;
			}

			if ($result) {
				if (pg_affected_rows ( $result ) > 0) {
					if ($this->printerType == KIA_BOOL) {
						return true;
					} else
					$this->printer->kiamsg ( "Data removed successfully" );
				} else {
					if ($this->printerType == KIA_BOOL) {
						return false;
					} else
					$this->printer->kiamsg ( "No data matching your criteria was deleted!" );
				}
			} else {
				if ($this->printerType == KIA_BOOL) {
					return false;
				} else {
					if (! $this->askSupervisor ( "delete" , $relation ))
					$this->printer->kiaerror ( "You do not have permission to delete this data!" );
					$this->printer->kiaerror ( "Unable to delete data. You may experience this error because you are trying to delete data that other data in the system depends on. " );
				}
			}
		} else if ($action == "create") { //	CREATE DATA FOR THE DATABASE ===================
			// Check FIRST, to make sure that ALL required keys are supplied
			for($i = 0; $i < sizeof ( $keys ); $i ++) {
				$key = $keys [$i];
				if (empty ( $criteria_arr [$key] ) && $this->isRequired ( $relation, $key )) {
					echo "$key is required!";
					$this->printer->kiaerror ( "Important data has not been supplied, cannot save the data supplied!" );
				}
			}

			$sql = "INSERT INTO $relation ";
			$insertColumns = array ();
			foreach ( $criteria_arr as $key => $value ) {
				if (! in_array ( $key, $columns )) {
					continue;
				}

				if (empty ( $criteria_arr [$key] ) && ! $this->isRequired ( $relation, $key ) && $criteria_arr[$key] !== 0){
					//					echo "Skipping $key<br>";
					continue; // if the value was empty and the field is not required... skipt it

				}

				$insertColumns [] = $key;
			}

			if (sizeof ( $insertColumns ) == 0) {
				$this->printer->kiaerror ( "No columns were specified for insertion" );
			} else {
				$sql .= " ( " . join ( ",", $insertColumns ) . " ) ";
			}

			$sql .= " VALUES ( ";

			$values = array ();
			for($i = 0; $i < sizeof ( $insertColumns ); $i ++) {
				$insertColumn = $insertColumns [$i];
				if (empty ( $criteria_arr [$insertColumn] ) && ! $this->isRequired ( $relation, $insertColumn ))
				$this->printer->kiaerror ( "No value supplied for $insertColumn" );
				$values [] = $this->quoteExpression ( $relation, $insertColumn, $criteria_arr [$insertColumn] );
			}

			$sql .= join ( ",", $values ) . " ) ";

			if ($this->askSupervisor ( "create" , $relation )) {

				$result = $this->runSQL ( $sql );
				$this->spy("create",$relation,$insertColumns,$values);
			} else {
				$result = NULL;
			}

//						die($sql);
//echo $sql . "<br>";
        
			if ($result) {
				if ($this->printerType == KIA_BOOL) {
					return true;
				} else
				$this->printer->kiamsg ( KIA_SUCCESS_MSG );
			} else {

				if ($this->printerType == KIA_BOOL) {
					return false;
				} else {
					if(!$this->askSupervisor("create",$relation)) $this->printer->kiaerror ( "You do not have sufficient permission to create this record!");
					$msg = KIA_ERROR_MSG . ceh_last_error();
					if($this->debug) $msg = $msg . " SQL: $sql";
					$this->printer->kiaerror ( $msg );
				}

			}
		}
	}

	// Gets the index of the column in the table... -1 returned for no match
	function getColumnIndex($tbl, $column) {
		$oid = $this->getTableOid ( $tbl );
		$sql = "select attnum from pg_attribute where attrelid = $oid and attname = '$column'";
		$res = $this->runSQL ( $sql );
		while ( $row = pg_fetch_array ( $res ) ) {
			return $row ["attnum"];
		}
		return - 1;
	}

	// Get the index of the column referenced by a f-key
	function getReferredColumnIndex($tbl, $column, $f_tbl) {
		//		echo "Considering $tbl $column $f_tbl<br>";
		if ($this->isKey ( $tbl, $column )) {
			$oid = $this->getTableOid ( $tbl );
			$f_oid = $this->getTableOid ( $f_tbl );
			$c_index = $this->getColumnIndex ( $tbl, $column );

			$sql = "select (conkey) from pg_constraint where conrelid = $f_oid and confrelid = $oid and contype = 'f'; ";
			//			echo $sql;
			$res = $this->runSQL ( $sql );
			if ($res) {
				while ( $row = $this->loopResult ( $res ) ) {
					$arrStr = $row ["conkey"];
					if (ereg ( "{[0-9]+}", $arrStr )) {
						return array (substr ( $arrStr, 1, strlen ( $arrStr ) - 2 ));
					} else if (ereg ( "{[0-9,]+}", $arrStr )) {
						return explode(",",substr ( $arrStr, 1, strlen ( $arrStr ) - 2 ));
					}
				}
			}
		}
		return array();
	}

	// Returns an oid for the table that is referenced by tbl.column ... -1 if not found or not a fkey
	function getReferredTable($tbl, $column) {
		$oid = $this->getTableOid ( $tbl );
		$c_index = $this->getColumnIndex ( $tbl, $column );
		$sql = "select confrelid from pg_constraint where contype = 'f' AND conrelid = $oid and ARRAY[$c_index]::smallint[] = confkey";
		$res = $this->runSQL ( $sql );
		if ($res) {
			while ( $row = pg_fetch_array ( $res ) ) {
				return $res ["confrelid"];
			}
		}
		return - 1;
	}

	function getColumnType($tbl, $column) {
		$oid = $this->getTableOid ( $relation );
		$sql = "select typname from pg_type, pg_attribute where atttypid = typelem and attrelid = $oid and attname = '$column'";
		$res = $this->runSQL ( $sql );
		while ( $row = $this->loopResult ( $res ) ) {
			return $row ["typename"];
		}

		return null;
	}

	// Given a column name and its table name, function return a bool indicatin whether the field should b quoted
	function quote($relation, $column) {

		if (! isset ( $this->quoteData ["$relation:$column"] )) {
			// retrieve the oid
			$oid = $this->getTableOid ( $relation );

			$sql = "select typname from pg_type, pg_attribute where atttypid = typelem and attrelid = $oid and attname = '$column'";

//            echo "$relation.$column $sql<br>";
			$result = $this->runSQL ( $sql );
			while ( $o = pg_fetch_object ( $result ) ) {
				$fieldType = $o->typname;
				if (ereg ( "int", $fieldType ) || ereg ( "float", $fieldType ) || ereg ( "oid", $fieldType ) || ereg ( "serial", $fieldType )) {
					$this->quoteData ["$relation:$column"] = "no";
				} else if (ereg ( "date", $fieldType ) || ereg ( "varchar", $fieldType ) || ereg ( "text", $fieldType ) || ereg ( "char", $fieldType ) || ereg ( "name", $fieldType ) || ereg ( "timestamp", $fieldType ) || ereg("bool",$fieldType)) {
					$this->quoteData ["$relation:$column"] = "yes";
				}
				break;
			}
		}
		return $this->quoteData ["$relation:$column"] == "yes";
	}

	function quoteExpression($relation, $column, $value) {
		if ($this->quote ( $relation, $column ) == "yes") {
			return "'$value'";
		} else {
			return $value;
		}
	}

	function joinConds($assoc_arr,$opProvided=false, $joinOp = " AND ") {
		$conditions = array ();
		foreach ( $assoc_arr as $key => $value ) {
			if($opProvided)
			$conditions [] = "$value";
			else
			$conditions [] = "$key = $value";
		}
		return implode ($joinOp , $conditions );
	}

	function filterFields($assoc_arr, $relation) {
//				echo "<br><br>Starting to filter fields:";
//				print_r($assoc_arr);
//				echo "<br>";
//       			echo "<br>";

		if(!$relation) return;
		$good_fields = array ();
		if (! $assoc_arr)
		return $good_fields;
		if (is_array ( $relation )) {
			// Grab all the fields from each relation
			$fields = array ();

			for($i = 0; $i < sizeof ( $relation ); $i ++) {
				//				echo "Trying to find fields for: " . $relation[$i] . "<br>";
				$fields [$relation [$i]] = $this->getColumnNames ( $relation [$i] );
			}

			// Now keep the fields that we can shall we?
			foreach ( $assoc_arr as $key => $value ) {
				//				echo "&nbsp;Considering $key<br>";
				$col_identified = false;
				$key_copy = $key;
				if(ereg(":[0-9]+$",$key)) $key = ereg_replace(":[0-9]+$","",$key);
				//				echo "&nbsp;Considering cleaned $key<br>";


				foreach ( $fields as $rel_name => $rel_fields ) {
					$key_tmp = explode(".",$key);
					if(sizeof($key_tmp) == 2){
						//						echo "&nbsp;&nbsp;Changing $key to "  .$key_tmp[1] . "<br>";
						$key = $key_tmp[1];
					}

					if (in_array ( $key, $rel_fields )) {
                        $value = $this->quoteExpression($rel_name,$key,$value); // quote expression???
						$good_fields ["$rel_name.$key_copy"] = $value;
						break;
					}
				}
				if ($col_identified)
				continue;
			}

		} else {
			$fields = $this->getColumnNames ( $relation );

			foreach ( $assoc_arr as $key => $value ) {
				$key_copy = $key;
				if(ereg(":[0-9]+$",$key)) {
					$key = ereg_replace(":[0-9]+$","",$key);
				}

				$tmp_arr = explode(".",$key);
				if(sizeof($tmp_arr) == 2 && $tmp_arr[0] == $relation){ // check if the name was qualified
					$key = $tmp_arr[1];
				}

				if (in_array ( $key, $fields )) {
					$good_fields ["$relation.$key_copy"] = $value;
				} else {
				}
			}
		}

		//		echo "Returning the following as good fields:<br>";
		//		print_r($good_fields);

		return $good_fields;
	}

	function getTableName($oid) {
		$sql = "SELECT relname FROM pg_class WHERE oid = $oid";
		$res = $this->runSQL ( $sql );
		while ( $o = pg_fetch_object ( $res ) ) {
			return $o->relname;
		}

		return null;
	}

	function getTableOid($rel) {
		if (! isset ( $this->tableOids [$rel] )) {
			$sql = "SELECT oid FROM pg_class WHERE relname = '$rel'";
			$res = $this->runSQL ( $sql );

			while ( $o = pg_fetch_object ( $res ) ) {
				$this->tableOids [$rel] = $o->oid;
				return $o->oid;
			}
		} else {
			return $this->tableOids [$rel];
		}

		return - 1;
	}

	function isRequired($relation, $column) {
		if (! isset ( $this->tableRequirements ["$relation:$column"] )) {
			$this->getRequiredColumns ( $relation );
		}
		return $this->tableRequirements ["$relation:$column"];
	}

	function isKey($relation, $column) {
		$columns = $this->getColumnKeys ( $relation );
		return in_array ( $column, $columns );
	}

	function isFKey($relation, $column) {
		$columns = $this->getColumnFKeys ( $relation );
		return in_array ( $column, $columns );
	}

	function isRequiredKey($relation, $column) {
		$columns = $this->getColumnKeys ( $relation );
		if ($this->isRequired ( $relation, $column ) && in_array ( $column, $columns )) {
			return true;
		} else {
			return false;
		}
	}

	function getRequiredColumns($relation) {

		$oid = $this->getTableOid ( $relation );
		$sql = "SELECT attname,atthasdef, attnotnull from pg_attribute where attrelid = $oid and attnum >= 0";
		//			echo "$sql<br>";
		$result = $this->runSQL ( $sql );
		while ( $o = pg_fetch_object ( $result ) ) {
			$isRequired = $o->attnotnull;
			// If it has a default... then we don't need to insert it
			if ($o->atthasdef == "t") {
				$isRequired = "f";
			}

			$array_key = "$relation:" . $o->attname;
			if ($isRequired == "t") {
				$this->tableRequirements ["$array_key"] = true;
				//				echo "$relation." . $o->attname . " is required<br>";
			} else {
				$this->tableRequirements ["$array_key"] = false;
				//				echo "$relation." . $o->attname . " is <u>NOT</u> required<br>";
			}
		}
	}

	function getColumnKeys($relation, $skipKeysNotRequired = false) {
		$oid = $this->getTableOid ( $relation );
		if ($oid) {
			$sql = "SELECT DISTINCT attname FROM pg_attribute, pg_constraint WHERE attrelid = conrelid AND attrelid = $oid AND attnum >= 0 AND attnum = ANY (conkey) AND (contype = 'f' OR contype = 'p')";
			$arr = array ();
			$res = $this->runSQL ( $sql );
			while ( $o = pg_fetch_object ( $res ) ) {
				if ($skipKeysNotRequired && $this->isKey ( $relation, $o->attname ) && ! $this->isRequiredKey ( $relation, $o->attname ))
				continue; // skip if not required, if so asked
				$arr [] = $o->attname;
			}
			return $arr;
		}

		return array ();
	}

	function getColumnFKeys($relation, $skipKeysNotRequired = false) {
		$oid = $this->getTableOid ( $relation );
		if ($oid) {
			$sql = "SELECT attname FROM pg_attribute, pg_constraint WHERE attrelid = conrelid AND attrelid = $oid AND attnum >= 0 AND attnum = ANY (conkey) AND (contype = 'f')";
			$arr = array ();
			$res = $this->runSQL ( $sql );
			while ( $o = pg_fetch_object ( $res ) ) {
				if ($skipKeysNotRequired && $this->isKey ( $relation, $o->attname ) && ! $this->isRequiredKey ( $relation, $o->attname ))
				continue; // skip if not required, if so asked
				$arr [] = $o->attname;
			}
			return $arr;
		}

		return array ();
	}

	function getColumnName($relation, $index) {
		$sql = "SELECT attname FROM pg_attribute where attrelid = " . $this->getTableOid ( $relation ) . " and attnum = $index ";
		$res = $this->runSQL ( $sql );
		if ($res) {
			while ( $row = $this->loopResult ( $res ) ) {
				return $row ["attname"];
			}
		}
		return null;
	}

	function getColumnNames($relation, $skipKeysNotRequired = false) {
		$oid = $this->getTableOid ( $relation );

		if ($oid) {
			$sql = "SELECT attname FROM pg_attribute WHERE attrelid = $oid  AND attnum >= 0 AND NOT attisdropped ";
			$arr = array ();
			$res = $this->runSQL ( $sql );
			while ( $o = pg_fetch_object ( $res ) ) {
				if ($skipKeysNotRequired && $this->isKey ( $relation, $o->attname ) && ! $this->isRequiredKey ( $relation, $o->attname )) {
					continue;
				} // skip if not required, if so asked
				$arr [] = $o->attname;
			}

			return $arr;
		}

		return array ();
	}

	public function __toString(){
		return "Kia Instance";
	}
}

abstract class KiaPrinter {

	private $kia = null;

	function __construct($kia){
		$this->kia = $kia;
	}

	function getKia(){
		return $this->kia;
	}

	abstract function kiaprint($rs, $relation, $printHeader = true);
	abstract function kiaprintarr($arr, $printHeader = true);
	abstract function kiaerror($msg, $printHeader = true);
	abstract function kiamsg($msg, $printHeader = true);
}

class XmlKiaPrinter extends KiaPrinter {
	function kiaprint($rs, $relation, $printHeader = true) {
		$content = array ();
		if ($printHeader) {
			header ( "Content-type: text/xml" );
		}
		$content [] = "<$relation" . "s>";
		if ($rs) {
			while ( $row = pg_fetch_assoc ( $rs ) ) {
				$content [] = "<$relation>";
				foreach ( $row as $key => $value ) {
					if($this->getKia()) $value = $this->getKia()->screen($relation,$key,$value);

					$content [] = "<$key>$value</$key>";
				}
				$content [] = "</$relation>";
			}
		}
		$content [] = "</$relation" . "s>";

		return join ( "", $content );
	}

	function kiamsg($msg, $printHeader = true) {
		//		$msg = htmlentities($msg);
		header ( "Content-type: text/xml" );
		die ( "<message type=\"notification\">$msg</message>" );
	}

	function kiaerror($msg, $printHeader = true) {
		header ( "Content-type: text/xml" );
		die ( "<message type=\"error\">$msg</message>" );
	}

	function kiaprintarr($arr, $printHeader = true) {
	}
}

class JsonKiaPrinter extends KiaPrinter {
	function kiaprint($rs, $relation, $printHeader = true) {
		$messages = array ();
		$messages [] = "({\"$relation" . "s\":[";

		$rowData = array ();

		if ($rs) {
			while ( $row = pg_fetch_assoc ( $rs ) ) {
				$cols = array ();
				foreach ( $row as $key => $value ) {

					$cols [] = "\"$key\":\"$value\"";
				}

				$str = " { " . join ( ",", $cols ) . " } ";
				$rowData [] = $str;
			}
		}

		$messages [] = join ( ",", $rowData );

		$messages [] = "]})";

		die ( join ( "", $messages ) );

	}

	function kiamsg($msg, $printHeader = true) {
		//		$msg = htmlentities($msg);
		die ( "({\"message\":{\"type\":\"notification\",\"content\":\"$msg\"}})" );
	}

	function kiaerror($msg, $printHeader = true) {
		//		$msg = htmlentities($msg);
		die ( "({\"message\":{\"type\":\"error\",\"content\":\"$msg\"}})" );
	}

	function kiaprintarr($arr, $printHeader = true) {
		//		$msg = htmlentities($msg);
		$this->kiamsg ( join ( ",", $arr ) );
	}
}

class BoolKiaPrinter extends KiaPrinter {
	function kiaprint($rs, $relation, $printHeader = true) {
	}

	function kiamsg($msg, $printHeader = true) {
	}

	function kiaerror($msg, $printHeader = true) {
	}

	function kiaprintarr($arr, $printHeader = true) {
	}

}

class XlsKiaPrinter extends KiaPrinter {
	function kiaprint($rs,$relation,$printHeader = true){
		$i = 0;
		$ts = strtotime ( "now" );
		$prefix = "mgi_report_";
		$file_name = "/tmp/" . $prefix . $ts . ".xls";
		$excel = new ExcelWriter ( $file_name );

		while($row = $this->getKia()->loopResult($rs)){
			$cols = array();
			$values = array();

			$header = array();
			foreach($row as $key=>$value){
				if(!is_numeric($key)) {
					$cols [] = $this->getKia()->getColumnAlias($key);
					$values [] = $value;
				}
			}
			if($i == 0) $excel->writeLine($cols);
			$excel->writeLine($values);
			$i++;
		}
		
		$excel->close();
		output_file($file_name,"mgi-report-" . time() . ".xls"); // prompt the user to download
		@unlink($file_name); // delete the file
	}

	function kiamsg($msg,$printHeader=true) {
	}

	function kiaerror($msg, $printHeader = true) {
	}

	function kiaprintarr($arr, $printHeader = true) {
	}

}

?>