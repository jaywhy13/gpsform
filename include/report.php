<html>
	<head>
		<title>Report Generator - JAMNAV</title>
		<script src="jquery-1.2.6.min.js"></script>
		<script src="jquery-ui-personalized-1.5.2.min.js"></script>
		<script src="jquery.blockUI.js"></script>
		<script src="crudmachine-client.js"></script>
		<style type="text/css">
		.div_relation {
			border-style:solid;
			border-width:1px;
			padding:5px;
			font-size:10px;
			font-family:Verdana;
			float:left;
			width:120px;
			margin:3px;
			background-color:rgb(220,220,235);
		}	
		
		DIV,SELECT {
			margin:3px;
		}
		
		BODY {
			font-size:11px;
			line-height:150%;
		}
		
		SELECT,INPUT {
			padding:3px;
			margin:3px;
			width:120px;
		}
		
		#div_customizeReport {
			background-color:rgb(140,150,160);
			padding:5px;
			overflow:auto;
			width:98%;
			display:none;
		}
		
		#div_relationList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		}
		
		
		#div_fieldList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		}
		
		#div_constraintList {
			padding:5px;
			background-color:rgb(245,250,255);
			border-style:solid;
			border-width:1px;
			overflow:auto;
			width:800px;
		
		}
		
		
		.div_relFieldList {
			width:50%;
			padding:5px;
			margin:3px;
			border-style:solid;
			border-width:1px;
			overflow:auto;
		}
		
		.div_fieldConstraint {
			width:500px;
			padding:5px;
			margin:3px;
			border-style:solid;
			border-width:1px;
		}
		
		.div_fieldConstraint TABLE TD {
		}
		
		
		.div_relFieldList DIV {
			border-style:solid;
			border-width:1px;
			border-color:rgb(230,230,230);
			float:left;
			padding:3px;
			font-size:10px;
		}
		
		.div_relFieldList SPAN {
			width:98%;
			display:block;
			padding:3px;
			background-color:black;
			color:white;
			font-weight:bold;
			
		}
		
		#div_report {
			clear:all;
			width:98%;
			overflow:auto;
			background-color:rgb(230,240,250);
			padding:5px;
		}
		
		TABLE TD {
			font-size:11px;
		}
		
		TABLE .headerRow TD {
			background-color:black;
			color:white;
			font-weight:bold;
			border-style:solid;
			border-width:1px;
			border-color:white;
			padding:5px;
		}
		
		.evenRow TD {
			background-color:white;
		}
		
		.oddRow TD {
			background-color:rgb(230,240,250);
		}
		
		.div_field {
			width:130px;
			overflow:auto;
		}
		
		</style>
		<script>
		function formatCurrency(num, prefix) {
			if (!prefix)
			var prefix = "J $";
			num = num.toString().replace(/\$|\,/g, '');
			if (isNaN(num))
			num = "0";
			sign = (num == (num = Math.abs(num)));
			num = Math.floor(num * 100 + 0.50000000001);
			cents = num % 100;
			num = Math.floor(num / 100).toString();
			if (cents < 10)
			cents = "0" + cents;
			for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
			num = num.substring(0, num.length - (4 * i + 3)) + ','
			+ num.substring(num.length - (4 * i + 3));
			return (((sign) ? '' : '-') + prefix + num + '.' + cents);
		}


		var processFields = {};

		function addProcessFunction(field,f){
			processFields[field] = f;
		}

		function processVar(field,val){
			if(processFields[field]){
				var f = processFields[field];
				return f(val);
			}
			return val;
		}


		var relationList = new Array("gps_customer:GPS Customer","gps_order:Order","gps_order_item:Order Item","gps_order_payment:Payment","gps_lock:GPS Lock","gps_package:GPS Package","gps_product:GPS Product");
		var relationListCopy = relationList;
		var fieldList = {};
		var savedReports = {
		"customers":{
		"title":"All Customers",
		"description":"Lists all customers and useful information",
		"relations":new Array("gps_customer"),
		"fields":new Array("gps_customer.cust_id","gps_customer.cust_lname","gps_customer.cust_fname")
		},
		"customerpackages":{
		"title":"Customer Packages",
		"description":"Lists all packages that the customer has purchased and their costs",
		"relations":new Array("gps_customer","gps_package","gps_order","gps_order_item"),
		"fields":new Array("gps_customer.cust_lname","gps_customer.cust_fname","gps_package.package_name","gps_order_item.order_item_total","gps_order.order_id")
		}
		};

		function report_init(){
			// Table names
			addAlias("gps_customer","Customer");
			addAlias("gps_order","Order");
			addAlias("gps_order_item","Order Item");
			addAlias("gps_order_payment","Payment");
			addAlias("gps_lock","Lock");

			addAlias("cust_id", "Customer");
			addAlias("cust_fname", "First Name");
			addAlias("cust_lname", "Surname");
			addAlias("cust_contact", "Contact");
			addAlias("cust_email", "Email");
			addAlias("cust_jam_id", "Id No.");
			addAlias("cust_jam_id_type", "Id Type");
			addAlias("cust_contact_sec", "Secondary Contact");
			addAlias("cust_email_sec", "Secondary Email");
			addAlias("cust_comment", "Other Info");

			addAlias("package_id", "Pacakge");
			addAlias("package_name", "Pacakge Name");
			addAlias("package_cost", "Package Cost US $");
			addAlias("package_icon", "Package Icon");
			addAlias("package_type", "Package Type");

			addAlias("order_id", "Order Id");
			addAlias("roe_id", "Date (YYYY-MM-DD)");
			addAlias("roe_curr_name", "Currency Name e.g. \"US\"");
			addAlias("roe_value", "Rate J$");

			addAlias("order_date", "Order Date");
			addAlias("order_total", "Order Total US$");
			addAlias("order_item_total", "Order Item Total US$");
			addAlias("order_amount_owing", "Amount Owing");
			addAlias("order_complete", "Complete?");

			addAlias("payment_id", "Payment Number");
			addAlias("payment_date", "Payment Date");
			addAlias("payment_comment", "Notes");
			addAlias("payment_receipt", "Payment Receipt");
			addAlias("uid", "Cashier");
			addAlias("payment_amount", "Amount J$");
			addAlias("payment_type", "Payment Type");

			// Add processor functions
			addProcessFunction("package_cost", formatCurrency);
			addProcessFunction("package_cost", formatCurrency);
			addProcessFunction("roe_cost", formatCurrency);
			addProcessFunction("order_total", formatCurrency);
			addProcessFunction("order_item_total", formatCurrency);
			addProcessFunction("order_amount_owing", formatCurrency);
			addProcessFunction("payment_amount", formatCurrency);

		}


		function loadRelations(rel){
			if(!rel) var rel = relationListCopy;
			clearSelect();

			for(var i = 0 ; i < relationList.length; i++){
				var relationTmp = relationList[i];
				var relation = relationTmp.split(":")[0];
				var relationName = relationTmp.split(":")[1];
				addRelationToSelect(relation,relationName);
			}
		}



		function loadFields(rel,callback){
			if(!fieldList[rel]){
				var sel = $("#sel_relationList").get()[0];
				if(!sel) return;
				sel.disabled = true;

				$.get("crudmachine.php?action=list&rel=" + rel,(function(){
					return function(data){
						if(!data) return;
						var obj = eval(data);
						if(!obj) return;
						if(obj.message.type == "notification"){
							var fields = obj.message.content.split(",");
							var fieldsObj = {};
							for(var i = 0; i < fields.length; i++){
								fieldsObj[fields[i]] = true;
							}
							fieldList[rel] = fieldsObj;
							addFields(rel,fieldsObj);

							var sel = $("#sel_relationList").get()[0];
							if(!sel) return;
							sel.disabled = false;

							if(callback) {
								callback();
							}
						}
					};
				})(rel,callback));
			}
			else {
				addFields(rel,fieldList[rel]);
				callback();
			}
		}



		function addFields(rel,fields){
			var divFieldList = $("#div_fieldList").get()[0];
			if(!divFieldList) return;

			var divName = "div_fieldList_" + rel;
			var divRelFieldList = null;
			if(document.getElementById(divName)){
				divRelFieldList = document.getElementById(divName);
				divRelFieldList.innerHTML = "";
			} else {
				divRelFieldList = document.createElement("div");
				divRelFieldList.id = divName;
				divRelFieldList.className = "div_relFieldList";
				divFieldList.appendChild(divRelFieldList);
			}

			divRelFieldList.innerHTML = "<span>" + getAlias(rel) + ":</span>";
			divRelFieldList.rel = rel;


			// Now to add the fields to the field list
			for(var field in fields){
				var div = document.createElement("div");
				div.id = "div_" + field + "_" + rel + "_container";
				div.className = "div_field";
				div.title = getAlias(field);
				div.field = field;
				var checkBox = document.createElement("input");
				checkBox.setAttribute("type","checkbox");
				checkBox.id = "cb_" + field + "_" + rel;
				div.cbId = "cb_" + field + "_" + rel;
				div.appendChild(checkBox);
				if(isTicked(rel,field)){
					checkBox.checked = false;
				}
				checkBox.rel = rel;
				checkBox.field = field;
				checkBox.onclick = function(){
					setTick(this.rel,this.field,this.checked);
				};

				var aAddConstraint = document.createElement("a");
				aAddConstraint.href = "javascript:void(0)";
				aAddConstraint.rel = rel;
				aAddConstraint.field = field;
				aAddConstraint.onclick = function(){
					addConstraint(this.rel,this.field);
				};
				aAddConstraint.appendChild(document.createTextNode("+"));
				aAddConstraint.title = "Add a constraint for " + getAlias(field);
				div.appendChild(document.createTextNode(getAlias(field)));
				div.appendChild(document.createTextNode(" "));
				div.appendChild(aAddConstraint);

				divRelFieldList.appendChild(div);
			}
		}


		function isTicked(rel,field){
			if(fieldList[rel]){
				var fieldsObj = fieldList[rel];
				if(fieldsObj[field] == true) return true;
				return false;
			}

			return false;
		}

		function setTick(rel,field,val){
			window.status = "Trying to set " + rel + "[" + field + "] to: " + val;
			try{
				fieldList[rel][field] = val;
			} catch(m){
				alert(m);
			}
		}



		function getConstraintCount(rel,field){
			var i = 0;
			$("#div_constraintList .div_fieldConstraint").each(function(){
				if(this.id.match("div_" + rel + "_" + field + "_[0-9]+")){
					i++;
				}
			});
			return i;
		}

		function addConstraint(rel,field){
			var divConstraintList = $("#div_constraintList").get()[0];
			if(!divConstraintList) return;

			var divRelFieldConstraint = document.createElement("div");
			divRelFieldConstraint.id = "div_" + rel + "_" + field + "_" + getConstraintCount(rel,field);
			divRelFieldConstraint.className = "div_fieldConstraint";
			divRelFieldConstraint.rel = rel;
			divRelFieldConstraint.field = field;

			// Create the table
			var consObj = constructConstraintTable(rel,field);
			divRelFieldConstraint.appendChild(consObj.tbl);

			divRelFieldConstraint.sel = consObj.sel;
			divRelFieldConstraint.inp = consObj.inp;

			divConstraintList.appendChild(divRelFieldConstraint);

		}


		function constructConstraintTable(rel,field){
			var tbl = document.createElement("table");
			var tbody = document.createElement("tbody");
			tbl.appendChild(tbody);

			var tr = document.createElement("tr");

			var td = document.createElement("td");
			td.innerHTML = getAlias(field);
			td.style.width = "150px";
			tr.appendChild(td);


			// Deal with the select bar
			td = document.createElement("td");
			td.style.width = "150px";
			var sel = document.createElement("select");
			var option;

			option = document.createElement("option");
			option.value = "equal";
			option.appendChild(document.createTextNode("Equals"));
			sel.appendChild(option);
			sel.style.width = "100%";


			option = document.createElement("option");
			option.value = "lt";
			option.appendChild(document.createTextNode("Less Than"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "gt";
			option.appendChild(document.createTextNode("Greater Than"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "bd";
			option.appendChild(document.createTextNode("Before"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "ad";
			option.appendChild(document.createTextNode("After"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "sw";
			option.appendChild(document.createTextNode("Starts With"));
			sel.appendChild(option);


			option = document.createElement("option");
			option.value = "ew";
			option.appendChild(document.createTextNode("Ends With"));
			sel.appendChild(option);

			option = document.createElement("option");
			option.value = "customlike";
			option.appendChild(document.createTextNode("Like"));
			sel.appendChild(option);



			td.appendChild(sel);
			tr.appendChild(td);

			// Input field
			td = document.createElement("td");
			var inp = document.createElement("input");
			td.appendChild(inp);
			tr.appendChild(td);

			td = document.createElement("td");
			var aRemoveConstraint = document.createElement("a");
			aRemoveConstraint.href = "javascript:void(0)";
			aRemoveConstraint.rel = rel;
			aRemoveConstraint.field = field;
			aRemoveConstraint.appendChild(document.createTextNode("Remove"));
			aRemoveConstraint.cId = getConstraintCount(rel,field);
			aRemoveConstraint.onclick = function(){
				removeConstraint(this.rel,this.field,this.cId);
			};
			td.appendChild(aRemoveConstraint);
			tr.appendChild(td);


			tbody.appendChild(tr);
			return {"tbl":tbl,
			"sel":sel,
			"inp":inp
			};
		}

		function removeConstraint(rel,field,index){

			if(!field){
				$("#div_constraintList DIV").each(function(){
					if(this.rel == rel){
						this.parentNode.removeChild(this);
					}
				});

			} else {
				var id = "div_" + rel + "_" + field + "_" + index;
				var div = $("#" + id).get()[0];
				if(div) div.parentNode.removeChild(div);
			}
		}

		function allConstraintsValid(){
			$("#div_constraintList .div_fieldConstraint").each(function(){
				if(this.inp.value == "") return false;
			});
			return true;
		}

		function getConstraints(){
			var constraints = new Array();
			$("#div_constraintList .div_fieldConstraint").each(function(){
				var constraintType = getComponentValue(this.sel);
				var constraintValue = this.inp.value;
				var ct = constraintType;
				var op = "";
				var constraintStr = "";

				if(ct == "equal") op = "=";
				if(ct == "lt") op = "<";
				if(ct == "gt") op = ">";
				constraintStr = this.rel + "." + this.field + op + "'" + constraintValue + "'";

				if(ct == "bd") constraintStr = this.rel + "." + this.field + " < " + "'#" + constraintValue + "#'";
				if(ct == "ad") constraintStr = this.rel + "." + this.field + " > " + "'#" + constraintValue + "#'";


				if(ct == "sw") constraintStr = this.rel + "." + this.field + " LIKE " + "'" + constraintValue + "%'";
				if(ct == "ew") constraintStr = this.rel + "." + this.field + " LIKE " + "'%" + constraintValue + "'";
				if(ct == "customlike") constraintStr = this.rel + "." + this.field + " LIKE " + "'%" + constraintValue + "%'";

				constraintStr = this.rel + "." + this.field + "::" + constraintStr;
				
				if(constraintStr) constraints.push(constraintStr);

			});

			return constraints;

		}


		function clearSelect(){
			var sel = $("#sel_relationList").get()[0];
			if(!sel) return;
			while(sel.options.length > 0) sel.removeChild(sel.options[sel.selectedIndex]);
		}

		function addRelationToSelect(relation,caption){
			var sel = $("#sel_relationList").get()[0];
			if(!sel) return;

			var option = document.createElement("option");
			option.value = relation;
			option.appendChild(document.createTextNode(caption));
			sel.appendChild(option);
		}

		function getIndexOfRelation(rel){
			var sel = $("#sel_relationList").get()[0];
			for(var i = 0; i < sel.options.length; i++){
				if(sel.options[i].value == rel) return i;
			}

			return null;
		}

		function addSelectedRelation(index,callback) {
			if(!index) var index = -1;
			var sel = $("#sel_relationList").get()[0];
			var divRelList = $("#div_relationList").get()[0];

			if(!sel) return;
			if(!divRelList) return;


			if(index != -1) if( (index+1) > sel.options.length) return;



			if(sel.options.length > 0) {
				if(index == -1) index = sel.selectedIndex;
				var relation = sel.options[index].value;
				var relationName = sel.options[index].text;
				sel.removeChild(sel.options[index]);
				// Loop through array
				for(var i = 0; i < relationList.length; i++){
					if(relationList[i] == relation){
						relationList.splice(i,1);
					}
				}
				divRelList.appendChild(constructRelationDiv(relation,relationName));
				loadFields(relation,callback);
			} else {
			}
		}

		function constructRelationDiv(relation,caption){
			var div = document.createElement("div");
			div.id = "div_relation_" + relation;
			div.className = "div_relation";
			div.rel = relation;
			div.caption = caption;
			var a = document.createElement("a");
			a.innerHTML = "Remove";
			a.relation = relation;
			a.caption = caption;
			a.href = "javascript:void(0)";
			a.onclick = function(){
				removeRelation(this.relation,this.caption);
			};

			div.innerHTML = caption + "&nbsp;&nbsp;";
			div.appendChild(a);

			return div;
		}

		function removeRelation(relation,caption){
			//			alert("Asked to remove: " + relation + " and " + caption);
			relationList.push(relation + ":" + caption);
			var divRelList = $("#div_relationList").get()[0];
			divRelList.removeChild($("#div_relation_" + relation).get()[0]);
			addRelationToSelect(relation,caption);
			// Remove from list of fields
			var divRelFieldList = $("#div_fieldList_" + relation).get()[0];
			if(divRelFieldList) divRelFieldList.parentNode.removeChild(divRelFieldList);

			removeConstraint(relation);
		}

		function removeAllRelations() {
			$("#div_relationList DIV").each(function(){
				removeRelation(this.rel,this.caption);
			});

		}



		function generateReport(){
			$("#btn_generateReport").get()[0].disabled = true;
			$("#btn_runCustomReport").get()[0].disabled = true;

			var relations = new Array();
			var fields = new Array();

			$("#div_relationList DIV").each(function(){
				relations.push(this.rel) ;
			});


			$("#div_fieldList .div_relFieldList").each(function(){
				var relation = this.rel;
				$("#" + this.id + " .div_field").each(function(){
					var cb = $("#" + this.cbId).get()[0];
					if(cb.checked){
						fields.push(relation + "." + cb.field);
					}
				});
			});

			if(relations.length > 0){
				var relationStr = relations.join(",");
				var fieldStr = fields.join(",");
				var url = "crudmachine.php?action=read&rel=" + relationStr + "&cl=" + fieldStr + "&p=j";

				// Grab all the constraints
				if(!allConstraintsValid()){
					alert("All constraints are not valid!");
					$("#btn_generateReport").get()[0].disabled = false;
					$("#btn_runCustomReport").get()[0].disabled = false;
					return;
				}

				var constraints = getConstraints();
				if(constraints.length > 0){
					url += "&sc=" + constraints.join(":::");
				}


//				alert(url);
				$.get(url,function(data){
					$("#btn_generateReport").get()[0].disabled = false;
					$("#btn_runCustomReport").get()[0].disabled = false;

					if(!data) { alert("No data returned, sorry"); return; }

					var obj = eval(data);
					for(var item in obj){
						// should be the first item
						var listData = obj[item];
						// should be an array
						printData(listData);
						break;
					}
				});

			} else {
				alert("Nothing to report on!");
				$("#btn_generateReport").get()[0].disabled = false;
				$("#btn_runCustomReport").get()[0].disabled = false;

			}
		}

		var customReports = {};

		function generateCustomReport(relationArr,fieldArr){
			$("#btn_runCustomReport").get()[0].disabled = true;

			removeAllRelations();

			var relCount = relationArr.length;
			var relationIndices = {};
			var fieldCount = fieldArr.length;

			var d = new Date();
			var utc = Date.UTC(d.getFullYear(),d.getMonth(),d.getDate());
			customReports[utc] = {
			"completed":0,
			"total":relCount,
			"fields":fieldArr,
			"relations":relationArr
			};


			for(var i = 0; i < relCount; i++){
				var relation = relationArr[i];
				var index = getIndexOfRelation(relation);
				addSelectedRelation(index,(function(){
					return function(){
						customReports[utc]["completed"] = customReports[utc]["completed"] + 1;

						if(customReports[utc]["completed"] == customReports[utc]["total"]) {


							var fields = customReports[utc]["fields"];
							var relations = customReports[utc]["relations"];

							for(var r = 0; r < relations.length; r++){
								var relation = relations[r];
								// Uncheck all first
								var cssStr = "#div_fieldList_" + relation + " INPUT";
								//								alert(cssStr);
								$(cssStr).each(function(){
									if(this.type == "checkbox") this.checked = false;
								});
							}

							for(var j = 0; j < fields.length; j++){
								var field = fields[j];
								var fieldName = field.split(".")[1];
								var relation = field.split(".")[0];
								var cbId = "cb_" + fieldName + "_" + relation;
								var cb = $("#" + cbId).get()[0];
								if(cb) cb.checked = true;
							}

							generateReport();
						}

					};
				})(utc));
			}
		}

		function printData(arr){ // takes an array of objects
			if(arr.length == 0){
				alert("No results found for your query, sorry.");
				return;
			}

			var divReport = $("#div_report").get()[0];
			if(divReport) {
				divReport.innerHTML = "";

				var tbl = document.createElement("table");
				tbl.setAttribute("cellpadding",5);
				tbl.setAttribute("cellspacing",0);

				var tbody = document.createElement("tbody");

				tbl.appendChild(tbody);

				// Get the first row
				var row = arr[0];
				var tr = document.createElement("tr");
				tr.className = "headerRow";
				for(var item in row){
					var td = document.createElement("td");
					td.innerHTML = getAlias(item);
					tr.appendChild(td);
				}
				tbody.appendChild(tr);

				// Now for the actual rows ...
				for(var i = 0; i < arr.length; i++){
					var tr = document.createElement("tr");
					if(parseInt(i/2) == (i/2)){
						tr.className = "evenRow";
					} else {
						tr.className = "oddRow";
					}
					var row = arr[i];
					for(var item in row){
						var td = document.createElement("td");
						td.innerHTML = processVar(item,row[item]);
						tr.appendChild(td);
					}
					tbody.appendChild(tr);
				}

				divReport.appendChild(tbl);
			}
		}

		function loadSavedReports(){
			var selSavedReportList = $("#sel_savedReports").get()[0];
			if(!selSavedReportList) return;

			for(var item in savedReports){
				var savedReport = savedReports[item];
				var option = document.createElement("option");
				option.value = item;
				var title = (savedReport["title"]);
				option.appendChild(document.createTextNode(title));
				selSavedReportList.appendChild(option);
			}


			showReport(selSavedReportList.options[selSavedReportList.selectedIndex].value);
		}

		function showReport(val){
			var divReportDescription = $("#div_reportDescription");
			if(divReportDescription){
				var description = savedReports[val]["description"];
				divReportDescription.innerHTML = description;
			}
		}

		function runCustomReport(){
			var selSavedReportList = $("#sel_savedReports").get()[0];
			var val = selSavedReportList.options[selSavedReportList.selectedIndex].value;
			if(savedReports[val]){
				var savedReport = savedReports[val];
				//				describe(savedReport);
				generateCustomReport(savedReport.relations,savedReport.fields);
			}
		}

		function getComponentValue(obj, proc) {
			if(!obj) return null;
			var val = obj.type == "select" ? obj.options[obj.selectedIndex].value
			: obj.value;

			if (proc) {
				var item = grabColumnName(obj.id);
				val = process(item, val);
			}

			return val;
		}

		</script>
	</head>
<body onLoad="loadRelations(); report_init(); loadSavedReports();">
<div id="div_customizeReport">

<select id="sel_relationList">
</select>
<a href="javascript:void(0);" onclick="addSelectedRelation();">Add Relation</a>

<div id="div_relationList">
<b>Relation List:</b><br>
</div>

<div id="div_fieldList">
<b>List of Fields:</b><br>
</div>

<div id="div_constraintList">
<b>List of Constraints:</b><br>

</div>
<br>
<input type="button" value="Generate Report" onclick="generateReport();" id="btn_generateReport"/>
<br>
<a href="javascript:void(0);" onclick="$('#div_customizeReport').hide('slow',function(){$('#btn_runCustomReport').get()[0].disabled = false;});" style="color:white; font-weight:bold;">Close Customization Window</a>

</div>



Pick a saved report format from below or click <a href="javascript:void(0);" onclick="$('#div_customizeReport').show('slow',function(){$('#btn_runCustomReport').get()[0].disabled = true;}); " style="font-weight:bold;">here</a> to 
customize the report.
<br>
<select id="sel_savedReports" style="width:150px;" onchange="showReport(this.options[this.selectedIndex].value);">
</select>&nbsp;&nbsp;<input type="button" value="Run Custom Report" onClick="runCustomReport()" id="btn_runCustomReport"/><br>
<div id="div_reportDescription">

</div>

<div id="div_report" style="float:none;">
</div>

</body>
</html>
