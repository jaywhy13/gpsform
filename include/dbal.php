<?
/* This is the DBAL - Database Abstraction Layer. Use this class to abstract away the details of database. Just use the xml file in "../"
* and your connection should work. The query string debug=on will test your connection. Use that if you're checking connections and
* passwords before going full time wit it ehh :D
*
* Usage:
* Configure the xml file located on dir up... or change "$configFile" if it's located else where
*
*
* $conn = new DbAbstractionLayer(); // it's a lil long... I kno... sorry
* $conn->connect(); // connect to the database
* $conn->queryDb($sql) // queries the database and returns the result or dies with mysql_error() or pg_last_error()....
*
* Only postgres and mysql connections are supported just now.
*
* DTD for the xml file shown below: (stored in dbconn.dtd) (one dir up by default)
* <!ELEMENT db-connection (username,password?,hostname,database?,port?)>
* <!ELEMENT username (#PCDATA)>
* <!ELEMENT password (#PCDATA)>
* <!ELEMENT hostname (#PCDATA)>
* <!ELEMENT database (#PCDATA)>
* <!ELEMENT port (#PCDATA)>
* <!ATTLIST db-connection conntype (mysql|pgsql) "mysql">
*
*
* TODO: Put in a close connection function
*
* Updates:
* connect() function now returns the connection object
*
*
*
*
*
* Jay Why (c) 2007-2008
*
*/

include_once("varlib.php");


class DbAbstractionLayer {

	public $MYSQLCONN = "mysql";
	public $PSQLCONN = "pgsql";
	private $connectionType = null;
	private $username = null;
	private $dbname = null;
	private $hostname = null;
	private $password = null;
	private $conn = null;
	private $configFile = "";
	private $connected = false;
	private $debugMode = false;

	function __construct(){
		$this->configFile = getParam("dbconfigfile");
	}

	public function setConnectionType($connType){
		$this->connectionType = $connType;
	}

	public function getConnectionType(){
		return $this->connectionType;
	}

	public function loadConfig($fileLocation){
		$this->configFile = $fileLocation;
	}

	public function isConnected(){
		return $this->connected;
	}

	public function connect(){

		if(file_exists($this->configFile)){
			$doc = new DOMDocument();
			$doc->load($this->configFile);
			if(!$doc->validate())
			throw Exception("Doc does NOT validate!");



			$dbconnTag = $doc->getElementsByTagName("db-connection")->item(0);
			$connType =  $dbconnTag->getAttribute("conntype");


			if(isset($connType)){
				$this->connectionType = $connType;
			}

			if($this->connectionType){
				for($i = 0; $i < $dbconnTag->childNodes->length; $i++){
					$child = $dbconnTag->childNodes->item($i);
					if($child->nodeName == "username"){
						$this->username = $child->childNodes->item(0)->nodeValue; //grab the username
						//echo "username=$this->username"; echo "<br>";
					}

					if($child->nodeName == "password"){
						$this->password = $child->childNodes->item(0)->nodeValue; //grab the username
						//						echo "password=$password"; echo "<br>";
					}
					if($child->nodeName == "hostname"){
						$this->hostname = $child->childNodes->item(0)->nodeValue; //grab the username
						//						echo "hostname=$this->hostname";
						//						echo "<br>";
					}
					if($child->nodeName == "database"){
						$this->dbname = $child->childNodes->item(0)->nodeValue; //grab the username
						//						echo "database=$this->dbname";
						//						echo "<br>";
					}
				}

				if(isset($this->username)){
					//Attempt connection to the db....
					$conn = $this->connectDb($this->connectionType);
					$this->password = null; // remove password
					//										echo "Conn successful!";
					// return the connection
					return $conn;
				} else {
					//					echo "Username not set";
					return null;
				}
			} else {
				echo "No connection specified!";
				return null;
			}
		}
		else {
			throw new Exception("Config file does NOT exist at given location: " . $this->configFile . ". Working directory is: " . exec("pwd"));
		}
	}


	function queryDb($stmt){
		if($this->debugMode == true){
			echo "SQL: " . $stmt;
		}

		if($this->connectionType == $this->MYSQLCONN){
			$result = mysql_query($stmt);
			return $result;
		} else {
			$result = pg_query($stmt);
			return $result;
		}
	}
	
	

	function fetchRows($stmt){
		$result = $this->queryDb($stmt);
		if($result){
			if($this->connectionType == $this->MYSQLCONN){
				$arr = mysql_fetch_array($result);
				return $arr;
			} else if($this->connectionType == $this->PSQLCONN){
				$arr = pg_fetch_array($result);
				return $arr;
			} else {
				return null;
			}
		}
	}


	function connectDb($connType){
		if($connType == $this->MYSQLCONN){
			$conn = mysql_connect($this->hostname,$this->username,$this->password) or die("Could not connect... " . mysql_error());
			return $conn;
		} else {
//			echo("host=$this->hostname dbname=$this->dbname user=$this->username password=$this->password");
			return pg_connect("host=$this->hostname dbname=$this->dbname user=$this->username password=$this->password");
		}
	}


	function getErrorMsg(){
		if($this->connectionType == $this->MYSQLCONN){
			return mysql_error();
		} else {
			return pg_errormessage();
		}
	}
	
	function loopResult($result){
		if($this->connectionType == $this->MYSQLCONN){
			return mysql_fetch_array($result);	
		} else {
			return pg_fetch_array($result);
		}
	}
} // end of class


// Debug mode.... creates an instance of the class and clicks connect :)
/*if($_GET["debug"] == "on"){
$dbtest = new DbAbstractionLayer();
try {
$dbtest->connect(); }
catch(Exception $e){
echo $e->getMessage();
}
}
*/
?>
