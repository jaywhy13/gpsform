<?php
include_once ("varlib.php");
include_once ("db.php");
include_once ("permguru.php");
include_once ("standards.php");
include_once ("logger.php");

///CRUD
define ( "C", 0 );
define ( "R", 1 );
define ( "U", 2 );
define ( "D", 3 );

///SUCCESS
define ( "SUCC", 0 );
///ERRORS
define ( "ERR_UNIQUE", - 1 );
define ( "ERR_DB", - 2 );
define ( "ERR_PERM", - 3 );
define ( "ERR_VALID", - 4 );

// MESSAGES
define ( "MSG_PERMERROR", "Insufficient Permissions" );


// LOG TAGS
define ("TAGUSER","USER");
define ("TAGSURVEY","SURVEY");
define ("TAGDRILLHOLE","DRILLHOLE");
define ("TAGPERMISSION","PERMISSION");
define ("TAGLAB","LAB");
define ("TAGGROUP","GROUP");
define ("TAGAREA", "AREA");
define ("TAGDEPOSIT" , "DEPOSIT");
define ("TAGSAMPLE", "SAMPLE");
define ("TAGLABRESULT","ANALYSIS");
define ("TAGCHEMICAL","CHEMICAL");

// Try and set the tag
$tags = array();
$tags["drill_hole"] = TAGDRILLHOLE;
$tags["usr"] = TAGUSER;
$tags["survey"] = TAGSURVEY;
$tags["rule"] = TAGPERMISSION;
$tags["perm_type"] = TAGPERMISSION;
$tags["grp"] = TAGGROUP;
$tags["chemical"] = TAGCHEMICAL;
$tags["lab"] = TAGLAB;
$tags["sample_result"] = TAGLABRESULT;
$tags["area"] = TAGAREA;
$tags["sample"] = TAGSAMPLE;

function guessTag($query){
	$tag = "MISC";
	$q = strtolower($query);
	
	if(empty($q)) return $tag;
	
	foreach($GLOBALS['tags'] as $key=>$value){
		if(ereg("into $key",$q) || ereg("delete from $key",$q) || ereg("update $key",$q)) {
			$tag = $value;
			break;
		}
	}
	return $tag;
}




class Controller {
	function __construct() {
		$this->db = getDBInstance ( "pg", getParam ( "dbconfigfile-psql" ) );
		$this->pg = new PermGuru ( "gpsform" );
		$this->po = $this->pg->getPermissionBot ();
		$this->l = new Logger ( $this->pg );
		$this->verbose = true;
	}

	public function isVerbose() {
		return $this->verbose;
	}

	public function setVerbose($val) {
		$this->verbose = $val;
	}

	// Logs activity on the table
	function logTableActivity($q){
		$query = strtolower($q);
		$clause = "";
		if(ereg("insert into [a-z_0-9]+ ",$query)){
			
		} else if(ereg("delete from [a-z_0-9]+ ",$query)){
			
		} else if(ereg("update [a-z_0-9]+",$query)){
			
		}
	}


	public function updateValue($tbl, $id, $idVal, $param, $val) {
		$this->checkValidity ( $tbl, array ($param, $id ), array ($val, $idVal ) );

		$q = "UPDATE $tbl set $param = '$val' where $id = '$idVal'";

		$this->executeQuery ( $q, "Value Updated!", "DB Update Failed" );
	}

	public function eraseValue($tbl, $id, $idVal, $param, $val) {
		$this->updateValue ( $tbl, $id, $param, NULL );
	}

	public function delete($tbl, $id, $idVal) {
		$this->checkValidity ( $tbl, array ($id ), array ($idVal ) );

		$q = "DELETE from $tbl where $id = '$idVal'";

		$this->executeQuery ( $q, "Delete Successful!", "DB Delete Failed" );
	}

	/**********************************************************Area*************************************************************/

	public function createArea($area_id, $old_area_id, $area_name) {
		$this->decideAndContinue ( C, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "old_area_id", "area_name" ), array ($area_id, $old_area_id, $area_name ) );

		$this->ensureUnique ( "area", "area_id", $area_id );

		$q = "INSERT into area(area_id, old_area_id, area_name) values ('$area_id', '$old_area_id', '$area_name')";

		return $this->executeQuery ( $q, "Area Created! [Data $area_id]", "DB Create Area Failed! [Data $area_id]" );
	}

	public function updateArea($area_id, $old_area_id, $area_name) {
		$this->decideAndContinue ( U, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "old_area_id", "area_name" ), array ($area_id, $old_area_id, $area_name ) );

		$q = "UPDATE area SET old_area_id = '$old_area_id', area_name = '$area_name' WHERE area_id = '$area_id'";

		return $this->executeQuery ( $q, "Area Updated! [Data $area_id]", "DB Update Area Failed! [Data $area_id]" );
	}

	public function deleteArea($area_id) {
		$this->decideAndContinue ( D, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id" ), array ($area_id ) );

		$q = "DELETE from area WHERE area_id = '$area_id'";

		return $this->executeQuery ( $q, "Area Deleted! [Data $area_id]", "DB Delete Area Failed! [Data $area_id]" );
	}

	public function updateAreaIdByAreaName($area_id, $area_name) {
		//$this->decideAndContinue(U, AREAID, "Insufficient Permissions!");
		$this->decideAndContinue ( U, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "area_name" ), array ($area_id, $area_name ) );

		$this->ensureUnique ( "area", "area_id", $area_id );

		$q = "UPDATE area SET area_id = '$area_id' WHERE area_name = '$area_name'";

		return $this->executeQuery ( $q, "Area ID Updated! [Data $area_id] ", "DB Update Area ID Failed! [Data $area_id]" );
	}

	public function updateAreaIdByOldAreaId($area_id, $old_area_id) {
		//$this->decideAndContinue(U, AREAID, "Insufficient Permissions!");


		$this->decideAndContinue ( U, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "old_area_id" ), array ($area_id, $old_area_id ) );

		$this->ensureUnique ( "area", "area_id", $area_id );

		$q = "UPDATE area SET area_id = '$area_id' WHERE old_area_id = '$old_area_id'";

		return $this->executeQuery ( $q, "Area ID Updated! [Data $area_id]", "DB Update Area ID Failed! [Data $area_id]" );
	}

	public function deleteAreaIdByAreaName($area_name) {
		//$this->decideAndContinue(D, AREAID, "Insufficient Permissions!");
		$this->decideAndContinue ( D, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_name" ), array ($area_name ) );

		return $this->updateAreaIdByAreaName ( "", $area_name );
	}

	public function deleteAreaIdByOldAreaId($old_area_id) {
		//$this->decideAndContinue(D, AREAID, "Insufficient Permissions!");
		$this->decideAndContinue ( D, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("old_area_id" ), array ($old_area_id ) );

		return $this->updateAreaIdByOldAreaId ( "", $old_area_id );
	}

	public function updateAreaNameByAreaId($area_name, $area_id) {
		//$this->decideAndContinue(U, AREANAME, "Insufficient Permissions!");


		$this->decideAndContinue ( U, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "area_name" ), array ($area_id, $area_name ) );

		$q = "UPDATE area SET area_name = '$area_name' WHERE area_id = '$area_id'";

		return $this->executeQuery ( $q, "Area Name Updated! [Data $area_id - $area_name]", "DB Update Area Name Failed! [Data $area_id - $area_name]" );
	}

	public function updateAreaNameByOldAreaId($area_name, $old_area_id) {
		//$this->decideAndContinue(U, AREANAME, "Insufficient Permissions!");


		$this->decideAndContinue ( U, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id", "old_area_id" ), array ($area_id, $old_area_id ) );

		$q = "UPDATE area SET area_name = '$area_name' WHERE old_area_id = '$old_area_id'";

		return $this->executeQuery ( $q, "Area ID Updated! [Data $area_id]", "DB Update Area ID Failed! [Data $area_id]" );
	}

	public function deleteAreaNameByAreaId($area_id) {
		//$this->decideAndContinue(D, AREANAME, "Insufficient Permissions!");


		$this->decideAndContinue ( D, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("area_id" ), array ($area_id ) );

		return $this->updateAreaNameByAreaId ( "", area_id );
	}

	public function deleteAreaNameByOldAreaId($old_area_id) {
		//$this->decideAndContinue(D, AREANAME, "Insufficient Permissions!");
		$this->decideAndContinue ( D, PERM_AREA, "Insufficient Permissions!" );

		$this->checkValidity ( "area", array ("old_area_id" ), array ($old_area_id ) );

		return $this->updateAreaNameByOldAreaId ( "", $old_area_id );
	}

	/***for all read functions, should check permissions first ****/

	public function readArea($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_AREA, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "area", "area_name", $arg );
		} else {
			$q = $this->specifyGenQuery ( "area", "area_name", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "areas", "area" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Area Successful! ", "Read Area Failed!", FALSE ) );
	}
	/************************************************************************************************************************/

	/**************************************** Deposit Table Routines *********************************************************/

	public function createDeposit($deposit_id, $old_deposit_id, $area_id) {
		$this->decideAndContinue ( C, PERM_DEPOSIT, "Insufficient Permissions!" );

		$this->checkValidity ( "deposit", array ("deposit_id", "old_deposit_id", "area_id" ), array ($deposit_id, $old_deposit_id, $area_id ) );

		$this->ensureUnique ( "deposit", array ("deposit_id" => $deposit_id, "area_id" => $area_id ) );

		$q = "INSERT into deposit(deposit_id, old_deposit_id, area_id) values ('$deposit_id', '$old_deposit_id', '$area_id')";

		return $this->executeQuery ( $q, "Deposit Created! [Data $area_id $deposit_id]", "DB Create Deposit Failed! [Data $area_id $deposit_id]" );
	}

	public function updateDeposit($deposit_id, $old_deposit_id, $area_id) {
		$this->decideAndContinue ( U, PERM_DEPOSIT, "Insufficient Permissions!" );

		$this->checkValidity ( "deposit", array ("deposit_id", "old_deposit_id", "area_id" ), array ($deposit_id, $old_deposit_id, $area_id ) );

		$q = "UPDATE deposit SET old_deposit_id = '$old_deposit_id', area_id = '$area_id' WHERE deposit_id = '$deposit_id'";

		return $this->executeQuery ( $q, "Deposit Updated! [Data $area_id $deposit_id]", "DB Update Deposit Failed! [Data $area_id $deposit_id]" );
	}

	public function deleteDeposit($area_id,$deposit_id) {
		$this->decideAndContinue ( D, PERM_DEPOSIT, "Insufficient Permissions!" );

		$this->checkValidity ( "deposit", array ("deposit_id" ), array ($deposit_id ) );

		$q = "DELETE from deposit WHERE deposit_id = '$deposit_id' AND area_id = '$area_id'";

		return $this->executeQuery ( $q, "Deposit Deleted! [Data $area_id $deposit_id]", "DB Delete Deposit Failed! [Data $area_id $deposit_id]" );
	}

	public function readDeposit($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_DEPOSIT, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "deposit", "deposit_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( "deposit", "deposit_id", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "deposits", "deposit" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Deposit Successful!", "Read Deposit Failed!", FALSE ) );
	}

	/************************************************************Drill Holes ***********************************************************/

	public function createDrillHole($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev, $dip, $azimuth, $depth, $date_started, $date_ended, $comment = '', $drill_unit_no, $crew_id) {

		$this->decideAndContinue ( C, PERM_DRILLHOLE, "Insufficient Permissions!" );

		if (! $this->exists ( "crew", "crew_id", "$crew_id" )) {
			die ( "error:" . ERR_VALID . ":The crew supplied does not exist!" );
		}

		if (! $this->exists ( "drill_machine", "drill_unit_no", $drill_unit_no )) {
			die ( "error:" . ERR_VALID . ":The drill machine you chose does not exist!" );
		}

		// JM Once again....  removed survey coords from this insertion
		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "gps_east", "gps_north", "gps_elev", "dip", "azimuth", "depth", "date_started", "date_ended", "drill_unit_no" ), array ($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev, $dip, $azimuth, $depth, $date_started, $date_ended, $drill_unit_no ) );

		$this->ensureUnique ( "drill_hole", array ("drill_hole_no" => $drill_hole_no, "deposit_id" => $deposit_id, "area_id" => $area_id ) );

		// JM Repeated here... removed survey coords from this insertion, ALSO removed drill_unit_no from it as well
		$q = "INSERT into drill_hole(drill_hole_no, deposit_id, area_id, gps_east, gps_north, gps_elev,
							dip, azimuth, depth, date_started, date_ended, comment,drill_unit_no,crew_id) values ('$drill_hole_no', '$deposit_id', '$area_id', $gps_east, $gps_north, $gps_elev, $dip, $azimuth, $depth, '$date_started', '$date_ended','$comment',$drill_unit_no,'$crew_id')";

		//		echo $q;
		$result = $this->executeQuery ( $q, "Drill Hole Created! [Data $area_id $deposit_id $drill_hole_no]", "DB Drill Hole Query  Failed! [Data $area_id $deposit_id $drill_hole_no]" );

		return $result;
	}

	// Removed all references to survey related coords from this function
	public function updateDrillHole($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev, $dip, $azimuth, $depth, $date_started, $date_ended, $comment = '', $drill_unit_no, $crew_id) {
		$this->decideAndContinue ( U, PERM_DRILLHOLE, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "gps_east", "gps_north", "gps_elev", "dip", "azimuth", "depth", "date_started", "date_ended" ), array ($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev, $dip, $azimuth, $depth, $date_started, $date_ended/*, $drill_unit_no*/) );

		if (! $this->exists ( "crew", "crew_id", "$crew_id" )) {
			die ( "error:" . ERR_VALID . ":The crew supplied does not exist!" );
		}

		if (! $this->exists ( "drill_machine", "drill_unit_no", $drill_unit_no )) {
			die ( "error:" . ERR_VALID . ":The drill machine you chose does not exist!" );
		}

		$q = "UPDATE drill_hole SET gps_east = $gps_east, gps_north = $gps_north, gps_elev = $gps_elev, dip = $dip, azimuth = $azimuth, depth = $depth, date_started = '$date_started', date_ended = '$date_ended', comment = '$comment', drill_unit_no = $drill_unit_no, crew_id = '$crew_id' WHERE drill_hole_no = '$drill_hole_no' and deposit_id = '$deposit_id' and area_id = '$area_id'";

		return $this->executeQuery ( $q, "Drill Hole Updated! [Data $area_id $deposit_id $drill_hole_no]", "DB Update Drill Hole Query Failed! [Data $area_id $deposit_id $drill_hole_no]" );
	}

	public function deleteDrillHole($drill_hole_no, $deposit_id, $area_id) {
		$this->decideAndContinue ( D, PERM_DRILLHOLE, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id" ), array ($drill_hole_no, $deposit_id, $area_id ) );

		$q = "DELETE from drill_hole WHERE drill_hole_no = '$drill_hole_no' and deposit_id = '$deposit_id' and area_id = '$area_id'";

		return $this->executeQuery ( $q, "Drill Hole Deleted! [Data $area_id $deposit_id $drill_hole_no]", "DB Delete Drill Hole Query Failed! [Data $area_id $deposit_id $drill_hole_no]" );
	}

	public function readDrillHole($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_DRILLHOLE, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "drill_hole", "drill_hole_no", $arg );
		} else {
			$q = $this->specifyGenQuery ( "drill_hole", "drill_hole_no", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "drill_holes", "drill_hole" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Drill Hole Successful!", "Read Drill Hole Failed!", FALSE ) );
	}
	/***********************************************************************************************************************************/

	/****************************************************Survery Coords*****************************************************************/
	public function createSurveyCoords($drill_hole_no, $deposit_id, $area_id, $survey_east, $survey_north, $survey_elev) {
		$this->decideAndContinue ( C, PERM_SURVEYCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$survey_east", "$survey_north", "$survey_elev" ), array ($drill_hole_no, $deposit_id, $area_id, $survey_east, $survey_north, $survey_elev ) );

		return $this->updateSurveyCoords ( $drill_hole_no, $deposit_id, $area_id, $survey_east, $survey_north, $survey_elev );
	}

	public function updateSurveyCoords($drill_hole_no, $deposit_id, $area_id, $survey_east, $survey_north, $survey_elev) {
		$this->decideAndContinue ( U, PERM_SURVEYCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$survey_east", "$survey_north", "$survey_elev" ), array ($drill_hole_no, $deposit_id, $area_id, $survey_east, $survey_north, $survey_elev ) );

		$q = "UPDATE drill_hole SET survey_east = $survey_east, survey_north = $survey_north, survey_elev = $survey_elev
			  WHERE drill_hole_no = '$drill_hole_no' and deposit_id = '$deposit_id' and area_id = '$area_id'";
		//		echo $q;
		return $this->executeQuery ( $q, "Survey Coordinates Updated! [Data $area_id $deposit_id $drill_hole_no]", "DB Update Survery Coordinates Failed! [Data $area_id $deposit_id $drill_hole_no]" );
	}

	public function deleteSurveyCoords($drill_hole_no, $deposit_id, $area_id) {
		$this->decideAndContinue ( D, PERM_SURVEYCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$survey_east", "$survey_north", "$survey_elev" ), array ($drill_hole_no, $deposit_id, $area_id, NULL, NULL, NULL ) );

		return $this->updateSurveyCoords ( $drill_hole_no, $deposit_id, $area_id, NULL, NULL, NULL );
	}
	/***********************************************************************************************************************/

	/*******************************************************GPS COORDS*******************************************************/
	public function createGPSCoords($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev) {
		$this->decideAndContinue ( C, PERM_GPSCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$gps_east", "$gps_north", "$gps_elev" ), array ($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev ) );

		return $this->updateGPSCoords ( $drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev );
	}

	public function updateGPSCoords($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev) {
		$this->decideAndContinue ( U, PERM_GPSCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$gps_east", "$gps_north", "$gps_elev" ), array ($drill_hole_no, $deposit_id, $area_id, $gps_east, $gps_north, $gps_elev ) );

		$q = "UPDATE drill_hole SET gps_east = $gps_east, gps_north = $gps_north, gps_elev = $gps_elev
			  WHERE drill_hole_no = '$drill_hole_no' and deposit_id = '$deposit_id' and area_id = '$area_id'";

		return $this->executeQuery ( $q, "GPS Coordinates Updated! [Data $area_id $deposit_id $drill_hole_no]", "DB Update Survery Coordinates Failed! [Data $area_id $deposit_id $drill_hole_no]" );
	}

	public function deleteGPSCoords($drill_hole_no, $deposit_id, $area_id) {
		$this->decideAndContinue ( D, PERM_GPSCOORDS, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_hole", array ("drill_hole_no", "deposit_id", "area_id", "$gps_east", "$gps_north", "$gps_elev" ), array ($drill_hole_no, $deposit_id, $area_id, NULL, NULL, NULL ) );

		return $this->updateGPSCoords ( $drill_hole_no, $deposit_id, $area_id, NULL, NULL, NULL );
	}
	/************************************************************************************************************************************/

	/**************************************** Samples Table Routines *******************************************************************/
	public function createSample($drill_hole_no, $sample_type, $interval_from, $interval_to, $sample_no, $comments, $deposit_id, $area_id) {
		$this->decideAndContinue ( C, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_info", array ("drill_hole_no", "sample_type", "interval_from", "interval_to", "sample_no", "comments", "deposit_id", "area_id" ), array ($drill_hole_no, $sample_type, $interval_from, $interval_to, $sample_no, $comments, $deposit_id, $area_id ) );

		$this->ensureUnique ( "sample_info", array ("drill_hole_no" => $drill_hole_no, "deposit_id" => $deposit_id, "area_id" => $area_id, "sample_no" => $sample_no, "sample_type" => $sample_type ) );

		$q = "INSERT into sample_info(drill_hole_no, sample_type, interval_from, interval_to, sample_no, comments, deposit_id, area_id)
				values('$drill_hole_no', '$sample_type', $interval_from, $interval_to, '$sample_no', '$comments', '$deposit_id', '$area_id'); ";

		return $this->executeQuery ( $q, "Sample Created! [Data $area_id $deposit_id $drill_hole_no $sample_type $sample_no]", "DB Create Sample Query Failed! [Data $area_id $deposit_id $drill_hole_no $sample_type $sample_no]" );
	}

	public function updateSample($drill_hole_no, $sample_type, $interval_from, $interval_to, $sample_no, $comments, $deposit_id, $area_id) {

		$this->decideAndContinue ( U, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_info", array ("drill_hole_no", "sample_type", "interval_from", "interval_to", "sample_no", "comments", "deposit_id", "area_id" ), array ($drill_hole_no, $sample_type, $interval_from, $interval_to, $sample_no, $comments, $deposit_id, $area_id ) );

		// $this->ensureUnique ( "sample_info", array ("drill_hole_no" => $drill_hole_no, "deposit_id" => $deposit_id, "area_id" => $area_id,  "sample_no" => $sample_no  ) );


		$q = "UPDATE sample_info SET interval_from = $interval_from, interval_to = $interval_to, comments = '$comments' WHERE sample_type = '$sample_type' AND drill_hole_no = '$drill_hole_no' AND deposit_id = '$deposit_id' AND area_id = '$area_id' AND sample_no = '$sample_no'";

		return $this->executeQuery ( $q, "Sample Updated! [Data $area_id $deposit_id $drill_hole_no $sample_type $sample_no]", "DB Update Sample Query Failed! [Data $area_id $deposit_id $drill_hole_no $sample_type $sample_no]" );
	}

	public function deleteSample($sample_no, $area_id, $deposit_id, $drill_hole_no) {
		$this->decideAndContinue ( D, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_info", array ("drill_hole_no", "sample_no", "deposit_id", "area_id" ), array ($drill_hole_no, $sample_no, $deposit_id, $area_id ) );

		//		$this->ensureUnique ( "sample_info", array ("drill_hole_no" => $drill_hole_no, "deposit_id" => $deposit_id, "area_id" => $area_id, "sample_no" => $sample_no ) );

		$q = "DELETE from sample_info WHERE sample_no = '$sample_no'";

		return $this->executeQuery ( $q, "Sample Deleted! [Data $area_id $deposit_id $drill_hole_no $sample_no]", "DB Delete Sample Query Failed! [Data $area_id $deposit_id $drill_hole_no $sample_no]" );
	}

	public function readSample($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "sample_info", "area_id,deposit_id,drill_hole_no,sample_no", $arg );
		} else {
			$q = $this->specifyGenQuery ( "sample_info", "area_id,deposit_id,drill_hole_no,sample_no", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "samples", "sample" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Sample Successful!", "Read Sample Failed!", FALSE ) );
	}

	// createSampleResult($area_id,$deposit_id,$drill_hole_no,$lab_id,$sample_no, $chem_id, $amount,$reading_status='');
	public function createSampleResult($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $chem_id, $amount, $reading_status = 'valid', $batch_id = 1) {
		$this->decideAndContinue ( C, PERM_LABSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_result", array ("area_id", "deposit_id", "drill_hole_no", "lab_id", "sample_no", "chem_id", "amount", "reading_status", "batch_id" ), array ($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $chem_id, $amount, $reading_status, $batch_id ) );

		$this->ensureUnique ( "sample_result", array ("area_id" => $area_id, "deposit_id" => $deposit_id, "drill_hole_no" => $drill_hole_no, "lab_id" => $lab_id, "sample_no" => $sample_no, "chem_id" => $chem_id ) );

		$q = "INSERT into sample_result(area_id,deposit_id,drill_hole_no,lab_id,sample_no, chem_id, amount,reading_status,batch_id) values('$area_id', '$deposit_id', '$drill_hole_no', '$lab_id', '$sample_no', '$chem_id', $amount, '$reading_status',$batch_id)";

		return $this->executeQuery ( $q, "Sample Result Created! [Data $area_id $deposit_id $drill_hole_no $sample_no $chem_id $amount]", "DB Create Sample Result Query Failed! [Data $area_id $deposit_id $drill_hole_no $sample_no $chem_id $amount]" );
	}

	public function updateSampleResult($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $chem_id, $amount, $reading_status = 'valid', $batch_id) {

		$this->decideAndContinue ( U, PERM_LABSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_result", array ("area_id", "deposit_id", "drill_hole_no", "lab_id", "sample_no", "chem_id", "amount", "reading_status", "batch_id" ), array ($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $chem_id, $amount, $reading_status, $batch_id ) );

		$q = "UPDATE sample_result SET amount = $amount, reading_status='$reading_status' WHERE sample_no = '$sample_no' AND area_id = '$area_id'AND deposit_id = '$deposit_id' AND lab_id = '$lab_id' AND batch_id = $batch_id AND chem_id = '$chem_id' AND drill_hole_no = '$drill_hole_no'";

		return $this->executeQuery ( $q, "Sample Result Updated! [Data $area_id $deposit_id $drill_hole_no $sample_no $chem_id $amount]", "DB Update Sample Result Query Failed! [Data $area_id $deposit_id $drill_hole_no $sample_no $chem_id $amount]" );
	}

	public function deleteSampleResult($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $batch_id) {
		$this->decideAndContinue ( D, PERM_LABSAMPLES, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_result", array ("area_id", "deposit_id", "drill_hole_no", "lab_id", "sample_no", "batch_id" ), array ($area_id, $deposit_id, $drill_hole_no, $lab_id, $sample_no, $batch_id ) );

		$q = "DELETE from sample_result WHERE sample_no = '$sample_no' AND area_id = '$area_id'AND deposit_id = '$deposit_id' AND lab_id = '$lab_id' AND batch_id = $batch_id";

		return $this->executeQuery ( $q, "Sample Result Deleted! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no $batch_id]", "DB Delete Sample Result Query Failed! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no $batch_id]" );
	}

	public function readSampleResult($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_LABSAMPLES, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "sample_result", "area_id,deposit_id,drill_hole_no,lab_id,sample_no,batch_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( "sample_result", "area_id,deposit_id,drill_hole_no,lab_id,sample_no,batch_id", $id, $val );
		}

		//		echo $q;


		if ($toXML) {
			$this->toXML ( $q, "sample_results", "sample_result" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Sample Result Successful!", "Read Sample Result Failed!", FALSE ) );
	}

	public function createSampleBatch($batch_id, $sample_type, $drill_hole_no_start, $drill_hole_no_stop, $sample_no_from, $sample_no_to, $comments, $deposit_id, $area_id, $lab_id) {
		$this->decideAndContinue ( C, PERM_SAMPLEBATCH, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_batch", array ("batch_id", "sample_type", "drill_hole_no_start", "drill_hole_no_stop", "sample_no_from", "sample_no_to", "comments", "deposit_id", "area_id", "lab_id" ), array ($batch_id, $sample_type, $drill_hole_no_start, $drill_hole_no_stop, $sample_no_from, $sample_no_to, $comments, $deposit_id, $area_id, $lab_id ) );

		$this->ensureUnique ( "sample_batch", array ("batch_id" => $batch_id, "area_id" => $area_id, "deposit_id" => $deposit_id, "lab_id" => $lab_id ) );

		$q = "INSERT into sample_batch (batch_id, sample_type, drill_hole_no_start, drill_hole_no_stop, sample_no_from, sample_no_to, comments, deposit_id, area_id, lab_id)
				values($batch_id, '$sample_type', '$drill_hole_no_start','$drill_hole_no_stop', '$sample_no_from', '$sample_no_to', '$comments','$deposit_id', '$area_id','$lab_id');";

		return $this->executeQuery ( $q, "Sample Batch Created! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]", "DB Create Sample Batch Query Failed! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]" );
	}

	public function updateSampleBatch($batch_id, $sample_type, $drill_hole_no_start, $drill_hole_no_stop, $sample_no_from, $sample_no_to, $comments, $deposit_id, $area_id, $lab_id) {
		$this->decideAndContinue ( U, PERM_SAMPLEBATCH, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_info", array ("batch_id", "drill_hole_no_start", "drill_hole_no_stop", "sample_type", "sample_no_from", "sample_no_to", "comments", "deposit_id", "area_id", "lab_id" ), array ($batch_id, $drill_hole_no_start, $drill_hole_no_stop, $sample_type, $sample_no_from, $sample_no_to, $comments, $deposit_id, $area_id, $lab_id ) );

		$q = "UPDATE sample_batch SET sample_type = '$sample_type', sample_no_from = '$sample_no_from', sample_no_to = '$sample_no_to', comments = '$comments', drill_hole_no_start = '$drill_hole_no_start', drill_hole_no_stop = '$drill_hole_no_stop', deposit_id = '$deposit_id', area_id = '$area_id' WHERE batch_id = $batch_id AND area_id = '$area_id' AND deposit_id = '$deposit_id' AND lab_id = '$lab_id' ";

		return $this->executeQuery ( $q, "Sample Batch Updated! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]", "DB Update Sample Batch Query Failed! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]" );
	}

	public function deleteSampleBatch($batch_id, $area_id, $deposit_id, $lab_id) {
		$this->decideAndContinue ( D, PERM_SAMPLEBATCH, "Insufficient Permissions!" );

		$this->checkValidity ( "sample_batch", array ("batch_id", "area_id", "deposit_id", "lab_id" ), array ($batch_id, $area_id, $deposit_id, $lab_id ) );

		$q = "DELETE from sample_batch WHERE batch_id = $batch_id AND $area_id = '$area_id' AND deposit_id = '$deposit_id' AND lab_id = '$lab_id'";

		return $this->executeQuery ( $q, "Sample Batch Deleted! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]", "DB Delete Sample Batch Query Failed! [Data $area_id $deposit_id $drill_hole_no $lab_id $sample_no_from $sample_no_to $batch_id]" );
	}

	public function readSampleBatch($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "sample_batch", "area_id,deposit_id,lab_id,batch_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( "sample_batch", "area_id,deposit_id,lab_id,batch_id", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "sample_batches", "sample_batch" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Sample Batch Successful!", "Read Sample Batch Failed!", FALSE ) );
	}

	/***********************************************************************************************************************/

	/**********************************************************Chemical Table***********************************************/

	public function createChemical($chem_id, $chem_name, $chem_default) {
		$this->checkValidity ( "chemical", array ("chem_id", "chem_name", "chem_default" ), array ($chem_id, $chem_name, $chem_default ) );

		$this->ensureUnique ( "chemical", "chem_id", $chem_id );

		$q = "INSERT into chemical(chem_id, chem_name, chem_default) values('$chem_id', '$chem_name', $chem_default)";

		return $this->executeQuery ( $q, "Chemical Created! [Data $chem_id $chem_name]", "DB Create Chemical Query Failed! [Data $chem_id $chem_name]" );
	}

	public function updateChemical($chem_id, $chem_name, $chem_default) {
		$this->checkValidity ( "chemical", array ("chem_id", "chem_name" ), array ($chem_id, $chem_name ) );

		$q = "UPDATE chemical SET chem_name = '$chem_name', chem_default = $chem_default WHERE chem_id = '$chem_id'";

		return $this->executeQuery ( $q, "Chemical Updated! [Data $chem_id $chem_name]", "DB Update Chemical Query Failed! [Data $chem_id $chem_name]" );
	}

	public function deleteChemical($chem_id) {
		$this->checkValidity ( "chemical", array ("chem_id" ), array ($chem_id ) );

		$q = "DELETE from chemical WHERE chem_id = '$chem_id'";

		return $this->executeQuery ( $q, "Chemical Deleted! [Data $chem_id]", "DB Delete Chemical Query Failed! [Data $chem_id]" );
	}

	public function setChemicalDefault($chem_id, $val) {
		// should check be done on $val???
		$this->checkValidity ( "chemical", array ("chem_id" ), array ($chem_id ) );

		$q = "UPDATE chemical SET chem_default = $val WHERE chem_id = '$chem_id'";

		return $this->executeQuery ( $q, "Chemical Updated! [Data $chem_id]", "DB Update Chemical Query Failed! [Data $chem_id]" );
	}

	public function readChemical($toXML = FALSE, $id = NULL, $val = NULL) {
		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "chemical", "chem_name", $arg );
		} else {
			$q = $this->specifyGenQuery ( "chemical", "chem_name", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "chemicals", "chemical" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Chemical Successful!", "Read Chemical Failed!", FALSE ) );
	}
	/**************************************************************************************************************************/

	/*********************************************************Crew and Teams**************************************************/

	public function createCrew($crew_id, $crew_name) {
		$this->decideAndContinue ( C, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "crew", array ("crew_id", "crew_name" ), array ($crew_id, $crew_name ) );

		$this->ensureUnique ( "crew", "crew_id", $crew_id );

		$q = "INSERT into crew(crew_id, crew_name) values('$crew_id', '$crew_name')";

		return $this->executeQuery ( $q, "Crew Created! [Data $crew_id $crew_name]", "DB Create Crew Query Failed! [Data $crew_id $crew_name]" );
	}

	public function updateCrew($crew_id, $crew_name) {
		$this->decideAndContinue ( U, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "crew", array ("crew_id", "crew_name" ), array ($crew_id, $crew_name ) );

		$q = "UPDATE crew SET crew_name = '$crew_name' WHERE crew_id = '$crew_id'";

		return $this->executeQuery ( $q, "Crew Updated! [Data $crew_id $crew_name]", "DB Update Crew Query Failed! [Data $crew_id $crew_name]" );
	}

	public function deleteCrew($crew_id) {
		$this->decideAndContinue ( D, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "crew", array ("crew_id" ), array ($crew_id ) );

		$q = "DELETE from crew WHERE crew_id = '$crew_id'";

		return $this->executeQuery ( $q, "Crew Deleted! [Data $crew_id]", "DB Delete Crew Query Failed! [Data $crew_id]" );
	}

	public function readCrew($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_CREW, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "crew", "crew_name", $arg );
		} else {
			$q = $this->specifyGenQuery ( "crew", "crew_name", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "crews", "crew" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Crew Successful!", "Read Crew Failed!", FALSE ) );
	}

	public function createTeam($crew_id, $person_id) {
		//$this->decideAndContinue(C, TEAM, "Insufficient Permissions!");


		$this->decideAndContinue ( C, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "team", array ("crew_id", "person_id" ), array ($crew_id, $person_id ) );

		$q = "INSERT into team(crew_id, person_id) values('$crew_id', $person_id)";

		return $this->executeQuery ( $q, "Team Created!", "DB Team Query Failed!" );
	}

	/*public function updateTeam($crew_id, $person_id)
	{
	$q = "UPDATE team SET person_id = '$person_id' WHERE crew_id = $crew_id";

	$this->executeQuery($q, "Team Updated!", "DB Update Team Query Failed!");
	}*/

	public function deleteTeam($crew_id, $person_id) {
		//$this->decideAndContinue(D, TEAM, "Insufficient Permissions!");


		$this->decideAndContinue ( D, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "team", array ("crew_id", "person_id" ), array ($crew_id, $person_id ) );

		$q = "DELETE from team WHERE crew_id = '$crew_id' and person_id = $person_id";

		return $this->executeQuery ( $q, "Team Deleted!", "DB Delete Team Query Failed!" );
	}

	public function readTeam($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_CREW, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "team", "crew_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( "team", "crew_id", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "teams", "team" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Team Successful!", "Read Team Failed!", FALSE ) );
	}

	public function createTeamMember($person_id, $first_name, $last_name) {
		//$this->decideAndContinue(C, TEAM, "Insufficient Permissions!");


		$this->decideAndContinue ( C, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "team_member", array ("person_id", "first_name", "last_name" ), array ($person_id, $first_name, $last_name ) );

		$this->ensureUnique ( "team_member", "person_id", $person_id );

		$q = "INSERT into team_member(person_id, first_name, last_name) values($person_id, '$first_name', '$last_name')";

		return $this->executeQuery ( $q, "Team Member Created!", "DB Team Member Query Failed!" );
	}

	public function updateTeamMember($person_id, $first_name, $last_name) {
		//$this->decideAndContinue(U, TEAM, "Insufficient Permissions!");


		$this->decideAndContinue ( U, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "team_member", array ("person_id", "first_name", "last_name" ), array ($person_id, $first_name, $last_name ) );

		$q = "UPDATE team_member SET first_name = '$first_name', last_name = '$last_name' WHERE person_id = $person_id";

		return $this->executeQuery ( $q, "Team Member Updated!", "DB Update Team Member Query Failed!" );
	}

	public function deleteTeamMember($person_id) {
		//$this->decideAndContinue(D, TEAM, "Insufficient Permissions!");


		$this->decideAndContinue ( D, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "team_member", array ("person_id" ), array ($person_id ) );

		$q = "DELETE from team_member WHERE person_id = $person_id";

		return $this->executeQuery ( $q, "Team Member Deleted!", "DB Delete Team Member Query Failed!" );
	}

	public function readTeamMember($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_CREW, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "team_member", "last_name", $arg );
		} else {
			$q = $this->specifyGenQuery ( "team_member", "last_name", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "team_members", "team_member" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Team Member Successful!", "Read Team Member Failed!", FALSE ) );
	}

	public function createCrewToDrillHole($crew_id, $drill_hole_no) {
		//$this->decideAndContinue(C, CREWHOLEASSIGNMENTS, "Insufficient Permissions!");


		$this->decideAndContinue ( C, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "crew2drill_hole", array ("crew_id", "drill_hole_no" ), array ($crew_id, $drill_hole_no ) );

		$this->ensureUnique ( "crew2drill_hole", array ("$crew_id" => $crew_id, "drill_hole_no" => $drill_hole_no ) );

		$q = "INSERT into crew2drill_hole(crew_id, drill_hole_no) values ('$crew_id', '$drill_hole_no')";

		return $this->executeQuery ( $q, "Crew to Drill Hole Mapping Created!", "DB Crew to Drill Hole Mapping Query Failed!" );
	}

	public function updateCrewToDrillHole($crew_id, $drill_hole_no) {
		/*$q = "UPDATE crew2drill_hole SET c_id = '$person_id' WHERE crew_id = $crew_id";

		$this->executeQuery($q, "Team Updated!", "DB Update Team Query Failed!");*/
	}

	public function deleteCrewToDrillHole($crew_id, $drill_hole_no) {
		//$this->decideAndContinue(D, CREWHOLEASSIGNMENTS, "Insufficient Permissions!");


		$this->decideAndContinue ( D, PERM_CREW, "Insufficient Permissions!" );

		$this->checkValidity ( "crew2drill_hole", array ("crew_id", "drill_hole_no" ), array ($crew_id, $drill_hole_no ) );

		$q = "DELETE from crew2drill_hole WHERE crew_id = '$crew_id' and dril_hole_no = '$drill_hole_no'";

		return $this->executeQuery ( $q, "Crew to Drill Hole Mapping Deleted!", "DB Delete Crew to Drill Hole Mapping Query Failed!" );
	}

	public function readCrewToDrillHole($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_CREW, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "crew2drill_hole", "crew_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( "crew2drill_hole", "crew_id", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "crew_to_drill_holes", "crew_to_drill_hole" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Crew-To-Drill Hole Successful!", "Read Crew-To-Drill Hole Failed!", FALSE ) );
	}

	/********************************************************************************************************************************/

	/**************************************************************Drill Machine****************************************************/

	public function createDrillMachine($drill_unit_no, $drill_name, $drill_type) {
		$this->decideAndContinue ( C, PERM_DRILLMACHINE, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_machine", array ("drill_unit_no", "drill_name", "drill_type" ), array ($drill_unit_no, $drill_name, $drill_type ) );

		$this->ensureUnique ( "drill_machine", "drill_unit_no", $drill_unit_no );

		$q = "INSERT into drill_machine(drill_unit_no, drill_name, drill_type) values ($drill_unit_no, '$drill_name', '$drill_type')";

		return $this->executeQuery ( $q, "Drill Machine Created! [Data $drill_unit_no $drill_name]", "DB Drill Machine Query Failed! [Data $drill_unit_no $drill_name]" );
	}

	public function updateDrillMachine($drill_unit_no, $drill_name, $drill_type) {
		$this->decideAndContinue ( U, PERM_DRILLMACHINE, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_machine", array ("drill_unit_no", "drill_name", "drill_type" ), array ($drill_unit_no, $drill_name, $drill_type ) );

		$q = "UPDATE drill_machine SET drill_name = '$drill_name', drill_type = '$drill_type' WHERE drill_unit_no = $drill_unit_no";

		return $this->executeQuery ( $q, "Drill Machine Updated! [Data $drill_unit_no $drill_name]", "DB Update Drill Machine Query Failed! [Data $drill_unit_no $drill_name]" );
	}

	public function deleteDrillMachine($drill_unit_no) {
		$this->decideAndContinue ( D, PERM_DRILLMACHINE, "Insufficient Permissions!" );

		$this->checkValidity ( "drill_machine", array ("drill_unit_no" ), array ($drill_unit_no ) );

		$q = "DELETE from drill_machine WHERE drill_unit_no = $drill_unit_no";

		return $this->executeQuery ( $q, "Drill Machine Deleted! [Data $drill_unit_no]", "DB Delete Drill Machine Query Failed! [Data $drill_unit_no]" );
	}

	public function readDrillMachine($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_DRILLMACHINE, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "drill_machine", "drill_name", $arg );
		} else {
			$q = $this->specifyGenQuery ( "drill_machine", "drill_name", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "drill_machines", "drill_machine" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Drill Machine Successful!", "Read Drill Machine Failed!", FALSE ) );
	}

	/*************************************************************************************************************************************/

	/**********Sample Types*********/
	public function readSampleType($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( C, PERM_DRILLSAMPLES, "Insufficient Permissions!" );

		$q = $this->specifyGenQuery ( "sample_type", "type", $id, $val );

		if ($toXML) {
			$this->toXML ( $q, "sample_types", "sample_type" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Sample Type Successful!", "Read Sample Type Failed!", FALSE ) );
	}
	/*******************************/

	/*******************************************Users & Groups*****************************************************************************/

	/*** NOTE: add validity checks and permission checks before modifying groups and permissions ***/

	public function createUser($usr, $pass) {
		//return $this->pg->addUser($usr, $pass);
		$this->decideAndContinue ( C, PERM_USERSGROUPS, "Insufficient Permissions!" );

		$this->l->lgg ( "creating user: " . $usr , TAGUSER);

		if ($this->pg->addUser ( $usr, $pass )) {
			$this->l->lgg ( "user: " . $usr . " created" , TAGUSER);
			return $this->initPermissions ( $usr, "user" , TAGUSER);
		}
		return false;
	}

	public function updateUserPassword($uid, $old, $new) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "updating user password of uid: " . $uid , TAGUSER );
		return $this->pg->changeUserPassword ( $uid, $old, $new );
	}

	public function deleteUserByAlias($usr) {
		$this->decideAndContinue ( D, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "deleting user: " . $usr , TAGUSER);
		return $this->pg->removeUserByAlias ( $usr );
	}

	public function deleteUser($uid) {
		$this->decideAndContinue ( D, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "deleting user of uid: " . $uid , TAGUSER);
		return $this->pg->removeUser ( $uid );
	}

	public function readUser($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$con_str = $this->getConcatStr ( $arg );
			$q = "select uid, alias from usr where $con_str order by alias";
		} else if ($id != NULL && $val != NULL) {
			$v = (is_string ( $val )) ? "'$val'" : "$val";
			$q = "select uid, alias from usr where $id = $v order by alias";
		} else {
			$q = "select uid, alias from usr order by alias";
		}

		if ($toXML) {
			$this->toXML ( $q, "users", "user" );
			return;
		}

		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read User Successful!", "Read User Failed!" ) );
	}

	public function createGroup($grp) {
		//return $this->pg->addGroup($grp);
		$this->decideAndContinue ( C, PERM_USERSGROUPS, "Insufficient Permissions!" );

		$this->l->lgg ( "creating group: " . $grp );

		if ($this->pg->addGroup ( $grp )) {
			$this->l->lgg ( "group: " . $grp . " created" );
			return $this->initPermissions ( $grp, "group" );
		}
		return false;
	}

	public function readGroup($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "grp", "alias", $arg );
		} else {
			$q = $this->specifyGenQuery ( "grp", "alias", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "groups", "group" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Group Successful!", "Read Group Failed!", FALSE ) );
	}

	public function renameGroup($gid, $ngrp) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "renaming group of gid: " . $gid , TAGGROUP);
		return $this->pg->changeGroupAlias ( $gid, $ngrp );
	}

	public function deleteGroup($gid) {
		$this->decideAndContinue ( D, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "deleting group of gid: " . $gid , TAGGROUP );
		return $this->pg->removeGroup ( $gid );
	}

	public function addUserToGroup($uid, $gid) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "adding uid: " . $uid . " to gid: " . $gid , TAGGROUP );
		return $this->pg->addUserToGroup ( $uid, $gid );
	}

	public function removeUserFromGroup($uid, $gid) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "removing uid: " . $uid . " from gid: " . $gid , TAGGROUP );
		return $this->pg->removeUserFromGroup ( $uid, $gid );
	}

	public function createUserInGroup($gid, $uid) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "adding uid: " . $uid . " to gid: " . $gid , TAGGROUP );
		return $this->addUserToGroup ( $uid, $gid );
	}

	public function deleteUserInGroup($gid, $uid) {
		$this->decideAndContinue ( U, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$this->l->lgg ( "removing uid: " . $uid . " from gid: " . $gid , TAGGROUP );
		return $this->removeUserFromGroup ( $uid, $gid );
	}

	public function readUserInGroup($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$q = "";

		/*		if(func_num_args() == 2 && is_array(func_get_arg(1))){
		$arg = func_get_arg(1);
		$q = $this->specifyGenQuery("usr_in_grp", "gid, uid", $arg);
		}else
		{
		$q = $this->specifyGenQuery("usr_in_grp", "gid, uid", $id, $val);
		}
		*/

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$con_str = $this->getConcatStr ( $arg );
			$q = "select usr.alias as user_alias, usr_in_grp.uid as uid, usr_in_grp.gid as gid from usr, usr_in_grp where $con_str and usr.uid=usr_in_grp.uid order by gid, user_alias";
		} else if ($id != NULL && $val != NULL) {
			$v = (is_string ( $val )) ? "'$val'" : "$val";
			$q = "select usr.alias as user_alias, usr_in_grp.uid as uid, usr_in_grp.gid as gid from usr, usr_in_grp where $id = $v and usr.uid=usr_in_grp.uid order by gid, user_alias";
		} else {
			$q = "select usr.alias as user_alias, usr_in_grp.uid as uid, usr_in_grp.gid as gid from usr, usr_in_grp where usr.uid=usr_in_grp.uid order by gid, user_alias";
		}

		if ($toXML) {
			$this->toXML ( $q, "user_in_groups", "user_in_group" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read User-In-Group Successful!", "Read User-In-Group Failed!", FALSE ) );
	}

	public function readUsersNotInGroup($toXML = FALSE, $gid) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );
		$q = "";

		$q = "select alias, uid from usr where uid not in (select uid from usr_in_grp where gid=$gid) order by alias";

		if ($toXML) {
			$this->toXML ( $q, "users_not_in_groups", "users_not_in_group" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Users-Not-In-Group Successful!", "Read User-Not-In-Group Failed!", FALSE ) );
	}

	public function readUserGroup($toXML = FALSE, $uid) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );
		// Assuming a user cannot be in more than one group! //
		$q = "";

		$q = "select grp.alias as alias, grp.gid as gid from grp, usr_in_grp where usr_in_grp.uid=$uid and grp.gid=usr_in_grp.gid order by alias";

		if ($toXML) {
			$this->toXML ( $q, "user_groups", "user_group" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read User Group Successful!", "Read User Group Failed!", FALSE ) );
	}

	public function readGroupNot($toXML = FALSE, $gid) {
		$this->decideAndContinue ( R, PERM_USERSGROUPS, "Insufficient Permissions!" );

		$q = "";

		if ($this->isAnyEmpty ( $gid )) {
			$gid = - 1;
		}

		$q = "select alias, gid from grp where gid != $gid order by alias";

		if ($toXML) {
			$this->toXML ( $q, "groups", "group" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Group-Not Successful!", "Read Group-Not Failed!", FALSE ) );
	}

	// Permissions //


	public function createPermission($perm, $tag, $description) {
		$this->decideAndContinue ( C, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "creating new permission: " . $perm , TAGPERMISSION );
		return $this->pg->addPermissionType ( $perm, $tag, $description );
	}

	public function deletePermission($perm) {
		$this->decideAndContinue ( D, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "deleting permission: " . $perm , TAGPERMISSION );
		return $this->pg->removePermissionType ( $perm );
	}

	public function addUserPermission($uid, $permType, $bits) {
		$this->decideAndContinue ( U, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "adding user permission for uid: " . $uid . " | perm: " . $permType . " => " . $bits , TAGPERMISSION );
		return $this->pg->addUserPermission ( $uid, $permType, $bits );
	}

	public function addGroupPermission($gid, $permType, $bits) {
		$this->decideAndContinue ( U, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "adding group permission for gid: " . $gid . " | perm: " . $permType . " => " . $bits , TAGPERMISSION );
		return $this->pg->addGroupPermission ( $gid, $permType, $bits );
	}

	/*public function overwriteUserPermission($uid, $permType, $bits)
	{
	return $this->overwriteUserPermission($uid, $permType, $bits);
	}

	public function overwriteGroupPermission($gid, $permType, $bits)
	{
	return overwriteUserPermission($uid, $permType, $bits);
	}*/

	public function initPermissions($alias, $scope) {
		if ($scope == "user") {
			$q = "select uid from " . USR_TBL . " WHERE alias = '$alias'";
			$r = $this->db->query ( $q );

			if ($this->db->rowCount ( $r ) == 1) {
				$arr = $this->db->fetchAssocArray ( $r );
				$uid = $arr ["uid"];
				$this->l->lgg ( "initializing user permission for user: " . $alias , TAGUSER );
				$this->pg->initializeUserRow ( $uid );
				return true;
			}
			return false;
		}

		if ($scope == "group") {
			$q = "select gid from " . GRP_TBL . " WHERE alias = '$alias'";
			$r = $this->db->query ( $q );

			if ($this->db->rowCount ( $r ) == 1) {
				$arr = $this->db->fetchAssocArray ( $r );
				$gid = $arr ["gid"];
				$this->l->lgg ( "initializing group permission for group: '$alias' " , TAGPERMISSION );
				$this->pg->initializeGroupRow ( $gid );
				return true;
			}
			return false;
		}
		return false;
	}

	public function clearUserPermissions($uid, $permType) {
		$this->decideAndContinue ( U, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "clearing user permission for uid: " . $uid . " | perm: " . $permType , TAGPERMISSION );
		return clearPermissions ( $uid, $permType, "user" );
	}

	public function clearGroupPermissions($gid, $permType) {
		$this->decideAndContinue ( U, PERM_PERMISSIONS, "Insufficient Permissions!" );
		$this->l->lgg ( "clearing group permission for gid: " . $gid . " | perm: " . $permType , TAGPERMISSION );
		return clearPermissions ( $gid, $permType, "group" );
	}

	public function readPermission($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_PERMISSIONS, "Insufficient Permissions!" );

		$q = "";

		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$arg = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( PERM_TBL, "pmt_tag, pmt_id", $arg );
		} else {
			$q = $this->specifyGenQuery ( PERM_TBL, "pmt_tag, pmt_id", $id, $val );
		}

		if ($toXML) {
			$this->toXML ( $q, "permissions", "permission" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Permission Successful!", "Read Permission Failed!", FALSE ) );
	}

	public function readPermissionTags($toXML = FALSE) {
		$this->decideAndContinue ( R, PERM_PERMISSIONS, "Insufficient Permissions!" );

		$q = "select distinct pmt_tag as tag from perm_type order by tag";

		if ($toXML) {
			$this->toXML ( $q, "perm_tags", "perm_tag" );
			return;
		}
		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Permission Tags Successful!", "Read Permission Tags Failed!", FALSE ) );
	}

	public function readUserPermissions($toXML = FALSE, $uid) {
		$this->decideAndContinue ( R, PERM_PERMISSIONS, "Insufficient Permissions!" );

		/*$rx = $this->pg->getUserPermissions($uid);

		if($toXML){
		$this->toXML("", "user_permissions", "user_permission", $rx);
		return;
		}
		return $this->db->fetchAll($rx);
		*/
		header ( 'Content-type: text/xml' );
		header ( 'Pragma: public' );
		header ( 'Cache-control: private' );
		header ( 'Expires: -1' );

		echo $this->pg->getUserPermissionsInXml ( $uid );
	}

	public function readGroupPermissions($toXML = FALSE, $gid) {
		$this->decideAndContinue ( R, PERM_PERMISSIONS, "Insufficient Permissions!" );
		/*$rx = $this->pg->getGroupPermissions($gid);

		if($toXML){
		$this->toXML("", "group_permissions", "group_permission", $rx);
		return;
		}
		return $this->db->fetchAll($rx);
		*/
		header ( 'Content-type: text/xml' );
		header ( 'Pragma: public' );
		header ( 'Cache-control: private' );
		header ( 'Expires: -1' );

		echo $this->pg->getGroupPermissionsInXml ( $gid );
	}

	/*************************************************************************************************************************************/

	/************Generic CRUD Modules********/

	// variable parameter function. Must have same number of database column names to values
	// E.g. create("samples", "drill_unit_no", "drill_name", "drill_type", 10, "thedrill", "powerful")
	/*private function create($tbl, $columns, $values)
	{
	if(func_num_args() != 3)
	{
	die("Wrong number of args; args should be: <tbl_name>, [<columns>], [<values>]");
	}

	$str_c = implode(',', $columns);
	$str_v = implode(',', $values);

	$q = "INSERT into $tbl ( $str_c ) values ( $str_v )";

	return $q
	}

	private function update($tbl, $colums, $values, $id, $id_val)
	{
	if(func_num_args() != 5)
	{
	die("Wrong number of args; args should be: <tbl_name>, [<columns>], [<values>]");
	}

	$count;

	if(($count = count($columns)) != count($values))
	{
	die("columns and values should be the same");
	}

	$str_updates = "";

	for($i = 0; $i < $count; $i++)
	{
	$col = $columns[$i];
	if($i == 0){
	$str_updates = "$col = $values[$col]";
	}else
	{
	$str_updates .= ", $col = $values[$col]";
	}
	}

	$q = "UPDATE $tbl SET $str_updates WHERE $id = $id_val";

	return $q;
	}

	private function delete($tbl, $id, $id_val)
	{
	$q = "DELETE from $tbl WHERE $id = $id_val";
	return $q;
	}
	*/

	/******* lab functions *********/
	function createLab($lab_id, $lab_name, $lab_access_code = "") {
		$this->decideAndContinue ( C, PERM_LAB, MSG_PERMERROR );
		$this->ensureUnique ( "lab", "lab_id", $lab_id );
		$this->checkValidity ( "lab", array ("lab_id", "lab_name", "lab_access_code" ), array ($lab_id, $lab_name, $lab_access_code ) );

		$sql = "INSERT INTO lab VALUES ('$lab_id','$lab_name','$lab_access_code')";
		return $this->executeQuery ( $sql, "Laboratory Created! [Data $lab_id $lab_name]", "DB Laboratory Query Failed! [Data $lab_id $lab_name]", true );

	}

	function updateLab($lab_id, $lab_name, $lab_access_code = '') {
		$this->decideAndContinue ( U, PERM_LAB, MSG_PERMERROR );
		if (count ( func_get_args () ) == 3) {
			$this->checkValidity ( "lab", array ("lab_name", "lab_access_code" ), array ($lab_name, $lab_access_code ) );
			$q = "UPDATE lab SET lab_name = '$lab_name', lab_access_code = '$lab_access_code' WHERE lab_id = '$lab_id'";
		} else {
			$this->checkValidity ( "lab", array ("lab_name" ), array ($lab_name ) );
			$q = "UPDATE lab SET lab_name = '$lab_name' WHERE lab_id = '$lab_id'";
		}
		return $this->executeQuery ( $q, "Laboratory Updated! [Data $lab_id $lab_name]", "DB Laboratory Update Failed! [Data $lab_id $lab_name]", true );
	}

	function deleteLab($lab_id) {
		$this->decideAndContinue ( D, PERM_LAB, MSG_PERMERROR );
		$q = "DELETE FROM lab WHERE lab_id = '$lab_id'";
		return $this->executeQuery ( $q, "Laboratory Deleted! [Data $lab_id]", "Laboratory Delete Failed! [Data $lab_id]", true );

	}

	function readLab($toXML = FALSE, $id = NULL, $val = NULL) {
		$this->decideAndContinue ( R, PERM_LAB, MSG_PERMERROR );

		$q = "";
		if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
			$assoc_arr = func_get_arg ( 1 );
			$q = $this->specifyGenQuery ( "lab", "lab_name", $assoc_arr );
		} else {
			$q = $this->specifyGenQuery ( "lab", "lab_name" );
		}

		if ($toXML) {
			$this->toXML ( $q, "labs", "lab" );
			return;
		}

		return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Lab Successful!", "Read Lab Failed!", FALSE ) );

	}

	/*

	public function readDrillHole($toXML = FALSE, $id = NULL, $val = NULL) {
	$this->decideAndContinue ( R, PERM_DRILLHOLE, "Insufficient Permissions!" );

	$q = "";

	if (func_num_args () == 2 && is_array ( func_get_arg ( 1 ) )) {
	$arg = func_get_arg ( 1 );
	$q = $this->specifyGenQuery ( "drill_hole", "drill_hole_no", $arg );
	} else {
	$q = $this->specifyGenQuery ( "drill_hole", "drill_hole_no", $id, $val );
	}

	if ($toXML) {
	$this->toXML ( $q, "drill_holes", "drill_hole" );
	return;
	}
	return $this->db->fetchAll ( $this->executeQuery ( $q, "Read Drill Hole Successful!", "Read Drill Hole Failed!", FALSE ) );
	}

	*/

	/*******util functions*********/

	private function toXML($q, $root, $sub, $rx = NULL) {
		header ( 'Content-type: text/xml' );
		header ( 'Pragma: public' );
		header ( 'Cache-control: private' );
		header ( 'Expires: -1' );
		echo ('<?xml version="1.0" encoding="utf-8"?>');

		$res = NULL;
		if (! $rx) {
			$res = $this->db->query ( $q );
		} else {
			$res = $rx;
		}
		$rows = $this->db->rowCount ( $res );
		$row = $this->db->fetchAssocArray ( $res );
		echo "<$root>";
		if ($rows > 0) {
			do {
				echo "<$sub>";
				foreach ( $row as $column => $value ) {
					//hack
					/*if(strcmp($column, "date_started") == 0 || strcmp($column, "date_ended") == 0)
					{
					$do = date_create($row[$column]);
					$date = date_format($do, "Y-m-d");
					echo "<$column>" . $date . "<$column>";
					}else*/					{
					echo "<$column>" . $row [$column] . "</$column>";
					}
				}
				echo "</$sub>";
			} while ( $row = $this->db->fetchAssocArray ( $res ) );
		}
		echo "</$root>";
		$this->db->freeResult ( $res );
	}

	private function emptyXML($root) {
		header ( 'Content-type: text/xml' );
		header ( 'Pragma: public' );
		header ( 'Cache-control: private' );
		header ( 'Expires: -1' );
		echo ('<?xml version="1.0" encoding="utf-8"?>');
		echo "<$root>";
		echo "</$root>";
	}

	private function specifyGenQuery($tbl, $orderby, $id = NULL, $val = NULL) {
		$arg_arr = NULL;
		$con_str = "";
		$i = 0;
		if (func_num_args () == 3) { // do concat select
			$arg_arr = func_get_arg ( 2 );
			if (is_array ( $arg_arr )) {
				/*foreach($arg_arr as $key=>$value)
				{
				$curr = $arg_arr[$key];
				if($i == 0){
				$con_str .= (is_string($curr))? "$key = " . "'$curr'" : "$key = " . "$curr";
				}
				else
				{
				$con_str .= (is_string($curr))? "and $key = " . "'$curr'" : "and $key = " . "$curr";
				}
				$i++;
				}*/
				$con_str = $this->getConcatStr ( $arg_arr );
				return "select * from $tbl where $con_str order by $orderby";
			}

		}

		if ($id != NULL && $val != NULL) {
			return (is_string ( $val )) ? "select * from $tbl where $id = '$val' order by $orderby" : "select * from $tbl where $id = $val order by $orderby";
		}
		return "select * from $tbl order by $orderby";
	}

	private function getConcatStr($arg_arr) {
		// do concat select
		//$arg_arr = func_get_arg(2);
		if (is_array ( $arg_arr )) {
			foreach ( $arg_arr as $key => $value ) {
				$curr = $arg_arr [$key];
				if ($i == 0) {
					$con_str .= (is_string ( $curr )) ? "$key = " . "'$curr'" : "$key = " . "$curr";
				} else {
					$con_str .= (is_string ( $curr )) ? "and $key = " . "'$curr'" : "and $key = " . "$curr";
				}
				$i ++;
			}
			return $con_str;
		}
	}


	private function executeQuery($q, $succMsg, $failMsg, $doLog = TRUE) {
		$tag = guessTag($q);

		$ret = NULL;

		if (($ret = $this->db->query ( $q )) == FALSE) {
			$fm = "error: " . ERR_DB . ": " . $failMsg;
			echo $fm; // should be rare!


			if ($doLog) {
				$this->l->lgg ( $failMsg , $tag );
			}

			return FALSE;
		}

		$sm = "success: " . SUCC . ": " . $succMsg;
		if ($this->verbose)
		echo $sm;

		if ($doLog) {
			$this->l->lgg ( $succMsg , $tag );

		}

		$this->logTableActivity($q);

		return $ret;
	}

	private function decide($crud, $permType) {
		$po = $this->po;

		switch ($crud) {
			case C :
				return $po->canCreate ( $permType );
				break;
			case R :
				return $po->canRead ( $permType );
				break;
			case U :
				return $po->canUpdate ( $permType );
				break;
			case D :
				return $po->canDelete ( $permType );
				break;
		}
		return false;
	}

	private function decideAndContinue($crud, $permType, $errorMsg) {
		if (! ($this->decide ( $crud, $permType ))) {
			die ( "error: " . ERR_PERM . ": " . $errorMsg );
		}
	}

	private function exists($table, $col, $value = NULL) {

		$q = "";
		$constr = "";

		if (func_num_args () == 2) {
			$arr = func_get_arg ( 1 );
			if (is_array ( $arr )) {
				$constr = $this->getConcatStr ( $arr );
				$karr = array_keys ( $arr );
				$cols = implode ( ",", $karr );

				$q = "select $cols from $table WHERE $constr";

			}
		} else {
			if ($value != NULL && is_string ( $value )) {
				$q = "select $col from $table WHERE $col = '$value'";
			} else {
				$q = "select $col from $table WHERE $col = $value";
			}
		}

		$r = $this->db->query ( $q );

		if ($this->db->rowCount ( $r ) > 0) {
			return true;
		}
		return false;
	}

	private function ensureUnique($table, $col, $value = NULL) {
		// JM ... try and get an alias for the function


		if ($value != NULL) {
			$alias = $this->getColumnAlias ( $table, $col );
			if (empty ( $alias )) {
				$alias = "Value";
			}

			if ($this->exists ( $table, $col, $value )) {
				die ( "error: " . ERR_UNIQUE . ": $alias is not unique!. Please enter a different value" ); // JM possibly we got an alias
			}
		} else {
			$aliases = array ();
			$cols = array_keys ( $col );
			for($i = 0; $i < count ( $cols ); $i ++) {
				$colname = $cols [$i];
				if ($this->getColumnAlias ( $table, $colname )) {
					$aliases [] = $this->getColumnAlias ( $table, $colname, '' );
				}
			}

			$valueStr = "Values are not unique! ";
			if (count ( $aliases ) > 0) {
				$valueStr = "The supplied " . join ( ",", $aliases ) . " combination is not unique! ";
			}

			if ($this->exists ( $table, $col )) {
				die ( "error: " . ERR_UNIQUE . ": $valueStr Please enter different values" );
			}
		}
	}

	private function checkValidity($table, $cols, $vals) {
		$count = 0;

		if (($count = count ( $cols )) != count ( $vals )) {
			die ( "error: columns and values should be the same" );
		}

		for($i = 0; $i < $count; $i ++) {
			if (! (isValid ( $table, $cols [$i], $vals [$i] ))) {
				$alias = $cols [$i];
				if ($this->getColumnAlias ( $table, $alias ))
				$alias = $this->getColumnAlias ( $table, $cols [$i] );
				die ( "error: " . ERR_VALID . ": Value \"" . $vals [$i] . "\" for column " . $alias . " is invalid!" );
			}
		}
	}

	public function isAnyEmpty() {
		for($i = 0; $i < func_num_args (); $i ++) {
			$v = func_get_arg ( $i );
			if ($v == NULL || $v == "" || $v == '') {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function user() {
		return $pg->getSessionUser ();
	}

	function getColumnAlias($tbl, $col, $prefix = "The") {

		$sql = "SELECT fa_alias FROM flag_alias WHERE fa_tbl = '$tbl' AND fa_col = '$col'";

		//$result = $this->executeQuery ( $sql, "", "", FALSE );
		$result = $this->db->query ( $sql );
		if ($result) {
			while ( $row = $this->db->fetchArray ( $result ) ) {
				return "$prefix " . $row ["fa_alias"];
			}
		}
		return null;
	}

	/*************************************************************************************************/

	private $db; // database abstraction
	private $po; // permission abstraction
	private $pg; // permission/usr/grp/abstraction
	private $l; // logger abstraction
}
?>