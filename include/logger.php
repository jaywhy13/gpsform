<?php

include_once("accountability.php");

class Logger
{
	function __construct($pg_)
	{
		$this->pg = $pg_;
	}

	public function lgg($msg, $tag=NULL)
	{
		if(!$tag){
			lg("$msg", $this->pg->getSessionUser());
		}else
		{
			lg("$msg", $this->pg->getSessionUser(), $tag);
		}
		return true;
	}

	private $pg;
}

?>
