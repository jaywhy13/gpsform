<?
include_once ("db.php");
include_once ("varlib.php");

define ( "LBOUNDREGEX", "[0-9]+:$" );
define ( "UBOUNDREGEX", "^:[0-9]+" );

$strict_standard_policy = false; //	when set to true, the isValid method will return false for tbl,col values it does not know about
//	when set to false, it'll b more forgivin... it'll return true for values it doesn't know crap about
define ( "STANDARD_TABLE", "standard" );


$stan_db = getDBInstance ( "pg", getParam ( "dbconfigfile-psql" ) );


$sql = "SELECT * FROM " . STANDARD_TABLE;


@$result = $stan_db->query ( $sql );
// TODO: Revisit this logic above

$validators = array ();
$validatorsBySid = array ();

if ($result) {
	while ( $row = $stan_db->fetchArray ( $result ) ) {
		$tbls = explode ( ",", $row ["tbl"] );
		
		for($i = 0; $i < count ( $tbls ); $i ++) {
			$tbl = $tbls[$i];
			$sid = $row ["sid"];
			$col = $row ["col"];
			$alias = $row ["alias"];
			$expression_type = $row ["expression_type"];
			$expression = $row ["expression"];
			$help = $row ["format_help"];
			
			if (! $validators [$tbl])
				$validators [$tbl] = array ();
			if (! $validators [$tbl] [$col])
				$validators [$tbl] [$col] = array ();
			if (! $validatorsBySid [$sid])
				$validatorsBySid [$sid] = array ();
			
			$validator = new MinValidator ( $sid, $tbl, $col, $expression_type, $expression, $alias, $help );
			
			$validators [$tbl] [$col] [] = $validator;
			$validatorsBySid [$sid] [] = $validator;
		}
	
	}
}

function addStandard($sid, $tbl, $col, $etype, $exp) {
	$sql = "INSERT INTO " . STANDARD_TABLE . "(sid,tbl,col,expression_type,expression) VALUES ('$sid',$tbl','$col','$etype','$exp')";
	if (validateStandard ( $tbl, $col, $etype, $exp )) {
		$result = @$stan_db->query ( $sql );
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	return false;
}

function validateStandard($tbl, $col, $etype, $exp) {
	if ($etype == "limit") {
		return (ereg ( "-?[0-9]+:-?[0-9]?", $exp ));
	} else if ($etype == "list" || $etype == "liste") {
		return $exp != "";
	} else if ($etype == "regex") {
		return ($exp != "");
	} else {
		return false;
	}
}

class MinValidator {
	private $id;
	private $tbl;
	private $col;
	private $expression_type;
	private $expression;
	private $alias;
	private $format_help;
	
	public function __construct($id, $tbl, $col, $expression_type, $expression, $alias, $help = "") {
		$this->id = $id;
		$this->tbl = $tbl;
		$this->col = $col;
		$this->expression = trim ( $expression );
		$this->expression_type = trim ( $expression_type );
		$this->alias = $alias;
		$this->format_help = $help;
	}
	
	public function isValid($value) {
		if ($this->expression_type == "limit") {
			$pieces = explode ( ":", $this->expression );
			if (ereg ( LBOUNDREGEX, $value )) {
				return $value >= $pieces [0];
			} else if (ereg ( UBOUNDREGEX, $value )) {
				return $value <= $pieces [1];
			} else {
				if ($value >= $pieces [0] && $value <= $pieces [1]) {
					//				echo "$value is valid here. <br>";
					return true;
				} else {
					return false;
				}
			}
		
		} else if ($this->expression_type == "list" || $this->expression_type == "listcs") {
			$val = $value;
			if ($this->expression_type == "list")
				$val = strtolower ( $value );
			$pieces = explode ( "|", $this->expression );
			for($i = 0; $i < count ( $pieces ); $i ++) {
				$piece = $pieces [$i];
				if ($piece == $val) {
					return true;
				}
			}
			return false;
		} else if ($this->expression_type == "notnull") {
			return ! ($value == null || $value == "" || ! isset ( $value ));
		} else if ($this->expression_type == "regex") {
			return (ereg ( $this->expression, $value ));
		}
		
		return false;
	
	}
	
	public function __toString() {
		$str = "";
		if ($this->expression_type == "limit") {
			$pieces = explode ( ":", $this->expression );
			if (ereg ( LBOUNDREGEX, $this->expression )) {
				$str = "The value for " . $this->alias . " must be greater than or equal to " . $pieces [0];
			} else if (ereg ( UBOUNDREGEX, $this->expression )) {
				$str = "The value for " . $this->alias . " must be less than or equal to " . $pieces [1];
			} else {
				$str = "The value for " . $this->alias . " must be between " . $pieces [0] . " and " . $pieces [1] . "(inclusive)";
			}
		} else if ($this->expression_type == "list" || $this->expression_type == "liste") {
			$str = "The value for " . $this->alias . " must have one of the following values: " . implode ( ",", explode ( "|", $this->expression ) );
		
		} else if ($this->expression_type == "notnull") {
			$str = "The value for " . $this->alias . " cannot be empty";
		} else if ($this->expression_type == "regex") {
			$str = $this->format_help;
			if (! $str)
				$str = "";
		} else {
		}
		return $str;
	}
}

// Supply the tbl and col and the value
function isValid($table, $col, $value) {
	$valueValid = true;
	try {
		if ($GLOBALS ['validators'] [$table] [$col]) {
			$validators = $GLOBALS ['validators'] [$table] [$col];
			for($i = 0; $i < count ( $validators ); $i ++) {
				$validator = $validators [$i];
				$valueValid = $valueValid & $validator->isValid ( $value );
			}
		} else {
			if ($GLOBALS ['strict_standard_policy']) {
				$valueValid = false;
			}
		}
	} catch ( Exception $e ) {
		if ($GLOBALS ['strict_standard_policy']) {
			$valueValid = false;
		}
	}
	return $valueValid;
}

function isValidBySid($sid, $value) {
	$valueValid = true;
	try {
		if ($GLOBALS ['validatorsBySid'] [$sid]) {
			$validators = $GLOBALS ['validatorsBySid'] [$sid];
			for($i = 0; $i < count ( $validators ); $i ++) {
				$validator = $validators [$i];
				$valueValid = $valueValid & $validator->isValid ( $value );
			}
		
		} else {
			if ($GLOBALS ['strict_standard_policy']) {
				$valueValid = false;
			}
		}
	
	} catch ( Exception $e ) {
		return $valueValid;
	}
	
	return $valueValid;
}

function describeValidator($tbl, $col) {
	if ($GLOBALS ['validators'] [$tbl] [$col]) {
		$validators = $GLOBALS ['validators'] [$tbl] [$col];
		$messages = array ();
		
		for($i = 0; $i < count ( $validators ); $i ++) {
			$validator = $validators [$i];
			$messages [] = $validator->__toString ();
		}
		
		return join ( ". ", $messages );
	
	} else {
		return null;
	}
}

function describeValidatorBySid($sid) {
	if ($GLOBALS ['validatorsBySid'] [$sid]) {
		$validators = $GLOBALS ['validatorsBySid'] [$sid];
		$messages = array ();
		for($i = 0; $i < count ( $validators ); $i ++) {
			$validator = $validators [$i];
			$message = $validator->__toString ();
			if(!in_array($message,$messages)) $messages [] = $message;
		}
		return join ( ". ", $messages );
	} else {
		return null;
	}
}

function printStandards() {
	foreach ( $GLOBALS ['validators'] as $tblName => $tblArray ) {
		echo "$tblName Validators<br>";
		foreach ( $tblArray as $colname => $colArr ) {
			for($i = 0; $i < count ( $colArr ); $i ++) {
				echo $colArr [$i];
				echo "<br>";
			}
		}
	}
}

?>